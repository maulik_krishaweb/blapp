<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAo2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job__ao2s', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('job_id')->unsigned();

            $table->string('ao2_surveyor_full_information')->nullable();
              $table->string('ao2_surveyor_name')->nullable();
              $table->string('ao2_surveyor_qualifications')->nullable();
              $table->string('ao2_surveyor_company_name')->nullable();
              $table->string('ao2_surveyor_company_address')->nullable();
              $table->string('ao2_surveyor_company_details')->nullable();
              $table->string('ao2_surveyor_contact_details')->nullable();
              $table->string('ao2_full_names')->nullable();
              $table->string('ao2_salutation')->nullable();
              $table->string('ao2_property_address_adjoining')->nullable();
              $table->string('ao2_contact_address')->nullable();
              $table->string('ao2_contact_details')->nullable();
              $table->string('ao2_owners_referral')->nullable();
              $table->string('ao2_has_appointed_have_appointed')->nullable();
              $table->string('ao2_i_we_referral')->nullable();
              $table->string('ao2_i_we_referral_lower')->nullable();
              $table->string('ao2_my_our_refferal')->nullable();
              $table->string('ao2_he_she_they_referral')->nullable();
              $table->string('ao2_his_her_their')->nullable();
              $table->string('ao2_owners_ao1_is_an_are')->nullable();
              $table->string('ao2_s_s')->nullable();
              $table->string('ao2_s1_section')->nullable();
              $table->string('ao2_s1_description')->nullable();
              $table->string('ao2_s2_section')->nullable();
              $table->string('ao2_s2_description')->nullable();
              $table->string('ao2_s6_section')->nullable();
              $table->string('ao2_s6_description')->nullable();
              $table->string('ao2_date_of_notice')->nullable();
              $table->string('ao2_notice_notices')->nullable();
              $table->string('ao2_section_sections')->nullable();
              $table->string('ao2_drawings')->nullable();
              $table->string('ao2_soc_date')->nullable();
              $table->string('ao2_third_surveyor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job__ao2s');
    }
}
