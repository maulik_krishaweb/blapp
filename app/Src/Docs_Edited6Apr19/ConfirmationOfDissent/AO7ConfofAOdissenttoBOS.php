<?php

namespace App\Src\Docs\ConfirmationOfDissent;
use Illuminate\Support\Facades\Storage;

class  AO7ConfofAOdissenttoBOS
{
	public $documentFolder="Confirmation of dissent Letters";
	public $documentName="AO 7 Conf of AO dissent to BOS";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak();
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText(ucwords($job->bo->surveyor_company_name));
		$section->addText(ucwords($job->bo->surveyor_company_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->full_name.',');
		$section->addTextBreak();
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao->property_address_adjoining),['bold' => true]);
		$section->addTextBreak();

		$section->addText('I have been passed a copy of the Party Wall '.$job->ao7->notice_notices.' dated '.$job->ao7->date_of_notice.' and served upon '.$job->ao7->full_names.', '.$job->ao7->owners_referral.' of '.$job->ao7->property_address_adjoining.', which adjoins '.$job->bo->property_address_proposed_work);
		$section->addTextBreak();
		$section->addText('I can confirm that '.$job->ao7->salutation.' '.$job->ao7->has_appointed_have_appointed.' dissented to the Party Wall Notices '.$job->ao7->notice_notices.' and '.$job->ao7->has_appointed_have_appointed.' appointed me to act as '.$job->ao7->his_her_their.' Party Wall Surveyor in accordance with the above Act. ');
		$section->addTextBreak();
	$section->addText('*I have attached my appointment letter for your records, if you could kindly send me yours along with Third Surveyor selections and a pack of information containing the land registry details, drawings and structural information. ');
	$section->addTextBreak();
	$section->addText('*I am in the process of obtaining my appointment letter and I will provide you with a copy as soon as it is available, in the mean time if you could kindly send me yours along with Third Surveyor selections and a pack of information containing the land registry details, drawings and structural information.');
	$section->addTextBreak();
	$section->addText('Please contact me so that we can arrange a mutually convenient date to visit the adjoining '.$job->ao7->owners_owners.' property to record the Schedule of Condition Report. ');
	$section->addTextBreak();
	$section->addText('I look forward to hearing from you so this matter can be progressed promptly. ');



		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();

		$section->addText(ucwords($job->ao7->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addText('cc. '.$job->ao7->salutation.'');
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
