<?php

namespace App\Src\Docs\Section8;
use Illuminate\Support\Facades\Storage;

class  AO7RequestforAccess
{
	public $documentFolder="Section 8 Request";
	public $documentName="AO 7 Request for Access";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11); 
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		// 	$header = $section->addHeader();
		// $header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		// $footer = $section->addFooter();
		// $footer->addimage('images/docs/footer.png', ['width' => 460]);

		
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addTextBreak(1);
		$section->addText(ucwords($job->ao7->full_names));
		$section->addText(ucwords($job->ao7->property_address_adjoining));
		$section->addTextBreak(8);

		
		$section->addText('Dear '.$job->ao7->salutation.',');
		$section->addTextBreak();

		$section->addText('In accordance with clause 4(ENTER CLAUSE) of the Party Wall Award dated '.$job->party_wall_notice_date.' and Section 8 of the Party Wall etc Act 1996. ');
		$section->addTextBreak();
		$section->addText('I hereby confirm that we kindly request access onto your rear garden to facilitate the proposed construction works, in particular to: ');
		$section->addTextBreak();
		$section->addText($job->ao7->s1_description,  ['bold'=>true] );
		$section->addTextBreak();
		$section->addText('Again this having been agreed within the Party Wall Award.');
		$section->addTextBreak();
		$section->addText('The proposed access will adhere clause to 4(ENTER CLAUSE)set out within the Party Wall Award:');
		$section->addTextBreak();
		$section->addText('“ENTER CLAUSE”.', ['italic'=>true]);
		$section->addTextBreak();
		$section->addText('I confirm we intend to gain access on the '.$job->party_wall_notice_date.' or earlier by agreement. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to discuss. ');
		$section->addTextBreak();

		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText(ucwords($job->bo->full_name), ['bold'=>true] );
		$section->addText(ucwords($job->bo->contact_address));

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
