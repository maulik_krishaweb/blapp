<?php

namespace App\Src\Docs\Section12Request;
use Illuminate\Support\Facades\Storage;

class  AO3toBOSection12request
{
	public $documentFolder="Section 12 Request";
	public $documentName="AO 3 to BO Section 12 request";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		// 	$header = $section->addHeader();
		// $header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		// $footer = $section->addFooter();
		// $footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(6);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addTextBreak();
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addTextBreak();

		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText('Re:'.$job->bo->property_address_proposed_work.' /',['bold' => true]);
		$section->addText($job->ao3->property_address_adjoining,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('In accordance with Section 12 of the above act, '.$job->ao3->i_we_referral_lower.' formally request the sum of £5000.00 (Five Thousand Pounds) £10,000.00 (Ten Thousand Pounds), £15,000.00 (Fifteen Thousand Pounds), £20,000.00 (Twenty Thousand Pounds) to be held on account in advance of the notifiable works commencing. ');
		$section->addTextBreak();
		$section->addText('It would be  '.$job->ao3->my_our_refferal.' preference that the security funds are held on account with an impartial solicitor’s or accountant’s client account, with written confirmation from the holder that the funds will only be released upon the signature of the Party Wall Surveyors. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		// $section->addTextBreak(2);
		$section->addText(ucwords($job->ao3->full_names));

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
