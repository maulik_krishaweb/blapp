<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO3CovertoBOawardBOS
{
	public $documentFolder="Award Letters";
	public $documentName="AO 3 Cover to BO award BOS";

    	public function create(\App\Job $job){

	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao3->property_address_adjoining),['bold' => true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to *my visit *my colleague ENTER NAME visit on '.$job->ao3->soc_date.'  to complete the Schedule of Condition Report of the adjoining '.$job->ao3->owners_owners.'  property with '.$job->ao3->surveyor_name.', the adjoining '.$job->ao3->owners_owners.' surveyor. ');
		$section->addText('I have now reviewed all the information, drawings and *structural calculations and I am pleased to confirm that I am happy to serve the enclosed Party Wall *Award *Awards in respect of this matter.');
		$section->addTextBreak();
		$section->addText('The Party Wall *Award *Awards is a are *legally binding *document *documents and not only *govern *governs the specifics of the notifiable works, *they *it also legally *protect *protects you and the adjoining '.$job->ao3->owners_owners.'  in the event of damage being alleged or caused to '.$job->ao3->his_her_their.' *property *properties.');
		$section->addTextBreak();
		$section->addText('In accordance with Section 10(17) of the Party Wall etc. Act 1996 you have a legal right to appeal the Party Wall *Award *Awards within 14 days of *its *their date, however to the best of my knowledge there is nothing within the Party Wall *Award *Awards that should cause you to do so.');
		$section->addTextBreak();
		$section->addText('You will note that clause X of the Party Wall *Award *Awards deals with X.');
		$section->addTextBreak();
		$section->addText('I hope your works go smoothly, however should any issues arise during the course of the works please contact me.');
		$section->addTextBreak();
		$section->addText('I have attached a copy of my invoice '.$job->invoice_no.', along with '.$job->ao3->surveyor_name.'’s invoice in respect of this matter for payment and I would be grateful if you could settle these at your earliest opportunity. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to assist. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
