<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;

class  ReportCoverLetter
{
	public $documentFolder="Report Cover Letter";
	public $documentName="Client Report Cover Letter";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");

    	
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		


		$section->addTextBreak();
		$section->addText(ucwords($job->surveying->client_full_name));
		$section->addText(ucwords($job->surveying->client_contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('Dear '.$job->surveying->client_salutation.',');
		$section->addTextBreak();
		$section->addText('Re: '.ucwords($job->surveying->surveying_service).' of '.ucwords($job->surveying->address_of_inspection),['bold' => true]);
		$section->addTextBreak();
	
		$section->addText('Further to our visit on '.$job->surveying->date_of_inspection.' to complete the '.$job->surveying->surveying_service.' of '.$job->surveying->address_of_inspection.', I am pleased to enclose the completed '.$job->surveying->surveying_service.' Report in respect of this matter.');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to assist. ');
	

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(5);
		$section->addText(ucwords('BERRY LODGE'));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);


		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
