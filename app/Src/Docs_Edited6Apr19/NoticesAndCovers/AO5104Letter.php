<?php

namespace App\Src\Docs\NoticesAndCovers;
use Illuminate\Support\Facades\Storage;

class  AO5104Letter
{
	public $documentFolder="Notices and Covers";
	public $documentName="AO 5 10.4 Letter";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		$section->addTextBreak(3);
		$section->addText(ucwords($job->ao5->full_names));
		$section->addText(ucwords($job->ao5->contact_address));
		
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak(2);
		$section->addText('Dear '.$job->ao5->salutation.',');
		$section->addTextBreak();
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addText($job->bo->property_address_proposed_work.' / ' ,['bold' => true]);	
		$section->addText($job->ao5->property_address_adjoining ,['bold' => true]);
		$section->addTextBreak();
		$section->addText('On '.$job->ao5->date_of_notice.', I sent Party Wall '.$job->ao5->notice_notices.' to you describing the works which are due to take place at '.$job->bo->property_address_proposed_work.'. Within that letter I explained the various ways in which you could respond to the Party Wall '.$job->ao5->notice_notices.' (copies of both are enclosed for your reference). ');
		 $section->addTextBreak();
		$section->addText('Despite the 14 day Party Wall '.$job->ao5->notice_notices.' period having expired all of the original Party Wall Notice response options remain open to you, although I must ask you to let me have your response to the Party Wall '.$job->ao5->notice_notices.' within 10 days of the date of this letter in accordance with Section 10(4) of the Party Wall etc. Act 1996.');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak();
		$section->addText('cc: '.$job->bo->salutation);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
