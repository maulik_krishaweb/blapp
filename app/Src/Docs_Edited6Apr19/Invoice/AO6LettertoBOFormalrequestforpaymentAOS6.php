<?php

namespace App\Src\Docs\Invoice;
use Illuminate\Support\Facades\Storage;

class  AO6LettertoBOFormalrequestforpaymentAOS6
{
	public $documentFolder="Invoice / Job Fee Quote";
	public $documentName="AO 6 Letter to BO Formal request for payment AOS 6";

    public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText('LETTER BEFORE ACTION',['bold' => true]);
		$section->addText('RE: '.$job->bo->property_address_proposed_work.' / '.$job->ao->property_address_adjoining,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		
		$section->addTextBreak();
		$section->addText('On AWARD DATE a Party Wall Award was agreed in which I acted as the agreed surveyor with yourself and '.$job->ao->full_names.'.');
		$section->addTextBreak();
		$section->addText('Despite emailing you reminder emails on DATE, DATE, DATE confirming that our our Invoice '.$job->invoice_no.' remained outstanding, my accounts team have confirmed this invoice remains unpaid. ');
		$section->addTextBreak();
		$section->addText('Please ensure this invoice is settled in the next 14 days to avoid any additional cost in respect of this matter. ');
		$section->addTextBreak();
		$section->addText('I have attached copies of the Award, the Service Letter, our Invoice '.$job->invoice_no.' for your records. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
