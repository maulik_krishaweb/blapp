<?php

namespace App\Src\Docs\FinalLetters;
use Illuminate\Support\Facades\Storage;

class  AO7FinalLetter
{
	public $documentFolder="Final Letters";
	public $documentName="AO 7 Final Letter";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);		
		$section->addTextBreak();
		$section->addText(ucwords($job->ao7->full_names));
		$section->addText(ucwords($job->ao7->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->ao7->salutation.',');
		$section->addTextBreak();
		$section->addText($job->bo->property_address_proposed_work.' / '.$job->ao7->property_address_adjoining,['bold'=>true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();	
		$section->addText('I write to advise you that after *visiting your property *my colleague ENTER NAME visited your property on the DATE, I am pleased to confirm that it appears to be in the same condition as the original Schedule of Condition Report dated DATE and undertaken in advance of the works with no issues having been noted. ');
		$section->addTextBreak();
		$section->addText('I have therefore closed the file on this matter. Should you notice any issues in the future please do not hesitate to contact me and I will of course be happy to assist. ');
		$section->addTextBreak();
		$section->addText('Should you wish to discuss anything to do with this matter please do not hesitate to contact me and I will be happy to discuss. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(5);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
