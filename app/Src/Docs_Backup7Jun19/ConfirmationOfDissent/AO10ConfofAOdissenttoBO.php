<?php

namespace App\Src\Docs\ConfirmationOfDissent;
use Illuminate\Support\Facades\Storage;

class  AO10ConfofAOdissenttoBO
{
	public $documentFolder="Confirmation of dissent Letters";
	public $documentName="AO 10 Conf of AO dissent to BO";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak();
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addTextBreak(1);
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak(1);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak(1);
			
		$section->addText(ucwords('RE: '.$job->bo->property_address_proposed_work.' / '.$job->ao10->property_address_adjoining),['bold' => true]);
		$section->addText(ucwords($job->ao->property_address_adjoining),['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('I have been passed a copy of the Party Wall '.$job->ao10->notice_notices.' dated '.$job->ao->date_of_notice.' and served upon '.$job->ao10->full_names.', '.$job->ao10->owners_referral.' of '.$job->ao10->property_address_adjoining.' which adjoins your property, '.$job->bo->property_address_proposed_work.'.');
		$section->addTextBreak();
		$section->addText('I can confirm that '.$job->ao10->salutation.' '.$job->ao10->has_appointed_have_appointed.' dissented to the Party Wall '.$job->ao10->notice_notices.' and '.$job->ao10->has_appointed_have_appointed.' appointed me to act as '.$job->ao10->his_her_their.' Party Wall Surveyor in accordance with the Party Wall etc Act 1996. ');
		$section->addTextBreak();
		$section->addText('Please kindly have your Party Wall Surveyor contact me at the first instance so we can progress this matter promptly, alternatively if you would like me to act as the Party Wall Agreed Surveyor please contact me and I will be happy to discuss this option with you.');
		$section->addTextBreak();
		$section->addText('Please ensure that no Notifiable Party Wall works commence until the Party Wall Award has been agreed.');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify.');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();

		$section->addText(ucwords($job->ao10->surveyor_name));
		$section->addText(ucwords($job->ao->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
$section->addText('cc. '.$job->ao10->salutation);
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
