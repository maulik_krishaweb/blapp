<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO2CovertoBOawardBOSwithINV
{
	public $documentFolder="Award Letters";
	public $documentName="AO 2 Cover to BO award BOS with INV";

    	public function create(\App\Job $job){

	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		
		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao2->property_address_adjoining),['bold' => true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to *my visit *my colleague ENTER NAME visit on '.$job->ao2->soc_date.' to complete the Schedule of Condition Report of the adjoining '.$job->ao2->owners_owners.'  property with '.$job->ao2->surveyor_name.', the adjoining '.$job->ao2->owners_owners.' surveyor. ');
		$section->addTextBreak();
		$section->addText('I have now reviewed all the information, drawings and *structural calculations and I am pleased to confirm that I am happy to serve the enclosed Party Wall *Award *Awards in respect of this matter.');
		$section->addTextBreak();
		$section->addText('The Party Wall *Award *Awards is a are *legally binding *document *documents and not only *govern *governs the specifics of the notifiable works, *they *it also legally *protect *protects you and the adjoining '.$job->ao2->owners_owners.'  in the event of damage being alleged or caused to '.$job->ao2->his_her_their.' *property *properties.');
		$section->addTextBreak();
		$section->addText('In accordance with Section 10(17) of the Party Wall etc. Act 1996 you have a legal right to appeal the Party Wall *Award *Awards within 14 days of *its *their date, however to the best of my knowledge there is nothing within the Party Wall *Award *Awards that should cause you to do so.');
		$section->addTextBreak();
		$section->addText('You will note that clause X of the Party Wall *Award *Awards deals with X.');
		$section->addTextBreak();
		$section->addText('I hope your works go smoothly, however should any issues arise during the course of the works please contact me.');
		$section->addTextBreak();
		$section->addText('I have attached a copy of my invoice '.$job->invoice_no.' , along with '.$job->ao2->surveyor_name.'’s invoice in respect of this matter for payment and I would be grateful if you could settle these at your earliest opportunity. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to assist. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		$section->addPageBreak();

		// invoice

		$section->addTextBreak(4);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));

		$section->addText(date("d F Y"), ['size'=>10], [ 'align' => 'right' ]);
		$textrun = $section->addTextRun(['alignment' => 'right']);
		$textrun->addText("Invoice Reference: ", ['bold' => true, 'size'=>10]);
		$textrun->addText($job->invoice_no, ['size'=>10]);

		$textrun = $section->addTextRun(['alignment' => 'right']);
		$textrun->addText("Our Ref: ", ['bold' => true, 'size'=>10]);
		$textrun->addText('BLSN'.$job->id, ['size'=>10]);


		$section->addText('INVOICE NUMBER '.$job->invoice_no, ['bold' => true, 'size'=>14]);
		$section->addText('RE: '.$job->bo->property_address_proposed_work.' / '.$job->ao->property_address_adjoining.' / '.$job->ao2->property_address_adjoining , ['bold' => true, 'size'=>10]);
		$section->addTextBreak();
		$phpWord->addTableStyle('myTable', ['borderColor' => '006699', 'borderSize'  => 6, 'cellMargin'  => 10, 'width' =>100]);
		$table = $section->addTable('myTable');
		$table->addRow();
		$table->addCell(6500)->addText('DESCRIPTION OF SERVICE:', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText('AMOUNT:', ['bold'=>true, 'size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Serving Party Wall Notices', ['size'=>10]);
		$table->addCell(2500)->addText($job->notice_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Party Wall Agreement Costs ', ['size'=>10]);
		$table->addCell(2500)->addText($job->award_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Vat at 20%', ['size'=>10]);
		$table->addCell(2500)->addText($job->vat_amount, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Land Registry costs at £6.00 per owner', ['size'=>10]);
		$table->addCell(2500)->addText($job->land_registry_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Printing & Postage Costs ', ['size'=>10]);
		$table->addCell(2500)->addText($job->printing_postage_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('TOTAL DUE: ', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText($job->final_amount, ['bold'=>true, 'size'=>10]);
		$section->addTextBreak();
		$section->addText('Please kindly make payment within 7 days of the invoice date. ', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('For ease of identification please enter the Invoice number '.$job->invoice_no.' as the payment reference.', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('Payment Methods',['bold'=>true,'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Bank Payment',['bold'=>true, 'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Please make a bank/online payment to the following account:', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('BERRY LODGE SURVEYORS ',['bold'=>true, 'size'=>10]);

		$textrun = $section->addTextRun();
		$textrun->addText("Bank Account Number: 		", ['size'=>10]);
		 $textrun->addText('		2 4 0 9 6 3 2 6', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Sort Code:				", ['size'=>10]);
		$textrun->addText('		5 6 - 0 0 - 1 4', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Bank:					", ['size'=>10]);
		$textrun->addText('		Natwest', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Branch:				", ['size'=>10]);
		$textrun->addText('		Baker Street, 69 Baker Street, W1U 6AT', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("IBAN: 					", ['size'=>10]);
		$textrun->addText('		B40NWBK56001424096326', ['bold' => true,'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("BIC: 					", ['size'=>10]);
		$textrun->addText('		NWBKGB2L', ['bold' => true, 'size'=>10]);
		$section->addTextBreak();
		$section->addText('THANK YOU IN ADVANCE FOR YOUR PAYMENT', ['bold' => true, 'size'=>10],  [ 'align' => 'center' ]);
		$section->addTextBreak();
		$section->addText('Company VAT number is 218 9003 17', ['size'=>10], [ 'align' => 'center' ]);


		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
