<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO7ReturningAwardtoBOS
{
	public $documentFolder="Award Letters";
	public $documentName="AO 7 Returning Award to BOS";

    	public function create(\App\Job $job){

	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		
		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->surveyor_name).' '.ucwords($job->bo->surveyor_qualifications));
		$section->addText(ucwords($job->bo->surveyor_company_name));
		$section->addText(ucwords($job->bo->surveyor_company_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->surveyor_name.',');
		$section->addTextBreak();
		$section->addText('Re: '.ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao7->property_address_adjoining).'  '.ucwords($job->ao->property_address_adjoining),['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for sending the fair copies of the Party Wall Awards, I can confirm I have signed those. ');
		$section->addTextBreak();
		$section->addText('Please find attached a copy of the building '.$job->bo->owners_owners.' Party Wall Award for you to serve upon your appointing '.$job->bo->owner_referral.'. I confirm I have served the Party Wall Award upon '.$job->ao7->salutation.'.');
		$section->addTextBreak();
		$section->addText('I have also attached my invoice for payment and I would be grateful if you could send this to your appointing '.$job->bo->owner_referral.'  along with '.$job->bo->his_her_their.' Party Wall Award.');
		$section->addTextBreak();
		$section->addText('Thank you for your help in this matter, I look forward to working with you again in the near future. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->ao7->surveyor_name));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
