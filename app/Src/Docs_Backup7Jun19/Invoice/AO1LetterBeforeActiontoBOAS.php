<?php

namespace App\Src\Docs\Invoice;
use Illuminate\Support\Facades\Storage;

class  AO1LetterBeforeActiontoBOAS
{
	public $documentFolder="Invoice / Job Fee Quote";
	public $documentName="AO1 Letter Before Action to BO AS";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		
		$section->addTextBreak(5);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		
		$section->addTextBreak();
		$section->addText('LETTER BEFORE ACTION',['bold' => true]);
		
		$section->addText('RE: '.$job->bo->property_address_proposed_work.' / ' ,['bold' => true]);		
		$section->addText($job->ao->property_address_adjoining ,['bold' => true]);	
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('On AWARD DATE a Party Wall Award was agreed in which I acted as the agreed surveyor with yourself and '.$job->ao->full_names.'.');
		$section->addTextBreak();
		$section->addText('Clause XX of that Award covered my fees with an invoice for those fees being provided to by post with the served Award on the AWARD DATE. ');
		$section->addTextBreak();
		$section->addText('In accordance with Section 10(16) of the above Act, as 14 days have passed since the Award was served and an appeal hasn’t been lodged in County Court, these fees are now enforceable in the Magistrates Court. ');
		$section->addTextBreak();
		$section->addText('Please accept this letter as formal notification that in pursuance of Section 17 of the above Act legal proceedings will be issued in the Magistrates Court and without further notice should our invoice '.$job->invoice_no.' not be settled in the next 7 days. ');
		$section->addTextBreak();
		$section->addText('We would like to formally put you on notice of our costs in respect of all time spent enforcing the Award which will be at the rate as confirmed in clause 2 of the Award. ');
		$section->addTextBreak();
		$section->addText('I have attached copies of the Award, the Service Letter, our Invoice '.$job->invoice_no.' for your records. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify.');
		


		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
