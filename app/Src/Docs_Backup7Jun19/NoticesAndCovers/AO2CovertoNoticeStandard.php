<?php

namespace App\Src\Docs\NoticesAndCovers;
use Illuminate\Support\Facades\Storage;

class  AO2CovertoNoticeStandard
{
	public $documentFolder="Notices and Covers";
	public $documentName="AO 2 Cover to Notice Standard";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak(5);
		$section->addText(ucwords($job->ao2->full_names));
		$section->addText(ucwords($job->ao2->contact_address));
		$section->addTextBreak();
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak(3);
		$section->addText('Dear '.$job->ao2->salutation.',');
		$section->addTextBreak();
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addText($job->bo->property_address_proposed_work.' / ',['bold' => true]);
		$section->addText($job->ao2->property_address_adjoining,['bold' => true]);
		$section->addTextBreak();
		$section->addText('I write to advise you that I am authorised on behalf of '.$job->bo->salutation.', '.$job->bo->owner_referral.' of '.$job->bo->property_address_proposed_work.' to serve upon you the enclosed Party Wall '.$job->ao2->notice_notices.' setting out the proposed work '.$job->bo->he_she_they_referral.' '.$job->bo->intend_intends.' to undertake to '.$job->bo->his_her_their.' property which adjoins your property '.$job->ao2->property_address_adjoining.'. ');
		$section->addTextBreak();
		$section->addText('This letter is to explain, in less formal terms, that in accordance with the above Act there are three response options to the Party Wall '.$job->ao2->notice_notices.' open to you and are itemised for clarity as follows:');
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText('1.', ['bold' => true, 'color' => '00B1A4']);
		$textrun->addText(' You may Consent to the works. '.$job->bo->salutation, ['bold' => false]);
		$textrun->addText(' will then be free to commence '.$job->bo->his_her_their.' works and no further formalities of the Party Wall etc Act 1996 will be followed. Should you select this option, I would still recommend a Schedule of Condition Report is undertaken of your property. A Schedule of Condition Report involves one of our Party Wall Surveyors visiting your property prior to '.$job->bo->salutation.''.$job->bo->s_s.' proposed works commencing to record its condition and take an in depth written and photographic schedule. This then acts as a record of proof in the event that damage is caused. If this is your selected option please tick');
		$textrun->addText(' box 1', ['bold' => true]);
		$textrun->addText(' on the enclosed form.');
		$section->addTextBreak();

		$textrun = $section->addTextRun();
		$textrun->addText('2.', ['bold' => true, 'color' => '00B1A4']);
		$textrun->addText(' You may Dissent to the works and agree in my appointment as an Agreed Party Wall Surveyor.', ['bold' => true]);
		$textrun->addText(' I would then act impartially on behalf of '.$job->bo->salutation.' and yourself preparing all the necessary documents including a Schedule of Condition Report and a Party Wall Award (a legal document governing the proposed work and protecting both owners in the event of damage). Should this be your selected option please tick and sign ');
		$textrun->addText('box 2', ['bold' => true]);
		$textrun->addText(' on the enclosed form. ');

		
		$section->addTextBreak();

		$textrun = $section->addTextRun();
		$textrun->addText('3.', ['bold' => true, 'color' => '00B1A4']);
		$textrun->addText(' You may Dissent to the works and appoint a Party Wall Surveyor of your choice. ', ['bold' => true]);
		$textrun->addText('I will then act on behalf of '.$job->bo->salutation.' and an Award will be agreed between myself and your chosen Party Wall Surveyor. Should this be your selected option please tick ');
		$textrun->addText('box 3', ['bold' => true]);
		$textrun->addText(' on the enclosed form and enter your selected Party Wall Surveyor’s details. ');


		$section->addTextBreak();
		$section->addText('The costs involved in producing a Party Wall Award will be paid by '.$job->bo->salutation.', '.$job->bo->he_she_they_referral.' will also be responsible for making good any damage in the unfortunate event that it is caused by '.$job->bo->his_her_their.' proposed notifiable Party Wall works. ');
		$section->addTextBreak();
		$section->addText('Please complete the enclosed Party Wall '.$job->ao2->notice_notices.' acknowledgement form confirming your selected Party Wall Notice response option and return it to me within 14 days of the date of this letter in accordance with the timeframes set out by the Party Wall etc Act 1996.  ');
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText('If you would like to obtain a little further information about the Party Wall Agreement procedures we would highly recommend visiting our website, not only do we have a number of');
		$textrun->addText(' informative Party Wall Videos,', ['bold' => true]);
		$textrun->addText(' we also have a ');
		$textrun->addText('Party Wall Podcast,',['bold' => true]);
		$textrun->addText(' an ');
		$textrun->addText('interactive Party Wall Guide ',['bold' => true]);
		$textrun->addText('and a ');
		$textrun->addText('Party Wall Blog ',['bold' => true]);
		$textrun->addText('all of which are designed to inform and clarify. ');

		$section->addTextBreak();
		$section->addText('If you would like me to explain the procedures and formalities involved in this matter in greater detail please do not hesitate to contact me and I would be happy to assist.');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak();
		$section->addText('cc: '.$job->bo->salutation);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
