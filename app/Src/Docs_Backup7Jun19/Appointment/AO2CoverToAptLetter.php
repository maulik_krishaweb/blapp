<?php

namespace App\Src\Docs\Appointment;
use Illuminate\Support\Facades\Storage;

class  AO2CoverToAptLetter
{
	public $documentFolder="Appointment Letters";
	public $documentName="AO 2 Cover to Apt Letter";

    	public function create(\App\Job $job){
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();

		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(4);
		$section->addText(ucwords($job->ao2->full_names));
		$section->addText(ucwords($job->ao2->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref:  BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->ao2->salutation.',');
		$section->addTextBreak();
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao2->property_address_adjoining),['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for confirming you would like me to act as your Party Wall Surveyor in respect of the building '.$job->bo->owners_owners.' proposed works at '.$job->bo->property_address_proposed_work.', I would be more than happy to take on the appointment.');
		$section->addTextBreak();
		$section->addText('Please find enclosed 2 copies of the appointment letter for signature and return, one copy for your files and another copy to be returned to me.');
		$section->addTextBreak();
		$section->addText('*In the mean time I have prepared and sent the enclosed letter to the building '.$job->bo->owner_referral.' confirming my appointment as your Party Wall Surveyor ');
		$section->addTextBreak();
		$section->addText('*In the mean time I have prepared and sent the enclosed letter to the building '.$job->bo->owners_owners.' surveyor confirming my appointment. ');
		$section->addTextBreak();

		$section->addText('The next step in the Party Wall Agreement process is to undertake a Schedule of Condition Report of your property. You can see an example of one of our Schedule of Condition Reports on our website in our experience we have found that Schedule of Condition Reports are one of the key steps in the Party Wall process. You can also watch a short informative video on the benefits of them on our website.  ');
		$section->addTextBreak();
		$section->addText('The inspection itself will take in the region of 45 minutes to an hour at which time we will be taking detailed photographs and an in depth record of your property. I will be in touch as soon as I have received a response from the building '.$job->bo->owners_owners.' Party Wall Surveyor to book this in with you. ');
		$section->addTextBreak();
		$section->addText('Should any works commence on site please do not hesitate to contact me and I will take the appropriate action. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText(ucwords($job->ao2->surveyor_name));
		$section->addText(ucwords($job->ao2->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}


}
