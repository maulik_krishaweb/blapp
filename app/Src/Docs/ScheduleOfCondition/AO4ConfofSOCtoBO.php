<?php

namespace App\Src\Docs\ScheduleOfCondition;
use Illuminate\Support\Facades\Storage;

class  AO4ConfofSOCtoBO
{
	public $documentFolder="Schedule of Condition/Cover Letters";
	public $documentName="AO 4 Conf of SOC to BO";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');

		$section->addTextBreak();
		$section->addText('Re: Schedule of Condition Report in respect of the construction works taking place at '.$job->bo->property_address_proposed_work ,['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for confirming you would like me to undertake a Schedule of Condition Report of '.$job->ao4->property_address_adjoining.', I can confirm the total fixed fee in respect of this matter will be '.$job->final_amount.' + VAT.');  
		$section->addTextBreak();
		$section->addText('I can confirm that I am due to inspect '.$job->ao4->property_address_adjoining.' on '.$job->ao4->soc_date.'.');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText('Bradley Mackenzie ',['bold' => true]);
		$section->addText('BA(Hons) MSc FFPWS MRICS RICS Registered Valuer');
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak(4);
		$section->addText('…………………………………….		      ……………………………….');
		$section->addText('Signed by '.$job->bo->full_name.'                                             Date', ['size' => 9]);
		$section->addTextBreak();
		$section->addText('I confirm that I agree for Berry Lodge Surveyors to undertake and prepare a Schedule of Condition Report in respect of the above properties for the agreed fee of £'.$job->final_amount.' + VAT, which is to be paid prior to the release of the Schedule of Condition Report.', ['size' => 9]);
		
		
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
