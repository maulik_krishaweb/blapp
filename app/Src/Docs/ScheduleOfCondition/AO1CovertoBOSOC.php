<?php

namespace App\Src\Docs\ScheduleOfCondition;
use Illuminate\Support\Facades\Storage;

class  AO1CovertoBOSOC
{
	public $documentFolder="Schedule of Condition/Cover Letters";
	public $documentName="AO 1 Cover to BO SOC";

   	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText($job->bo->property_address_proposed_work.' / ',['bold' => true]);
		$section->addText($job->ao->property_address_adjoining,['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to *my visit *my colleague Beau Davies’ visit on '.$job->ao->soc_date.' to complete the Schedule of Condition Report of the adjoining '.$job->ao->owners_owners.' *property *properties, I am pleased to enclose the completed *report *reports. ');
		$section->addTextBreak();
		$section->addText('I hope your works go smoothly, however should any issues arise during the course of the works please contact me.');
		$section->addTextBreak();
		$section->addText('I have attached a copy of my invoice '. $job->invoice_no.' in respect of this matter for payment and I would be grateful if you could settle this at your earliest opportunity. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to assist. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText('Bradley Mackenzie ',['bold' => true]);
		$section->addText('BA(Hons) MSc FFPWS MRICS RICS Registered Valuer');
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
