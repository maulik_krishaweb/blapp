<?php

namespace App\Src\Docs\ScheduleOfCondition;
use Illuminate\Support\Facades\Storage;

class  AO8CovertoSOC
{
	public $documentFolder="Schedule of Condition/Cover Letters";
	public $documentName="AO 8 Cover to SOC";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak(1);
		$section->addText($job->ao8->full_names);
		$section->addText(ucwords($job->ao8->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->ao8->salutation.',');
		$section->addTextBreak();
		$section->addText($job->bo->property_address_proposed_work.' / ',['bold' => true]);
		$section->addText($job->ao8->property_address_adjoining,['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to *my visit *my colleague NAME visit on '. $job->ao8->soc_date.' to complete the Schedule of Condition Report of your property, I am pleased enclose a copy of the completed report.');
		$section->addTextBreak();
		$section->addText('Should you notice any issues or damage to your property during the course of the works, please contact me at the first instance and I will take the appropriate action. ');
		$section->addTextBreak();
		$section->addText('Should you wish to discuss anything to do with this matter please do not hesitate to contact me and I will be happy to discuss. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(3);
		$section->addText(ucwords($job->bo->surveyor_name),['bold' => true]);
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
