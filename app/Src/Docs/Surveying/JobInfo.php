<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use File;
class  JobInfo
{
	public $documentFolder="Job Information";

	public $completedFolder="Completed Report";
	public $desktopFolder="Desk Top Information";
	public $inspectionFolder="Inspection Photographs";
	public $landRegisteryFolder="Land Registry Details";
	public $reportSiteFolder="Report Site Notes";
	public $scannedFolder="Scanned File";
	// public $sketchFolder="Sketchpad";

	public $documentName="Job Info Sheet";

    	public function create(\App\Job $job){
    		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
    		// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
    		// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");
	    	// make other empty folders
	    	$completedFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->completedFolder}/");
	    	$desktopFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->desktopFolder}/");
	    	$inspectionFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->inspectionFolder}/");
	    	$landRegisteryFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->landRegisteryFolder}/");
	    	$reportSiteFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->reportSiteFolder}/");
	    	$scannedFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->scannedFolder}/");

	    

		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans MT');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection(['orientation'=>'landscape']);

		$tableStyle = array(
		    'borderColor' => '006699',
		    'borderSize'  => 6,
		    'cellMargin'  => 10,
		    'width' =>100
		);

		$phpWord->addTableStyle('myTable', $tableStyle);
		$table = $section->addTable('myTable');
		// $table->addRow();
		$table->addRow(500);
		$table->addCell(6000)->addText('Surveying Service:', ['bold'=>true]);
		$table->addCell(6000)->addText($job->surveying->surveying_service, ['bold'=>true]);
		$table->addRow(500);
		$table->addCell()->addText('Surveyor dealing with the File:', ['bold'=>true]);
		$table->addCell()->addText($job->surveying->surveyor_dealing_with_file, ['bold'=>true]);
		$table->addRow(500);
		$table->addCell()->addText('Job Number: ', ['bold'=>true]);
		$table->addCell()->addText('BLSN'.$job->id, ['bold'=>true]);
		$table->addRow(500);
		$table->addCell()->addText('Invoice No: ', ['bold'=>true]);
		$table->addCell()->addText('BLSN'.$job->invoice_no, ['bold'=>true]);



		$phpWord->addTableStyle('table2', $tableStyle);
 		$table = $section->addTable('table2');
		$table->addRow(500);
		$table->addCell(12000)->addText('Client Information:', ['bold'=>true]);
		$table->addRow(500);
		$table->addCell(12000)->addText($job->surveying->client_full_name, ['bold'=>false]);
		$table->addRow(500);
		$table->addCell(12000)->addText('Client Contact Address:', ['bold'=>true]);
		$table->addRow(500);
		$table->addCell(12000)->addText($job->surveying->client_contact_address, ['bold'=>false]);
		$table->addRow(500);
		$table->addCell(12000)->addText('Client Contact Information:', ['bold'=>true]);
		$table->addRow(500);
		$table->addCell(12000)->addText($job->surveying->client_contact_details, ['bold'=>false]);
		$table->addRow(500);
		$table->addCell(12000)->addText('Address of Inspection:', ['bold'=>true]);
		$table->addRow(500);
		$table->addCell(12000)->addText($job->surveying->address_of_inspection, ['bold'=>false]);


		$section->addTextBreak(1);

		$section->addPageBreak();

		$section->addText('File Checklist:',['bold' => true]);
		$section->addTextBreak();
		$phpWord->addTableStyle('Table', $tableStyle);
		$table = $section->addTable('Table');
		$table->addRow(600);
		$table->addCell(6000)->addText('Action:', ['bold'=>true]);
		$table->addCell(3000)->addText('Date', ['bold'=>true]);
		$table->addCell(3000)->addText('Tick', ['bold'=>true]);
		$table->addRow(600);
		$table->addCell()->addText('Letter of authorisation signed and received:');
		$table->addCell();
		$table->addCell();
		$table->addRow(600);
		$table->addCell()->addText('Inspection date booked:');
		$table->addCell();
		$table->addCell();
		$table->addRow(600);
		$table->addCell()->addText('Update Client:');
		$table->addCell();
		$table->addCell();
		$table->addRow(600);
		$table->addCell()->addText('Report Sent Out: ');
		$table->addCell();
		$table->addCell();

		$phpWord->addTableStyle('Table', $tableStyle);
		$table = $section->addTable('Table');
		$table->addRow();
		$table->addCell(3000)->addText('Additional File/Client Information: ');
		$table->addCell(9000)->addText($job->surveying->additional_info);



		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		// $phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		// $xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.pdf");
		
		// $file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}");
		// $file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
