<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;
use PDF;
use File;
class DraftWord
{
	public $documentFolder="Draft Report";


	public $documentName="Boundary Report Word Document";

    	public function create(\App\Job $job){
    		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
    		// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
    		// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section1 = $phpWord->addSection();
		// $header1 = $section1->addHeader();
		// $header1->addImage('images/bgberry-lodge-top.jpg',['height' => 70, 'width' => 500]);
		// $section1->addTextBreak();
		// $section1->addText($job->surveying->client_full_name);
		// $section1->addText($job->surveying->client_contact_address);
		// $section1->addTextBreak();
		// /*$section1->addText($job->surveying->date_of_confirmation_letter,['align' => 'right','size' => 12,'name' => 'Gill Sans MT']);*/
		// $section1->addText($job->surveying->date_of_confirmation_letter, [], [ 'align' => 'right' ]);
		// $section1->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		// $section1->addTextBreak();
		// $section1->addText('Dear Dr '.$job->surveying->client_full_name.",",['size' => 12,'name' => 'Gill Sans MT']);
		// $section1->addTextBreak();
		// $section1->addText("Re: Boundary Determination Report of 1 BO Street, London, W1U 5LA",['bold' => true,'name' => 'Gill Sans MT','size' => 12]);

		// $section1->addTextBreak();
		// $section1->addText("Further to our visit on ".$job->surveying->created_at->format('d/m/y')." to complete the Inspection of ".$job->surveying->address_of_boundary_determination.", I am pleased to enclose the completed Boundary Determination Report in respect of this matter.",['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addTextBreak();
		// $section1->addText("I would advise having a thorough read of the report to accommodate yourself with the information enclosed within. Once you have reviewed it I would then encourage you to get in touch with any questions you many have and I will be happy to assist.",['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addTextBreak();
		// $section1->addText($job->surveying->surveryor_name,['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addText($job->surveying->s_surveyor_qualifications,['name' => 'Gill Sans MT','size'=>12,]);


		// $footer1 = $section1->addFooter();
		// $footer1->addImage('images/docs/footer2.png',['width' => 500]);

		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		
		$section1->addText('Boundary Report:',['size' => 12,'name' => 'Poppins','color' => '1aae93'],['align'=>'right']);
		$section1->addText($job->surveying->address_of_boundary_determination,['size' => 12,'name' => 'Gill Sans MT'],['align'=>'right']);
		/*$imageStyle = array(
		'width' => 768,
		'height' => 768,
		'wrappingStyle' => 'square',
		'positioning' => 'absolute',
		'posHorizontalRel' => 'margin',
		'posVerticalRel' => 'line',
		);
		$section1->addImage('images/docs/dashboard.png', $imageStyle);
		$section1->addText('Boundary Report: ',['name' => 'Poppins', 'bold' => true, 'color' => '1aae93','size' => 12, 'align' => 'right']);*/

		

		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$table = $header->addTable(['border' => 0]);
		$table->addRow();
		$table->addCell(5000)->addImage(
		'images/docs/notice-logo.png',
		array('width' => 115, 'marginTop' => -1 )
		);
		$cell = $table->addCell(5000);
		$textrun = $cell->addTextRun(['alignment' => 'right']);
		$textrun->addText('Boundary Report: '.$job->surveying->address_of_boundary_determination,['name' => 'Poppins', 'bold' => true, 'color' => '1aae93', 'size' => 8], ['alignment' => 'right'] );
		$textrun->addTextBreak();
		$textrun->addText('Our Reference Number: '.$job->id,['name' => 'Poppins', 'color' => '1aae93', 'size' => 8],['alignment' => 'right'] );
		$textrun->addTextBreak();
		$textrun->addText('Inspection Date: '.$job->surveying->created_at->format('d-M-Y'),['name' => 'Poppins', 'color' => '1aae93', 'size' => 8],['alignment' => 'right']);
		/*$header->addimage('images/docs/header.png', ['width' => 115, 'marginRight' => -20]);
		$header->addText('Boundary Report:'.$job->surveying->address_of_boundary_determination,['name' => 'Poppins', 'bold' => true, 'color' => '1aae93', 'marginLeft' => 600,] );
		$header->addText('Our Reference Number: '.$job->id,['name' => 'Poppins', 'color' => '1aae93', 'marginLeft' => 600,] );
		$header->addText('Inspection Date: '.$job->surveying->created_at->format('d-M-Y'),['name' => 'Poppins', 'color' => '1aae93', 'marginLeft' => 600,] );*/
		// $footer = $section->addFooter();
		// $table1 = $footer->addTable();
		// $table1->addRow();
		// $cell2 = $table1->addCell(5000)->addImage(
		// 	'images/docs/footer1.png',
		//     array(
		//         'width'         => 50,
		//         'height'        => 50,
		//         'marginTop'     => 600,
		//         'marginLeft'    => 2,
		//         'marginBottom' => 0
		//     )
		// );


		// $cell3 = $table1->addCell(5000);
		// $cell1 = $table1->addCell(5000);
		// //$textrun1 = $cell1->addTextRun();
		// $cell1->addPreserveText('{PAGE}',['color' => '1aae93','name' => 'Gill Sans MT','size'=>12, 'align' => 'right']);
		// $table1->addCell(5000);
		// // $table1->addCell(5000)->addImage(
		// // 'images/docs/wfooter.png',
		// //     array(
		// //         'width'         => 100,
		// //         'height'        => 100,
		// //         'marginTop'     => -1,
		// //         'marginRight'    => -1,
		// //         'align' => 'right',
		// //         'wrappingStyle' => 'behind'
		// //     )
		// // );
		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		/*$footer->addImage(
		    'images/docs/wfooter.png',
		    array(
		        'width'         => 100,
		        'height'        => 100,
		        'marginTop'     => -1,
		        'marginRight'    => -1,
		        'align' => 'right',
		        'wrappingStyle' => 'behind'
		    )
		);*/

		/*$footer->addimage('images/docs/wfooter.png',['width' => 150, 'align' => 'right', 'marginRight' => -1 ]);*/
		$section->addTextBreak();
		$section->addText('1.	Instructions',['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("I ".$job->surveying->surveryor_name.", refer to your instructions as per Berry Lodge Surveyor’s confirmation letter dated ".$job->surveying->boundary_date_of_confirmation_letter." to provide the following:",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("To provide opinion in an effort to determine the location of the boundary line, in particular to the ".$job->surveying->boundary_being_determined." of ".$job->surveying->address_of_boundary_determination." which adjoins the ".$job->surveying->boundary_being_determined." of ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,'bold' => true]);
		$section->addTextBreak();
		$section->addText("I ".$job->surveying->surveryor_name." have restricted my inspection and research to these properties. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("2.	Client Name",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText($job->surveying->client_full_name,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("3.	Address of Client",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText($job->surveying->client_contact_address,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("4.	Date of Site Visit",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("The date of the site visit was ".$job->surveying->date_of_inspection.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("5.	Address of Property ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText($job->surveying->address_of_boundary_determination,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("6.	Description of Property ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText($job->surveying->address_of_boundary_determination." is a ".$job->surveying->type_of_property." property and would have likely been constructed circa ".$job->surveying->age_of_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("7.	Address of Property to which the Dispute Exists: ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText($job->surveying->address_of_adjoining_property.". As per Her Majesty’s Land Registry Title Deed ".$job->surveying->adjoining_property.", I can confirm ".$job->surveying->name_of_legal_owner." legally owns this property.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("8.	Standard Terminology ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In an effort to help you better understand some standard terminology used within this report and within the context of boundary surveying generally I have included a short glossary of terms below: ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("Trespass ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("A trespass within the context of a boundary dispute occurs when one owner directly enters upon another owner’s land without right or permission, or places some form of item whether it be fence, wall, tree, foundation, guttering, sign etc in the land of the neighboring owner without their permission. A trespass is classified as an intentional tort, therefore and if proved can result in damages from one owner to the other or an injunction via the courts. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Boundary Line ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("A boundary line is the line that separates one owner’s property from another. This can be in the form of a legal boundary line, which is the line indicated on Her Majesty’s Land Registry (HMLR) Title Deeds and Plans. It can also be a physical boundary line or separation which is usually some form of physical feature between two properties. Commonly this will be in the form of a fence, hedge or wall. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("De minimis ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("A de minumus trespass relates to the Latin expression “the law does not concern itself with trifles”, in the context of boundary surveying, if a trespass is de minumus, it would be so insignificant (narrow or small) it is considered negligible and the courts will not concern themselves with such an issue.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Adverse Possession ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Adverse Possession is an occupation of land inconsistent with the right of the ‘true owner’. A true owner is someone who holds legal title of either the registered or unregistered land under the Land Registration Act 2002. Under the Land Registration Act 2002, the person claiming adverse possession must apply to the Land Registry and not the court. The claimant must show 10 years of adverse possession ending on the date of application. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Meandering Boundary Line",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("A meandering boundary line is a boundary that does not have a straight or continual line from one point to another. Instead the boundary line will have a winding course as it progresses along the junction of the two properties. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Constant Boundary Line",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("A constant boundary line is a boundary that is a straight or continual line from one point to another. The boundary line will not change direction as it progresses along the junction of two properties. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Boundary Line Responsibility",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Unless Her Majesty’s Land Registry (HMLR) Title Deeds and Plans specifically confirm that one owner is either responsible or owns the boundary line, there is no overriding legal principal on the matter. It is not uncommon for owners to believe they own and maintain the “right” or “left” boundary line and the other owner has the same responsibility for the other. This is incorrect and there is no principal in law to support such a view. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("9.	Inspection Conditions",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("The weather at the time of my inspection was ".$job->surveying->inspection_weather." and ".$job->surveying->inspection_humidity.".",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("10.	Limitations to this Report",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("This report is prepared by ".$job->surveying->surveryor_name." of Berry Lodge Surveyors for use by ".$job->surveying->client_full_name." of ".$job->surveying->client_contact_address.", in respect of ".$job->surveying->address_of_boundary_determination." which adjoins ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("The report is my opinion of the boundary is based upon visual inspection, measured examination, and desktop research on ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.".",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("In preparing this report I can confirm I have only had access to ".$job->surveying->address_of_boundary_determination.", I have not inspected ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("I have prepared this report with an impartial approach and have ensured that bias hasn’t been applied to either ".$job->surveying->address_of_boundary_determination." or ".$job->surveying->address_of_adjoining_property.", the aim of this to hopefully encourage neighborly discussion and resolution.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("This report does not give a legal determination to the boundary line, nor does it provide a measured boundary. This report confirms my learned opinion and experience as a Boundary Surveyor based off site inspection, research and consideration of the facts. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("During my inspection on site, I have relied upon the following tools to conduct the survey:",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Each Tool is to be customised or confirm where is was used. If not used it is to be deleted.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("A Hand Held 5m Tape Measure",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$textrun1 = $section->addTextRun();
		$textrun1->addText("I have used a 5m tape measure during my inspection. In particular I have used this to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun1->addText("enter information.",['bgcolor'=>'ffff00','name' => 'Gill Sans MT','size'=>12,]);
		$textrun1->addText(" I can confirm that with any measurement, I also undertake 3 measurement checks before recording the results. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		
		$section->addText("A Hand Held 30m (Reel) Tape Measure",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun2 = $section->addTextRun();
		$textrun2->addText("I have used a 30m tape measure during my inspection. In particular I have used this to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun2->addText("enter information.",['bgcolor'=>'ffff00','name' => 'Gill Sans MT','size'=>12,]);
		$textrun2->addText(" I can confirm that with any measurement, I also undertake 3 measurement checks before recording the results.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("An Electronic Distance Laser Measure",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun3 = $section->addTextRun();
		$textrun3->addText("I have used an electronic distance laser measure to accurately measure distances to within 1.5mm. An electronic distance laser measure operates by sending a pulse (in the form of a laser) to a specified point and then measuring the time it takes for the reflection to return, it then calculates that into distance form. In using an electronic distance laser measure I am able to ensure the findings are accurate and precise. In this instance, I have used this tool to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun3->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun3->addText(" I can confirm that with any measurement, I also undertake 3 measure checks before recording the results. I can confirm the laser measure is calibrated prior to inspection and cross checked with a colleague. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("A Plumb Bob",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun4 = $section->addTextRun();
		$textrun4->addText("A plumb line was used as an accurate measure in determining the position of the boundary line. A plumb line operates by a central point being located, with a weighted bob or plummet then being suspended and balanced. Once the bob or plummet has balanced, the central point has been located. In order to determine this central point, the bob or plummet was suspended from the centre point of the party wall between the ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun4->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("A String Line",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun5 = $section->addTextRun();
		$textrun5->addText("A hi-vis orange string line was used to help me map out and locate the boundary line, while also acting as a visual indicator to assist in my determination of the boundary line’s position. In this case I can confirm the string level was used ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun5->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("Pins and Tack",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun6 = $section->addTextRun();
		$textrun6->addText("Pins, chalk and tack were used during my site inspection to assist in mapping out the boundary line. In this case I can confirm I used these ",['name' => 'Gill Sans MT','size'=>12,]);		
		$textrun6->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("Ladders",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun7 = $section->addTextRun();
		$textrun7->addText("A 3.2 metre ladders was used to assist in my measurement of ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun7->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun7->addText(" In order to do this, the ladder was placed against the ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun7->addText("enter information,",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun7->addText(" measurement was then taken.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("A Camera ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("I can confirm I have used a camera to photograph my findings while inspecting ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.", these photograph have been annotated with text, color, markers and arrows to confirm my findings and inform you of these. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("11.	External Sources of Information ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun8 = $section->addTextRun();
		$textrun8->addText("In preparing this report, I can confirm I have used the following information to help formulate and arrive at my opinion of the Boundary: ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun8->addText("If not used it is to be deleted.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addListItem(htmlspecialchars("Her Majesty’s Land Registry (HMLR) Title deeds for:"), 0, null,['name' => 'Gill Sans MT','size'=>12]);
		$section->addText($job->surveying->address_of_boundary_determination." (".$job->surveying->boundary_determination_address.") ",['name' => 'Gill Sans MT','size'=>12, 'align'  => 'center']);
		$section->addText($job->surveying->address_of_adjoining_property." (".$job->surveying->adjoining_property.") ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Her Majesty’s Land Registry (HMLR) Map Search for "), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);
		$section->addText($job->surveying->address_of_boundary_determination." (".$job->surveying->boundary_determination_address.") ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addText($job->surveying->address_of_adjoining_property." (".$job->surveying->adjoining_property.") ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Maps searched from the National Library of Scotland covering "), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);
		$section->addText($job->surveying->address_of_boundary_determination." ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addText($job->surveying->address_of_adjoining_property,['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addListItem(htmlspecialchars($job->surveying->local_authority." Planning Records, both historic and current records covering both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." as well as any surrounding properties that may contain pertinent information.  "), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);

		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Google Street View Search of both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property."."), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);


		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Google Earth Search of both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property."."), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);


		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Bing Maps Street View Search of both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property."."), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);

		$section->addTextBreak();
		$section->addListItem(htmlspecialchars("Historical Photographs provided by ".$job->surveying->client_full_name."."), 0, null,['name' => 'Gill Sans MT','size'=>12, 'isFile' => true]);

		$section->addTextBreak();
		$section->addText("12.	".$job->surveying->surveryor_name."’s Experience" ,['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("During my time as a Building Surveyor, I ".$job->surveying->surveryor_name." have been involved in commercial instructions for all types of properties from residential to commercial property, notably these have included Party Wall Disputes which are governed by the Party Wall etc Act 1996. Pre-Purchase Surveys including RICS HomeBuyer Reports, Building Surveys and Valuations. Defect Analysis Reports that have determined issues such as heave, subsidence and all forms of damp. I have also been involved in Boundary Determination reports throughout London and Surrey. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("During my career, I have also assisted with over 3,000 neighbourly disputes between owners under the Party Wall etc Act 1996 and also civil disputes.  ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("In inspecting this property and providing my opinion of the boundary, I have relied upon the information provided within my governing bodies mandatory codes of conduct and guidance notes. In particular; Boundaries: Procedures for Boundary Identification, Demarcation and Dispute Resolution, 3rd Edition and The Code of Measuring Practice, 6th Edition.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("I ".$job->surveying->surveryor_name.", confirm I have applied my learned knowledge in preparing this report. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("13.	Desktop Information Review",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);

		$section->addText("14.	Land Registry Title Plans",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Her Majesty’s Land Registry (HMLR) weren’t and aren’t intended to determine the boundary line between properties, however they were and are intended to clearly indicate boundary lines between properties.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In this case, the scale at which the Land Registry title plans were drawn is at 1 to 1,250. This means that every 10mm (1cm) on the plan would represent 12.5m on site. Attempting to try to scale measure a of a 1 to 1,250 plan would mean that a 1mm measurement on the printed plan would represent 1.25m on site. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("For this reason, I have not attempted to scale the plans as it wouldn’t allow for effective measurement and would lead to vastly incorrect information. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("14.2	".$job->surveying->address_of_boundary_determination." (".$job->surveying->boundary_determination_address.")",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("The Land Registry title plan number ".$job->surveying->boundary_determination_address." for ".$job->surveying->address_of_boundary_determination." indicates that the boundary line between ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." is ".$job->surveying->plane_of_boundary." and runs ".$job->surveying->adjoining_property." along ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information. ",['name' => 'Gill Sans MT','size'=>12, 'bgcolor' => 'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Land Registry title plan ".$job->surveying->boundary_determination_address." labelled",['name' => 'Gill Sans MT','size'=>12, 'bgcolor' => 'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix 1: Screen shot of ".$job->surveying->boundary_determination_address."",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("14.3	".$job->surveying->address_of_adjoining_property." (".$job->surveying->adjoining_property.").",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun9 = $section->addTextRun();
		$textrun9->addText("As with ".$job->surveying->address_of_boundary_determination.", the Land Registry title plan (".$job->surveying->adjoining_property.") of ".$job->surveying->address_of_adjoining_property.", follows the information confirmed within 14.2 ".$job->surveying->address_of_boundary_determination." ’s Land Registry title plan number (".$job->surveying->boundary_determination_address.")",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Land Registry title plan ".$job->surveying->adjoining_property." labelled",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix 2: Screen shot of ".$job->surveying->adjoining_property."",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("14.4	Her Majesty’s Land Registry Title Plan Conclusion ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Having reviewed the Land Registry title plans for ".$job->surveying->address_of_boundary_determination." (".$job->surveying->boundary_determination_address.") and ".$job->surveying->address_of_adjoining_property." (".$job->surveying->adjoining_property.") respectively and then together, one constant is that the boundary line separating the properties has a ".$job->surveying->direction_of_boundary." direction. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("15.	Her Majesty’s Land Registry Map Search",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("This is an interactive search and locate tool that allows me to both locate the properties ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.", obtain their legal documentation for download and review and also cross check with the respective title plans to confirm if there are any discrepancies in the information. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("While the map search isn’t intended to determine boundary lines, it does none the less illustrate the direction of boundary lines. I have used this tool to identify if there are any conflicts between the title plans and map search. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("15.1	".$job->surveying->address_of_boundary_determination." ".$job->surveying->boundary_determination_address,['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Insert Screenshot of Land Registry title plan ".$job->surveying->boundary_determination_address." labelled",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		// $section->addText('E');
		$section->addText("Appendix 3: Screen shot of HMLR Map search for ".$job->surveying->boundary_determination_address.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("As you can see this *is the same *is different to the results confirmed within 14.2 of this report.",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("15.2	".$job->surveying->address_of_adjoining_property." ".$job->surveying->adjoining_property."",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Insert Screenshot of Land Registry title plan ".$job->surveying->adjoining_property." labelled",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix 4: Screen shot of HMLR Map search ".$job->surveying->adjoining_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("As you can see this *is the same *is different to the results confirmed within 14.3 of this report.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("15.3	Her Majesty’s Land Registry Map Search Conclusion ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In this case, having reviewed both properties; ".$job->surveying->address_of_boundary_determination." (".$job->surveying->boundary_determination_address.") and ".$job->surveying->address_of_adjoining_property." (".$job->surveying->adjoining_property.") it is my opinion that the map search results mirror that of the title plans, in so far as the boundary line is ".$job->surveying->direction_of_boundary." and ".$job->surveying->plane_of_boundary.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I have no reason to believe the Land Registry details are inaccurate or incorrect.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("16.	London Borough of ".$job->surveying->local_authority." Planning Application Register ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("I have reviewed the London Borough of ".$job->surveying->local_authority." Planning Application Register for previous and current relevant planning applications for both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." as well as the surrounding properties.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In this instance, there ".$job->surveying->planning_records_of_assistance." applications for the properties that contained information that assisted in the determination of the boundary line. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("16.1	Full Property Address ".$job->surveying->local_authority." Planning Application Number:",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);

		$section->addText("Date of Application: ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addTextBreak();
		$textrun11 = $section->addTextRun();
		// $textrun11->addText('I can confirm ',['name' => 'Gill Sans MT','size'=>12,]);
		$textrun11->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of ".$job->surveying->local_authority." Planning Application Number. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Appendix *X: Screen shot of *Site Address ".$job->surveying->local_authority." Planning Application Number ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("16.X London Borough of ".$job->surveying->local_authority." Planning Application Register Conclusion",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Upon review of London Borough of ".$job->surveying->local_authority." Planning Application Register, I am of the opinion that the boundary line between ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." mirrors the findings of both Sections 14 and 15. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("17.	Historic Map Search",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Historic maps are often recorded and illustrated at scales far in excess of 1 to 1,250 and more often than not can be at a scale in the region of 1 to 2,500 which is known as the inch to mile map. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("While the historic maps may not always have scales that allow measurement, they do clearly indicate historic boundary positions and can often be very useful in determining and confirming the boundary line’s position towards the start of the properties life circa ".$job->surveying->age_of_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$textrun10 = $section->addTextRun();
		$textrun10->addText("*I have reviewed historic maps for the area, which came to a total of ".$job->surveying->historic_maps_were_present." maps ranging from the year of ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun10->addText("YEAR ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun10->addText("to ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun10->addText("YEAR ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun10->addText("which covered both ".$job->surveying->historic_maps_were_present." and ".$job->surveying->address_of_adjoining_property.". Of those ".$job->surveying->historic_maps_were_present." maps, ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun10->addText("*number ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun10->addText("were relevant with the findings as follows: ",['name' => 'Gill Sans MT','size'=>12,]);


		$section->addTextBreak();
		$section->addText("17.1	*Full Map Title",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);

		$section->addText("Date of Map: Enter",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Map",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Screen shot of Map covering ".$job->surveying->address_of_boundary_determination." dated Enter.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("17.X	Historic Map Search Conclusion ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun12 = $section->addTextRun();
		$textrun12->addText("Having reviewed the ".$job->surveying->historic_maps_were_present." historic maps in this instance, ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun12->addText("Enter site specific information and change wording if different.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addTextBreak();
		$section->addText("18.	Google Street View ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In an effort to try and look at both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." from a historical perspective I have looked at Google’s Street View maps and photographs. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Google Street View",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Screen shot from Google Street View of ".$job->surveying->address_of_boundary_determination.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("19.	Google Earth View ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In an effort to try and look at both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." from a different perspective I have looked at Google’s Earth maps and photographs. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Google Earth View",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Screen shot from Google Earth View of ".$job->surveying->address_of_boundary_determination.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("20.	Bing Map Views ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In an effort to try and look at both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property." from a different perspective I have looked at Bing Maps and photographs. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert Screenshot of Google Earth View",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Screen shot from Bing Maps of ".$job->surveying->address_of_boundary_determination.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("21.	Historic Photographs",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Insert historic photographs commenting on them. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Historic Photographs of ".$job->surveying->address_of_boundary_determination.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("22.	Onsite Inspection of ".$job->surveying->created_at->format('d-M-Y'),['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("In an effort to determine the exact location of the boundary line between ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.", I have undertaken a site inspection. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In undertaking my inspection on site, the first logical point of reference as a Boundary Surveyor is always to locate a position on site that has remained unchanged from the day the properties were built circa ".$job->surveying->age_of_property.", or attempt to try and uncover or locate any physical or historical features that remain on the ground between ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In this case, I can confirm I used enter as that point. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor' => 'ffff00']);
		$section->addTextBreak();
		$section->addText("Below is a collection of shapes to use when annotating photographs (copy and paste)",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun11 = $section->addTextRun();
		$textrun11->addImage('images/shapes/fillarrow.png',['height'=>70 , 'width'=>70]);
		$textrun11->addImage('images/shapes/circle.png',['height'=>70 , 'width'=>70]);
		$textrun11->addImage('images/shapes/circledotted.png',['height'=>70 , 'width'=>70]);
		$section->addImage('images/shapes/thinarrow.png',['height'=>20 , 'width'=>210]);
		$section->addImage('images/shapes/thinarrow-dotted.png',['height'=>20 , 'width'=>210]);

		$section->addTextBreak();
		$section->addText("Insert photographs commenting and annotating them. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Photographs taken on ".$job->surveying->created_at->format('d-M-Y')." of ".$job->surveying->address_of_boundary_determination.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Repeat As required, there must be a minimum of 3 annotated photographs ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("23.	Onsite Health and Safety Observations ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$textrun13 = $section->addTextRun();
		$textrun13->addText("While I wasn’t tasked to comment on these types of issues, as a property professional I feel I have a duty of care to point out the following; ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun13->addText("Enter site specific information and risks such as asbestos, falling fence, dangerous structure, damp. ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$textrun13->addText("I would advise having these issue looked at by the appropriate professional. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("Insert photograph",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("Appendix *X: Photograph of X ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText(" Repeat As required",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("24.	Not to Scale Sketch ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText(" In an effort to help clarify my findings, I have annotated Land Registry title plan ".$job->surveying->boundary_determination_address." to confirm my findings in a clear and concise manner. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Appendix *X: annotated Land Registry title plan ".$job->surveying->boundary_determination_address,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Enter site specific information and change wording if different.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("25.	Report Conclusions ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Having inspected ".$job->surveying->address_of_boundary_determination." and undertaken research into both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$textrun14 = $section->addTextRun();
		$textrun14->addText("I am of the opinion of ",['name' => 'Gill Sans MT','size'=>12,]);
		$textrun14->addText("Enter site specific information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor' => 'ffff00']);
		$section->addTextBreak();
		$section->addText("I believe the facts stated in this document to be true and accurate and represent the lay of the land as inspected on ".$job->surveying->created_at->format('d-M-Y').".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I ".$job->surveying->surveryor_name.", of Berry Lodge Surveyors, hereby confirm that I have prepared this Boundary Report and believe the contents to be a true, impartial and arm’s length report in respect of both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addText("Signed: ………………………………………………………….",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		// $section->addTextBreak();
		$section->addText(	$job->surveying->surveryor_name ,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addText(	$job->surveying->s_surveyor_qualifications ,['name' => 'Gill Sans MT','size'=>12,]);
		// $section->addTextBreak();
		$section->addText("BERRY LODGE SURVEYORS ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Date:	".date('d-m-y'),['name' => 'Gill Sans MT','size'=>12,'color' => '1aae93']);
		$section->addTextBreak();

		$section->addText("26.	Statement of Duty to the Court ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("I ".$job->surveying->surveryor_name." can confirm I have used all information listed in Section 11 In of this report to help formulate and arrive at my opinion of the Boundary.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I am aware that pursuant to Rule 35.3 of the Civil Procedure Rules (the “CPR”), it is my overriding duty to assist the court on matters within my expertise and that my duty to the court overrides any obligation to ".$job->surveying->client_full_name." as a client.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I confirm that I have complied with my obligations pursuant to CPR Part 35.3 in producing this report.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I have also had regard to CPR part 35 as a whole and its Practice Direction, and the Protocol for the Instruction of Experts to give Evidence in Civil Claims.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("27.	What to do Next",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addText("Change/select below as required ",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText($job->surveying->address_of_adjoining_property." in breach ");
		$section->addTextBreak();
		$section->addText("From a legal perspective, a boundary between two properties can only be determined through the legal agreement of two respective owners via Her Majesty’s Land Registry (HMLR) or via the civil court procedures, which are commonly in the form of a civil legal dispute between the respective property owners. The latter option is usually exorbitant in time and cost.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("To put these types of costs into perspective, from start to finish I would expect an average boundary dispute and determination to be in the region of £15,000 - £100,000 when taking into account professional fees. In my experience boundary matters are likely to take in the region of 3 – 18 months to be resolved by the courts.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I personally advise clients against this route as in many cases I have seen the benefits of even the most a successful outcome far outweighed by the cost of the exercise. It is also worth mentioning that even a successful outcome via the courts will not necessarily mean that all of your costs are recovered.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("As confirmed in my findings and conclusion I believe the contents to be a true, impartial and arm’s length report in respect of both ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I would therefore suggest providing ".$job->surveying->name_of_legal_owner." of ".$job->surveying->address_of_adjoining_property." a copy of this report and asking for response. You may also want to give ".$job->surveying->name_of_legal_owner." a set period of time to respond such as 14 or 28 days.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Onward from that time if you haven’t received response, it may be worth having a solicitor write to ".$job->surveying->name_of_legal_owner." to request the issue is addressed and again set period of time to respond such as 14 or 28 days for this to take place.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("If that still fails, the only option would be to proceed to court. Prior to doing that I would highly advise taking legal advice and getting a firm idea of the likely costs involved. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I would be happy to provide you with a couple of solicitors who have assisted previous clients in the past. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText($job->surveying->address_of_boundary_determination." unsuccessful outcome After site inspection and much research, unfortunately I note that this report goes against your belief in regards to the ".$job->surveying->boundary_being_determined." boundary line adjoining ".$job->surveying->address_of_adjoining_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("At this stage, you can seek a second opinion from another Surveyor within Berry Lodge Surveyors at the fixed cost of £450 + VAT (£540.00) or alternatively you could seek opinion from another Surveying company. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("I would actually advise against both of these as I can confirm I have looked at all of the factors and points surrounding the ".$job->surveying->boundary_being_determined." boundary line between ".$job->surveying->address_of_boundary_determination." and ".$job->surveying->address_of_adjoining_property.", in this instance I was unable to locate issue. ",['name' => 'Gill Sans MT','size'=>12,]);

		$section->addTextBreak();
		$section->addText("28.	Appendix",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addTextBreak();

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		// $phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		// $xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.pdf");

		// $file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}");
		// $file->execute();

		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
