<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;
use PDF;
use File;
class ValuationWord
{
	public $documentFolder="Valuation Report";


	public $documentName="Valuation Report Word Document";

    	public function create(\App\Job $job){
    		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
    		// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
    		// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");
	    	
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section1 = $phpWord->addSection();
		// $header1 = $section1->addHeader();
		// $header1->addImage('images/bgberry-lodge-top.jpg',['height' => 70, 'width' => 500]);
		// $section1->addTextBreak();
		// $section1->addText($job->surveying->client_full_name);
		// $section1->addText($job->surveying->client_contact_address);
		// $section1->addTextBreak();
		// /*$section1->addText($job->surveying->date_of_confirmation_letter,['align' => 'right','size' => 12,'name' => 'Gill Sans MT']);*/
		// $section1->addText($job->surveying->date_of_confirmation_letter, [], [ 'align' => 'right' ]);
		// $section1->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		// $section1->addTextBreak();
		// $section1->addText('Dear Dr '.$job->surveying->client_full_name.",",['size' => 12,'name' => 'Gill Sans MT']);
		// $section1->addTextBreak();
		// $section1->addText("Re: Boundary Determination Report of 1 BO Street, London, W1U 5LA",['bold' => true,'name' => 'Gill Sans MT','size' => 12]);

		// $section1->addTextBreak();
		// $section1->addText("Further to our visit on ".$job->surveying->created_at->format('d/m/y')." to complete the Inspection of ".$job->surveying->address_of_boundary_determination.", I am pleased to enclose the completed Boundary Determination Report in respect of this matter.",['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addTextBreak();
		// $section1->addText("I would advise having a thorough read of the report to accommodate yourself with the information enclosed within. Once you have reviewed it I would then encourage you to get in touch with any questions you many have and I will be happy to assist.",['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addTextBreak();
		// $section1->addText($job->surveying->surveryor_name,['name' => 'Gill Sans MT','size'=>12,]);

		// $section1->addText($job->surveying->s_surveyor_qualifications,['name' => 'Gill Sans MT','size'=>12,]);


		// $footer1 = $section1->addFooter();
		// $footer1->addImage('images/docs/footer2.png',['width' => 500]);
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		$section1->addTextBreak();
		// $textrun1 = $section1->addTextRun();
		// $textrun1->addImage('images/docs/dashboard.png',['width'=>500, 'height'=>600,'wrappingStyle' => 'behind']);
		$section1->addText($job->surveying->valuation_type,['size' => 12,'name' => 'Poppins','color' => '1aae93','wrapDistanceTop'=>6],['align'=>'right']);
		$section1->addText('Valuation Report:',['size' => 12,'name' => 'Poppins','color' => '1aae93','wrapDistanceTop'=>6],['align'=>'right']);
		$section1->addText($job->surveying->address_of_valued_property,['size' => 12,'name' => 'Gill Sans MT'],['align'=>'right']);
		/*$imageStyle = array(
		'width' => 768,
		'height' => 768,
		'wrappingStyle' => 'square',
		'positioning' => 'absolute',
		'posHorizontalRel' => 'margin',
		'posVerticalRel' => 'line',
		);
		$section1->addImage('images/docs/dashboard.png', $imageStyle);
		$section1->addText('Boundary Report: ',['name' => 'Poppins', 'bold' => true, 'color' => '1aae93','size' => 12, 'align' => 'right']);*/

		

		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$table = $header->addTable(['border' => 0]);
		$table->addRow();
		$table->addCell(5000)->addImage(
		'images/docs/notice-logo.png',
		array('width' => 115, 'marginTop' => -1 )
		);
		$cell = $table->addCell(5000);
		$textrun = $cell->addTextRun(['alignment' => 'right']);
		$textrun->addText('Valuation Report of: '.$job->surveying->address_of_valued_property,['name' => 'Poppins', 'bold' => true, 'color' => '1aae93',] );
		$textrun->addTextBreak();
		$textrun->addText('Our Reference Number: '.$job->id,['name' => 'Poppins', 'color' => '1aae93','align' => 'right'] );
		$textrun->addTextBreak();
		$textrun->addText('Inspection Date: '.$job->surveying->created_at->format('d-M-Y'),['name' => 'Poppins', 'color' => '1aae93','align' => 'right'] );
		/*$header->addimage('images/docs/header.png', ['width' => 115, 'marginRight' => -20]);
		$header->addText('Boundary Report:'.$job->surveying->address_of_valued_property,['name' => 'Poppins', 'bold' => true, 'color' => '1aae93', 'marginLeft' => 600,] );
		$header->addText('Our Reference Number: '.$job->id,['name' => 'Poppins', 'color' => '1aae93', 'marginLeft' => 600,] );
		$header->addText('Inspection Date: '.$job->surveying->created_at->format('d-M-Y'),['name' => 'Poppins', 'color' => '1aae93', 'marginLeft' => 600,] );*/
		// $footer = $section->addFooter();
		// $table1 = $footer->addTable();
		// $table1->addRow();
		// $cell2 = $table1->addCell(5000)->addImage(
		// 	'images/docs/valuation_footer.png',
		//     array(
		//         'width'         => 100,
		//         'height'        => 50,
		//         'marginTop'     => 600,
		//         'marginLeft'    => 2,
		//         'marginBottom' => 0
		//     )
		// );


		// $cell3 = $table1->addCell(5000);
		// $cell1 = $table1->addCell(5000);
		// //$textrun1 = $cell1->addTextRun();
		// $cell1->addPreserveText('{PAGE}',['color' => '1aae93','name' => 'Gill Sans MT','size'=>12, 'align' => 'right']);
		// // $table1->addCell(5000)->addImage(
		// // 'images/docs/wfooter.png',
		// //     array(
		// //         'width'         => 100,
		// //         'height'        => 100,
		// //         'marginTop'     => -1,
		// //         'marginRight'    => -1,
		// //         'align' => 'right',
		// //         'wrappingStyle' => 'behind'
		// //     )
		// // );
		// $table1->addCell(5000);
		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		/*$footer->addImage(
		    'images/docs/wfooter.png',
		    array(
		        'width'         => 100,
		        'height'        => 100,
		        'marginTop'     => -1,
		        'marginRight'    => -1,
		        'align' => 'right',
		        'wrappingStyle' => 'behind'
		    )
		);*/

		/*$footer->addimage('images/docs/wfooter.png',['width' => 150, 'align' => 'right', 'marginRight' => -1 ]);*/
		$section->addTextBreak();
		$section->addText('1.	Surveyor Instructions',['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("I, ".$job->surveying->surveryor_name.", refer to your instructions as per Berry Lodge Surveyor’s confirmation letter dated ".$job->surveying->date_of_confirmation_letter." to provide ".$job->surveying->valuation_type." Valuation of ".$job->surveying->address_of_valued_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("I, ".$job->surveying->surveryor_name.", declare that as a member of the Royal Institute of Chartered Surveyors (RICS) and an RICS Registered Valuer, I am in the position to provide an objective and unbiased ".$job->surveying->valuation_type." Valuation and I am competent to undertake this Valuation assignment.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("I further declare that I have no material connection or involvement to ".$job->surveying->client_full_name." or ".$job->surveying->address_of_valued_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("Finally, I can confirm there are no factors that could limit my ability to provide an impartial and independent valuation of ".$job->surveying->address_of_valued_property.". ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("2.	Client’s Information",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("The request for the ".$job->surveying->valuation_type." Valuation was made by ".$job->surveying->client_full_name." of ".$job->surveying->client_contact_address." who are the ".$job->surveying->valuation_property_ownership_type." owners of ".$job->surveying->address_of_valued_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("3.	Date of Site Visit",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("I, ".$job->surveying->surveryor_name.", can confirm I undertook site inspection of ".$job->surveying->address_of_valued_property." on ".$job->surveying->date_of_inspection.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("4.	Valuation Date",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText($job->surveying->address_of_valued_property." has been valued for the date of ".$job->surveying->valuation_date.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("5.	Inspection Conditions ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("The weather at the time of my inspection ".$job->surveying->inspection_weather." was and ".$job->surveying->inspection_humidity.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("6.	Description of Property ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText($job->surveying->address_of_valued_property." is a ".$job->surveying->valuation_property_ownership_type." ".$job->surveying->type_of_property." and would have likely been constructed circa ".$job->surveying->age_of_property.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("The property is set over ".$job->surveying->floors_in_the_property." floors and made up as follows:",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$txtrun = $section->addTextRun();
		$txtrun->addText("Lower Ground Floor: ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93']);
		$txtrun->addText("Information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$txtrun1 = $section->addTextRun();
		$txtrun1->addText("Ground Floor: ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93']);
		$txtrun1->addText("Information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$txtrun2 = $section->addTextRun();
		$txtrun2->addText("First Floor: ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93']);
		$txtrun2->addText("Information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$txtrun3 = $section->addTextRun();
		$txtrun3->addText("Second Floor: ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93']);
		$txtrun3->addText("Information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$txtrun4 = $section->addTextRun();
		$txtrun4->addText("Third Floor: ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93']);
		$txtrun4->addText("Information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("7.	Valuation Currency ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("I ".$job->surveying->surveryor_name.", declare that the ".$job->surveying->valuation_type." Valuation of the property has been valued in Pound Sterling (£).",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("8.	Limitations to this Report ",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("This report is prepared by ".$job->surveying->surveryor_name.", of Berry Lodge Surveyors for use by ".$job->surveying->client_full_name." of ".$job->surveying->client_contact_address.".",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("9.	Experience of ".$job->surveying->surveryor_name,['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("During my time as a Valuation Surveyor, I, ".$job->surveying->surveryor_name.", have been involved in commercial instructions for all types of properties from residential to commercial property throughout London, the UK and Wales.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Notably these have included Valuations of residential and commercial property, Party Wall Dispute, Pre-Purchase Surveys including RICS HomeBuyer Reports and Building Surveys.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In inspecting ".$job->surveying->address_of_valued_property." and providing my opinion of the value, I have relied upon the information provided within my governing bodies mandatory codes of conduct and guidance notes.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("I, ".$job->surveying->surveryor_name.", confirm I have applied my learned knowledge in preparing this ".$job->surveying->valuation_type." Valuation Report.",['name' => 'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("10.	Standard Terminology",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("In an effort to help you better understand some standard terminology used within this report and within the context of ".$job->surveying->valuation_type." Valuations generally, I have included a short glossary of terms below:",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Arm’s Length Transaction",['name'=>'Gill Sans MT','size'=>12,'bold'=>true,'color'=>'1aae93']);
		$section->addTextBreak();
		$section->addText("An Arm’s Length Transaction, is the estimated amount for which the subject property (".$job->surveying->address_of_valued_property.") should exchange for between a willing buyer and a willing seller, after properly marketing and where the parties had each acted knowledgably, prudently and without compulsion.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("Comparable Evidence",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("Comparable evidence is at the heart of virtually all property valuations. It is the process of identifying, analysing and applying comparable evidence to the subject property (".$job->surveying->address_of_valued_property.") that is being valued. It is fundamental to producing a sound valuation.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("The Estimated Amount",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("The Estimated Amount, refers to a price expressed in terms of money payable for the asset (".$job->surveying->address_of_valued_property.") in an arm’s length market transaction. As confirmed in Section 6 of this report, this property has been valued in Pound Sterling.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("Market Value",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("Market Value, is the most probable price reasonably obtainable of the subject property (".$job->surveying->address_of_valued_property.") in the market on the valuation date (".$job->surveying->valuation_date."). It is the best price reasonably obtainable by the seller and the most advantageous price reasonably obtainable by the buyer.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("An Asset or Liability should Exchange",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("An Asset or Liability should Exchange, refers to the fact that the value of an asset or liability is an estimated amount rather than a predetermined amount or actual sale price. ",['name'=>'Gill Sans MT','size'=>12]);

		$section->addTextBreak();
		$section->addText("It is the price in a transaction that meets all the elements of the Market Value definition at the valuation date. As confirmed in Section 3 of this report, that date is confirmed as ".$job->surveying->valuation_date.".",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("Between a Willing Buyer",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("Between a Willing Buyer, refers to one who is motivated, but not compelled to buy. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("This buyer is neither over eager nor determined to buy at any price.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("This buyer is also one who purchases in accordance with the realities of the current market and with current market expectations, rather than in relation to an imaginary or hypothetical market that cannot be demonstrated or anticipated to exist. The assumed buyer would also not pay a higher price than the market requires.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("And a Willing Seller",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("A Willing Seller, is neither an over eager nor a forced seller prepared to sell at any price, nor one prepared to hold out for a price not considered reasonable in the current market. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("The willing seller is motivated to sell the property (".$job->surveying->address_of_valued_property.") at market terms for the best price attainable in the open market after proper marketing, whatever that price maybe.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("After Proper Marketing ",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("After Proper Marketing, means that the subject property (".$job->surveying->address_of_valued_property.") has been exposed to the market in the most appropriate manner to affect its disposal at the best price reasonably obtainable in accordance with the Market Value definition. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("The method of sale is deemed to be that most appropriate to obtain the best price in the market to which the seller has access. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("The length of exposure time is not a fixed period but will vary according to the type of asset and market conditions. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("The only criterion is that there must have been sufficient time to allow the asset to be brought to the attention of an adequate number of market participants. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("The exposure period occurs prior to the valuation date (".$job->surveying->valuation_date."). ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("Where the Parties had each acted Knowledgeably and Prudently",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("Where the parties had each acted knowledgeably and prudently, presumes that both the willing buyer and the willing seller are reasonably informed about the nature and characteristics of the asset, its actual and potential uses, and the state of the market as of the valuation date (".$job->surveying->valuation_date."). ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("Each is further presumed to use that knowledge prudently to seek the price that is most favourable for their respective positions in the transaction. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("Prudence is assessed by referring to the state of the market at the International Valuation Standards, not with the benefit of hindsight at some later date. For example, it is not necessarily imprudent for a seller to sell assets in a market with falling prices at a price that is lower than previous market levels. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText("In such cases, as is true for other exchanges in markets with changing prices, the prudent buyer or seller will act in accordance with the best market information available at the time.",['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();

		$section->addText("And Without Compulsion",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("And Without Compulsion, establishes that each party is motivated to undertake the transaction, but neither is forced or unduly coerced to complete it. ",['name'=>'Gill Sans MT','size'=>12]);

		$section->addTextBreak();
		$section->addText("11.	Tools used during my Site Inspection",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("During my inspection of ".$job->surveying->address_of_valued_property." on the ".$job->surveying->date_of_inspection.", I have relied upon the following tools to undertake my inspection:",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("Each Tool is to be customised or confirm where is was used. If not used it is to be deleted. ",['name' => 'Gill Sans MT', 'size'=>12, 'bgcolor' => 'ffff00']);
		$section->addTextBreak();

		$section->addText("A Hand Held 5m Tape Measure",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$txtrun5 = $section->addTextRun();
		$txtrun5->addText("I have used a 5m tape measure during my inspection. In particular I have used this to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$txtrun5->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$txtrun5->addText(" I can confirm that with any measurement, I also undertake 3 measurement checks before recording the results. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("A Hand Held 30m (Reel) Tape Measure",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$txtrun5 = $section->addTextRun();
		$txtrun5->addText("I have used a 30m tape measure during my inspection. In particular I have used this to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$txtrun5->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$txtrun5->addText(" I can confirm that with any measurement, I also undertake 3 measurement checks before recording the results.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("An Electronic Distance Laser Measure",['name'=>'Gill Sans MT','size'=>12,'color'=>'1aae93','bold'=>true]);
		$section->addTextBreak();
		$section->addText("I have used an electronic distance laser measure to accurately measure distances to within 1.5mm. An electronic distance laser measure operates by sending a pulse (in the form of a laser) to a specified point and then measuring the time it takes for the reflection to return, it then calculates that into distance form. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();
		$section->addText("In using an electronic distance laser measure, I am able to ensure the findings are accurate and precise.",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$txtrun6 = $section->addTextRun();
		$txtrun6->addText("In this instance, I have used this tool to measure ",['name' => 'Gill Sans MT','size'=>12,]);
		$txtrun6->addText("enter information.",['name' => 'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("I can confirm that with any measurement, I also undertake 3 measure checks before recording the results. I can confirm the laser measure is calibrated prior to inspection and cross checked with a colleague. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("A Camera",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("I can confirm I have used a camera to photograph ".$job->surveying->address_of_valued_property.". These are held on file as part of my due diligence. ",['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("12.	External Sources of Information ",['name' => 'Gill Sans MT','size'=>12,'bold'=>true,'color' => '1aae93']);
		$section->addText("In preparing this ".$job->surveying->valuation_type." Valuation Report, I can confirm I have used the following information to help formulate and arrive at my opinion of the value. ",['name'=>'Gill Sans MT','size'=>12]);
		$section->addText("If not used it is to be deleted.",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addListItem(htmlspecialchars("Her Majesty’s Land Registry (HMLR) UK House Price Index "), 0, null,['name' => 'Arial','size'=>12]);
		$section->addListItem(htmlspecialchars("Right Move Sold Property Prices "), 0, null,['name' => 'Arial','size'=>12]);
		$section->addListItem(htmlspecialchars("Zoopla Sold Property Prices"), 0, null,['name' => 'Arial','size'=>12]);
		$section->addListItem(htmlspecialchars("Net House Prices Sold Property Prices"), 0, null,['name' => 'Arial','size'=>12]);
		$section->addListItem(htmlspecialchars("Comparable Evidence obtained from local Estate Agents"), 0, null,['name' => 'Arial','size'=>12]);
		$txtrun7 = $section->addTextRun();
		$txtrun7->addText("including ",['name'=>'Gill Sans MT','size'=>12]);
		$txtrun7->addText("enter information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);

		$section->addListItem(htmlspecialchars("Other Sources"), 0, null,['name' => 'Arial','size'=>12]);
		$txtrun8 = $section->addTextRun();
		$txtrun8->addText("enter information",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();

		$section->addText("13.	Valuation Report Conclusions",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		$section->addText("Having inspected ".$job->surveying->address_of_valued_property." on ".$job->surveying->date_of_inspection." and undertaking desktop research and utilizing the sources confirmed in Section 12 of this report, I confirm as follows:",['name'=>'Gill Sans MT', 'size'=>12, ]);
		$section->addTextBreak();
		$section->addText("Enter Findings",['name'=>'Gill Sans MT','size'=>12,'bgcolor'=>'ffff00']);
		$section->addTextBreak();
		$section->addText("I can confirm that my overall unbiased and professional opinion of the overall ".$job->surveying->valuation_type." Value of ".$job->surveying->address_of_valued_property." is £000,000.00 (Million, Thousand, Hundred, Pounds and Zero Pence).",['name'=>'Gill Sans MT', 'size'=>12,]);
		$section->addTextBreak();
		$section->addText("I believe the facts stated in this ".$job->surveying->valuation_type." Valuation Report to be true and accurate and represent an arm’s length opinion of the Value of ".$job->surveying->address_of_valued_property.". ",['name'=>'Gill Sans MT', 'size'=>12]);
		$section->addTextBreak();
		$section->addText("I ".$job->surveying->surveryor_name.", of Berry Lodge Surveyors, hereby confirm that I have prepared this ".$job->surveying->type_of_property." Valuation Report and believe the contents to be a true, impartial and arm’s length report for ".$job->surveying->address_of_valued_property.". ",['name'=>'Gill Sans MT', 'size'=>12]);
		$section->addTextBreak();
		$section->addText("I can confirm that the valuation has been undertaken in accordance with the International Valuation Standards (IVS) and that all significant inputs have been assessed by myself and found to be appropriate for the valuation provided.",['name'=>'Gill Sans MT', 'size'=>12]);
		$section->addTextBreak();

		$section->addText("Signed: ………………………………………………………….",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		$section->addTextBreak();
		$section->addText(	$job->surveying->surveryor_name ,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addText(	$job->surveying->s_surveyor_qualifications ,['name' => 'Gill Sans MT','size'=>12,]);
		$section->addText("BERRY LODGE SURVEYORS ",['name' => 'Gill Sans MT','size'=>12,'bold' => true]);
		$section->addTextBreak();
		$section->addText("Date:	".date('m-d-y'),['name' => 'Gill Sans MT','size'=>12,]);
		$section->addTextBreak();

		$section->addText("14.	Appendix",['name' => 'Poppins', 'bold' => true, 'size'=>12, 'color' => '1aae93']);
		
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		// $phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		// $xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.pdf");
		
		// $file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}");
		// $file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
