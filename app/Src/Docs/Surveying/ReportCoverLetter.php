<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;
use File;
use PDF;
class  ReportCoverLetter
{
	public $documentFolder="Report Cover Letter";
	public $documentName="Client Report Cover Letter";

    	public function create(\App\Job $job){
    		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
    		// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
    		// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");

    	
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans MT');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		


		$section->addTextBreak();
		$section->addText(ucwords($job->surveying->client_full_name));
		$section->addText(ucwords($job->surveying->client_contact_address));
		$section->addText($job->surveying->boundary_date_of_confirmation_letter ? $job->surveying->boundary_date_of_confirmation_letter : $job->surveying->valuation_date_of_confirmation_letter, [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('Dear '.$job->surveying->client_salutation.',');
		$section->addTextBreak();
		$section->addText('Re: '.ucwords($job->surveying->surveying_service).' of '.ucwords($job->surveying->address_of_inspection),['bold' => true]);
		$section->addTextBreak();
	
		$section->addText('Further to our visit on '.$job->surveying->date_of_inspection.' to complete the Inspection of '.$job->surveying->address_of_inspection.', I am pleased to enclose the completed '.$job->surveying->surveying_service.' in respect of this matter.');
		$section->addTextBreak();
		$section->addText('I would advise having a thorough read of the report to accommodate yourself with the information enclosed within. Once you have reviewed it I would then encourage you to get in touch with any questions you many have and I will be happy to assist.');
	

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addText($job->surveying->surveryor_name,['name'=>'Gill Sans MT','size'=>12]);
		$section->addText($job->surveying->s_surveyor_qualifications,['name'=>'Gill Sans MT','size'=>12]);
		$section->addTextBreak();
		$section->addText(ucwords('BERRY LODGE SURVEYORS'),['bold' => true]);
		// $section->addText('',['bold' => true]);


		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		// $phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		// $xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.pdf");
		
		// $file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}");
		// $file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
