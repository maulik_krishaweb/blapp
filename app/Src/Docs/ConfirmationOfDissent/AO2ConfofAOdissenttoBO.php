<?php

namespace App\Src\Docs\ConfirmationOfDissent;
use Illuminate\Support\Facades\Storage;

class  AO2ConfofAOdissenttoBO
{
	public $documentFolder="Confirmation of dissent Letters";
	public $documentName="AO 2 Conf of AO dissent to BO";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both', 'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0)));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		//$section->addTextBreak();
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		//$section->addTextBreak();
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak(1);
		
		$section->addText('Re: '.ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao2->property_address_adjoining),['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996', ['bold' => true]);
		$section->addTextBreak();
		$section->addText('I have been passed a copy of the Party Wall '.$job->ao2->notice_notices.' dated '.$job->ao2->date_of_notice.' and served upon '.$job->ao2->full_names.', '.$job->ao2->owners_referral.' of '.$job->ao2->property_address_adjoining.' which adjoins your property, '.$job->bo->property_address_proposed_work.'. ');
		$section->addTextBreak();
		$section->addText('I can confirm that '.$job->ao2->salutation.' '.$job->ao2->has_appointed_have_appointed.' dissented to the Party Wall '.$job->ao2->notice_notices.' and '.$job->ao2->has_appointed_have_appointed.' appointed me to act as '.$job->ao2->his_her_their.' Party Wall Surveyor in accordance with the Party Wall etc Act 1996. ');
		$section->addTextBreak();
		$section->addText('Please kindly have your Party Wall Surveyor contact me at the first instance so we can progress this matter promptly, alternatively if you would like me to act as the Agreed Party Wall Surveyor please contact me and I will be happy to discuss this option with you. ');
		$section->addTextBreak();
		$section->addText('Please ensure that no Notifiable Party Wall works commence until the Party Wall Award has been agreed. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');

		


		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addText(ucwords($job->ao2->surveyor_name));
		$section->addText(ucwords($job->ao2->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak();
		$section->addText('cc. '.$job->ao2->salutation);
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
