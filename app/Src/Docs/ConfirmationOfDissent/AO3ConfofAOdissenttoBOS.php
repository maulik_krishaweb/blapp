<?php

namespace App\Src\Docs\ConfirmationOfDissent;
use Illuminate\Support\Facades\Storage;

class  AO3ConfofAOdissenttoBOS
{
	public $documentFolder="Confirmation of dissent Letters";
	public $documentName="AO 3 Conf of AO dissent to BOS";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak();
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText(ucwords($job->bo->surveyor_company_name));
		$section->addText(ucwords($job->bo->surveyor_company_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->surveyor_name.',');
		$section->addTextBreak();
		$section->addText('RE: '.$job->bo->property_address_proposed_work.' / '.$job->ao3->property_address_adjoining,['bold' => true]);
		$section->addText('The Party Wall etc Act 1996',['bold' => true]);
		$section->addTextBreak();


		$section->addText('I have been passed a copy of the Party Wall '.$job->ao3->notice_notices.' dated '.$job->ao3->date_of_notice.' and served upon '.$job->ao3->full_names.', '.$job->ao3->owners_referral.' of '.$job->ao3->property_address_adjoining.' which adjoins '.$job->bo->property_address_proposed_work.'. ');
		$section->addTextBreak();
		$section->addText('I can confirm that '.$job->ao3->salutation.' '.$job->ao3->has_appointed_have_appointed.' dissented to the '.$job->ao3->notice_notices.' and '.$job->ao3->has_appointed_have_appointed.' appointed me to act as '.$job->ao3->his_her_their.' Party Wall Surveyor in accordance with the above Act. ');
		$section->addTextBreak();
		$section->addText('*I have attached my appointment letter for your records, if you could kindly send me yours along with Third Surveyor selections and a pack of information containing the land registry details, drawings and structural information. ');
		$section->addTextBreak();
		$section->addText('*I am in the process of obtaining my appointment letter and I will provide you with a copy as soon as it is available, in the mean time if you could kindly send me yours along with Third Surveyor selections and a pack of information containing the land registry details, drawings and structural information.');
		$section->addTextBreak();
		$section->addText('Please contact me so that we can arrange a mutually convenient date to visit the adjoining '.$job->ao3->owners_owners.' property to record the Schedule of Condition Report. ');
		$section->addTextBreak();
		$section->addText('I look forward to hearing from you so this matter can be progressed promptly. ');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addTextBreak();
		$section->addTextBreak();

		$section->addText(ucwords($job->ao3->surveyor_name));
		$section->addText(ucwords($job->ao3->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
      $section->addTextBreak();
		$section->addText('cc. '.$job->ao3->salutation);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
