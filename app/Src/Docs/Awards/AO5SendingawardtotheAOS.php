<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO5SendingAwardToTheAOS
{
	public $documentFolder="Award Letters";
	public $documentName="AO 5 Sending award to the AOS";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText(ucwords($job->ao5->surveyor_name).' '.ucwords($job->ao5->surveyor_qualifications));
		$section->addText(ucwords($job->ao5->surveyor_company_name));
		$section->addText(ucwords($job->ao5->surveyor_company_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->ao5->surveyor_name.',');
		$section->addTextBreak();

		$section->addText('Re: '.ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao5->property_address_adjoining),['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to our recent telephone and email discussions, I would like to take this opportunity to thank you for agreeing the Party Wall Awards with me. ');
		$section->addTextBreak();$section->addText('Please find attached the signed fair copies of the Party Wall Awards, if you could kindly ensure that you enter the following information: ');
		$section->addTextBreak();
		$section->addListItem('Please date the Party Wall Award');
		$section->addListItem('Please insert your signature into the Party Wall Award');
		$section->addTextBreak();
		$section->addText('Please serve your appointing '.$job->ao5->owners_owners.' Party Wall Award upon them and kindly return the building '.$job->bo->owners_owners.' Party Wall Award to me along with your invoice and service letter. I will then ensure your invoice is enclosed when I serve the Party Wall Award upon the building '.$job->bo->owner_referral.'. ');
		$section->addTextBreak();
		$section->addText('Thank you for your help in this matter, I look forward to working with you again in the near future. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);

		$section->addText(ucwords($job->bo->surveyor_name));
      $section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
