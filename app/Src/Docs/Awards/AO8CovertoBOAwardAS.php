<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO8CoverToBOAwardAs
{
	public $documentFolder="Award Letters";
	public $documentName="AO 8 Cover to BO award AS";

    	public function create(\App\Job $job){
	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();

		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao8->property_address_adjoining),['bold' => true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();

		$section->addText('Further to *my visit *my colleague ENTER NAME visit on '.$job->ao8->soc_date.' to complete the Schedule of Condition Report of the adjoining '.$job->ao8->owners_owners.' *property *properties, I have now reviewed all the information and I am pleased to confirm that I am happy to serve the enclosed Party Wall *Award *Awards in respect of this matter.');
		$section->addTextBreak();
		$section->addText('The Party Wall *Award *Awards *is a *are legally binding *document *documents and not only governs the specifics of the notifiable works, *it *they also legally *protect *protects you and the adjoining '.$job->ao8->owners_referral.'  in the event of damage being alleged or caused to '.$job->ao8->his_her_their.'  *property *properties.');
		$section->addTextBreak();
		$section->addText('In accordance with Section 10(17) of the Party Wall etc. Act 1996 you have a legal right to appeal the Party Wall *Award *Awards within 14 days of *its *their date, however to the best of my knowledge there is nothing within the Party Wall *Award *Awards that should cause you to do so. ');
		$section->addTextBreak();
		$section->addText('You will note that clause X of the Party Wall *Award *Awards deals with X.');
		$section->addTextBreak();
		$section->addText('I hope your works go smoothly, however should any issues arise during the course of the works please contact me.');
		$section->addTextBreak();
		$section->addText('I have attached a copy of my invoice '.$job->invoice_no.' in respect of this matter for payment and I would be grateful if you could settle this at your earliest opportunity. ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me and I will be happy to assist. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
