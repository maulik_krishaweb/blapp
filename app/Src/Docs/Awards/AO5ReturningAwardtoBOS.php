<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO5ReturningAwardToBos
{
	public $documentFolder="Award Letters";
	public $documentName="AO 5 Returning Award to BOS";

    	public function create(\App\Job $job){
	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText(ucwords($job->bo->surveyor_name).' '.ucwords($job->bo->surveyor_qualifications));
		$section->addText(ucwords($job->bo->surveyor_company_name));
		$section->addText(ucwords($job->bo->surveyor_company_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);

		$section->addText('Dear '.$job->bo->surveyor_name.',');
		$section->addTextBreak();

		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao5->property_address_adjoining),['bold' => true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for sending the fair copies of the Party Wall Awards, I can confirm I have signed those. ');
		$section->addTextBreak();
		$section->addText('Please find attached a copy of the building '.$job->bo->owners_owners.' Party Wall Award for you to serve upon your appointing '.$job->bo->owner_referral.'. I confirm I have served the Party Wall Award upon '.$job->ao5->salutation.'.');
		$section->addTextBreak();
		$section->addText('I have also attached my invoice for payment and I would be grateful if you could send this to your appointing '.$job->bo->owner_referral.' along with '.$job->bo->his_her_their.' Party Wall Award.');
		$section->addTextBreak();
		$section->addText('Thank you for your help in this matter, I look forward to working with you again in the near future. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->ao5->surveyor_name));
      $section->addText(ucwords($job->ao5->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
