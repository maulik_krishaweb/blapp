<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  DraftAO9ASAward
{
	public $documentFolder="Draft Awards/Final Awards";
	public $documentName="AO 9 AS Award";

    	public function create(\App\Job $job)
    	{

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both','spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0), 'lineHeight' => 1.5));
		$section = $phpWord->addSection();

		$footer = $section->addFooter();
		//$footer->addimage('images/docs/footer.png', ['width' => 460]);
	

		$section->addTextBreak(10);
		//$section->addTextBreak(12);
		$section->addText('Party Wall Award: ', ['bold' => true, 'color'=>'00b29c'],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->property_address_proposed_work),['bold' => false],['alignment' => 'right']);
		$section->addText('&', ['bold' => false],['alignment' => 'right']);
		$section->addText(ucwords($job->ao9->property_address_adjoining),['bold' => false],['alignment' => 'right']);


		$body = $phpWord->addSection();
		$header = $body->addHeader();
		$header->addText('The Party Wall etc Act 1996', ['bold' => true, 'color'=>'00b29c'],['alignment' => 'right']);
		$header->addText('Party Wall Award', ['bold' => true, 'color'=>'00b29c'],['alignment' => 'right']);

		$body->addTextBreak(1);

		$body->addText('AN AWARD under the provisions of the Party Wall etc Act 1996 to be served forthwith on the Appointing Owners under Section 10(14) of the above Act. ', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('WHEREAS '.$job->bo->full_name.' of '.$job->bo->contact_address.' (‘the building '.$job->bo->owner_referral.'’) '.$job->bo->is_are.' '.$job->bo->owner_referral.' within the meaning of the Party Wall etc. Act 1996 (‘the Act’) of the premises known as '.$job->bo->property_address_proposed_work.' (‘the building '.$job->bo->owners_owners.' property’).', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('AND '.$job->ao9->full_names.' of '.$job->ao9->contact_address.' (‘the adjoining '.$job->ao9->owners_referral.'’) '.$job->ao9->is_an_are.' '.$job->ao9->owners_referral.' within the meaning of the Act of the premises known as '.$job->ao9->property_address_adjoining.' (‘the adjoining '.$job->ao9->owners_owners.' property’).', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('AND on the '.$job->ao9->date_of_notice.' the building '.$job->bo->owner_referral.' served '.$job->ao9->notice_notices.' on the adjoining '.$job->ao9->owners_referral.' under '.$job->ao9->section_sections.' '.$job->ao9->s1_section.', '.$job->ao9->s2_section.', '.$job->ao9->s6_section.'of the Act of '.$job->bo->his_her_their.' intention to execute the building works described therein between the building '.$job->bo->owners_owners.' property and the adjoining '.$job->ao9->owners_owners.' property (‘the two properties’).', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('AND a dispute has arisen between the building '.$job->bo->owner_referral.' and the adjoining '.$job->ao9->owners_referral.' (hereinafter together called ‘the parties’) within the meaning of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('AND the building '.$job->bo->owner_referral.' and the adjoining '.$job->ao9->owners_referral.' have appointed '.$job->bo->surveyor_name.' '.$job->bo->surveyor_qualifications.' of Berry Lodge Surveyors, Head Office, Upper Floor, 61 Highgate High Street, London, N6 5JY to act as Agreed Surveyor.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('It is a requirement of the Act that the Agreed Surveyor shall settle by Award all or any matter which is connected with any work to which the Act relates and which is in dispute between the building '.$job->bo->owner_referral.' and the adjoining '.$job->ao9->owners_referral.' including: the right to execute the work, the time and manner of executing the work, and any other matter arising out of the dispute including the cost of obtaining and making this Award.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('This Award and its conditions relate only to the works described in clause 2 of this Award and do not relate to other works outside the scope of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('Any acceptance made by the Agreed Surveyor in this Award or subsequently during works on site shall not be taken to imply any responsibility by him or his appointed technical delegates for any structural or any other insufficiency in any part of the works whether existing or executed.', array('lineHeight' => 1.5));
		 $body->addTextBreak();
		$body->addText('That nothing in this Award shall be held as conferring, admitting or affecting any easement of light or other easement in or relating to the party wall.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('The said premises having been inspected, I the undersigned, being the Agreed Surveyor, and having considered the proposals made by the building '.$job->bo->owner_referral.' and any other relevant matters brought to my attention but without prejudice to any other rights of the parties or of any other persons DO HEREBY MAKE THIS MY AWARD.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('1.', array('lineHeight' => 1.5));
		$body->addText('(a) *That the wall or structure separating the two properties *is a *are a party wall and a party fence wall or party structure within the meaning of the Act. *That the adjoining property is an independent building standing close to or adjoining the building '.$job->bo->owners_owners.' property, within the meaning of Section 20 of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(b) That the *party wall/party fence wall and the parts of the adjoining '.$job->ao9->owners_owners.' property described in the attached Schedule of Condition Report dated '.$job->ao9->soc_date.' is sufficient for the present purposes of the adjoining '.$job->ao9->owners_referral.'. ', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(c) That the Schedule of Condition Report dated '.$job->ao9->soc_date.' attached hereto as a record of fact relates to the adjacent parts of the adjoining '.$job->ao9->owners_owners.' premises prior to the execution of the said work so far as can be ascertained without opening up or disturbing the structure or finishings.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('(d) That the following documents, attached hereto, form part of this Award:', array('lineHeight' => 1.5));
		$body->addText('	(I) Drawings: '.$job->ao9->drawings, array('lineHeight' => 1.5));
		$body->addText('	(II) Schedule of Condition Report dated '.$job->ao9->soc_date, array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('2. That after the service of the signed Award the building '.$job->bo->owner_referral.' shall be at liberty, but shall be under no obligation, to carry out the following works:', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(a)	'.$job->ao9->s1_description, ['bold' => true], array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(b)	'.$job->ao9->s2_description , ['bold' => true], array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(c)	'.$job->ao9->s6_description , ['bold' => true], array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('3. That no deviation from the works shall be made without the prior written agreement of the owners and with their express authority, or in the event of a dispute determined by the Agreed Surveyor in accordance with Section 10 of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('4. That if the building '.$job->bo->owner_referral.' '.$job->bo->carry_carries.' out the works '.$job->bo->he_she_they_referral.' shall:', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(a) Execute the whole of the works and do so at the sole cost of the building '.$job->bo->owner_referral.'.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(b) Take all reasonable precautions and provide all necessary support to retain the land and buildings comprised within the adjoining '.$job->ao9->owners_owners.' property.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(c) Should scaffolding not be fully weatherproofed, provide temporary weathering in the form of heavy duty felt, polythene sheeting, tarpaulin (or similar) and timber battens at 1m centres both horizontally and vertically to those parts of the party wall exposed as a result of the works and maintain this until permanent weathering has been provided unless otherwise agreed in writing by the Agreed Surveyor. Photographic proof of the weathering solution is to be sent to the Agreed Surveyor upon erection for his records.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(d) Make good all damage to the adjoining '.$job->ao9->owners_owners.' property occasioned by the works in materials to match the existing fabric and finishes, to the reasonable satisfaction of the Agreed Surveyor, with such making good to be executed upon completion of the works, or at any earlier time deemed appropriate by the Agreed Surveyor. If so required by the adjoining owner make payment in lieu of carrying out the work to make the damage good, with such sum to be agreed between the owners or determined by the Agreed Surveyor.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(e) Compensate any adjoining '.$job->ao9->owners_referral.' and any adjoining occupier for any loss or damage which may result to any of them by reason of any work executed in pursuance of this Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(f) Permit the Agreed Surveyor to have access to the relevant parts of the building '.$job->bo->owners_owners.' property at all reasonable times during, and to inspect, the progress of the works.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(g) *Carry out the whole of the works so far as practicable from the building '.$job->bo->owners_owners.' property. Where access to the adjoining '.$job->ao9->owners_owners.' property is required, reasonable notice shall be given in accordance with Section 8 of the Act, for the avoidance of any doubt this is a minimum of 14 days written notice from the building '.$job->bo->owner_referral.' to the adjoining '.$job->ao9->owners_referral.'. This written notice is to be provided to the Agreed Surveyor for his records.  *In the event of the building '.$job->bo->owner_referral.' wishing to carry out the works from, or to erect scaffolding on or over the adjoining '.$job->ao9->owners_owners.' property for the purpose of works, details thereof shall first be submitted to and approved by Agreed Surveyor and such approval shall be subject to such conditions as the Agreed Surveyor may agree. ', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(h) *Should access over the adjoining '.$job->ao9->owners_owners.' roof be required for the proposed loft works, it is to be achieved via cantilevered scaffolding. Scaffolding is to be double boarded and fully encapsulated in monarflex sheeting thereby stopping any debris, tools or other items from falling into the adjoining '.$job->ao9->owners_owners.' property. Access is to be restricted to a maximum period of *4-6 weeks. In the event of further access being required the building '.$job->bo->owner_referral.' are to inform the Agreed Surveyor of the proposed details of this. Photographic proof of the cantilevered scaffolding is to be sent to the Agreed Surveyor upon erection for his records. ', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(i) *In the event of the building '.$job->bo->owners_owners.' wishing to carry out the work confirmed in clause XX of this Award on the adjoining '.$job->ao9->owners_owners.' property. The contractor shall adhere to the following requirements: prior to commencing the works, the contractor is to carefully remove the *existing fence, *plant pots, *other items. *Instruct a gardener to visit the adjoining '.$job->ao9->owners_owneAO1CoverToAptLetterrs.' property in advance of the processed access commencing. The gardener is to undertake a full inventory of all the plants within the proposed area of access and arrange for these to temporarily relocated or placed in temporary pots during the course of access. All trellis and fencing is also to be carefully removed, encapsulated in HD polythene sheeting and stored in a place agreed with the adjoining '.$job->ao9->owners_referral.'. The contractor is then to erect a minimum of 2m high timber hoarding, located no more than 1m from the *existing fence. The hoarding is to take into account the operational use of the adjoining '.$job->ao9->owners_owners.' *reception room, *kitchen, *patio doors ensuring these are not hindered in any way. Once hoarding has been erected, the contractor is to cover all surfaces within the proposed area of access with heavy duty polythene sheeting, with timber board laid upon it thereby fully protecting the area. Access is to be restricted to a maximum period of *4-6 weeks. In the event of further access being required the building '.$job->bo->owner_referral.' are to inform the Agreed Surveyor of the proposed details of this. Photographic proof of the hoarding is to be sent to the Agreed Surveyor upon erection for his records.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(j) Restrict noisy works to which this Award relates to the party wall or party structure or adjoining '.$job->ao9->owners_owners.' property to between the hours of 8.30am and 5.30pm Monday to Fridays, 9.00am and 1.00pm on Saturday excepting Sundays and Bank Holidays when no works to which this Award relates shall be undertaken. *Restrict works to which access relates to between the hours of 9.30am and 5.00pm Monday to Fridays excepting Saturdays, Sundays and Bank Holidays when no works to which this access relates shall be undertaken.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(k) The foundations running parallel to the adjoining '.$job->ao9->owners_owners.' *property extension *conservatory *garage are to be dug in 1m sequential hit and miss bays, ensuring no two bays alongside one and other are open at the same time. Bays are to be filled within 12 hours, or otherwise fully supported. Photographic proof of the sequential hit and miss bays is to be sent to the Agreed Surveyor upon digging and filling for his records.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(l) Prior to filling the foundation *foundation bays adjoining the adjoining '.$job->ao9->owners_owners.' *property extension *conservatory *garage, the contractor is to install a slip membrane to ensure both structures are independent of one and other. Photographic proof of the slip membrane is to be sent to the Agreed Surveyor for his record prior to filling the trench.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(m) Ensure that cutting into the Party Wall is to be undertaken using rotary disc cutters/grinders or un-powered hand tools to minimise the risk of damage to the Party Wall. Under no circumstances should powered percussion tools (Jack hammers or Kangos) be used on the Party Wall.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(n) Ensure that all works directly to the Party Wall, in particular cutting into the Party Wall is no further than half the thickness of the Wall itself. ', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(o) Prior to commencing the works directly to the Party Wall, the contractor is to visit the adjoining '.$job->ao9->owners_owners.' property and cover all open vents and chimney breasts with HD polythene sheeting taped in place. In the event that access isn’t given to the contractor, he is to leave HD polythene sheeting and tape outside the adjoining '.$job->ao9->owners_owners.' property for the adjoining '.$job->ao9->owners_referral.' to install themselves. Confirmation of this is to be provided to the Agreed Surveyor for his records. Once the works directly to the Party Wall are complete the contractor is to revisit and remove the temporary protection ensuring the areas are thoroughly cleaned.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(p) Should access be required and agreed by the Agreed Surveyor, remove any scaffolding or screens from the adjoining '.$job->ao9->owners_owners.' property and land as soon as possible.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(q) Upon completion of required access, remove all temporary protections confirmed in clause 4(XX) of this Award from the adjoining '.$job->ao9->owners_owners.' property and land as soon as possible and fully clean the proposed area of access ensuring there is no trace of the contractor’s presence. The contractor is to then reinstate all *pots, *plants in their previous position. In the event that any debris has fallen into the drains or gutters these are to be fully cleaned. Photographic proof of the removed *and/or *cantilevered scaffolding *hoarding is to be sent to Agreed Surveyor for his records.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('(r) Clear away any dust and debris from time to time as necessary, or when directed by the Agreed Surveyor.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('5. That the Agreed Surveyor shall be permitted access to the relevant parts of the adjoining '.$job->ao9->owners_owners.' property from time to time during, and to inspect, the progress of the works at reasonable times and after giving reasonable notice.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('6. That the whole of the works shall be executed in accordance with the Building Regulations, Construction (Design and Management) Regulations 2015, and all requirements and by-laws of statutory authorities where these apply and shall be executed in a proper and workman-like manner in sound and suitable materials in accordance with the terms of this Award to the reasonable satisfaction of the Agreed Surveyor.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('7. That the building '.$job->bo->owner_referral.' shall not exercise any right conferred by this Award in such a manner or at such time as to cause unnecessary inconvenience to the adjoining '.$job->ao9->owners_referral.' or any adjoining occupier, in accordance with Section 7(1) of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('8. That the works shall be carried through with reasonable expedition after commencement and so as to avoid unnecessary inconvenience to the adjoining '.$job->ao9->owners_referral.' or occupiers.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('9. That the building '.$job->bo->owner_referral.' shall immediately on the service of this Award pay the Agreed Surveyor’s fees in the sum of '.$job->award_costs.' plus VAT and disbursements ('.$job->final_amount.') in connection with the obtaining and making of this Award. In the event of damage being caused, or other contingencies or variations arising, a further reasonable fee shall be payable in accordance with Section 10(13) of the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('10. That the Agreed Surveyor reserves the right to make and serve any further Award or Awards that may be necessary, as provided in the Act.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('11. That the building '.$job->bo->owners_owners.' authority to carry out the works under this award is conditional upon the works being commenced within 12 months from the date of this Award.', array('lineHeight' => 1.5));
		$body->addTextBreak();

		$body->addText('12. That either of the parties to the dispute may within 14 days from the date this Award is served upon them appeal to the County Court against this Award.', array('lineHeight' => 1.5));
		$body->addTextBreak();
		$body->addText('I, the Agreed Surveyor have set my hand this       day of                                    202. ', array('lineHeight' => 1.5));
		$body->addTextBreak(3);


		$body->addText('………………………………………… ');
		$body->addText($job->bo->surveyor_name, ['bold' => true]);
		$body->addText($job->bo->surveyor_qualifications, ['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
