<?php

namespace App\Src\Docs\Awards;
use Illuminate\Support\Facades\Storage;

class  AO2CoverToAwardAOS
{
	public $documentFolder="Award Letters";
	public $documentName="AO 2 Cover to Award AOS";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak(1);
		$section->addText($job->ao2->full_names);
		$section->addText(ucwords($job->ao2->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);

		$section->addText('Dear '.$job->ao2->salutation.',');
		$section->addTextBreak();

		$section->addText(ucwords($job->bo->property_address_proposed_work).' / '.ucwords($job->ao2->property_address_adjoining),['bold' => true]);
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();

		$section->addText('Further to *my visit *my colleague ENTER NAME visit on '.$job->ao2->soc_date.' to complete the Schedule of Condition Report of your property, I have now reviewed all the information and drawings sent through by the building '.$job->bo->owners_owners.' Surveyor and I am pleased to confirm that I am happy to serve the Party Wall Award in respect of this matter.');
		$section->addTextBreak();
		$section->addText('The Party Wall Award is a legally binding document and not only governs the specifics of the notifiable Party Wall works, it also legally protects you in the event of damage being caused to your property.');
		$section->addTextBreak();
		$section->addText('In accordance with Section 10(17) of the Party Wall etc Act 1996 you have a legal right to appeal the Party Wall Award within 14 days of its date, however to the best of my knowledge there is nothing within the Party Wall Award that should cause you to do so.');
		$section->addTextBreak();
		$section->addText('You will note that clause X of the Party Wall Award deals with X.');
		$section->addTextBreak();
		$section->addText('Should you notice any issues or damage to your property during the course of the works, please contact me at the first instance and I will take the appropriate action.');
		$section->addTextBreak();
		$section->addText('Should you wish to discuss anything to do with this matter please do not hesitate to contact me and I will be happy to discuss.');

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->ao2->surveyor_name));
		$section->addText(ucwords($job->ao2->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
