<?php

namespace App\Src\Docs\NoticesAndCovers;
use Illuminate\Support\Facades\Storage;

class  AO2AllNoticesandAck
{
	public $documentFolder="Notices and Covers";
	public $documentName="AO 2 All Notices and Ack";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		//$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$phpWord->setDefaultParagraphStyle(array('align' => 'both', 'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0), 'lineHeight' => 1.15));
		// $phpWord->setDefaultParagraphStyle(['spacing' => 100]);
		$section = $phpWord->addSection(['marginTop'=>600, 'marginBottom'=>200, 'marginLeft'=>1000, 'marginRight'=>1000]);

		$title = $section->addTextRun();
		
		// $section->addTextBreak();
		$title->addimage('images/docs/top-1.jpg',array(
		    'width' => 120,
		    'positioning' => 'relative', 
		    'wrappingStyle' => 'behind',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		));

		$section->addTextBreak(1);
		$title = $section->addTextRun();
		$title->addText('The Party Wall etc. Act 1996 (Section 1) ',['bold' => true]);
		$section->addText('LINE OF JUNCTION PARTY WALL NOTICE',['bold' => true, 'size'=>18, 'name'=>'Poppins'] );
		$section->addimage('images/docs/greenbar.png', ['width' => 495, ]);
		//$section->addLine(['weight' => 2, 'width' => 600, 'height' => 0, 'color' => '#00B1A4']);

		$textrun = $section->addTextRun();
		$textrun->addText('To: ', ['bold' => true]);
		$textrun->addText($job->ao2->full_names.' of '.$job->ao2->contact_address);
		$section->addText('(adjoining '.$job->ao2->owners_referral.')', ['bold' => false],['alignment' => 'right']);
		$section->addTextBreak();

		$textrun2 = $section->addTextRun();
		$textrun2->addText($job->bo->i_we_referral_upper_case.': ', ['bold' => true]);
		$textrun2->addText($job->bo->full_name.' of '.$job->bo->contact_address.' ');
		$section->addText('(building '.$job->bo->owner_referral.')', ['bold' => false], ['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('being '.$job->bo->owner_referral.' of '.$job->bo->property_address_proposed_work.' which adjoins your property '.$job->ao2->property_address_adjoining);
		$section->addTextBreak();
		$section->addText('Hereby serve you with Party Wall '.$job->ao2->notice_notices.' as required under Section '.$job->ao2->s1_section.' and as shown in drawings '.$job->ao2->drawings.' it is intended to carry out the works detailed below after the expiration of one month from the date of this Party Wall Notice or earlier by agreement: ');
		$section->addTextBreak();
		$section->addText($job->ao2->s1_description, ['bold' => true]);
		$section->addTextBreak();
		$textrun3 = $section->addTextRun();
		$textrun3->addText('If you do not consent to the works within 14 days, you are deemed to have dissented to the Party Wall Notice and a dispute is deemed to have arisen. In this case, Section 10 of the Party Wall etc. Act 1996 requires that both parties should ');
		$textrun3->addText('concur in the appointment of an Agreed Party Wall surveyor ', ['bold' => true]);
		$textrun3->addText('or should each appoint a Party Wall Surveyor and in those circumstances '.$job->bo->i_we_referral_lower_case.' would appoint:');
		$section->addTextBreak(1);

		$section->addText($job->bo->surveyor_name, ['bold' => true]);
		$section->addText($job->bo->surveyor_qualifications);
		$section->addText('BERRY LODGE SURVEYORS ', ['bold' => true, 'color' => '00B1A4'] );
		$section->addText('Head Office ', ['bold' => true]);
		$section->addText('Upper Floor, 61 Highgate High Street');
		$section->addText('London, N6 5JY');
		$section->addTextBreak(2);

		$section->addText('Signed: ', ['bold' => true]);
		$section->addText('Authorised to sign on behalf of building '.$job->bo->owner_referral, ['bold' => true]);
		$section->addText('Date: '.date('d/m/Y'));
		
		$section->addPageBreak();

		// page 2		
		
		$title = $section->addTextRun();
		
		// $section->addTextBreak();
		$title->addimage('images/docs/top-1.jpg',array(
		    'width' => 120,
		    'positioning' => 'relative', 
		    'wrappingStyle' => 'behind',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		));
		$section->addTextBreak(1);
		$title = $section->addTextRun();
		$title->addText('The Party Wall etc. Act 1996 (Section 3) ',['bold' => true]);
		$section->addText('PARTY STRUCTURE PARTY WALL NOTICE',['bold' => true, 'size'=>18, 'name'=>'Poppins'] );
		$section->addimage('images/docs/greenbar.png', ['width' => 495, ]);
		//$section->addLine(['weight' => 2, 'width' => 600, 'height' => 0, 'color' => '#00B1A4']);

		$textrun = $section->addTextRun();
		$textrun->addText('To: ', ['bold' => true]);
		$textrun->addText($job->ao2->full_names.' of '.$job->ao2->contact_address);
		$section->addText('(adjoining '.$job->ao2->owners_referral.')', [], ['alignment' => 'right']);
		$section->addTextBreak();

		$textrun = $section->addTextRun();
		$textrun->addText($job->bo->i_we_referral_upper_case.': ', ['bold' => true]);
		$textrun->addText($job->bo->full_name.' of '.$job->bo->contact_address.' ');
		$section->addText('(building '.$job->bo->owner_referral.')', [], ['alignment' => 'right']);
		$section->addTextBreak();

		$section->addText('being '.$job->bo->owner_referral.' of '.$job->bo->property_address_proposed_work.' which adjoins your property '.$job->ao2->property_address_adjoining);
		$section->addTextBreak();
		$section->addText('Hereby serve you with Party Wall '.$job->ao2->notice_notices.' as required under Section '.$job->ao2->s2_section.' and as shown in drawings '.$job->ao2->drawings.' it is intended to carry out the works detailed below after the expiration of two months from the date of this Party Wall Notice or earlier by agreement: ');
		$section->addTextBreak();
		$section->addText($job->ao2->s2_description, ['bold' => true]);
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText('If you do not consent to the works within 14 days, you are deemed to have dissented and a dispute is deemed to have arisen. In this case, Section 10 of the Party Wall etc Act 1996 requires that both parties should');
		$textrun->addText(' concur in the appointment of an Agreed Party Wall Surveyor ', ['bold' => true]);
		$textrun->addText('or should each appoint a Party Wall Surveyor and in those circumstances '.$job->bo->i_we_referral_lower_case.' would appoint:');
		
		$section->addTextBreak();
		$section->addText($job->bo->surveyor_name, ['bold' => true]);
		$section->addText($job->bo->surveyor_qualifications);
		$section->addText('BERRY LODGE SURVEYORS ', ['bold' => true, 'color' => '00B1A4']);
		$section->addText('Head Office ', ['bold' => true]);
		$section->addText('Upper Floor, 61 Highgate High Street');
		$section->addText('London, N6 5JY');
		$section->addTextBreak(2);
		$section->addText('Signed: ', ['bold' => true]);
		$section->addText('Authorised to sign on behalf of building '.$job->bo->owner_referral, ['bold' => true]);
		$section->addText('Date: '.date('d/m/Y'));
		$section->addPageBreak();

		// page 3
		
		$title = $section->addTextRun();
		
		// $section->addTextBreak();
		$title->addimage('images/docs/top-1.jpg',array(
		    'width' => 120,
		    'positioning' => 'relative', 
		    'wrappingStyle' => 'behind',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		));
		$section->addTextBreak(1);
		$title = $section->addTextRun();
		$title->addText('The Party Wall etc. Act 1996 (Section 6) ',['bold' => true]);
		$section->addText('PARTY WALL NOTICE OF ADJACENT EXCAVATION',['bold' => true, 'size'=>18, 'name'=>'Poppins'] );
		$section->addimage('images/docs/greenbar.png', ['width' => 495, ]);
		//$section->addLine(['weight' => 2, 'width' => 600, 'height' => 0, 'color' => '#00B1A4']);
		

		$textrun = $section->addTextRun();
		$textrun->addText('To: ', ['bold' => true]);
		$textrun->addText($job->ao2->full_names.' of '.$job->ao2->contact_address);
		$section->addText('(adjoining '.$job->ao2->owners_referral.')', [], ['alignment' => 'right'] );
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText($job->bo->i_we_referral_upper_case.': ', ['bold' => true]);
		$textrun->addText($job->bo->full_name.' of '.$job->bo->contact_address);
		$section->addText('(building '.$job->bo->owner_referral.')', [], ['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('being '.$job->bo->owner_referral.' of '.$job->bo->property_address_proposed_work.' which adjoins your property '.$job->ao2->property_address_adjoining);
		$section->addTextBreak();
		$section->addText('Hereby serve you with Party Wall '.$job->ao2->notice_notices.' as required under Section '.$job->ao2->s6_section.' and as shown in drawings '.$job->ao2->drawings.' it is intended to carry out the works detailed below after the expiration of one month from the date of this Party Wall Notice or earlier by agreement: ');
		$section->addTextBreak();
		$section->addText($job->ao2->s6_description, ['bold'=>true]);
		$section->addTextBreak();

		$textrun = $section->addTextRun();
		$textrun->addText('*It is proposed  ');
		$textrun->addText('to underpin or otherwise strengthen the foundations of your building or structure to safeguard them. Or *It is not proposed to underpin or otherwise strengthen the foundations of your building or structure as it is not considered necessary. ');
		$section->addTextBreak(1);
		$textrun = $section->addTextRun();
		$textrun->addText('If you do not consent to the works within 14 days, you are deemed to have dissented and a dispute is deemed to have arisen. In this case, Section 10 of the Party Wall etc 1996 requires that both parties should ');
		$textrun->addText('concur in the appointment of an Agreed Party Wall Surveyor ', ['bold' => true]);
		$textrun->addText('or should each appoint a Party Wall Surveyor and in those circumstances '.$job->bo->i_we_referral_lower_case.' would appoint:');
		$section->addTextBreak();

		$section->addText($job->bo->surveyor_name, ['bold' => true]);
		$section->addText($job->bo->surveyor_qualifications);
		$section->addText('BERRY LODGE SURVEYORS ', ['bold' => true, 'color' => '00B1A4']);
		$section->addText('Head Office ', ['bold' => true]);
		$section->addText('Upper Floor, 61 Highgate High Street');
		$section->addText('London, N6 5JY');
		$section->addTextBreak(2);
		$section->addText('Signed: ', ['bold' => true]);
		$section->addText('Authorised to sign on behalf of building '.$job->bo->owner_referral, ['bold' => true]);
		$section->addText('Date: '.date('d/m/Y'));

		$section->addPageBreak();


		// page 4
		
		$title = $section->addTextRun();
		
		// $section->addTextBreak();
		$title->addimage('images/docs/top-1.jpg',array(
		    'width' => 120,
		    'positioning' => 'relative', 
		    'wrappingStyle' => 'behind',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		));
		$section->addTextBreak(1);
		$title = $section->addTextRun();
		$title->addText('The Party Wall etc. Act 1996',['bold' => true]);
		$section->addText('ACKNOWLEDGEMENT FORM TO PARTY WALL NOTICES',['bold' => true, 'size'=>18, 'name'=>'Poppins'] );

		$section->addText('Job Number: BLSN'.$job->id, ['bold' => false],['alignment' => 'right']);
		$section->addimage('images/docs/greenbar.png', ['width' => 495, ]);
		//$section->addLine(['weight' => 2, 'width' => 600, 'height' => 0, 'color' => '#00B1A4']);
	

		$textrun = $section->addTextRun();
		$textrun->addText('In accordance with the Party Wall etc Act 1996 and as confirmed in the enclosed cover letter, if you do not respond to the Party Wall '.$job->ao2->notice_notices.' within ',['size'=>9]);
		$textrun->addText('14 days', ['bold' => true, 'size'=>9]);
		$textrun->addText(', it shall be deemed that you have ',['size'=>9]);
		$textrun->addText('Dissented ', ['bold' => true, 'size'=>9]);
		$textrun->addText('and therefore under Section 10 of the Party Wall etc Act 1996 both parties shall agree in the appointment of one ', ['size'=>9]);
		$textrun->addText('Agreed Party Wall Surveyor, ', ['bold' => true, 'size'=>9]);
		$textrun->addText('or each party shall appoint their own Party Wall Surveyor.', ['size'=>9]);
		$section->addTextBreak(1);
		$section->addText('There are 3 response options to the Party Wall '.$job->ao2->notice_notices.' available to you, please tick the appropriate box: ', ['bold' => true, 'size'=>9]);
		$section->addTextBreak(1);
		$textrun = $section->addTextRun();
		$textrun->addText($job->ao2->i_we_referral.', ', ['bold' => true, 'size'=>9]);
		$textrun->addText($job->ao2->full_names.' '.$job->ao2->owners_referral.' of '.$job->ao2->property_address_adjoining.' and having received the Party Wall '.$job->ao2->notice_notices.' served by '.$job->bo->salutation.', '.$job->bo->owner_referral.' of '.$job->bo->property_address_proposed_work.' in respect of their proposed works confirm that '.$job->ao2->i_we_referral.' select: ', ['size'=>9]);
		$section->addTextBreak(1);
		
		$section->addText('Please ensure that all owners of '.$job->ao2->property_address_adjoining.' sign the selected option', ['size'=>7.5]);
		$section->addTextBreak(1);
		$section->addText('Option 1', ['bold' => true, 'size'=>9]); //......1........
		$textrun = $section->addTextRun();
		$textrun->addText('Hereby', ['size'=>9]);	
		$textrun->addText(' Consent ', ['bold' => true, 'size'=>9]);	
		$textrun->addText('to the proposed work and waive the statutory Party Wall '.$job->ao2->notice_notices.' period:', ['size'=>9]);	
	
		$section->addimage('images/docs/box.png',array(
		    'width' => 15,
		    'wrappingStyle' => 'square',
		    'positioning' => 'absolute',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		));
	
		$textrun = $section->addTextRun(['lineHeight'=>1.5]); //'space' => ['before' => 600, 'after' => 250]
		$textrun->addText('Print Name(s):…………………………………………………….', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 180, 'height' => 0, 'dash'=>'rounddot']);
		$textrun->addText('Contact Number:………………………………......', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 140, 'height' => 0, 'dash'=>'rounddot']);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		// $textrun->addText('                                                                          ');
		$textrun->addText('Email Address:…………………..………………………………………………………………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 225, 'height' => 0, 'dash'=>'rounddot']);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Signed:……………………………………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 250, 'height' => 0, 'dash'=>'rounddot']);
		$textrun->addText('Dated:………………………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 140, 'height' => 0, 'dash'=>'rounddot']);
		
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText('I would like a Schedule of Condition Report on my property undertaken:', ['size'=>9]);
		$textrun->addimage('images/docs/yesno.png',[
		    'height' => 15, 
		    'wrappingStyle' => 'square',
		    'positioning' => 'absolute',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		]);
		$section->addTextBreak();

		$section->addText('Option 2', ['bold'=>true, 'size'=>9]); //......2........
		$textrun = $section->addTextRun();
		$textrun->addText('Hereby ', ['size'=>9]);
		$textrun->addText('Dissent ', ['size'=>9]);
		$textrun->addText('to the Party Wall '.$job->ao2->notice_notices.' and appoint: ', ['size'=>9]);
		$section->addimage('images/docs/box.png',[
		    'width' => 15,
		    'wrappingStyle' => 'square',
		    'positioning' => 'absolute',
		    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
		    'posHorizontalRel' => 'margin',
		    'posVerticalRel' => 'line',
		]);

		$section->addText($job->bo->surveyor_name, ['bold' => true, 'size'=>10]);
		$section->addText($job->bo->surveyor_qualifications, ['size'=>10]);
		$section->addText('BERRY LODGE SURVEYORS ', ['bold' => true, 'color' => '00B1A4','size'=>10]);
		$section->addText('Head Office ', ['bold' => true,'size'=>10]);
		$section->addText('Upper Floor, 61 Highgate High Street',['size'=>10]);
		$section->addText('London, N6 5JY',['size'=>10]);
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		$textrun->addText('To act as the ', ['size'=>9]);
		$textrun->addText('Agreed Party Wall Surveyor ', ['bold' => true, 'size'=>9] );
		$textrun->addText(' in accordance with Section 10 of the Party Wall etc Act 1996.  ', ['size'=>9]);
		$section->addTextBreak();

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Print Name(s):..……………………………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 180, 'height' => 0, 'dash'=>'rounddot']);
		$textrun->addText('Contact Number:…………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 140, 'height' => 0, 'dash'=>'rounddot']);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Email Address:…………………………………………………………………………………………………………..', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 220, 'height' => 0, 'dash'=>'rounddot']);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Signed:……………………………………………………………', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 250, 'height' => 0, 'dash'=>'rounddot']);
		$textrun->addText('Dated:………………….…………………………...', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 140, 'height' => 0, 'dash'=>'rounddot']);
		$section->addTextBreak();
		$section->addText('Option 3', ['bold' => true, 'size'=>9]);

		$section->addText('Hereby Dissent to the Party Wall '.$job->ao2->notice_notices.' and appoint my own Party Wall Surveyor to act on '.$job->ao2->my_our_refferal.' behalf: ', ['size'=>9]);
		$section->addimage('images/docs/box.png',[
				    'width' => 15,
				    'wrappingStyle' => 'square',
				    'positioning' => 'absolute',
				    'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
				    'posHorizontalRel' => 'margin',
				    'posVerticalRel' => 'line',
				]);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Name:………………………………………………………………………………………………………………………..', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 400, 'height' => 0, 'dash'=>'rounddot']);

		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Address:.………………………………………………………………….………………….………………….…………..', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 540, 'height' => 0, 'dash'=>'rounddot']);
		//$section->addTextBreak();
		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('…………………………………………………………………………………………………………………………….....', ['size'=>9]);
		$textrun = $section->addTextRun(['lineHeight'=>1.5]);
		$textrun->addText('Contact Details: …………………………………………………………….……………………………….……………...', ['size'=>9]);
		// $textrun->addLine(['weight' => 1, 'width' => 450, 'height' => 0, 'dash'=>'rounddot']);
		
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");
		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
