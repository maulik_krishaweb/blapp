<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Task,User, User_History, Job_Tasks, Job, Bo,Ao_1,Ao_2,Ao_3,Ao_4,Ao_5,Ao_6,Ao_7,Ao_8,Ao_9,Ao_10};
use Illuminate\Support\Facades\{Auth, Mail};

class TaskController extends Controller
{
    public function index($jobno=null){
        $jobs=Auth::user()->jobs;

        foreach ($jobs as $job) {
            if ($job->tasks->count()>=1) {
                $job->percent= round(($job->tasks->where('status',1)->count()/$job->tasks->count() ) * 100) ;
            }
            
        }
        
        return view('admin.tasks', ['jobs'=>$jobs]);
    }

    public function client($job_no){

        $job= Auth::user()->jobs->where('id', $job_no)->first();
        // $tasks=\App\Job::where('id', $job_no)->first()->tasks->where('is_internal', 0);
        $tasks=\App\Job::where('id', $job_no)->first()->tasks;

        return view('client.tasks', ['tasks'=>  $tasks, 'job'=>$job ]);
    }



    public function addTask(Request $request){
	     $this->validate($request,  [
            'task' => 'required|max:255'
        ]);
	     $task=Task::create([
            'task' => $request->input('task'),
            'job_type' => $request->input('job_type'),
            'is_internal'=> $request->input('internal') ?? 0,
        ]);
    	
    	  User_History::create([
            'user_id' => Auth::user()->id,
            'action' => 'Task created',
            'description'=> 'New Task with id: '.$task->id.' - created. Task: '.$task->task.' - Internal: '.$task->is_internal.' - Type: '.$task->job_type,
        ]);

        return json_encode(['status'=>'good'], true);

    }

    public function renameTask(Request $request)
    {
      $this->validate($request,  [
        'task' => 'required|max:255'
      ]);

      $task = Task::find($request->id);
      $task_name = $this->space($task->task);
      $task->task = $request->task;
      $task->save();

      $job_tasks = job_tasks::where([['job_type', '=', $task->job_type],['task', '=', $task_name]])->get();

      foreach ($job_tasks as $job_task) 
      {
        $job_task = job_tasks::find($job_task->id);
        $job_task->task = $request->task;
        $job_task->task_id = $task->id;
        $job_task->save();
      }

      return 'true';
    }


    public function delTask(Request $request){ 
        $task =  Task::find($request->task_id);
        $task->delete();
        return "good";
    }


    public function checked(Request $request)
    {
      $jobtasks = Job_Tasks::find($request->taskid);
      $jobtasks->status= $request->status;
      $taskmsg = $request->taskmsg;
      $jobtasks->additional_msg = $taskmsg;
      $jobtasks->save();
      $job= Job::find($jobtasks->job_id);


      if($request->status == 1)
      {
        $user_history = new User_History;
        $user_history->user_id = Auth::user()->id;
        $user_history->action = "complete task";
        $user_history->description = (Auth::user()->name." has completed ".$jobtasks->task." task of Job ID ".$job->id);
        $user_history->save();
      }
      else
      {
        $user_history = new User_History;
        $user_history->user_id = Auth::user()->id;
        $user_history->action = "uncomplete task";
        $user_history->description = (Auth::user()->name." has not completed ".$jobtasks->task." task of Job ID ".$job->id);
        $user_history->save(); 
      }

      // if ( ($jobtasks->task=='Update building owner' or $jobtasks->task=='Update Owners') and $request->status=='1') {

      // $surveyor_name = $job->users->whereIn('role', [7,9] )->first()->name;
      // if( $bo_contact ){
      // $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
      // preg_match_all($pattern, $bo_contact, $matches);
      // $bo_email = (!empty($matches))? $matches[0][0] : "";

      // $bo_name = $bo->full_name;
      // if( $bo_email ){

      // Mail::send('emails.task_complete_bo', ['client_name' => $bo_name, 'taskmsg' => $taskmsg, 'surveyor_name'=>$surveyor_name ], 
      // function ($message)  {
      // $message->subject('Task Complete - Berry Lodge Surveyors');
      // $message->to($bo_email); 
      // }
      // );

      // }
      // }

      // /*foreach ($job->users as $user){

      // if ($user->notification ==1 && $user->role == 3 && $request->status=='1'){
      // $sento = $user->email; //env('TEST_MAIL');

      // Mail::send('emails.task_complete', ['client_name' => $user->name, 'taskmsg' => $taskmsg, 'surveyor_name'=>$job->users->whereIn('role', [7,9] )->first()->name ], 
      // function ($message) use($sento) {

      // $message->subject('Task Complete - Berry Lodge Surveyors');
      // $message->to($sento); 
      // }
      // );
      // } 
      // }*/
      // }

      $job->percent= round(($job->tasks->where('status',1)->count()/$job->tasks->count() ) * 100) ;

      return $job;
    }

    public function render(Request $request)
    {
      $request->name = $this->space($request->name);
      $jobtasks = Job_Tasks::find($request->task_id);

      $job= Job::find($jobtasks->job_id);
      $client_email = "";
      $job_users = $job->users;
      foreach ($job_users as $user) {
        if ($user->role == 3) {
          $client_name  = $user->name;
          $client_email  = $user->email;
          $client_id  = $user->id;
          $client_password = $user->secret_key;
        }
      }
      $surveying_service = "";
      $bo = \App\Bo::where('job_id', $jobtasks->job_id)->first();
      if ($request->get('type') == 1) {
        $surveyor_name = $bo->surveyor_name ?? ''; 
        $surveying_service = "Party wall";
        if ($jobtasks->task_id == 2 ) {
          $address = $bo->property_address_proposed_work ?? '';
          return view('emails.AOS.file_set_up',compact('address'));
        }
        if ($jobtasks->task_id == 1) {
          return view('emails.AOS.letter_of_appointment');
        }
        if ($jobtasks->task_id == 5 || $jobtasks->task_id == 4 || $jobtasks->task_id == 3) 
        {
          $owners = $bo->owners_owners ?? ''; 
          $surveyor_full_information = $bo->surveyor_full_information ?? '';
          return view('emails.AOS.valid_party_wall_notice',compact('surveyor_name','owners','surveyor_full_information'));
        }
        if ($jobtasks->task_id == 6 || $jobtasks->task_id == 7) 
        {
          return view('emails.AOS.update_adjoining_owner',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 8) 
        {
          $owner = $bo->owner_referral ?? '';
          return view('emails.AOS.schedule_of_condition_booked',compact('surveyor_name','owner'));
        }
        if ($jobtasks->task_id == 9) 
        {
          $owner = $bo->owner_referral ?? '';
          return view('emails.AOS.schedule_of_condition_inspection_booked',compact('surveyor_name','owner'));
        }
        if ($jobtasks->task_id == 10 || $jobtasks->task_id == 11 || $jobtasks->task_id == 12) 
        {
          return view('emails.AOS.update_adjoining_owner1',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 13 || $jobtasks->task_id == 14 || $jobtasks->task_id == 15) 
        {
          return view('emails.AOS.update_adjoining_owner2',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 16) 
        {
          return view('emails.AOS.award_agreed',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 17 )
        {
          return view('emails.AOS.award_sent_to_the_adjoining_owner',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 18) 
        {
          return view('emails.AOS.invoice_sent_to_the_building_owner',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 65) 
        {
          return view('emails.login_details_email',compact('surveyor_name','client_name','client_email','client_password','surveying_service'));
        }
        if ($jobtasks->task_id == 69) 
        {
          return view('emails.thank_you_email',compact('surveyor_name','client_name','client_email','client_password'));
        }
      }
      elseif($request->get('type') == 2)
      {
        $surveyor_name = $bo->surveyor_name ?? '';
        $salutation = $bo->salutation ?? '';
        $property_address_proposed_work = $bo->property_address_proposed_work ?? '';
        $property_address_proposed_work = $bo->property_address_proposed_work ?? '';
        $email = $this->bosEmails($request->task_id);
        $surveying_service = "Party wall";
        if ($jobtasks->task_id == 19) {
          return view('emails.bos.file_set_up',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 20 || $jobtasks->task_id == 21 || $jobtasks->task_id == 22) {
          return view('emails.bos.letter_of_appointment',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 23) {
          $ten_4_party_wall_notice_date = $job->ten_4_party_wall_notice_date ?? '';
          return view('emails.bos.party_wall_notice_served',compact('surveyor_name','property_address_proposed_work','salutation','ten_4_party_wall_notice_date'));
        }
        elseif ($jobtasks->task_id == 24  || $jobtasks->task_id == 25  || $jobtasks->task_id == 26) {
          $ten_4_party_wall_notice_expiry_date  = $job->ten_4_party_wall_notice_expiry_date ?? '';
          return view('emails.bos.section_ten_4_served',compact('surveyor_name','ten_4_party_wall_notice_expiry_date','salutation'));
        }
        elseif ($jobtasks->task_id == 27 || $jobtasks->task_id == 28  || $jobtasks->task_id == 29 ) {
          return view('emails.bos.notice_response_received',compact('surveyor_name','salutation'));
        }
        elseif ($jobtasks->task_id == 30 || $jobtasks->task_id == 31) {
          return view('emails.bos.third_surveyor_selected',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 32 || $jobtasks->task_id == 33) {
          return view('emails.bos.schedule_of_condition_booked',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 34 || $jobtasks->task_id == 35 || $jobtasks->task_id == 36) {
          return view('emails.bos.update_building_owner',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 37 ) {
          return view('emails.bos.draft_award_sent_to_adjoining_owners_surveyor_for_comment',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 38 ) {
          return view('emails.bos.award_agrred',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 39 ) {
          return view('emails.bos.update_building_owner_second',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        elseif ($jobtasks->task_id == 40) {
          return view('emails.bos.invoice_sent_to_building_owner_for_settlement',compact('surveyor_name','property_address_proposed_work','salutation'));
        }
        if ($jobtasks->task_id == 66)
        {
          return view('emails.login_details_email',compact('surveyor_name','client_name','client_email','client_password','surveying_service'));
        }
        if ($jobtasks->task_id == 70) 
        {
          return view('emails.thank_you_email',compact('surveyor_name','client_name','client_email'));
        }
      }
      elseif ($request->type == 3) {
        $surveyor_name = $bo->surveyor_name ?? '';
        $salutation = $bo->salutation ?? '';
        $property_address = $bo->property_address_proposed_work ?? '';
        $email = $this->bosEmails($request->task_id);
        $surveying_service = "Party wall";
        if ($jobtasks->task_id == 41 ) 
        {
          return view('emails.AS.file_set_up',compact('salutation','property_address'));
        }
        if ($jobtasks->task_id == 42 || $jobtasks->task_id == 43 || $jobtasks->task_id == 44 || $jobtasks->task_id == 45)
        {
          $ten_4_party_wall_notice_date = $job->ten_4_party_wall_notice_date ?? '';
          return view('emails.AS.party_wall_notice_served',compact('salutation','property_address','surveyor_name','ten_4_party_wall_notice_date'));
        }
        if ($jobtasks->task_id == 46 || $jobtasks->task_id == 47 || $jobtasks->task_id == 48) 
        {
          $ten_4_party_wall_notice_expiry_date = $job->ten_4_party_wall_notice_expiry_date ?? '';
          return view('emails.AS.section_10(4)_served',compact('salutation','ten_4_party_wall_notice_expiry_date'));
        }
        if ( $jobtasks->task_id == 49 ) 
        {
          return view('emails.AS.update_building_owner',compact('salutation','surveyor_name'));
        }
        if ($jobtasks->task_id == 50 || $jobtasks->task_id == 51 ) 
        {
          return view('emails.AS.notice_response_received',compact('salutation','surveyor_name')); 
        }
        if ($jobtasks->task_id == 52 ) 
        {
          return view('emails.AS.schedule_of_condition_booked',compact('salutation'));  
        }
        if ($jobtasks->task_id == 53 || $jobtasks->task_id == 54) 
        {
          return view('emails.AS.update_owners');   
        }
        if ($jobtasks->task_id == 55 ) 
        {
          return view('emails.AS.owners_on_onward_parUpdatety_wall_procedures',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 56 ) {
          return view('emails.AS.requested_final_structural_pack',compact('salutation','surveyor_name'));
        }
        if ($jobtasks->task_id == 57 ) 
        {
          return view('emails.AS.award_agreed',compact('surveyor_name'));
        }
        if ($jobtasks->task_id == 58) 
        {
          return view('emails.AS.update_owners2',compact('surveyor_name'));
        }

        if ($jobtasks->task_id == 59) 
        {
          return view('emails.AS.invoice_sent_to_the_building_owner',compact('salutation'));
        }
        if ($jobtasks->task_id == 67) 
        {
          return view('emails.login_details_email',compact('surveyor_name','client_name','client_email','client_password','surveying_service'));
        }
        if ($jobtasks->task_id == 71) 
        {
          return view('emails.thank_you_email',compact('surveyor_name','client_name','client_email'));
        }
      }
      elseif ($request->type == 4) {
        $surveying_service = $job->surveying->surveying_service ?? '';
        $surveyor_name = $job->surveying->surveryor_name ?? ''; 
        $client_name = $job->surveying->client_full_name ?? '';
        $date_of_inspection = $job->surveying->date_of_inspection ?? '';
        if ($jobtasks->task_id == 60 ) 
        {
          return view('emails.surveying_tasks.letter_of_authorisation_signed_and_received',compact('client_name','surveyor_name','surveying_service','date_of_inspection'));
        }
        elseif ($jobtasks->task_id == 61 ) 
        {
          return view('emails.surveying_tasks.inspection_date_booked',compact('client_name','surveyor_name','surveying_service','date_of_inspection'));
        }
        elseif ($jobtasks->task_id == 62 ) 
        {
          return view('emails.surveying_tasks.desktop_reserach_undertaken',compact('client_name','surveyor_name','surveying_service','date_of_inspection'));
        }
        elseif ($jobtasks->task_id == 63 ) 
        {
          return view('emails.surveying_tasks.inspection_undertaken',compact('client_name','surveyor_name','surveying_service','date_of_inspection'));
        }
        elseif ($jobtasks->task_id == 64 ) 
        {
          return view('emails.surveying_tasks.report_send_out',compact('client_name','surveyor_name','surveying_service','date_of_inspection'));
        }
        if ($jobtasks->task_id == 68)
        {
          return view('emails.login_details_email',compact('surveyor_name','client_name','client_email','client_password','surveying_service'));
        }
        if ($jobtasks->task_id == 72)
        {
          return view('emails.thank_you_email',compact('surveyor_name','client_name','client_email'));
        }
      }

    }

    public function sendMail(Request $request)
    {
      $request->name = $this->space($request->name);
      $jobtasks = Job_Tasks::find($request->task_id);
      $job= Job::find($jobtasks->job_id);

      $job_users = $job->users;
      $client_email = '';
      $client_password = '';
      foreach ($job_users as $user) {
        if ($user->role == 3) {
          $client_name  = $user->name;
          $client_email  = $user->email;
          $client_id = $user->id;
          $client_password = $user->secret_key;
        }
      }
      $surveying_service = "";
      if($request->type == 1)
      {
        $surveying_service = "Party wall";
        $emails = $this->emails($request->task_id);
        $subject = 'Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!';
        $ao = \App\Ao_1::where('job_id', $jobtasks->job_id)->first();
        $bo = \App\Bo::where('job_id', $jobtasks->job_id)->first();
        if ($jobtasks->task_id == 2) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.file_set_up', ['address' => $bo->property_address_proposed_work], function ($message) use($email,$subject) {
                $message->subject($subject);
                $message->to($email);  
              });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 1) {
         if($emails != []){
          foreach ($emails as $email) {
            Mail::send('emails.AOS.letter_of_appointment', ['address' => $bo->property_address_proposed_work], 
              function ($message) use($email,$subject) {
                $message->subject($subject);
                $message->to($email);  
              });
          }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 5 || $jobtasks->task_id == 4 || $jobtasks->task_id == 3) {
         if($emails != []){
          foreach ($emails as $email) {
            Mail::send('emails.AOS.valid_party_wall_notice', ['surveyor_name' => $bo->surveyor_name ?? '', 'owners' => $bo->owners_owners ?? '', 'surveyor_full_information' => $bo->surveyor_full_information ?? ''], 
              function ($message) use($email,$subject) {
                $message->subject($subject);
                $message->to($email);  
              });
          }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 6 || $jobtasks->task_id == 7) {
         if($emails != []){
          foreach ($emails as $email) {
            Mail::send('emails.AOS.update_adjoining_owner', ['surveyor_name' => $bo->surveyor_name ?? ''], 
              function ($message) use($email,$subject) {
                $message->subject($subject);
                $message->to($email);  
              });
          }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 8) {
          if($emails != []){
            foreach ($emails as $email) {
            Mail::send('emails.AOS.schedule_of_condition_booked', ['surveyor_name' => $bo->surveyor_name ?? '', 'owner' => $bo->owner_referral], 
              function ($message) use($email,$subject) {
                $message->subject($subject);
                $message->to($email);  
              });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 9 ) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.schedule_of_condition_inspection_booked', ['surveyor_name' => $bo->surveyor_name ?? '', 'owner' => $bo->owner_referral], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 10 || $jobtasks->task_id == 11 || $jobtasks->task_id == 12) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.update_adjoining_owner1', ['surveyor_name' => $bo->surveyor_name ?? ''], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 13 || $jobtasks->task_id == 14 || $jobtasks->task_id == 15) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.update_adjoining_owner2', ['surveyor_name' => $bo->surveyor_name ?? ''], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 16) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.award_agreed', ['surveyor_name' => $bo->surveyor_name ?? ''], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 17) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.award_sent_to_the_adjoining_owner', ['surveyor_name' => $bo->surveyor_name ?? ''], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 18) {
          if($emails != []){
            foreach ($emails as $email) {
              Mail::send('emails.AOS.invoice_sent_to_the_building_owner', ['surveyor_name' => $bo->surveyor_name ?? ''], 
                function ($message) use($email,$subject) {
                  $message->subject($subject);
                  $message->to($email);  
                });
            }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }

        if ($jobtasks->task_id == 69) {
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.thank_you_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 65) {
          // $client_password = $this->rand_string(8,$client_id);
          // $client_password = $user->secret_key;
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.login_details_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name,'client_email'=>$client_email,'client_password'=>$client_password,'surveying_service'=>$surveying_service], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
      } 
      elseif ($request->type  == 2)
      {
        $email = $this->bosEmails($request->task_id);
        $subject = 'Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!';
        $bo = \App\Bo::where('job_id', $jobtasks->job_id)->first();
        $bo_name = $bo->full_name ?? '';
        $surveyor_name = $bo->surveyor_name ?? '';
        $salutation = $bo->salutation ?? '';
        $property_address_proposed_work = $bo->property_address_proposed_work ?? '';
        $surveying_service = "Party wall";
        if ($jobtasks->task_id == 19)
        {
          if($email != ""){
                  // foreach ($emails as $email) {
            Mail::send('emails.bos.file_set_up', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work, 'salutation' => $salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                  // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 20 || $jobtasks->task_id == 21 || $jobtasks->task_id == 22)
        {
          if($email != "")
          {
            // foreach ($emails as $email) {
              Mail::send('emails.bos.letter_of_appointment', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=> $property_address_proposed_work, 'salutation' => $salutation], 
                function ($message) use ($email,$subject)  {
                  $message->subject($subject);
                  $message->to($email); 
                }); 
                    // }
              $jobtasks->mail = 1;
              $jobtasks->save();
              return 'true';
          }
          return 'false';
        }
        // }
        elseif ($jobtasks->task_id == 23 )
        {

          $ten_4_party_wall_notice_date = $job->ten_4_party_wall_notice_date;
          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.party_wall_notice_served', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation,'ten_4_party_wall_notice_date'=>$ten_4_party_wall_notice_date],
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
            //not automatic
        elseif ($jobtasks->task_id == 24 || $jobtasks->task_id == 25 || $jobtasks->task_id == 26)
        {
          $ten_4_party_wall_notice_expiry_date = $job->ten_4_party_wall_notice_expiry_date ?? '';
          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.section_ten_4_served', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name,'salutation'=>$salutation,'ten_4_party_wall_notice_expiry_date'=>$ten_4_party_wall_notice_expiry_date], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
            //not automatic
        elseif ($jobtasks->task_id == 27 || $jobtasks->task_id == 28 || $jobtasks->task_id == 29)
        {
          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.notice_response_received', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'salutation'=>$salutation],
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
            //not automatic
        elseif ($jobtasks->task_id == 30 || $jobtasks->task_id == 31)
        {

          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.third_surveyor_selected', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';  
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 32 || $jobtasks->task_id == 33 )
        {

          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.schedule_of_condition_booked', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 34 || $jobtasks->task_id == 35 || $jobtasks->task_id == 36)
        {

          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.update_building_owner', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 37 )
        {

          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.draft_award_sent_to_adjoining_owners_surveyor_for_comment', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 38 )
        {

          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.award_agrred', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 39)
        {
          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.update_building_owner_second', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 40)
        {
          if($email != ""){
                // foreach ($emails as $email) {
            Mail::send('emails.bos.invoice_sent_to_building_owner_for_settlement', ['client_name' => $bo_name,  'surveyor_name'=>$surveyor_name ,'property_address_proposed_work'=>$property_address_proposed_work,'salutation'=>$salutation], 
              function ($message) use ($email,$subject)  {
                $message->subject($subject);
                $message->to($email); 
              }); 
                // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 66) {
          // $client_password = $this->rand_string(8,$client_id);
          // $client_password = $user->secret_key;
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.login_details_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name,'client_email'=>$client_email,'client_password'=>$client_password,'surveying_service'=>$surveying_service], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        elseif ($jobtasks->task_id == 70)
        {
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.thank_you_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }

      }    
      elseif ($request->type == 3) 
      {
        $email = $this->bosEmails($request->task_id);
        $subject = 'Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!';
        $bo = \App\Bo::where('job_id', $jobtasks->job_id)->first();
        $surveyor_name = $bo->surveyor_name ?? '';
        $surveying_service = "Party wall";
        if ($jobtasks->task_id == 41) 
        {
          if($email != ""){
                  // foreach ($emails as $email) {
            Mail::send('emails.AS.file_set_up', ['salutation' => $bo->salutation ?? '', 'property_address' => $bo->property_address_proposed_work ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                  // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 42 || $jobtasks->task_id == 43 || $jobtasks->task_id == 44 || $jobtasks->task_id == 45) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {

            $ten_4_party_wall_notice_date = $job->ten_4_party_wall_notice_date;
            Mail::send('emails.AS.party_wall_notice_served', ['salutation' => $bo->salutation ?? '', 'property_address' => $bo->property_address_proposed_work ?? '', 'surveyor_name' => $surveyor_name, 'ten_4_party_wall_notice_date' => $ten_4_party_wall_notice_date], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 46 || $jobtasks->task_id == 47 || $jobtasks->task_id == 48) 
        {
          if($email != ""){
                      // foreach ($emails as $email) { 
            $ten_4_party_wall_notice_expiry_date = $job->ten_4_party_wall_notice_expiry_date;
            Mail::send('emails.AS.section_10(4)_served', ['salutation' => $bo->salutation ?? '','ten_4_party_wall_notice_expiry_date' => $ten_4_party_wall_notice_expiry_date], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ( $jobtasks->task_id == 49 ) 
        {
          if($email != ""){
                      // foreach ($emails as $email) { 
            Mail::send('emails.AS.update_building_owner', ['salutation' => $bo->salutation ?? '','surveyor_name' => $surveyor_name], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 50 || $jobtasks->task_id == 51) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.notice_response_received', ['salutation' => $bo->salutation ?? '','surveyor_name' => $surveyor_name], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 52) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.schedule_of_condition_booked', ['salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 53 || $jobtasks->task_id == 54) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.update_owners', ['salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 55) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.owners_on_onward_parUpdatety_wall_procedures', ['salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 56) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.requested_final_structural_pack', ['surveyor_name' => $surveyor_name,'salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 57) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.award_agreed', ['surveyor_name' => $surveyor_name,'salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 58) 
        {
         if($email != ""){
                    // foreach ($emails as $email) {

          Mail::send('emails.AS.update_owners2', ['surveyor_name' => $surveyor_name], function ($message) use($email,$subject) {
            $message->subject($subject);
            $message->to($email);  
          });
                    // }
          $jobtasks->mail = 1;
          $jobtasks->save();
          return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 59) 
        {
          if($email != ""){
                      // foreach ($emails as $email) {
            Mail::send('emails.AS.invoice_sent_to_the_building_owner', ['salutation' => $bo->salutation ?? ''], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                      // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 67) {
          // $client_password = $this->rand_string(8,$client_id);
          // $client_password = $user->secret_key;
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.login_details_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name,'client_email'=>$client_email,'client_password'=>$client_password,'surveying_service'=>$surveying_service], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 71) 
        {
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.thank_you_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
      }
      elseif ($request->type == 4) {
        $email = $this->surveyingEmail($request->task_id);
        $subject = 'Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!';
        $surveying_service = $job->surveying->surveying_service;
        $surveyor_name = $job->surveying->surveryor_name; 
        $client_name = $job->surveying->client_full_name; 
        $date_of_inspection = $job->surveying->date_of_inspection; 
        if ($jobtasks->task_id== 60 ) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {
            Mail::send('emails.surveying_tasks.letter_of_authorisation_signed_and_received', ['surveying_service' => $surveying_service, 'surveyor_name' => $surveyor_name,'client_name'=>$client_name,'date_of_inspection'=>$date_of_inspection], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id== 61) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {
            Mail::send('emails.surveying_tasks.inspection_date_booked', ['surveying_service' => $surveying_service, 'surveyor_name' => $surveyor_name,'client_name'=>$client_name,'date_of_inspection'=>$date_of_inspection], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id== 62 ) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {
            Mail::send('emails.surveying_tasks.desktop_reserach_undertaken', ['surveying_service' => $surveying_service, 'surveyor_name' => $surveyor_name,'client_name'=>$client_name,'date_of_inspection'=>$date_of_inspection], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id== 63) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {
            Mail::send('emails.surveying_tasks.inspection_undertaken', ['surveying_service' => $surveying_service, 'surveyor_name' => $surveyor_name,'client_name'=>$client_name,'date_of_inspection'=>$date_of_inspection], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id== 64 ) 
        {
          if($email != ""){
                    // foreach ($emails as $email) {
            Mail::send('emails.surveying_tasks.report_send_out', ['surveying_service' => $surveying_service, 'surveyor_name' => $surveyor_name,'client_name'=>$client_name,'date_of_inspection'=>$date_of_inspection], function ($message) use($email,$subject) {
              $message->subject($subject);
              $message->to($email);  
            });
                    // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id == 68) {
          // $client_password = $this->rand_string(8,$client_id);
          // $client_password = $user->secret_key;
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.login_details_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name,'client_email'=>$client_email,'client_password'=>$client_password,'surveying_service'=>$surveying_service], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        }
        if ($jobtasks->task_id== 72 ) 
        {
          if($client_email != ''){
          //   foreach ($emails as $email) {
              Mail::send('emails.thank_you_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name], 
                function ($message) use($client_email,$subject) {
                  $message->subject($subject);
                  $message->to($client_email);  
                });
            // }
            $jobtasks->mail = 1;
            $jobtasks->save();
            return 'true';
          }
          return 'false';
        } 
      }
    }

    public function rand_string( $length, $client_id ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $user = User::find($client_id);
        $random = substr(str_shuffle($chars),0,$length);
        $user->password = bcrypt($random);
        $user->save();
        return $random;

    }
    public function emails($id)
    {
      $jobtasks = Job_Tasks::find($id);

      $emails = [];
      $final_emails = [];
      for ($i=0; $i <=10 ; $i++) { 
          if ($i >0) 
          {
              $a = 'ao'.$i;
              if ($jobtasks->job->$a != null) {
                  if($jobtasks->job->$a->contact_details != null)
                  {
                      array_push($emails, ['email' => $jobtasks->job->$a->contact_details] );
                  }

              }
              
          }
          else
          {
              if ($jobtasks->job->ao != null) {
                  if ($jobtasks->job->ao->contact_details != null) 
                  {
                    array_push($emails, ['email' => $jobtasks->job->ao->contact_details]);
                  }
              }
          }
      }
      // if ($jobtasks->job->bo != null) 
      // {
      //    if ($jobtasks->job->bo->contact_details_telephone_numbers_email != null) {
      //         array_push($emails,['email' => $jobtasks->job->bo->contact_details_telephone_numbers_email]);
      //    }
      // }
      // if ($jobtasks->job->surveying != null) {
      //   if ($jobtasks->job->surveying->client_contact_details != null) {
      //         array_push($emails,['email' => $jobtasks->job->surveying->client_contact_details]);
      //    } 
      // }
      foreach ($emails as $email) {
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $email['email'], $matches);
        array_push($final_emails, (!empty($matches))? $matches[0][0] : "");
      }
      return $final_emails;
    }
    //bos as mail format
    public function bosEmails($id)
    {
      $jobtasks = Job_Tasks::find($id);
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $jobtasks->job->bo->contact_details_telephone_numbers_email, $matches);        
        $final_email = $matches[0][0] ?? '';
      return $final_email;
    }
    //surveying format
    public function surveyingEmail($id)
    {
      $jobtasks = Job_Tasks::find($id);
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $jobtasks->job->surveying->client_contact_details, $matches);        
        $final_email = $matches[0][0] ?? '';
      return $final_email;
    }
    public function space($string)
    {
      $name = $string;
      $string = htmlentities($name, null, 'utf-8');
      $p = strrpos($string, '&nbsp');
      if ($p !== false) {        // space was found
        $s = substr($name, 0, $p);  // cut off
        $s1 = substr($name, $p+2);
        if ($s1 != "") {
          return $s.' '.$s1;
        }
        return $s;
      }
      return $name;
      //$content = str_replace("&nbsp;", "", $string);
      //$content = html_entity_decode($content);
    }
    
}