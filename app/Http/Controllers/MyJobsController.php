<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use App\{Job, Ao_1, Ao_2, Ao_3, Ao_4, Ao_5, Ao_6, Ao_7, Ao_8, Ao_9, Ao_10, Bo, Job_Tasks, Task, Payment, Surveying, User_History,User,JobDocs};
use Illuminate\Support\Facades\Storage;
use PDF;
use Mail;
class MyJobsController extends Controller
{

  // load my jobs main page
  public function index()
  {
    $user =Auth::user();

    if ($user->role==7) {
      $jobs = \App\User::find($user->id)->jobs()->where('status', '<>', 8)->latest()->paginate(2147483648);
    }
    else
    {
      $jobs=Job::where('status', '<>', 8)->latest()->paginate(400); //2147483648
    }

    $tasks = Task::all()->groupBy('job_type');
  	 return view('admin.my_jobs',['jobs'=>$jobs, 'tasks' => $tasks]);
  }
  //get standard document
  public function getStandardFolder(Request $request)
  {
    $job_docs = JobDocs::where([['job_id','=',$request->job_id],['upload','=',0]])->get();

    return response($job_docs,202);
  }
  //get uploaded document
  public function getUploadedFolder(Request $request)
  {
    $job_docs = JobDocs::where([['job_id','=',$request->job_id],['type','=',"pdf"],['upload','=',1]])->get();

    return response($job_docs,202);
  }
  
  // load my jobs archived
  public function archived()
  {
    $user =Auth::user();
    if ($user->role==7) {
      $jobs = \App\User::find($user->id)->jobs()->where('status',   8)->latest()->paginate(2147483648);
    }
    else
    {
      $jobs=Job::where('status', 8)->latest()->paginate(2147483648);
    }

    $tasks = Task::all()->groupBy('job_type');
    return view('admin.my_jobs',['jobs'=>$jobs, 'tasks' => $tasks]);
  }

  // archive job
  public function archive(Request $req)
  {
    $job=Job::find($req->job_id);
    $job->status=$req->status;
    $job->save();
    if($req->status == 8 ){
      $job_docs = JobDocs::where([['job_id','=',$job->id],['upload','!=',1]])->get();
      foreach ($job_docs as $job_doc) {
          $job_doc = JobDocs::find($job_doc->id);
          $job_doc->upload = 2;
          $job_doc->save();
      }
    }
    else
    {
      $job_docs = JobDocs::where([['job_id','=',$job->id],['upload','!=',1]])->get();
      foreach ($job_docs as $job_doc) {
          $job_doc = JobDocs::find($job_doc->id);
          $job_doc->upload = 0;
          $job_doc->save();
      }
    }
    return 'archived';
  }

  // add / edit  my jobs page
  public function add($type)
  {
    $surveyors = User::whereIN('role',['9','7'])->get();
    $surveyor= \App\User::whereNotNull('qualifications')->get();
    if ($type=='survey') {
      return view('admin.my_jobs_add_jobs_survey',['addOrUpdate'=>'add', 'surveyor'=>$surveyor, 'surveyors_email'=>$surveyors]);
    }
  	 if ($type=='partywall') {
      return view('admin.my_jobs_add_jobs',['addOrUpdate'=>'add', 'surveyor'=>$surveyor ,'surveyors_email'=>$surveyors ]);
    }
  }

  // delete job
  public function deleteJob(Request $request)
  {

    $job = Job::destroy($request->job_id);
    $ao1 = Ao_1::where('job_id', $request->job_id )->delete();
    $ao2=Ao_2::where('job_id', $request->job_id )->delete();
    $ao3=Ao_3::where('job_id', $request->job_id )->delete();
    $ao4=Ao_4::where('job_id', $request->job_id )->delete();
    $ao5=Ao_5::where('job_id', $request->job_id )->delete();
    $ao6=Ao_6::where('job_id', $request->job_id )->delete();
    $ao7=Ao_7::where('job_id', $request->job_id )->delete();
    $ao8=Ao_8::where('job_id', $request->job_id )->delete();
    $ao9=Ao_9::where('job_id', $request->job_id )->delete();
    $ao10=Ao_10::where('job_id', $request->job_id )->delete();
    $bo= Bo::where('job_id', $request->job_id )->delete();
    $task=Job_Tasks::where('job_id', $request->job_id )->delete();
    $payment=Payment::where('job_id', $request->job_id )->delete();
    $surveying= Surveying::where('job_id', $request->job_id )->delete();

    User_History::create([
      'user_id' => Auth::user()->id,
      'action' => 'Deleted Job',
      'description'=> 'Job deleted id:: '.$request->job_id.'  - by User email: '.Auth::user()->email ]);
    return json_encode(['status'=>'good'], true);
  }

  // loads jobs payments to model
  public function payment(Request $request)
  {
    return Job::find($request->job_id)->payments;
  }

  //  add payment to my jobs
  public function addPayment(Request $request)
  {
    $payment = new Payment;
    $payment->job_id = $request->job_id;
    $payment->amount = $request->amount;
    $payment->description = $request->description;
    $payment->save();
    return "added";
  }

  // delete my jobs payments
  public function deletePayment(Request $request)
  {
    $payment = Payment::find($request->payment_id);
    $payment->delete();
    return "deleted";
  }

  // change payments status to settled
  public function settlePayment(Request $request)
  {
    $payment = Payment::find($request->payment_id);
    $payment->paid = 1;
    $payment->save();
    // archive job after payment is settled
    $job = \App\Job::find( $payment->job_id);
    $job->status= 8;
    $job->save();
    return "updated";
  }

  public function clientPayment($job_no=null)
  {
    $user= Auth::user();
    $job_no ? $job = $user->jobs->where('id', $job_no)->first() : $job = $user->jobs->last();
    if(is_null($job)){
      return view('client.waiting');
    }
    else {
      return view('client.payment', ['job' => $job, 'payments'=>$job->payments]);
    }
  }

  public function paymentProcess(Request $request)
  {
    $payment= Payment::find($request->payment_id);
    $amount = str_replace('.','',$payment->amount);
    $token = $request->stripeToken;
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    \Stripe\Stripe::setApiKey("sk_test_cTtRAp2zFSYTdz5hLS0svSq0");
    $charge = \Stripe\Charge::create([
        'amount' => $amount,
        'currency' => 'gbp',
        'description' => 'BLsurveyors Charge',
        'source' => $token,
    ]);

    if ($charge->status=="succeeded") {
      $payment->paid=1;
      $payment->method="paid via card payment";
      $message="Your payment was collected successfully. Thank you.";
    }
    else{
      $payment->error=$charge->status;
      $message=" There was a problem with your payment, please try again.";
    }
    $payment->save();
    return redirect('/client/payment')->with('status',  $message);
  }


  //  load my jobs tasks page for each job
  public function task($job_id)
  {
    $tasks = Task::all()->groupBy('job_type');
    $job_tasks = Job_Tasks::where('job_id', $job_id)->get();
    $jobs = Job::find($job_id);
    return view('admin.my_jobs_tasks', ['tasks' => $tasks, 'job_tasks'=>$job_tasks, 'job_id'=>$job_id,'job'=>$jobs]);
  }

  // add task or group of tasks to job
  public function addTask(Request $request)
  {
    if($request->type== 'all'){
      $tasks = Task::where('job_type', $request->key)->get();
    }
    elseif($request->type== 'sub')
    {
      $tasks = Task::where('id', $request->key)->get();
    }
    foreach ($tasks as $task) {
      Job_Tasks::create([
        'job_type' => $task->job_type,
        'task' =>$task->task,
        'is_internal' =>$task->is_internal,
        'status' =>0,
        'task_id' => $task->id,
        'job_id' =>$request->job_id,
        'order_no'=>$task->order_no,
      ]);
    }
    return json_encode(['status'=>'good'], true);
  }

  // delete task from job
  public function deleteTask(Request $request)
  {
    $job_task =Job_Tasks::find($request->task_id);
    $job_task->delete();
    return $request->all();
  }

  public function users($job_id){
    $users =   \App\User::all();
    $job_users = \App\Job::find($job_id);
    return view('admin.my_jobs_users', ['users' => $users, 'job_users'=>$job_users, 'job_id'=>$job_id]);
  }

  public function addUser($job_id,$user_id)
  {
    if (\App\Job_User::where('job_id',$job_id)->where('user_id', $user_id)->count() >=1) {
      return redirect('/admin/my-jobs/users/'.$job_id)->with('message', 'User all ready assigned to job!');
    }
    $job_user = new \App\Job_User;
    $job_user->job_id=$job_id;
    $job_user->user_id=$user_id;
    $job_user->save();
    $user = User::find($user_id);
    if ($user->role == 5) {
      $bo = \App\Bo::where('job_id', $job_id)->first();
      $job = Job::find($job_id);
      $surveying_service = $job->surveying->surveying_service ?? "Party wall";
      $client_email = $user->email;
      $client_name  = $user->name;
      $client_password = $user->secret_key;  
      $subject = 'Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!';
      Mail::send('emails.login_details_email', ['surveyor_name' => $bo->surveyor_name ?? '','client_name'=>$client_name,'client_email'=>$client_email,'client_password'=>$client_password,'surveying_service'=>$surveying_service], 
        function ($message) use($client_email,$subject) {
          $message->subject($subject);
          $message->to($client_email);  
        });
    }

    return redirect('/admin/my-jobs/users/'.$job_id)->with('message', 'User added successfully');
  }

  public function deleteUser($job_id,$user_id)
  {
    $job_user = \App\Job_User::where('job_id',$job_id)->where('user_id', $user_id)->first();
    $job_user->delete();
    return redirect('/admin/my-jobs/users/'.$job_id)->with('message', 'User deleted successfully');
  }

  // create and update jobs
  public function create($jobid=null, Request $request)
  {
   
    // update
    if($jobid) {
      $job =  \App\Job::find($jobid);
      $message= 'Updated successfully!';
    }
    else {
      $job = new  \App\Job;
      $job->status = 1;
      $message= 'Add a client to the job ';

    }
    if ($request->enq_id) {
      $job->enq_id = $request->enq_id;
    }

	  $job->invoice_no = $request->invoice_no;
    $job->job_type = $request->job_type;
	  $job->notice_costs =$request->notice_costs;
	  $job->award_costs =$request->award_costs;
	  $job->land_registry_costs =$request->land_registry_costs;
	  $job->printing_postage_costs =$request->printing_postage_costs;
	  $job->vat_amount =$request->vat_amount;
	  $job->final_amount =$request->final_amount;
    $job->survey_email =$request->survey_email;
	  $job->surveyor_who_had_first_contact_with_owner =$request->surveyor_who_had_first_contact_with_owner;
	  $job->surveyor_dealing_with_day_to_day =$request->surveyor_dealing_with_day_to_day;
	  $job->party_wall_notice_date =$request->party_wall_notice_date;
	  $job->ten_4_party_wall_notice_date =$request->ten_4_party_wall_notice_date;
	  $job->ten_4_party_wall_notice_expiry_date =$request->ten_4_party_wall_notice_expiry_date;
	  $job->schedule_of_condition_date =$request->schedule_of_condition_date;
    $job->save();

    if($jobid){
      $bo=  Bo::where('job_id',$jobid)->first();
      $ao=  Ao_1::where('job_id',$jobid)->first();
      $ao2=  Ao_2::where('job_id',$jobid)->first();
      $ao3=  Ao_3::where('job_id',$jobid)->first();
      $ao4=  Ao_4::where('job_id',$jobid)->first();
      $ao5=  Ao_5::where('job_id',$jobid)->first();
      $ao6=  Ao_6::where('job_id',$jobid)->first();
      $ao7=  Ao_7::where('job_id',$jobid)->first();
      $ao8=  Ao_8::where('job_id',$jobid)->first();
      $ao9=  Ao_9::where('job_id',$jobid)->first();
      $ao10=  Ao_10::where('job_id',$jobid)->first();
    }
    else
    {
      # create a folder for the job
      Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/");
      Storage::makeDirectory("public/jobs/{$job->id}/jobs/");
      $bo= new Bo;
      $bo->job_id = $job->id;
      $ao = new Ao_1;
      $ao->job_id = $job->id;
      $ao2 = new Ao_2;
      $ao2->job_id = $job->id;

      $ao3 = new Ao_3;
      $ao3->job_id = $job->id;
      $ao4 = new Ao_4;
      $ao4->job_id = $job->id;
      $ao5 = new Ao_5;
      $ao5->job_id = $job->id;
      $ao6 = new Ao_6;
      $ao6->job_id = $job->id;
      $ao7 = new Ao_7;
      $ao7->job_id = $job->id;
      $ao8 = new Ao_8;
      $ao8->job_id = $job->id;
      $ao9 = new Ao_9;
      $ao9->job_id = $job->id;
      $ao10 = new Ao_10;
      $ao10->job_id = $job->id;

    }



    if ($request->j_type =='survey') {
      if($jobid){
        return back()->with('message', 'Updated successfully!');
      }
      return redirect('/admin/my-jobs/users/'.$job->id)->with('message', $message);
    }

	  $bo->surveyor_full_information=$request->surveyor_full_information;
  	$bo->surveyor_name=$request->surveyor_name;
  	$bo->surveyor_qualifications=$request->surveyor_qualifications;
  	$bo->surveyor_company_name=$request->surveyor_company_name;
  	$bo->surveyor_company_address=$request->surveyor_company_address;
  	$bo->surveyor_contact_details=$request->surveyor_contact_details;
	  $bo->full_name=$request->full_name;
	  $bo->salutation=$request->salutation;
	  $bo->property_address_proposed_work=$request->property_address_proposed_work;
	  $bo->contact_address=$request->contact_address;
	  $bo->contact_details_telephone_numbers_email=$request->contact_details_telephone_numbers_email;
	  $bo->owner_referral=$request->owner_referral;
	  $bo->has_appointed_have_appointed=$request->has_appointed_have_appointed;
	  $bo->owners_owners=$request->owners_owners;
    $bo->is_are=$request->is_are;
	  $bo->i_we_referral_upper_case=$request->i_we_referral_upper_case;
	  $bo->i_we_referral_lower_case=$request->i_we_referral_lower_case;
	  $bo->i_we_referral_upper_case_2=$request->i_we_referral_upper_case_2;
	  $bo->my_our_referral=$request->my_our_referral;
	  $bo->he_she_they_referral=$request->he_she_they_referral;
	  $bo->intend_intends=$request->intend_intends;
	  $bo->s_s=$request->s_s;
	  $bo->carry_carries=$request->carry_carries;
	  $bo->his_her_their=$request->his_her_their;
		$bo->save();

    $ao->surveyor_full_information = $request->a_surveyor_full_information;
    $ao->surveyor_name = $request->a_surveyor_name;
    $ao->surveyor_qualifications = $request->a_surveyor_qualifications;
    $ao->surveyor_company_name = $request->a_surveyor_company_name;
    $ao->surveyor_company_address = $request->a_surveyor_company_address;
    $ao->surveyor_contact_details = $request->a_surveyor_contact_details;
    $ao->full_names = $request->a_full_names;
    $ao->salutation = $request->a_salutation;
    $ao->property_address_adjoining = $request->a_property_address_adjoining;
    $ao->contact_address = $request->a_contact_address;
    $ao->contact_details = $request->a_contact_details;
    $ao->owners_referral = $request->a_owners_referral;
    $ao->has_appointed_have_appointed = $request->a_has_appointed_have_appointed;
    $ao->i_we_referral = $request->a_i_we_referral;
    $ao->i_we_referral_lower = $request->a_i_we_referral_lower;
    $ao->my_our_refferal = $request->a_my_our_refferal;
    $ao->he_she_they_referral = $request->a_he_she_they_referral;
    $ao->his_her_their = $request->a_his_her_their;
    $ao->owners_owners = $request->a_owners_owners;
    $ao->is_an_are = $request->a_is_an_are;
    $ao->s_s = $request->a_s_s;
    $ao->s1_section = $request->a_s1_section;
    $ao->s1_description = $request->a_s1_description;
    $ao->s2_section = $request->a_s2_section;
    $ao->s2_description = $request->a_s2_description;
    $ao->s6_section = $request->a_s6_section;
    $ao->s6_description = $request->a_s6_description;
    $ao->date_of_notice = $request->a_date_of_notice;
    $ao->notice_notices = $request->a_notice_notices;
    $ao->section_sections = $request->a_section_sections;
    $ao->drawings = $request->a_drawings;
    $ao->soc_date = $request->a_soc_date;
    $ao->third_surveyor = $request->a_third_surveyor;
	  $ao->save();


    $ao2->surveyor_full_information = $request->ao_2_surveyor_full_information;
    $ao2->surveyor_name = $request->ao_2_surveyor_name;
    $ao2->surveyor_qualifications = $request->ao_2_surveyor_qualifications;
    $ao2->surveyor_company_name = $request->ao_2_surveyor_company_name;
    $ao2->surveyor_company_address = $request->ao_2_surveyor_company_address;
    $ao2->surveyor_contact_details = $request->ao_2_surveyor_contact_details;
    $ao2->full_names = $request->ao_2_full_names;
    $ao2->salutation = $request->ao_2_salutation;
    $ao2->property_address_adjoining = $request->ao_2_property_address_adjoining;
    $ao2->contact_address = $request->ao_2_contact_address;
    $ao2->contact_details = $request->ao_2_contact_details;
    $ao2->owners_referral = $request->ao_2_owners_referral;
    $ao2->has_appointed_have_appointed = $request->ao_2_has_appointed_have_appointed;
    $ao2->i_we_referral = $request->ao_2_i_we_referral;
    $ao2->i_we_referral_lower = $request->ao_2_i_we_referral_lower;
    $ao2->my_our_refferal = $request->ao_2_my_our_refferal;
    $ao2->he_she_they_referral = $request->ao_2_he_she_they_referral;
    $ao2->his_her_their = $request->ao_2_his_her_their;
    $ao2->owners_owners = $request->ao_2_owners_owners;
    $ao2->is_an_are = $request->ao_2_is_an_are;
    $ao2->s_s = $request->ao_2_s_s;
    $ao2->s1_section = $request->ao_2_s1_section;
    $ao2->s1_description = $request->ao_2_s1_description;
    $ao2->s2_section = $request->ao_2_s2_section;
    $ao2->s2_description = $request->ao_2_s2_description;
    $ao2->s6_section = $request->ao_2_s6_section;
    $ao2->s6_description = $request->ao_2_s6_description;
    $ao2->date_of_notice = $request->ao_2_date_of_notice;
    $ao2->notice_notices = $request->ao_2_notice_notices;
    $ao2->section_sections = $request->ao_2_section_sections;
    $ao2->drawings = $request->ao_2_drawings;
    $ao2->soc_date = $request->ao_2_soc_date;
    $ao2->third_surveyor = $request->ao_2_third_surveyor;
  	$ao2->save();

    $ao3->surveyor_full_information = $request->ao_3_surveyor_full_information;
    $ao3->surveyor_name = $request->ao_3_surveyor_name;
    $ao3->surveyor_qualifications = $request->ao_3_surveyor_qualifications;
    $ao3->surveyor_company_name = $request->ao_3_surveyor_company_name;
    $ao3->surveyor_company_address = $request->ao_3_surveyor_company_address;
    $ao3->surveyor_contact_details = $request->ao_3_surveyor_contact_details;
    $ao3->full_names = $request->ao_3_full_names;
    $ao3->salutation = $request->ao_3_salutation;
    $ao3->property_address_adjoining = $request->ao_3_property_address_adjoining;
    $ao3->contact_address = $request->ao_3_contact_address;
    $ao3->contact_details = $request->ao_3_contact_details;
    $ao3->owners_referral = $request->ao_3_owners_referral;
    $ao3->has_appointed_have_appointed = $request->ao_3_has_appointed_have_appointed;
    $ao3->i_we_referral = $request->ao_3_i_we_referral;
    $ao3->i_we_referral_lower = $request->ao_3_i_we_referral_lower;
    $ao3->my_our_refferal = $request->ao_3_my_our_refferal;
    $ao3->he_she_they_referral = $request->ao_3_he_she_they_referral;
    $ao3->his_her_their = $request->ao_3_his_her_their;
    $ao3->owners_owners = $request->ao_3_owners_owners;
    $ao3->is_an_are = $request->ao_3_is_an_are;
    $ao3->s_s = $request->ao_3_s_s;
    $ao3->s1_section = $request->ao_3_s1_section;
    $ao3->s1_description = $request->ao_3_s1_description;
    $ao3->s2_section = $request->ao_3_s2_section;
    $ao3->s2_description = $request->ao_3_s2_description;
    $ao3->s6_section = $request->ao_3_s6_section;
    $ao3->s6_description = $request->ao_3_s6_description;
    $ao3->date_of_notice = $request->ao_3_date_of_notice;
    $ao3->notice_notices = $request->ao_3_notice_notices;
    $ao3->section_sections = $request->ao_3_section_sections;
    $ao3->drawings = $request->ao_3_drawings;
    $ao3->soc_date = $request->ao_3_soc_date;
    $ao3->third_surveyor = $request->ao_3_third_surveyor;
    $ao3->save();

    $ao4->surveyor_full_information = $request->ao_4_surveyor_full_information;
    $ao4->surveyor_name = $request->ao_4_surveyor_name;
    $ao4->surveyor_qualifications = $request->ao_4_surveyor_qualifications;
    $ao4->surveyor_company_name = $request->ao_4_surveyor_company_name;
    $ao4->surveyor_company_address = $request->ao_4_surveyor_company_address;
    $ao4->surveyor_contact_details = $request->ao_4_surveyor_contact_details;
    $ao4->full_names = $request->ao_4_full_names;
    $ao4->salutation = $request->ao_4_salutation;
    $ao4->property_address_adjoining = $request->ao_4_property_address_adjoining;
    $ao4->contact_address = $request->ao_4_contact_address;
    $ao4->contact_details = $request->ao_4_contact_details;
    $ao4->owners_referral = $request->ao_4_owners_referral;
    $ao4->has_appointed_have_appointed = $request->ao_4_has_appointed_have_appointed;
    $ao4->i_we_referral = $request->ao_4_i_we_referral;
    $ao4->i_we_referral_lower = $request->ao_4_i_we_referral_lower;
    $ao4->my_our_refferal = $request->ao_4_my_our_refferal;
    $ao4->he_she_they_referral = $request->ao_4_he_she_they_referral;
    $ao4->his_her_their = $request->ao_4_his_her_their;
    $ao4->owners_owners = $request->ao_4_owners_owners;
    $ao4->is_an_are = $request->ao_4_is_an_are;
    $ao4->s_s = $request->ao_4_s_s;
    $ao4->s1_section = $request->ao_4_s1_section;
    $ao4->s1_description = $request->ao_4_s1_description;
    $ao4->s2_section = $request->ao_4_s2_section;
    $ao4->s2_description = $request->ao_4_s2_description;
    $ao4->s6_section = $request->ao_4_s6_section;
    $ao4->s6_description = $request->ao_4_s6_description;
    $ao4->date_of_notice = $request->ao_4_date_of_notice;
    $ao4->notice_notices = $request->ao_4_notice_notices;
    $ao4->section_sections = $request->ao_4_section_sections;
    $ao4->drawings = $request->ao_4_drawings;
    $ao4->soc_date = $request->ao_4_soc_date;
    $ao4->third_surveyor = $request->ao_4_third_surveyor;
    $ao4->save();

    $ao5->surveyor_full_information = $request->ao_5_surveyor_full_information;
    $ao5->surveyor_name = $request->ao_5_surveyor_name;
    $ao5->surveyor_qualifications = $request->ao_5_surveyor_qualifications;
    $ao5->surveyor_company_name = $request->ao_5_surveyor_company_name;
    $ao5->surveyor_company_address = $request->ao_5_surveyor_company_address;
    $ao5->surveyor_contact_details = $request->ao_5_surveyor_contact_details;
    $ao5->full_names = $request->ao_5_full_names;
    $ao5->salutation = $request->ao_5_salutation;
    $ao5->property_address_adjoining = $request->ao_5_property_address_adjoining;
    $ao5->contact_address = $request->ao_5_contact_address;
    $ao5->contact_details = $request->ao_5_contact_details;
    $ao5->owners_referral = $request->ao_5_owners_referral;
    $ao5->has_appointed_have_appointed = $request->ao_5_has_appointed_have_appointed;
    $ao5->i_we_referral = $request->ao_5_i_we_referral;
    $ao5->i_we_referral_lower = $request->ao_5_i_we_referral_lower;
    $ao5->my_our_refferal = $request->ao_5_my_our_refferal;
    $ao5->he_she_they_referral = $request->ao_5_he_she_they_referral;
    $ao5->his_her_their = $request->ao_5_his_her_their;
    $ao5->owners_owners = $request->ao_5_owners_owners;
    $ao5->is_an_are = $request->ao_5_is_an_are;
    $ao5->s_s = $request->ao_5_s_s;
    $ao5->s1_section = $request->ao_5_s1_section;
    $ao5->s1_description = $request->ao_5_s1_description;
    $ao5->s2_section = $request->ao_5_s2_section;
    $ao5->s2_description = $request->ao_5_s2_description;
    $ao5->s6_section = $request->ao_5_s6_section;
    $ao5->s6_description = $request->ao_5_s6_description;
    $ao5->date_of_notice = $request->ao_5_date_of_notice;
    $ao5->notice_notices = $request->ao_5_notice_notices;
    $ao5->section_sections = $request->ao_5_section_sections;
    $ao5->drawings = $request->ao_5_drawings;
    $ao5->soc_date = $request->ao_5_soc_date;
    $ao5->third_surveyor = $request->ao_5_third_surveyor;
    $ao5->save();

    $ao6->surveyor_full_information = $request->ao_6_surveyor_full_information;
    $ao6->surveyor_name = $request->ao_6_surveyor_name;
    $ao6->surveyor_qualifications = $request->ao_6_surveyor_qualifications;
    $ao6->surveyor_company_name = $request->ao_6_surveyor_company_name;
    $ao6->surveyor_company_address = $request->ao_6_surveyor_company_address;
    $ao6->surveyor_contact_details = $request->ao_6_surveyor_contact_details;
    $ao6->full_names = $request->ao_6_full_names;
    $ao6->salutation = $request->ao_6_salutation;
    $ao6->property_address_adjoining = $request->ao_6_property_address_adjoining;
    $ao6->contact_address = $request->ao_6_contact_address;
    $ao6->contact_details = $request->ao_6_contact_details;
    $ao6->owners_referral = $request->ao_6_owners_referral;
    $ao6->has_appointed_have_appointed = $request->ao_6_has_appointed_have_appointed;
    $ao6->i_we_referral = $request->ao_6_i_we_referral;
    $ao6->i_we_referral_lower = $request->ao_6_i_we_referral_lower;
    $ao6->my_our_refferal = $request->ao_6_my_our_refferal;
    $ao6->he_she_they_referral = $request->ao_6_he_she_they_referral;
    $ao6->his_her_their = $request->ao_6_his_her_their;
    $ao6->owners_owners = $request->ao_6_owners_owners;
    $ao6->is_an_are = $request->ao_6_is_an_are;
    $ao6->s_s = $request->ao_6_s_s;
    $ao6->s1_section = $request->ao_6_s1_section;
    $ao6->s1_description = $request->ao_6_s1_description;
    $ao6->s2_section = $request->ao_6_s2_section;
    $ao6->s2_description = $request->ao_6_s2_description;
    $ao6->s6_section = $request->ao_6_s6_section;
    $ao6->s6_description = $request->ao_6_s6_description;
    $ao6->date_of_notice = $request->ao_6_date_of_notice;
    $ao6->notice_notices = $request->ao_6_notice_notices;
    $ao6->section_sections = $request->ao_6_section_sections;
    $ao6->drawings = $request->ao_6_drawings;
    $ao6->soc_date = $request->ao_6_soc_date;
    $ao6->third_surveyor = $request->ao_6_third_surveyor;
    $ao6->save();

    $ao7->surveyor_full_information = $request->ao_7_surveyor_full_information;
    $ao7->surveyor_name = $request->ao_7_surveyor_name;
    $ao7->surveyor_qualifications = $request->ao_7_surveyor_qualifications;
    $ao7->surveyor_company_name = $request->ao_7_surveyor_company_name;
    $ao7->surveyor_company_address = $request->ao_7_surveyor_company_address;
    $ao7->surveyor_contact_details = $request->ao_7_surveyor_contact_details;
    $ao7->full_names = $request->ao_7_full_names;
    $ao7->salutation = $request->ao_7_salutation;
    $ao7->property_address_adjoining = $request->ao_7_property_address_adjoining;
    $ao7->contact_address = $request->ao_7_contact_address;
    $ao7->contact_details = $request->ao_7_contact_details;
    $ao7->owners_referral = $request->ao_7_owners_referral;
    $ao7->has_appointed_have_appointed = $request->ao_7_has_appointed_have_appointed;
    $ao7->i_we_referral = $request->ao_7_i_we_referral;
    $ao7->i_we_referral_lower = $request->ao_7_i_we_referral_lower;
    $ao7->my_our_refferal = $request->ao_7_my_our_refferal;
    $ao7->he_she_they_referral = $request->ao_7_he_she_they_referral;
    $ao7->his_her_their = $request->ao_7_his_her_their;
    $ao7->owners_owners = $request->ao_7_owners_owners;
    $ao7->is_an_are = $request->ao_7_is_an_are;
    $ao7->s_s = $request->ao_7_s_s;
    $ao7->s1_section = $request->ao_7_s1_section;
    $ao7->s1_description = $request->ao_7_s1_description;
    $ao7->s2_section = $request->ao_7_s2_section;
    $ao7->s2_description = $request->ao_7_s2_description;
    $ao7->s6_section = $request->ao_7_s6_section;
    $ao7->s6_description = $request->ao_7_s6_description;
    $ao7->date_of_notice = $request->ao_7_date_of_notice;
    $ao7->notice_notices = $request->ao_7_notice_notices;
    $ao7->section_sections = $request->ao_7_section_sections;
    $ao7->drawings = $request->ao_7_drawings;
    $ao7->soc_date = $request->ao_7_soc_date;
    $ao7->third_surveyor = $request->ao_7_third_surveyor;
    $ao7->save();

    $ao8->surveyor_full_information = $request->ao_8_surveyor_full_information;
    $ao8->surveyor_name = $request->ao_8_surveyor_name;
    $ao8->surveyor_qualifications = $request->ao_8_surveyor_qualifications;
    $ao8->surveyor_company_name = $request->ao_8_surveyor_company_name;
    $ao8->surveyor_company_address = $request->ao_8_surveyor_company_address;
    $ao8->surveyor_contact_details = $request->ao_8_surveyor_contact_details;
    $ao8->full_names = $request->ao_8_full_names;
    $ao8->salutation = $request->ao_8_salutation;
    $ao8->property_address_adjoining = $request->ao_8_property_address_adjoining;
    $ao8->contact_address = $request->ao_8_contact_address;
    $ao8->contact_details = $request->ao_8_contact_details;
    $ao8->owners_referral = $request->ao_8_owners_referral;
    $ao8->has_appointed_have_appointed = $request->ao_8_has_appointed_have_appointed;
    $ao8->i_we_referral = $request->ao_8_i_we_referral;
    $ao8->i_we_referral_lower = $request->ao_8_i_we_referral_lower;
    $ao8->my_our_refferal = $request->ao_8_my_our_refferal;
    $ao8->he_she_they_referral = $request->ao_8_he_she_they_referral;
    $ao8->his_her_their = $request->ao_8_his_her_their;
    $ao8->owners_owners = $request->ao_8_owners_owners;
    $ao8->is_an_are = $request->ao_8_is_an_are;
    $ao8->s_s = $request->ao_8_s_s;
    $ao8->s1_section = $request->ao_8_s1_section;
    $ao8->s1_description = $request->ao_8_s1_description;
    $ao8->s2_section = $request->ao_8_s2_section;
    $ao8->s2_description = $request->ao_8_s2_description;
    $ao8->s6_section = $request->ao_8_s6_section;
    $ao8->s6_description = $request->ao_8_s6_description;
    $ao8->date_of_notice = $request->ao_8_date_of_notice;
    $ao8->notice_notices = $request->ao_8_notice_notices;
    $ao8->section_sections = $request->ao_8_section_sections;
    $ao8->drawings = $request->ao_8_drawings;
    $ao8->soc_date = $request->ao_8_soc_date;
    $ao8->third_surveyor = $request->ao_8_third_surveyor;
    $ao8->save();

    $ao9->surveyor_full_information = $request->ao_9_surveyor_full_information;
    $ao9->surveyor_name = $request->ao_9_surveyor_name;
    $ao9->surveyor_qualifications = $request->ao_9_surveyor_qualifications;
    $ao9->surveyor_company_name = $request->ao_9_surveyor_company_name;
    $ao9->surveyor_company_address = $request->ao_9_surveyor_company_address;
    $ao9->surveyor_contact_details = $request->ao_9_surveyor_contact_details;
    $ao9->full_names = $request->ao_9_full_names;
    $ao9->salutation = $request->ao_9_salutation;
    $ao9->property_address_adjoining = $request->ao_9_property_address_adjoining;
    $ao9->contact_address = $request->ao_9_contact_address;
    $ao9->contact_details = $request->ao_9_contact_details;
    $ao9->owners_referral = $request->ao_9_owners_referral;
    $ao9->has_appointed_have_appointed = $request->ao_9_has_appointed_have_appointed;
    $ao9->i_we_referral = $request->ao_9_i_we_referral;
    $ao9->i_we_referral_lower = $request->ao_9_i_we_referral_lower;
    $ao9->my_our_refferal = $request->ao_9_my_our_refferal;
    $ao9->he_she_they_referral = $request->ao_9_he_she_they_referral;
    $ao9->his_her_their = $request->ao_9_his_her_their;
    $ao9->owners_owners = $request->ao_9_owners_owners;
    $ao9->is_an_are = $request->ao_9_is_an_are;
    $ao9->s_s = $request->ao_9_s_s;
    $ao9->s1_section = $request->ao_9_s1_section;
    $ao9->s1_description = $request->ao_9_s1_description;
    $ao9->s2_section = $request->ao_9_s2_section;
    $ao9->s2_description = $request->ao_9_s2_description;
    $ao9->s6_section = $request->ao_9_s6_section;
    $ao9->s6_description = $request->ao_9_s6_description;
    $ao9->date_of_notice = $request->ao_9_date_of_notice;
    $ao9->notice_notices = $request->ao_9_notice_notices;
    $ao9->section_sections = $request->ao_9_section_sections;
    $ao9->drawings = $request->ao_9_drawings;
    $ao9->soc_date = $request->ao_9_soc_date;
    $ao9->third_surveyor = $request->ao_9_third_surveyor;
    $ao9->save();

    $ao10->surveyor_full_information = $request->ao_10_surveyor_full_information;
    $ao10->surveyor_name = $request->ao_10_surveyor_name;
    $ao10->surveyor_qualifications = $request->ao_10_surveyor_qualifications;
    $ao10->surveyor_company_name = $request->ao_10_surveyor_company_name;
    $ao10->surveyor_company_address = $request->ao_10_surveyor_company_address;
    $ao10->surveyor_contact_details = $request->ao_10_surveyor_contact_details;
    $ao10->full_names = $request->ao_10_full_names;
    $ao10->salutation = $request->ao_10_salutation;
    $ao10->property_address_adjoining = $request->ao_10_property_address_adjoining;
    $ao10->contact_address = $request->ao_10_contact_address;
    $ao10->contact_details = $request->ao_10_contact_details;
    $ao10->owners_referral = $request->ao_10_owners_referral;
    $ao10->has_appointed_have_appointed = $request->ao_10_has_appointed_have_appointed;
    $ao10->i_we_referral = $request->ao_10_i_we_referral;
    $ao10->i_we_referral_lower = $request->ao_10_i_we_referral_lower;
    $ao10->my_our_refferal = $request->ao_10_my_our_refferal;
    $ao10->he_she_they_referral = $request->ao_10_he_she_they_referral;
    $ao10->his_her_their = $request->ao_10_his_her_their;
    $ao10->owners_owners = $request->ao_10_owners_owners;
    $ao10->is_an_are = $request->ao_10_is_an_are;
    $ao10->s_s = $request->ao_10_s_s;
    $ao10->s1_section = $request->ao_10_s1_section;
    $ao10->s1_description = $request->ao_10_s1_description;
    $ao10->s2_section = $request->ao_10_s2_section;
    $ao10->s2_description = $request->ao_10_s2_description;
    $ao10->s6_section = $request->ao_10_s6_section;
    $ao10->s6_description = $request->ao_10_s6_description;
    $ao10->date_of_notice = $request->ao_10_date_of_notice;
    $ao10->notice_notices = $request->ao_10_notice_notices;
    $ao10->section_sections = $request->ao_10_section_sections;
    $ao10->drawings = $request->ao_10_drawings;
    $ao10->soc_date = $request->ao_10_soc_date;
    $ao10->third_surveyor = $request->ao_10_third_surveyor;
    $ao10->save();

    if ( $request->enq_id) {
        //Automatically assign employee to created job
        MyJobsController::addUser($job->id, Auth::user()->id);
        return $job->id;
    }
    if($jobid){

      return back()->with('message', 'Updated successfully!');
    }
    $user_history = new User_History;
    $user_history->user_id = Auth::user()->id;
    $user_history->action = "create/update enquiry";
    $user_history->description = (Auth::user()->name." has created/updated enquiry ");
    $user_history->save();
    //Automatically assign employee to created job
    MyJobsController::addUser($job->id, Auth::user()->id);
    return redirect('/admin/my-jobs/users/'.$job->id)->with('message', $message);
  }

  // create and update jobs
  public function createSurvey($jobid=null, Request $request)
  {
    // update
    if($jobid)
    {
      $job = Job::find($jobid);
      $message= 'Updated successfully!';
    }
    // new job
    else
    {
      $job = new Job;
      $job->status = 1;
      $message= 'Add a client to the job !';
    }

    if ($request->enq_id) {
      $job->enq_id = $request->enq_id;
    }
    $job->invoice_no = $request->invoice_no;
    $job->job_type = $request->job_type;
    $job->notice_costs =$request->notice_costs;
    $job->award_costs =$request->award_costs;
    $job->land_registry_costs =$request->land_registry_costs;
    $job->survey_email =$request->survey_email;
    if ($request->printing_postage_costs1 == null) {
      if(mb_substr($request->printing_postage_costs,0,1) == '£')
      {
        $job->printing_postage_costs =$request->printing_postage_costs;
      }
      else
      {
        $job->printing_postage_costs = '£'.$request->printing_postage_costs;
      }
    }
    else{
      if(mb_substr($request->printing_postage_costs1,0,1) == '£')
      {
        $job->printing_postage_costs = $request->printing_postage_costs1; 
      }
      else
      {
        $job->printing_postage_costs = '£'.$request->printing_postage_costs1;
      }
    }
    $job->vat_amount =$request->vat_amount;
    $job->final_amount =$request->final_amount;
    $job->surveyor_who_had_first_contact_with_owner =$request->surveyor_who_had_first_contact_with_owner;
    $job->surveyor_dealing_with_day_to_day =$request->surveyor_dealing_with_day_to_day;
    $job->party_wall_notice_date =$request->party_wall_notice_date;
    $job->ten_4_party_wall_notice_date =$request->ten_4_party_wall_notice_date;
    $job->ten_4_party_wall_notice_expiry_date =$request->ten_4_party_wall_notice_expiry_date;
    $job->schedule_of_condition_date =$request->schedule_of_condition_date;
    $job->save();
    //dd($job);

    if($jobid){
      $survey=Surveying::where('job_id',$jobid)->first();
    }
    else
    {
      # create a folder for the job
      Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/");
      Storage::makeDirectory("public/jobs/{$job->id}/jobs/");
      $survey = new Surveying;
      $survey->job_id = $job->id;
    }

    $survey->surveying_service = $request->surveying_service;
    $survey->address_of_boundary_determination = $request->address_of_boundary_determination;
    $survey->boundary_determination_address = $request->boundary_determination_address;
    if ($request->surveying_service == "Valuation Report")
    {
      $survey->type_of_property = $request->v_type_of_property;
      $survey->age_of_property = $request->v_age_of_property;
      $survey->inspection_humidity = $request->v_inspection_humidity;
      $survey->inspection_weather = $request->v_inspection_weather;
      $survey->valuation_date_of_confirmation_letter = $request->valuation_date_of_confirmation_letter;
    }
    else
    {
      $survey->type_of_property = $request->type_of_property;
      $survey->age_of_property = $request->age_of_property;
      $survey->inspection_weather = $request->inspection_weather;
      $survey->inspection_humidity = $request->inspection_humidity;
      $survey->boundary_date_of_confirmation_letter = $request->boundary_date_of_confirmation_letter;
    }    
    $survey->address_of_adjoining_property = $request->address_of_adjoining_property;
    $survey->adjoining_property = $request->adjoining_property;
    $survey->name_of_legal_owner = $request->name_of_legal_owner;
    $survey->boundary_being_determined = $request->boundary_being_determined;
    $survey->direction_of_boundary = $request->direction_of_boundary;
    $survey->plane_of_boundary = $request->plane_of_boundary;          
    $survey->local_authority = $request->local_authority;
    $survey->historic_maps_were_present = $request->historic_maps_were_present;
    $survey->planning_records_of_assistance = $request->planning_records_of_assistance;
    $survey->date_of_report = $request->date_of_report;
    $survey->date_of_inspection =$request->date_of_inspection;
    $survey->cost_of_service =$request->cost_of_service;
    $survey->surveyor_who_had_first_contact_with_client =$request->surveyor_who_had_first_contact_with_client;
    $survey->surveyor_dealing_with_file =$request->surveyor_dealing_with_file;
    $survey->s_surveyor_full_information =$request->s_surveyor_full_information;
    $survey->surveryor_name =$request->surveryor_name;
    $survey->s_surveyor_qualifications =$request->s_surveyor_qualifications;
    $survey->surveyor_contact_details = $request->surveyor_contact_details;
    $survey->client_full_name =$request->client_full_name;
    $survey->client_salutation =$request->client_salutation;
    $survey->address_of_inspection =$request->address_of_inspection;
    $survey->client_contact_address =$request->client_contact_address;
    $survey->client_contact_details =$request->client_contact_details;
    $survey->client_i_we_upper =$request->client_i_we_upper;
    $survey->client_i_we_lower =$request->client_i_we_lower;
    $survey->additional_info =$request->additional_info;
    $survey->valuation_type =$request->valuation_type;
    $survey->address_of_valued_property =$request->address_of_valued_property;
    $survey->floors_in_the_property =$request->floors_in_the_property;
    $survey->valuation_date =$request->valuation_date;
    $survey->valuation_property_ownership_type =$request->valuation_property_ownership_type;
    $survey->save();
    //dd($request->all());

    if ( $request->enq_id) {
      //Automatically assign employee to created job
      MyJobsController::addUser($job->id, Auth::user()->id);
        return $job->id;
    }
    if ($request->j_type =='survey') {
      if($jobid){
        //Automatically assign employee to created job
        MyJobsController::addUser($job->id, Auth::user()->id);
        return back()->with('message', 'Updated successfully!');
      }
      //Automatically assign employee to created job
        MyJobsController::addUser($job->id, Auth::user()->id);
      return redirect('/admin/my-jobs/users/'.$job->id)->with('message', $message);
    }
    if($jobid){
      return back()->with('message', 'Updated successfully!');
    }
    //Automatically assign employee to created job
    MyJobsController::addUser($job->id, Auth::user()->id);
    return redirect('/admin/my-jobs/users/'.$job->id)->with('message', $message);
  }


  public function view($jobid){ 
    $jobs=Job::find($jobid);
    $surveyors = User::whereIN('role',['9','7'])->get();
     $surveyor= \App\User::whereNotNull('qualifications')->get();

    if ($jobs->surveying){
      return view('admin.my_jobs_add_jobs_survey',['jobs'=>$jobs, 'addOrUpdate'=>'update', 'jobid'=>$jobid, 'surveyor'=>$surveyor , 'surveyors_email'=>$surveyors]);
    }
    return view('admin.my_jobs_add_jobs',['jobs'=>$jobs, 'addOrUpdate'=>'update', 'jobid'=>$jobid, 'surveyor'=>$surveyor, 'surveyors_email'=>$surveyors ]);
  }


  public function upload(Request $request){
    
    $doc = new \App\JobDocs;
    $doc->job_id =$request->job_id;
    $doc->user_id = Auth::user()->id;
    $subf = "";
    if( !empty($request->parentfold) ){
    	$subf = ($request->filetype)? preg_replace('/[^a-zA-Z0-9_.]/', ' ', $request->filetype):"";
	 	$doc->folder = $request->parentfold;
      $doc->subfolder = $subf;
	 }else{
	 	$doc->folder = $request->filetype;
    	$doc->subfolder = NULL;
	 }
	  
    $foldext = substr($doc->folder, 0, strpos($doc->folder, "/"));
	 $parent = ($subf!="")? (($foldext)? $foldext: $doc->folder) :""; //;
    // $upload = $request->file('file');
    // $store = $upload->storeAs("public/jobs/{$request->job_id}/my-jobs/uploads/$parent/$subf", time().'-'.$upload->getClientOriginalName());
	 $upload = $request->file('file');
   if ($upload->getClientOriginalExtension() == "png" || $upload->getClientOriginalExtension() == "jpeg" || $upload->getClientOriginalExtension() == "jpg") {
    Storage::makeDirectory("public/jobs/{$request->job_id}/my-jobs/uploads/$parent/$subf");
    $store = "public/jobs/{$request->job_id}/my-jobs/uploads/$parent/$subf".time().'-'.$upload->getClientOriginalName();
    $image_resize = Image::make($upload->getRealPath())->resize(500, null, function ($constraint) {
        $constraint->aspectRatio();
    })->save(base_path()."/storage/app/".$store,90);
    
     // $store = $image_resize->save("public/jobs/{$request->job_id}/my-jobs/uploads/$parent/$subf",80, time().'-'.$upload->getClientOriginalName());
   }
   else{
    $store = $upload->storeAs("public/jobs/{$request->job_id}/my-jobs/uploads/$parent/$subf", time().'-'.$upload->getClientOriginalName());
   }

    $doc->original_name =$upload->getClientOriginalName();
    $doc->type =$upload->getClientOriginalExtension();
    $doc->mimetype =$upload->getMimeType();
    $doc->link = str_replace('public','storage',$store);
    $doc->size = $upload->getSize();
    $doc->upload = 1;
    $doc->save();
    
    $user_history = new User_History;
    $user_history->user_id = Auth::user()->id;
    $user_history->action = "upload file";
    $user_history->description = (Auth::user()->name." has uploaded ".$doc->original_name ." file on Job ID ".$request->job_id );
    $user_history->save();
    return $doc;
  }

  public function deleteDoc(Request $req)
  {
    $doc =  \App\JobDocs::find($req->doc_id);
    $del=  Storage::delete(str_replace('storage','public', $doc->link) );
    if ($del) {
      $doc->delete();
      $user_history = new User_History;
      $user_history->user_id = Auth::user()->id;
      $user_history->action = "delete document";
      $user_history->description = (Auth::user()->name." has deleted Document ID ".$req->doc_id ." of Job ID ".$req->job_id);
      $user_history->save();
      return 'done';
    }
  }

  public function transferDoc(Request $req)
  {
    $doc =  \App\JobDocs::find($req->doc_id);
    $path = explode('/', $doc->link);
    $path_cnt = sizeof($path);
    // dd(file_exists(public_path()."/storage/jobs/".$doc->job_id."/jobs/".$path[$path_cnt-1]));
    if (strpos($doc->link, '///')) {
      str_replace('///', '/', $doc->link);
    }
    if (file_exists(public_path()."/storage/jobs/".$doc->job_id."/jobs/".$path[$path_cnt-1]) || file_exists(public_path()."/storage/jobs/".$doc->job_id."/jobs/".$path[$path_cnt-3]."/".$path[$path_cnt-2]."/".$path[$path_cnt-1]) ) {

      return 'already exists';
    }
    if (strpos($doc->link, 'my-jobs/uploads') ) {
      $copy=  Storage::copy(str_replace('storage','public', $doc->link), str_replace(['storage', 'my-jobs/uploads'],['public', 'jobs'], $doc->link) );
    }
    else{
      $copy=  Storage::copy(str_replace('storage','public', $doc->link), str_replace(['storage', 'my-jobs'],['public', 'jobs'], $doc->link) );
    }
    /*/storage/jobs/100550/my-jobs/surveying/Draft Report/Draft Report.xlsx

    dd($copy);*/

    if ($copy) {
      $j = new  \App\Document;
      $j->job_id =$doc->job_id;
      $j->user_id = Auth::user()->id;
      $j->original_name =$doc->original_name;
      $j->extension =$doc->type;
      $j->type=$req->folder;
      $j->mimetype =$doc->mimetype;
      $j->link = str_replace( 'my-jobs/uploads','jobs', $doc->link);
      $j->size = $doc->size;
      $j->save();

      $user_history = new User_History;
      $user_history->user_id = Auth::user()->id;
      $user_history->action = "Transfer file ";
      $user_history->description = (Auth::user()->name." has transfer this ".$doc->original_name." file on Job ID ".$doc->job_id);
      $user_history->save();
      return 'done';
    }
    else{
      return 'not done';
    }
  }

  //download file history management
  public function downloadFile(Request $request)
  {
    $user_history = new User_History;
    $user_history->user_id = Auth::user()->id;
    $user_history->action = "download file ";
    $user_history->description = (Auth::user()->name." has downloaded this ".$request->file." file from Job ID ".$request->get('job_id'));
    $user_history->save();
    return response('success',202);
  }

  //download user history
  public function downloadUserHistory($id)
  {

    $user_history  = User_History::where('user_id','=',$id)->get();
    $pdf = PDF::loadView('admin.user_history',compact('user_history')); 
    return $pdf->download('history.pdf');
    // return view('admin.user_history',compact('user_history'));
  }

  public function selectUserHistory(Request $request)
  {

    $user_history = User_History::where('user_id', '=', $request->user_id)->where('created_at','>=',$request->start_date.' 00:00:00')->where('created_at','<=',$request->end_date.' 00:00:00')->get();
    return view('admin.user_history',compact('user_history'));
  }
  //rename of file
  public function rename(Request $request)
  {
    $user =  \App\JobDocs::where('id','=',$request->job_id)->where('original_name','=',$request->old_name)->first();
    $user->original_name = $request->name.".".$user->type;
    $user->save();
    return response('success',202);
  }
  //move to sketchpad
  public function sketchPad($job_id=null)
  {
    
    $url = url()->previous();
      return view('admin.sketch',compact('job_id','url'));
  }
}