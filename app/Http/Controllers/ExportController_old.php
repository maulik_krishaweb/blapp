<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\{JobDocs};
use Auth;
use PDF;
use View;
use File;
use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;

class ExportController extends Controller
{
    public function gen($jobid=null){
      //$finfo = finfo_open(FILEINFO_MIME, "/etc/magic");
    	# get job details from db
    	$job= \App\Job::find($jobid);
      $docs=[];
      $dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/sketchpad");
      // appointments
  	   $appointment = new \App\Src\Docs\Appointment\AO1CoverToAptLetter();
      $docs[$appointment->documentFolder][$appointment->create($job)]=$appointment->documentName;

      $appointment2 = new \App\Src\Docs\Appointment\AO2CoverToAptLetter();
      $docs[$appointment2->documentFolder][$appointment2->create($job)]=$appointment2->documentName;
      
      $appointment3 = new \App\Src\Docs\Appointment\BOCoverToAptLetter();
      $docs[$appointment3->documentFolder][$appointment3->create($job)] =($appointment3)->documentName;

      $appointment4 = new \App\Src\Docs\Appointment\AO3CoverToAptLetter();
      $docs[$appointment4->documentFolder][$appointment4->create($job)]=$appointment4->documentName;

      $appointment5 = new \App\Src\Docs\Appointment\AO4CoverToAptLetter();
      $docs[$appointment5->documentFolder][$appointment5->create($job)]=$appointment5->documentName;

      $appointment6 = new \App\Src\Docs\Appointment\AO5CoverToAptLetter();
      $docs[$appointment6->documentFolder][$appointment6->create($job)]=$appointment6->documentName;

      $appointment7 = new \App\Src\Docs\Appointment\AO6CoverToAptLetter();
      $docs[$appointment7->documentFolder][$appointment7->create($job)]=$appointment7->documentName;

      $appointment8 = new \App\Src\Docs\Appointment\AO7CoverToAptLetter();
      $docs[$appointment8->documentFolder][$appointment8->create($job)]=$appointment8->documentName;

      $appointment9 = new \App\Src\Docs\Appointment\AO8CoverToAptLetter();
      $docs[$appointment9->documentFolder][$appointment9->create($job)]=$appointment9->documentName;

      $appointment10 = new \App\Src\Docs\Appointment\AO9CoverToAptLetter();
      $docs[$appointment10->documentFolder][$appointment10->create($job)]=$appointment10->documentName;

      $appointment11 = new \App\Src\Docs\Appointment\AO10CoverToAptLetter();
      $docs[$appointment11->documentFolder][$appointment11->create($job)]=$appointment11->documentName;


      //Appointment Letters
      $LetterOfAppointmentAO1 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO1();
      $docs[$LetterOfAppointmentAO1->documentFolder][$LetterOfAppointmentAO1->create($job)] =($LetterOfAppointmentAO1)->documentName;

      $LetterOfAppointmentAO2 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO2();
      $docs[$LetterOfAppointmentAO2->documentFolder][$LetterOfAppointmentAO2->create($job)] =($LetterOfAppointmentAO2)->documentName;

      $LetterOfAppointmentAO3 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO3();
      $docs[$LetterOfAppointmentAO3->documentFolder][$LetterOfAppointmentAO3->create($job)] =($LetterOfAppointmentAO3)->documentName;

      $LetterOfAppointmentAO4 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO4();
      $docs[$LetterOfAppointmentAO4->documentFolder][$LetterOfAppointmentAO4->create($job)] =($LetterOfAppointmentAO4)->documentName;

      $LetterOfAppointmentAO5 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO5();
      $docs[$LetterOfAppointmentAO5->documentFolder][$LetterOfAppointmentAO5->create($job)] =($LetterOfAppointmentAO5)->documentName;

      $LetterOfAppointmentAO6 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO6();
      $docs[$LetterOfAppointmentAO6->documentFolder][$LetterOfAppointmentAO6->create($job)] =($LetterOfAppointmentAO6)->documentName;

      $LetterOfAppointmentAO7 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO7();
      $docs[$LetterOfAppointmentAO7->documentFolder][$LetterOfAppointmentAO7->create($job)] =($LetterOfAppointmentAO7)->documentName;

      $LetterOfAppointmentAO8 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO8();
      $docs[$LetterOfAppointmentAO8->documentFolder][$LetterOfAppointmentAO8->create($job)] =($LetterOfAppointmentAO8)->documentName;

      $LetterOfAppointmentAO9 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO9();
      $docs[$LetterOfAppointmentAO9->documentFolder][$LetterOfAppointmentAO9->create($job)] =($LetterOfAppointmentAO9)->documentName;

      $LetterOfAppointmentAO10 = new \App\Src\Docs\Appointment\LetterOfAppointmentAO10();
      $docs[$LetterOfAppointmentAO10->documentFolder][$LetterOfAppointmentAO10->create($job)] =($LetterOfAppointmentAO10)->documentName;

      $LetterOfAppointmentBO = new \App\Src\Docs\Appointment\LetterOfAppointmentBO();
      $docs[$LetterOfAppointmentBO->documentFolder][$LetterOfAppointmentBO->create($job)] =($LetterOfAppointmentBO)->documentName;


      // awards

      // AO1 START //
      $AO1CoverToAwardAOS = new \App\Src\Docs\Awards\AO1CoverToAwardAOS();
      $docs[$AO1CoverToAwardAOS->documentFolder][$AO1CoverToAwardAOS->create($job)] =($AO1CoverToAwardAOS)->documentName;

      $AO1CoverToAwardAS = new \App\Src\Docs\Awards\AO1CoverToAwardAS();
      $docs[$AO1CoverToAwardAS->documentFolder][$AO1CoverToAwardAS->create($job)] =($AO1CoverToAwardAS)->documentName;

      $AO1CoverToBOAwardAs = new \App\Src\Docs\Awards\AO1CoverToBOAwardAs();
      $docs[$AO1CoverToBOAwardAs->documentFolder][$AO1CoverToBOAwardAs->create($job)] =($AO1CoverToBOAwardAs)->documentName;

      $AO1CoverToBOAwardAsWithInv = new \App\Src\Docs\Awards\AO1CoverToBOAwardAsWithInv();
      $docs[$AO1CoverToBOAwardAsWithInv->documentFolder][$AO1CoverToBOAwardAsWithInv->create($job)] =($AO1CoverToBOAwardAsWithInv)->documentName;

      $AO1CoverToBoAwardBos = new \App\Src\Docs\Awards\AO1CoverToBoAwardBos();
      $docs[$AO1CoverToBoAwardBos->documentFolder][$AO1CoverToBoAwardBos->create($job)] =($AO1CoverToBoAwardBos)->documentName;

      $AO1CoverToBoAwardBosWithInv = new \App\Src\Docs\Awards\AO1CoverToBoAwardBosWithInv();
      $docs[$AO1CoverToBoAwardBosWithInv->documentFolder][$AO1CoverToBoAwardBosWithInv->create($job)] =($AO1CoverToBoAwardBosWithInv)->documentName;

      $AO1ReturningAwardToBos = new \App\Src\Docs\Awards\AO1ReturningAwardToBos();
      $docs[$AO1ReturningAwardToBos->documentFolder][$AO1ReturningAwardToBos->create($job)] =($AO1ReturningAwardToBos)->documentName;

      $AO1SendingAwardToTheAOS = new \App\Src\Docs\Awards\AO1SendingAwardToTheAOS();
      $docs[$AO1SendingAwardToTheAOS->documentFolder][$AO1SendingAwardToTheAOS->create($job)] =($AO1SendingAwardToTheAOS)->documentName;
      // AO1 END //

      // AO2 START //
      $AO2CoverToAwardAOS = new \App\Src\Docs\Awards\AO2CoverToAwardAOS();
      $docs[$AO2CoverToAwardAOS->documentFolder][$AO2CoverToAwardAOS->create($job)] =($AO2CoverToAwardAOS)->documentName;

      $AO2CovertoBOAwardAS = new \App\Src\Docs\Awards\AO2CovertoBOAwardAS();
      $docs[$AO2CovertoBOAwardAS->documentFolder][$AO2CovertoBOAwardAS->create($job)] =($AO2CovertoBOAwardAS)->documentName;

      $AO2CovertoBOawardBOS = new \App\Src\Docs\Awards\AO2CovertoBOawardBOS();
      $docs[$AO2CovertoBOawardBOS->documentFolder][$AO2CovertoBOawardBOS->create($job)] =($AO2CovertoBOawardBOS)->documentName;

      $AO2CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO2CovertoBOawardBOSwithINV();
      $docs[$AO2CovertoBOawardBOSwithINV->documentFolder][$AO2CovertoBOawardBOSwithINV->create($job)] =($AO2CovertoBOawardBOSwithINV)->documentName;

      $AO2ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO2ReturningAwardtoBOS();
      $docs[$AO2ReturningAwardtoBOS->documentFolder][$AO2ReturningAwardtoBOS->create($job)] =($AO2ReturningAwardtoBOS)->documentName;

      $AO2SendingawardtotheAOS = new \App\Src\Docs\Awards\AO2SendingawardtotheAOS();
      $docs[$AO2SendingawardtotheAOS->documentFolder][$AO2SendingawardtotheAOS->create($job)] =($AO2SendingawardtotheAOS)->documentName;
      // AO2 END //

      // AO3 START //
      $AO3CoverToAwardAOS = new \App\Src\Docs\Awards\AO3CoverToAwardAOS();
      $docs[$AO3CoverToAwardAOS->documentFolder][$AO3CoverToAwardAOS->create($job)] =($AO3CoverToAwardAOS)->documentName;

      $AO3CovertoBOAwardAS = new \App\Src\Docs\Awards\AO3CovertoBOAwardAS();
      $docs[$AO3CovertoBOAwardAS->documentFolder][$AO3CovertoBOAwardAS->create($job)] =($AO3CovertoBOAwardAS)->documentName;

      $AO3CovertoBOawardBOS = new \App\Src\Docs\Awards\AO3CovertoBOawardBOS();
      $docs[$AO3CovertoBOawardBOS->documentFolder][$AO3CovertoBOawardBOS->create($job)] =($AO3CovertoBOawardBOS)->documentName;

      $AO3CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO3CovertoBOawardBOSwithINV();
      $docs[$AO3CovertoBOawardBOSwithINV->documentFolder][$AO3CovertoBOawardBOSwithINV->create($job)] =($AO3CovertoBOawardBOSwithINV)->documentName;

      $AO3ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO3ReturningAwardtoBOS();
      $docs[$AO3ReturningAwardtoBOS->documentFolder][$AO3ReturningAwardtoBOS->create($job)] =($AO3ReturningAwardtoBOS)->documentName;

      $AO3SendingawardtotheAOS = new \App\Src\Docs\Awards\AO3SendingawardtotheAOS();
      $docs[$AO3SendingawardtotheAOS->documentFolder][$AO3SendingawardtotheAOS->create($job)] =($AO3SendingawardtotheAOS)->documentName;
      // AO3 END //

      // AO4 START //
      $AO4CoverToAwardAOS = new \App\Src\Docs\Awards\AO4CoverToAwardAOS();
      $docs[$AO4CoverToAwardAOS->documentFolder][$AO4CoverToAwardAOS->create($job)] =($AO4CoverToAwardAOS)->documentName;

      $AO4CovertoBOAwardAS = new \App\Src\Docs\Awards\AO4CovertoBOAwardAS();
      $docs[$AO4CovertoBOAwardAS->documentFolder][$AO4CovertoBOAwardAS->create($job)] =($AO4CovertoBOAwardAS)->documentName;

      $AO4CovertoBOawardBOS = new \App\Src\Docs\Awards\AO4CovertoBOawardBOS();
      $docs[$AO4CovertoBOawardBOS->documentFolder][$AO4CovertoBOawardBOS->create($job)] =($AO4CovertoBOawardBOS)->documentName;

      $AO4CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO4CovertoBOawardBOSwithINV();
      $docs[$AO4CovertoBOawardBOSwithINV->documentFolder][$AO4CovertoBOawardBOSwithINV->create($job)] =($AO4CovertoBOawardBOSwithINV)->documentName;

      $AO4ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO4ReturningAwardtoBOS();
      $docs[$AO4ReturningAwardtoBOS->documentFolder][$AO4ReturningAwardtoBOS->create($job)] =($AO4ReturningAwardtoBOS)->documentName;

      $AO4SendingawardtotheAOS = new \App\Src\Docs\Awards\AO4SendingawardtotheAOS();
      $docs[$AO4SendingawardtotheAOS->documentFolder][$AO4SendingawardtotheAOS->create($job)] =($AO4SendingawardtotheAOS)->documentName;
      // AO4 END //

      // AO5 START //
      $AO5CoverToAwardAOS = new \App\Src\Docs\Awards\AO5CoverToAwardAOS();
      $docs[$AO5CoverToAwardAOS->documentFolder][$AO5CoverToAwardAOS->create($job)] =($AO5CoverToAwardAOS)->documentName;

      $AO5CovertoBOAwardAS = new \App\Src\Docs\Awards\AO5CovertoBOAwardAS();
      $docs[$AO5CovertoBOAwardAS->documentFolder][$AO5CovertoBOAwardAS->create($job)] =($AO5CovertoBOAwardAS)->documentName;

      $AO5CovertoBOawardBOS = new \App\Src\Docs\Awards\AO5CovertoBOawardBOS();
      $docs[$AO5CovertoBOawardBOS->documentFolder][$AO5CovertoBOawardBOS->create($job)] =($AO5CovertoBOawardBOS)->documentName;

      $AO5CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO5CovertoBOawardBOSwithINV();
      $docs[$AO5CovertoBOawardBOSwithINV->documentFolder][$AO5CovertoBOawardBOSwithINV->create($job)] =($AO5CovertoBOawardBOSwithINV)->documentName;

      $AO5ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO5ReturningAwardtoBOS();
      $docs[$AO5ReturningAwardtoBOS->documentFolder][$AO5ReturningAwardtoBOS->create($job)] =($AO5ReturningAwardtoBOS)->documentName;

      $AO5SendingawardtotheAOS = new \App\Src\Docs\Awards\AO5SendingawardtotheAOS();
      $docs[$AO5SendingawardtotheAOS->documentFolder][$AO5SendingawardtotheAOS->create($job)] =($AO5SendingawardtotheAOS)->documentName;
      // AO5 END //

      // AO6 START //
      $AO6CoverToAwardAOS = new \App\Src\Docs\Awards\AO6CoverToAwardAOS();
      $docs[$AO6CoverToAwardAOS->documentFolder][$AO6CoverToAwardAOS->create($job)] =($AO6CoverToAwardAOS)->documentName;

      $AO6CovertoBOAwardAS = new \App\Src\Docs\Awards\AO6CovertoBOAwardAS();
      $docs[$AO6CovertoBOAwardAS->documentFolder][$AO6CovertoBOAwardAS->create($job)] =($AO6CovertoBOAwardAS)->documentName;

      $AO6CovertoBOawardBOS = new \App\Src\Docs\Awards\AO6CovertoBOawardBOS();
      $docs[$AO6CovertoBOawardBOS->documentFolder][$AO6CovertoBOawardBOS->create($job)] =($AO6CovertoBOawardBOS)->documentName;

      $AO6CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO6CovertoBOawardBOSwithINV();
      $docs[$AO6CovertoBOawardBOSwithINV->documentFolder][$AO6CovertoBOawardBOSwithINV->create($job)] =($AO6CovertoBOawardBOSwithINV)->documentName;

      $AO6ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO6ReturningAwardtoBOS();
      $docs[$AO6ReturningAwardtoBOS->documentFolder][$AO6ReturningAwardtoBOS->create($job)] =($AO6ReturningAwardtoBOS)->documentName;

      $AO6SendingawardtotheAOS = new \App\Src\Docs\Awards\AO6SendingawardtotheAOS();
      $docs[$AO6SendingawardtotheAOS->documentFolder][$AO6SendingawardtotheAOS->create($job)] =($AO6SendingawardtotheAOS)->documentName;
      // AO6 END //

      // AO7 START //
      $AO7CoverToAwardAOS = new \App\Src\Docs\Awards\AO7CoverToAwardAOS();
      $docs[$AO7CoverToAwardAOS->documentFolder][$AO7CoverToAwardAOS->create($job)] =($AO7CoverToAwardAOS)->documentName;

      $AO7CovertoBOAwardAS = new \App\Src\Docs\Awards\AO7CovertoBOAwardAS();
      $docs[$AO7CovertoBOAwardAS->documentFolder][$AO7CovertoBOAwardAS->create($job)] =($AO7CovertoBOAwardAS)->documentName;

      $AO7CovertoBOawardBOS = new \App\Src\Docs\Awards\AO7CovertoBOawardBOS();
      $docs[$AO7CovertoBOawardBOS->documentFolder][$AO7CovertoBOawardBOS->create($job)] =($AO7CovertoBOawardBOS)->documentName;

      $AO7CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO7CovertoBOawardBOSwithINV();
      $docs[$AO7CovertoBOawardBOSwithINV->documentFolder][$AO7CovertoBOawardBOSwithINV->create($job)] =($AO7CovertoBOawardBOSwithINV)->documentName;

      $AO7ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO7ReturningAwardtoBOS();
      $docs[$AO7ReturningAwardtoBOS->documentFolder][$AO7ReturningAwardtoBOS->create($job)] =($AO7ReturningAwardtoBOS)->documentName;

      $AO7SendingawardtotheAOS = new \App\Src\Docs\Awards\AO7SendingawardtotheAOS();
      $docs[$AO7SendingawardtotheAOS->documentFolder][$AO7SendingawardtotheAOS->create($job)] =($AO7SendingawardtotheAOS)->documentName;
      // AO7 END //

      // AO8 START //
      $AO8CoverToAwardAOS = new \App\Src\Docs\Awards\AO8CoverToAwardAOS();
      $docs[$AO8CoverToAwardAOS->documentFolder][$AO8CoverToAwardAOS->create($job)] =($AO8CoverToAwardAOS)->documentName;

      $AO8CovertoBOAwardAS = new \App\Src\Docs\Awards\AO8CovertoBOAwardAS();
      $docs[$AO8CovertoBOAwardAS->documentFolder][$AO8CovertoBOAwardAS->create($job)] =($AO8CovertoBOAwardAS)->documentName;

      $AO8CovertoBOawardBOS = new \App\Src\Docs\Awards\AO8CovertoBOawardBOS();
      $docs[$AO8CovertoBOawardBOS->documentFolder][$AO8CovertoBOawardBOS->create($job)] =($AO8CovertoBOawardBOS)->documentName;

      $AO8CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO8CovertoBOawardBOSwithINV();
      $docs[$AO8CovertoBOawardBOSwithINV->documentFolder][$AO8CovertoBOawardBOSwithINV->create($job)] =($AO8CovertoBOawardBOSwithINV)->documentName;

      $AO8ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO8ReturningAwardtoBOS();
      $docs[$AO8ReturningAwardtoBOS->documentFolder][$AO8ReturningAwardtoBOS->create($job)] =($AO8ReturningAwardtoBOS)->documentName;

      $AO8SendingawardtotheAOS = new \App\Src\Docs\Awards\AO8SendingawardtotheAOS();
      $docs[$AO8SendingawardtotheAOS->documentFolder][$AO8SendingawardtotheAOS->create($job)] =($AO8SendingawardtotheAOS)->documentName;
      // AO8 END //

      // AO9 START //
      $AO9CoverToAwardAOS = new \App\Src\Docs\Awards\AO9CoverToAwardAOS();
      $docs[$AO9CoverToAwardAOS->documentFolder][$AO9CoverToAwardAOS->create($job)] =($AO9CoverToAwardAOS)->documentName;

      $AO9CovertoBOAwardAS = new \App\Src\Docs\Awards\AO9CovertoBOAwardAS();
      $docs[$AO9CovertoBOAwardAS->documentFolder][$AO9CovertoBOAwardAS->create($job)] =($AO9CovertoBOAwardAS)->documentName;

      $AO9CovertoBOawardBOS = new \App\Src\Docs\Awards\AO9CovertoBOawardBOS();
      $docs[$AO9CovertoBOawardBOS->documentFolder][$AO9CovertoBOawardBOS->create($job)] =($AO9CovertoBOawardBOS)->documentName;

      $AO9CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO9CovertoBOawardBOSwithINV();
      $docs[$AO9CovertoBOawardBOSwithINV->documentFolder][$AO9CovertoBOawardBOSwithINV->create($job)] =($AO9CovertoBOawardBOSwithINV)->documentName;

      $AO9ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO9ReturningAwardtoBOS();
      $docs[$AO9ReturningAwardtoBOS->documentFolder][$AO9ReturningAwardtoBOS->create($job)] =($AO9ReturningAwardtoBOS)->documentName;

      $AO9SendingawardtotheAOS = new \App\Src\Docs\Awards\AO9SendingawardtotheAOS();
      $docs[$AO9SendingawardtotheAOS->documentFolder][$AO9SendingawardtotheAOS->create($job)] =($AO9SendingawardtotheAOS)->documentName;
      // AO9 END //

      // AO10 START //
      $AO10CoverToAwardAOS = new \App\Src\Docs\Awards\AO10CoverToAwardAOS();
      $docs[$AO10CoverToAwardAOS->documentFolder][$AO10CoverToAwardAOS->create($job)] =($AO10CoverToAwardAOS)->documentName;

      $AO10CovertoBOAwardAS = new \App\Src\Docs\Awards\AO10CovertoBOAwardAS();
      $docs[$AO10CovertoBOAwardAS->documentFolder][$AO10CovertoBOAwardAS->create($job)] =($AO10CovertoBOAwardAS)->documentName;

      $AO10CovertoBOawardBOS = new \App\Src\Docs\Awards\AO10CovertoBOawardBOS();
      $docs[$AO10CovertoBOawardBOS->documentFolder][$AO10CovertoBOawardBOS->create($job)] =($AO10CovertoBOawardBOS)->documentName;

      $AO10CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO10CovertoBOawardBOSwithINV();
      $docs[$AO10CovertoBOawardBOSwithINV->documentFolder][$AO10CovertoBOawardBOSwithINV->create($job)] =($AO10CovertoBOawardBOSwithINV)->documentName;

      $AO10ReturningAwardtoBOS = new \App\Src\Docs\Awards\AO10ReturningAwardtoBOS();
      $docs[$AO10ReturningAwardtoBOS->documentFolder][$AO10ReturningAwardtoBOS->create($job)] =($AO10ReturningAwardtoBOS)->documentName;

      $AO10SendingawardtotheAOS = new \App\Src\Docs\Awards\AO10SendingawardtotheAOS();
      $docs[$AO10SendingawardtotheAOS->documentFolder][$AO10SendingawardtotheAOS->create($job)] =($AO10SendingawardtotheAOS)->documentName;
      // AO10 END //


      // Drafts
      // AO1 START //
      $DraftAO12SurvAward = new \App\Src\Docs\Awards\DraftAO12SurvAward();
      $docs[$DraftAO12SurvAward->documentFolder][$DraftAO12SurvAward->create($job)] =($DraftAO12SurvAward)->documentName;

      $DraftAO1ASAward = new \App\Src\Docs\Awards\DraftAO1ASAward();
      $docs[$DraftAO1ASAward->documentFolder][$DraftAO1ASAward->create($job)] =($DraftAO1ASAward)->documentName;
      // AO1 END //

      // AO2 START //
      $DraftAO22SurvAward = new \App\Src\Docs\Awards\DraftAO22SurvAward();
      $docs[$DraftAO22SurvAward->documentFolder][$DraftAO22SurvAward->create($job)] =($DraftAO22SurvAward)->documentName;

      $DraftAO2ASAward = new \App\Src\Docs\Awards\DraftAO2ASAward();
      $docs[$DraftAO2ASAward->documentFolder][$DraftAO2ASAward->create($job)] =($DraftAO2ASAward)->documentName;
      // AO2 END //

      // AO3 START //
      $DraftAO32SurvAward = new \App\Src\Docs\Awards\DraftAO32SurvAward();
      $docs[$DraftAO32SurvAward->documentFolder][$DraftAO32SurvAward->create($job)] =($DraftAO32SurvAward)->documentName;

      $DraftAO3ASAward = new \App\Src\Docs\Awards\DraftAO3ASAward();
      $docs[$DraftAO3ASAward->documentFolder][$DraftAO3ASAward->create($job)] =($DraftAO3ASAward)->documentName;
      // AO3 END //

      // AO4 START //
      $DraftAO42SurvAward = new \App\Src\Docs\Awards\DraftAO42SurvAward();
      $docs[$DraftAO42SurvAward->documentFolder][$DraftAO42SurvAward->create($job)] =($DraftAO42SurvAward)->documentName;

      $DraftAO4ASAward = new \App\Src\Docs\Awards\DraftAO4ASAward();
      $docs[$DraftAO4ASAward->documentFolder][$DraftAO4ASAward->create($job)] =($DraftAO4ASAward)->documentName;
      // AO4 END //

      // AO5 START //
      $DraftAO52SurvAward = new \App\Src\Docs\Awards\DraftAO52SurvAward();
      $docs[$DraftAO52SurvAward->documentFolder][$DraftAO52SurvAward->create($job)] =($DraftAO52SurvAward)->documentName;

      $DraftAO5ASAward = new \App\Src\Docs\Awards\DraftAO5ASAward();
      $docs[$DraftAO5ASAward->documentFolder][$DraftAO5ASAward->create($job)] =($DraftAO5ASAward)->documentName;
      // AO5 END //

      // AO6 START //
      $DraftAO62SurvAward = new \App\Src\Docs\Awards\DraftAO62SurvAward();
      $docs[$DraftAO62SurvAward->documentFolder][$DraftAO62SurvAward->create($job)] =($DraftAO62SurvAward)->documentName;

      $DraftAO6ASAward = new \App\Src\Docs\Awards\DraftAO6ASAward();
      $docs[$DraftAO6ASAward->documentFolder][$DraftAO6ASAward->create($job)] =($DraftAO6ASAward)->documentName;
      // AO6 END //

      // AO7 START //
      $DraftAO72SurvAward = new \App\Src\Docs\Awards\DraftAO72SurvAward();
      $docs[$DraftAO72SurvAward->documentFolder][$DraftAO72SurvAward->create($job)] =($DraftAO72SurvAward)->documentName;

      $DraftAO7ASAward = new \App\Src\Docs\Awards\DraftAO7ASAward();
      $docs[$DraftAO7ASAward->documentFolder][$DraftAO7ASAward->create($job)] =($DraftAO7ASAward)->documentName;
      // AO7 END //

      // AO8 START //
      $DraftAO82SurvAward = new \App\Src\Docs\Awards\DraftAO82SurvAward();
      $docs[$DraftAO82SurvAward->documentFolder][$DraftAO82SurvAward->create($job)] =($DraftAO82SurvAward)->documentName;

      $DraftAO8ASAward = new \App\Src\Docs\Awards\DraftAO8ASAward();
      $docs[$DraftAO8ASAward->documentFolder][$DraftAO8ASAward->create($job)] =($DraftAO8ASAward)->documentName;
      // AO8 END //

      // AO9 START //
      $DraftAO92SurvAward = new \App\Src\Docs\Awards\DraftAO92SurvAward();
      $docs[$DraftAO92SurvAward->documentFolder][$DraftAO92SurvAward->create($job)] =($DraftAO92SurvAward)->documentName;

      $DraftAO9ASAward = new \App\Src\Docs\Awards\DraftAO9ASAward();
      $docs[$DraftAO9ASAward->documentFolder][$DraftAO9ASAward->create($job)] =($DraftAO9ASAward)->documentName;
      // AO9 END //

      // AO10 START //
      $DraftAO102SurvAward = new \App\Src\Docs\Awards\DraftAO102SurvAward();
      $docs[$DraftAO102SurvAward->documentFolder][$DraftAO102SurvAward->create($job)] =($DraftAO102SurvAward)->documentName;

      $DraftAO10ASAward = new \App\Src\Docs\Awards\DraftAO10ASAward();
      $docs[$DraftAO10ASAward->documentFolder][$DraftAO10ASAward->create($job)] =($DraftAO10ASAward)->documentName;
      // AO10 END //


      // Confirmations
      // AO1 START //
      $AO1ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO1ConfofAOdissenttoBO();
      $docs[$AO1ConfofAOdissenttoBO->documentFolder][$AO1ConfofAOdissenttoBO->create($job)] =($AO1ConfofAOdissenttoBO)->documentName;

      $AO1ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO1ConfofAOdissenttoBOS();
      $docs[$AO1ConfofAOdissenttoBOS->documentFolder][$AO1ConfofAOdissenttoBOS->create($job)] =($AO1ConfofAOdissenttoBOS)->documentName;
      // AO1 END //

      // AO2 START //
      $AO2ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO2ConfofAOdissenttoBO();
      $docs[$AO2ConfofAOdissenttoBO->documentFolder][$AO2ConfofAOdissenttoBO->create($job)] =($AO2ConfofAOdissenttoBO)->documentName;

      $AO2ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO2ConfofAOdissenttoBOS();
      $docs[$AO2ConfofAOdissenttoBOS->documentFolder][$AO2ConfofAOdissenttoBOS->create($job)] =($AO2ConfofAOdissenttoBOS)->documentName;
      // AO2 END //

      // AO3 START //
      $AO3ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO3ConfofAOdissenttoBO();
      $docs[$AO3ConfofAOdissenttoBO->documentFolder][$AO3ConfofAOdissenttoBO->create($job)] =($AO3ConfofAOdissenttoBO)->documentName;

      $AO3ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO3ConfofAOdissenttoBOS();
      $docs[$AO3ConfofAOdissenttoBOS->documentFolder][$AO3ConfofAOdissenttoBOS->create($job)] =($AO3ConfofAOdissenttoBOS)->documentName;
      // AO3 END //

      // AO4 START //
      $AO4ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO4ConfofAOdissenttoBO();
      $docs[$AO4ConfofAOdissenttoBO->documentFolder][$AO4ConfofAOdissenttoBO->create($job)] =($AO4ConfofAOdissenttoBO)->documentName;

      $AO4ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO4ConfofAOdissenttoBOS();
      $docs[$AO4ConfofAOdissenttoBOS->documentFolder][$AO4ConfofAOdissenttoBOS->create($job)] =($AO4ConfofAOdissenttoBOS)->documentName;
      // AO4 END //

      // AO5 START //
      $AO5ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO5ConfofAOdissenttoBO();
      $docs[$AO5ConfofAOdissenttoBO->documentFolder][$AO5ConfofAOdissenttoBO->create($job)] =($AO5ConfofAOdissenttoBO)->documentName;

      $AO5ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO5ConfofAOdissenttoBOS();
      $docs[$AO5ConfofAOdissenttoBOS->documentFolder][$AO5ConfofAOdissenttoBOS->create($job)] =($AO5ConfofAOdissenttoBOS)->documentName;
      // AO5 END //

      // AO6 START //
      $AO6ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO6ConfofAOdissenttoBO();
      $docs[$AO6ConfofAOdissenttoBO->documentFolder][$AO6ConfofAOdissenttoBO->create($job)] =($AO6ConfofAOdissenttoBO)->documentName;

      $AO6ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO6ConfofAOdissenttoBOS();
      $docs[$AO6ConfofAOdissenttoBOS->documentFolder][$AO6ConfofAOdissenttoBOS->create($job)] =($AO6ConfofAOdissenttoBOS)->documentName;
      // AO6 END //

      // AO7 START //
      $AO7ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO7ConfofAOdissenttoBO();
      $docs[$AO7ConfofAOdissenttoBO->documentFolder][$AO7ConfofAOdissenttoBO->create($job)] =($AO7ConfofAOdissenttoBO)->documentName;

      $AO7ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO7ConfofAOdissenttoBOS();
      $docs[$AO7ConfofAOdissenttoBOS->documentFolder][$AO7ConfofAOdissenttoBOS->create($job)] =($AO7ConfofAOdissenttoBOS)->documentName;
      // AO7 END //

      // AO8 START //
      $AO8ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO8ConfofAOdissenttoBO();
      $docs[$AO8ConfofAOdissenttoBO->documentFolder][$AO8ConfofAOdissenttoBO->create($job)] =($AO8ConfofAOdissenttoBO)->documentName;

      $AO8ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO8ConfofAOdissenttoBOS();
      $docs[$AO8ConfofAOdissenttoBOS->documentFolder][$AO8ConfofAOdissenttoBOS->create($job)] =($AO8ConfofAOdissenttoBOS)->documentName;
      // AO8 END //

      // AO9 START //
      $AO9ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO9ConfofAOdissenttoBO();
      $docs[$AO9ConfofAOdissenttoBO->documentFolder][$AO9ConfofAOdissenttoBO->create($job)] =($AO9ConfofAOdissenttoBO)->documentName;

      $AO9ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO9ConfofAOdissenttoBOS();
      $docs[$AO9ConfofAOdissenttoBOS->documentFolder][$AO9ConfofAOdissenttoBOS->create($job)] =($AO9ConfofAOdissenttoBOS)->documentName;
      // AO9 END //

      // AO10 START //
      $AO10ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO10ConfofAOdissenttoBO();
      $docs[$AO10ConfofAOdissenttoBO->documentFolder][$AO10ConfofAOdissenttoBO->create($job)] =($AO10ConfofAOdissenttoBO)->documentName;

      $AO10ConfofAOdissenttoBOS = new \App\Src\Docs\ConfirmationOfDissent\AO10ConfofAOdissenttoBOS();
      $docs[$AO10ConfofAOdissenttoBOS->documentFolder][$AO10ConfofAOdissenttoBOS->create($job)] =($AO10ConfofAOdissenttoBOS)->documentName;
      // AO10 END //


      // AO1CovertoBOFinalLetter

      // AO1 START //
      $AO1CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO1CovertoBOFinalLetterAS();
      $docs[$AO1CovertoBOFinalLetterAS->documentFolder][$AO1CovertoBOFinalLetterAS->create($job)] =($AO1CovertoBOFinalLetterAS)->documentName;

      $AO1FinalLetter = new \App\Src\Docs\FinalLetters\AO1FinalLetter();
      $docs[$AO1FinalLetter->documentFolder][$AO1FinalLetter->create($job)] =($AO1FinalLetter)->documentName;
      
      $AO1FinalLetterAS = new \App\Src\Docs\FinalLetters\AO1FinalLetterAS();
      $docs[$AO1FinalLetterAS->documentFolder][$AO1FinalLetterAS->create($job)] =($AO1FinalLetterAS)->documentName;
      // AO1 END //

      // AO2 START  //
      $AO2CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO2CovertoBOFinalLetterAS();
      $docs[$AO2CovertoBOFinalLetterAS->documentFolder][$AO2CovertoBOFinalLetterAS->create($job)] =($AO2CovertoBOFinalLetterAS)->documentName;

      $AO2FinalLetter = new \App\Src\Docs\FinalLetters\AO2FinalLetter();
      $docs[$AO2FinalLetter->documentFolder][$AO2FinalLetter->create($job)] =($AO2FinalLetter)->documentName;
      
      $AO2FinalLetterAS = new \App\Src\Docs\FinalLetters\AO2FinalLetterAS();
      $docs[$AO2FinalLetterAS->documentFolder][$AO2FinalLetterAS->create($job)] =($AO2FinalLetterAS)->documentName;
      // AO2 END //

      // AO3 START  //
      $AO3CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO3CovertoBOFinalLetterAS();
      $docs[$AO3CovertoBOFinalLetterAS->documentFolder][$AO3CovertoBOFinalLetterAS->create($job)] =($AO3CovertoBOFinalLetterAS)->documentName;

      $AO3FinalLetter = new \App\Src\Docs\FinalLetters\AO3FinalLetter();
      $docs[$AO3FinalLetter->documentFolder][$AO3FinalLetter->create($job)] =($AO3FinalLetter)->documentName;
      
      $AO3FinalLetterAS = new \App\Src\Docs\FinalLetters\AO3FinalLetterAS();
      $docs[$AO3FinalLetterAS->documentFolder][$AO3FinalLetterAS->create($job)] =($AO3FinalLetterAS)->documentName;
      // AO3 END //

      // AO4 START  //
      $AO4CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO4CovertoBOFinalLetterAS();
      $docs[$AO4CovertoBOFinalLetterAS->documentFolder][$AO4CovertoBOFinalLetterAS->create($job)] =($AO4CovertoBOFinalLetterAS)->documentName;

      $AO4FinalLetter = new \App\Src\Docs\FinalLetters\AO4FinalLetter();
      $docs[$AO4FinalLetter->documentFolder][$AO4FinalLetter->create($job)] =($AO4FinalLetter)->documentName;
      
      $AO4FinalLetterAS = new \App\Src\Docs\FinalLetters\AO4FinalLetterAS();
      $docs[$AO4FinalLetterAS->documentFolder][$AO4FinalLetterAS->create($job)] =($AO4FinalLetterAS)->documentName;
      // AO4 END //

      // AO5 START  //
      $AO5CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO5CovertoBOFinalLetterAS();
      $docs[$AO5CovertoBOFinalLetterAS->documentFolder][$AO5CovertoBOFinalLetterAS->create($job)] =($AO5CovertoBOFinalLetterAS)->documentName;

      $AO5FinalLetter = new \App\Src\Docs\FinalLetters\AO5FinalLetter();
      $docs[$AO5FinalLetter->documentFolder][$AO5FinalLetter->create($job)] =($AO5FinalLetter)->documentName;
      
      $AO5FinalLetterAS = new \App\Src\Docs\FinalLetters\AO5FinalLetterAS();
      $docs[$AO5FinalLetterAS->documentFolder][$AO5FinalLetterAS->create($job)] =($AO5FinalLetterAS)->documentName;
      // AO5 END //

      // AO6 START  //
      $AO6CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO6CovertoBOFinalLetterAS();
      $docs[$AO6CovertoBOFinalLetterAS->documentFolder][$AO6CovertoBOFinalLetterAS->create($job)] =($AO6CovertoBOFinalLetterAS)->documentName;

      $AO6FinalLetter = new \App\Src\Docs\FinalLetters\AO6FinalLetter();
      $docs[$AO6FinalLetter->documentFolder][$AO6FinalLetter->create($job)] =($AO6FinalLetter)->documentName;
      
      $AO6FinalLetterAS = new \App\Src\Docs\FinalLetters\AO6FinalLetterAS();
      $docs[$AO6FinalLetterAS->documentFolder][$AO6FinalLetterAS->create($job)] =($AO6FinalLetterAS)->documentName;
      // AO6 END //

      // AO7 START  //
      $AO7CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO7CovertoBOFinalLetterAS();
      $docs[$AO7CovertoBOFinalLetterAS->documentFolder][$AO7CovertoBOFinalLetterAS->create($job)] =($AO7CovertoBOFinalLetterAS)->documentName;

      $AO7FinalLetter = new \App\Src\Docs\FinalLetters\AO7FinalLetter();
      $docs[$AO7FinalLetter->documentFolder][$AO7FinalLetter->create($job)] =($AO7FinalLetter)->documentName;
      
      $AO7FinalLetterAS = new \App\Src\Docs\FinalLetters\AO7FinalLetterAS();
      $docs[$AO7FinalLetterAS->documentFolder][$AO7FinalLetterAS->create($job)] =($AO7FinalLetterAS)->documentName;
      // AO7 END //

      // AO8 START  //
      $AO8CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO8CovertoBOFinalLetterAS();
      $docs[$AO8CovertoBOFinalLetterAS->documentFolder][$AO8CovertoBOFinalLetterAS->create($job)] =($AO8CovertoBOFinalLetterAS)->documentName;

      $AO8FinalLetter = new \App\Src\Docs\FinalLetters\AO8FinalLetter();
      $docs[$AO8FinalLetter->documentFolder][$AO8FinalLetter->create($job)] =($AO8FinalLetter)->documentName;
      
      $AO8FinalLetterAS = new \App\Src\Docs\FinalLetters\AO8FinalLetterAS();
      $docs[$AO8FinalLetterAS->documentFolder][$AO8FinalLetterAS->create($job)] =($AO8FinalLetterAS)->documentName;
      // AO8 END //

      // AO9 START  //
      $AO9CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO9CovertoBOFinalLetterAS();
      $docs[$AO9CovertoBOFinalLetterAS->documentFolder][$AO9CovertoBOFinalLetterAS->create($job)] =($AO9CovertoBOFinalLetterAS)->documentName;

      $AO9FinalLetter = new \App\Src\Docs\FinalLetters\AO9FinalLetter();
      $docs[$AO9FinalLetter->documentFolder][$AO9FinalLetter->create($job)] =($AO9FinalLetter)->documentName;

      $AO9FinalLetterAS = new \App\Src\Docs\FinalLetters\AO9FinalLetterAS();
      $docs[$AO9FinalLetterAS->documentFolder][$AO9FinalLetterAS->create($job)] =($AO9FinalLetterAS)->documentName;
      // AO9 END //

      // AO10 START  //
      $AO10CovertoBOFinalLetterAS = new \App\Src\Docs\FinalLetters\AO10CovertoBOFinalLetterAS();
      $docs[$AO10CovertoBOFinalLetterAS->documentFolder][$AO10CovertoBOFinalLetterAS->create($job)] =($AO10CovertoBOFinalLetterAS)->documentName;

      $AO10FinalLetter = new \App\Src\Docs\FinalLetters\AO10FinalLetter();
      $docs[$AO10FinalLetter->documentFolder][$AO10FinalLetter->create($job)] =($AO10FinalLetter)->documentName;
      
      $AO10FinalLetterAS = new \App\Src\Docs\FinalLetters\AO10FinalLetterAS();
      $docs[$AO10FinalLetterAS->documentFolder][$AO10FinalLetterAS->create($job)] =($AO10FinalLetterAS)->documentName;
      // AO10 END //

      // AO1LetterBeforeActiontoBOAOS
      $AO1LetterBeforeActiontoBOAOS = new \App\Src\Docs\Invoice\AO1LetterBeforeActiontoBOAOS();
      $docs[$AO1LetterBeforeActiontoBOAOS->documentFolder][$AO1LetterBeforeActiontoBOAOS->create($job)] =($AO1LetterBeforeActiontoBOAOS)->documentName;

      $AO1LetterBeforeActiontoBOAS = new \App\Src\Docs\Invoice\AO1LetterBeforeActiontoBOAS();
      $docs[$AO1LetterBeforeActiontoBOAS->documentFolder][$AO1LetterBeforeActiontoBOAS->create($job)] =($AO1LetterBeforeActiontoBOAS)->documentName;

      $AO1LettertoBOFormalrequestforPaymentAS = new \App\Src\Docs\Invoice\AO1LettertoBOFormalrequestforPaymentAS();
      $docs[$AO1LettertoBOFormalrequestforPaymentAS->documentFolder][$AO1LettertoBOFormalrequestforPaymentAS->create($job)] =($AO1LettertoBOFormalrequestforPaymentAS)->documentName;


      $AO1LettertoBOFormalrequestforpaymentAOS1 = new \App\Src\Docs\Invoice\AO1LettertoBOFormalrequestforpaymentAOS1();
      $docs[$AO1LettertoBOFormalrequestforpaymentAOS1->documentFolder][$AO1LettertoBOFormalrequestforpaymentAOS1->create($job)] =($AO1LettertoBOFormalrequestforpaymentAOS1)->documentName;


      $AO2LetterBeforeActiontoBOAOS = new \App\Src\Docs\Invoice\AO2LetterBeforeActiontoBOAOS();
      $docs[$AO2LetterBeforeActiontoBOAOS->documentFolder][$AO2LetterBeforeActiontoBOAOS->create($job)] =($AO2LetterBeforeActiontoBOAOS)->documentName;


      $AO2LetterBeforeActiontoBOAS = new \App\Src\Docs\Invoice\AO2LetterBeforeActiontoBOAS();
      $docs[$AO2LetterBeforeActiontoBOAS->documentFolder][$AO2LetterBeforeActiontoBOAS->create($job)] =($AO2LetterBeforeActiontoBOAS)->documentName;

      $AO2LettertoBOFormalrequestforpaymentAOS2 = new \App\Src\Docs\Invoice\AO2LettertoBOFormalrequestforpaymentAOS2();
      $docs[$AO2LettertoBOFormalrequestforpaymentAOS2->documentFolder][$AO2LettertoBOFormalrequestforpaymentAOS2->create($job)] =($AO2LettertoBOFormalrequestforpaymentAOS2)->documentName;


      $AO3LettertoBOFormalrequestforpaymentAOS3 = new \App\Src\Docs\Invoice\AO3LettertoBOFormalrequestforpaymentAOS3();
      $docs[$AO3LettertoBOFormalrequestforpaymentAOS3->documentFolder][$AO3LettertoBOFormalrequestforpaymentAOS3->create($job)] =($AO3LettertoBOFormalrequestforpaymentAOS3)->documentName;

      $AO4LettertoBOFormalrequestforpaymentAOS4 = new \App\Src\Docs\Invoice\AO4LettertoBOFormalrequestforpaymentAOS4();
      $docs[$AO4LettertoBOFormalrequestforpaymentAOS4->documentFolder][$AO4LettertoBOFormalrequestforpaymentAOS4->create($job)] =($AO4LettertoBOFormalrequestforpaymentAOS4)->documentName;

      $AO5LettertoBOFormalrequestforpaymentAOS5 = new \App\Src\Docs\Invoice\AO5LettertoBOFormalrequestforpaymentAOS5();
      $docs[$AO5LettertoBOFormalrequestforpaymentAOS5->documentFolder][$AO5LettertoBOFormalrequestforpaymentAOS5->create($job)] =($AO5LettertoBOFormalrequestforpaymentAOS5)->documentName;

      $AO6LettertoBOFormalrequestforpaymentAOS6 = new \App\Src\Docs\Invoice\AO6LettertoBOFormalrequestforpaymentAOS6();
      $docs[$AO6LettertoBOFormalrequestforpaymentAOS6->documentFolder][$AO6LettertoBOFormalrequestforpaymentAOS6->create($job)] =($AO6LettertoBOFormalrequestforpaymentAOS6)->documentName;

      $AO7LettertoBOFormalrequestforpaymentAOS7 = new \App\Src\Docs\Invoice\AO7LettertoBOFormalrequestforpaymentAOS7();
      $docs[$AO7LettertoBOFormalrequestforpaymentAOS7->documentFolder][$AO7LettertoBOFormalrequestforpaymentAOS7->create($job)] =($AO7LettertoBOFormalrequestforpaymentAOS7)->documentName;

      $AO8LettertoBOFormalrequestforpaymentAOS8 = new \App\Src\Docs\Invoice\AO8LettertoBOFormalrequestforpaymentAOS8();
      $docs[$AO8LettertoBOFormalrequestforpaymentAOS8->documentFolder][$AO8LettertoBOFormalrequestforpaymentAOS8->create($job)] =($AO8LettertoBOFormalrequestforpaymentAOS8)->documentName;

      $AO9LettertoBOFormalrequestforpaymentAOS9 = new \App\Src\Docs\Invoice\AO9LettertoBOFormalrequestforpaymentAOS9();
      $docs[$AO9LettertoBOFormalrequestforpaymentAOS9->documentFolder][$AO9LettertoBOFormalrequestforpaymentAOS9->create($job)] =($AO9LettertoBOFormalrequestforpaymentAOS9)->documentName;

      $AO10LettertoBOFormalrequestforpaymentAOS10 = new \App\Src\Docs\Invoice\AO10LettertoBOFormalrequestforpaymentAOS10();
      $docs[$AO10LettertoBOFormalrequestforpaymentAOS10->documentFolder][$AO10LettertoBOFormalrequestforpaymentAOS10->create($job)] =($AO10LettertoBOFormalrequestforpaymentAOS10)->documentName;

      $InvoiceNoBLPW = new \App\Src\Docs\Invoice\InvoiceNoBLPW();
      $docs[$InvoiceNoBLPW->documentFolder][$InvoiceNoBLPW->create($job)] =($InvoiceNoBLPW)->documentName;
      
  
      //  job info
      $JobInfo = new \App\Src\Docs\JobInformation\JobInfo();
      $docs[$JobInfo->documentFolder][$JobInfo->create($job)] =($JobInfo)->documentName;
      $docs[$JobInfo->drawingFolder]['']='';
      $docs[$JobInfo->landRegistyFolder]['']='';
      $docs[$JobInfo->scannedFolder]['']='';
      

      //  App\Docs\NoticesAndCovers

      // AO1 START //
      $AO1104Letter = new \App\Src\Docs\NoticesAndCovers\AO1104Letter();
      $docs[$AO1104Letter->documentFolder][$AO1104Letter->create($job)] =($AO1104Letter)->documentName;

      $AO1AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO1AllNoticesandAck();
      $docs[$AO1AllNoticesandAck->documentFolder][$AO1AllNoticesandAck->create($job)] =($AO1AllNoticesandAck)->documentName;

      $AO1Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO1Confof104Appointment();
      $docs[$AO1Confof104Appointment->documentFolder][$AO1Confof104Appointment->create($job)] =($AO1Confof104Appointment)->documentName;

      $AO1ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO1ConfofBLS104Appointment();
      $docs[$AO1ConfofBLS104Appointment->documentFolder][$AO1ConfofBLS104Appointment->create($job)] =($AO1ConfofBLS104Appointment)->documentName;

      $AO1CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO1CovertoNoticeStandard();
      $docs[$AO1CovertoNoticeStandard->documentFolder][$AO1CovertoNoticeStandard->create($job)] =($AO1CovertoNoticeStandard)->documentName;
      // AO1 END //

      // AO2 START //
      $AO2104Letter = new \App\Src\Docs\NoticesAndCovers\AO2104Letter();
      $docs[$AO2104Letter->documentFolder][$AO2104Letter->create($job)] =($AO2104Letter)->documentName;

      $AO2AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO2AllNoticesandAck();
      $docs[$AO2AllNoticesandAck->documentFolder][$AO2AllNoticesandAck->create($job)] =($AO2AllNoticesandAck)->documentName;

      $AO2Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO2Confof104Appointment();
      $docs[$AO2Confof104Appointment->documentFolder][$AO2Confof104Appointment->create($job)] =($AO2Confof104Appointment)->documentName;

      $AO2ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO2ConfofBLS104Appointment();
      $docs[$AO2ConfofBLS104Appointment->documentFolder][$AO2ConfofBLS104Appointment->create($job)] =($AO2ConfofBLS104Appointment)->documentName;

      $AO2CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO2CovertoNoticeStandard();
      $docs[$AO2CovertoNoticeStandard->documentFolder][$AO2CovertoNoticeStandard->create($job)] =($AO2CovertoNoticeStandard)->documentName;
      // AO2 END //

      // AO3 START //
      $AO3104Letter = new \App\Src\Docs\NoticesAndCovers\AO3104Letter();
      $docs[$AO3104Letter->documentFolder][$AO3104Letter->create($job)] =($AO3104Letter)->documentName;

      $AO3AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO3AllNoticesandAck();
      $docs[$AO3AllNoticesandAck->documentFolder][$AO3AllNoticesandAck->create($job)] =($AO3AllNoticesandAck)->documentName;

      $AO3Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO3Confof104Appointment();
      $docs[$AO3Confof104Appointment->documentFolder][$AO3Confof104Appointment->create($job)] =($AO3Confof104Appointment)->documentName;

      $AO3ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO3ConfofBLS104Appointment();
      $docs[$AO3ConfofBLS104Appointment->documentFolder][$AO3ConfofBLS104Appointment->create($job)] =($AO3ConfofBLS104Appointment)->documentName;

      $AO3CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO3CovertoNoticeStandard();
      $docs[$AO3CovertoNoticeStandard->documentFolder][$AO3CovertoNoticeStandard->create($job)] =($AO3CovertoNoticeStandard)->documentName;
      // AO3 END //

      // AO4 START //
      $AO4104Letter = new \App\Src\Docs\NoticesAndCovers\AO4104Letter();
      $docs[$AO4104Letter->documentFolder][$AO4104Letter->create($job)] =($AO4104Letter)->documentName;

      $AO4AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO4AllNoticesandAck();
      $docs[$AO4AllNoticesandAck->documentFolder][$AO4AllNoticesandAck->create($job)] =($AO4AllNoticesandAck)->documentName;

      $AO4Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO4Confof104Appointment();
      $docs[$AO4Confof104Appointment->documentFolder][$AO4Confof104Appointment->create($job)] =($AO4Confof104Appointment)->documentName;

      $AO4ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO4ConfofBLS104Appointment();
      $docs[$AO4ConfofBLS104Appointment->documentFolder][$AO4ConfofBLS104Appointment->create($job)] =($AO4ConfofBLS104Appointment)->documentName;

      $AO4CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO4CovertoNoticeStandard();
      $docs[$AO4CovertoNoticeStandard->documentFolder][$AO4CovertoNoticeStandard->create($job)] =($AO4CovertoNoticeStandard)->documentName;
      // AO4 END //

      // AO5 START //
      $AO5104Letter = new \App\Src\Docs\NoticesAndCovers\AO5104Letter();
      $docs[$AO5104Letter->documentFolder][$AO5104Letter->create($job)] =($AO5104Letter)->documentName;

      $AO5AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO5AllNoticesandAck();
      $docs[$AO5AllNoticesandAck->documentFolder][$AO5AllNoticesandAck->create($job)] =($AO5AllNoticesandAck)->documentName;

      $AO5Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO5Confof104Appointment();
      $docs[$AO5Confof104Appointment->documentFolder][$AO5Confof104Appointment->create($job)] =($AO5Confof104Appointment)->documentName;

      $AO5ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO5ConfofBLS104Appointment();
      $docs[$AO5ConfofBLS104Appointment->documentFolder][$AO5ConfofBLS104Appointment->create($job)] =($AO5ConfofBLS104Appointment)->documentName;

      $AO5CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO5CovertoNoticeStandard();
      $docs[$AO5CovertoNoticeStandard->documentFolder][$AO5CovertoNoticeStandard->create($job)] =($AO5CovertoNoticeStandard)->documentName;
      // AO5 END //

      // AO6 START //
      $AO6104Letter = new \App\Src\Docs\NoticesAndCovers\AO6104Letter();
      $docs[$AO6104Letter->documentFolder][$AO6104Letter->create($job)] =($AO6104Letter)->documentName;

      $AO6AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO6AllNoticesandAck();
      $docs[$AO6AllNoticesandAck->documentFolder][$AO6AllNoticesandAck->create($job)] =($AO6AllNoticesandAck)->documentName;

      $AO6Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO6Confof104Appointment();
      $docs[$AO6Confof104Appointment->documentFolder][$AO6Confof104Appointment->create($job)] =($AO6Confof104Appointment)->documentName;

      $AO6ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO6ConfofBLS104Appointment();
      $docs[$AO6ConfofBLS104Appointment->documentFolder][$AO6ConfofBLS104Appointment->create($job)] =($AO6ConfofBLS104Appointment)->documentName;

      $AO6CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO6CovertoNoticeStandard();
      $docs[$AO6CovertoNoticeStandard->documentFolder][$AO6CovertoNoticeStandard->create($job)] =($AO6CovertoNoticeStandard)->documentName;
      // AO6 END //

      // AO7 START //
      $AO7104Letter = new \App\Src\Docs\NoticesAndCovers\AO7104Letter();
      $docs[$AO7104Letter->documentFolder][$AO7104Letter->create($job)] =($AO7104Letter)->documentName;

      $AO7AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO7AllNoticesandAck();
      $docs[$AO7AllNoticesandAck->documentFolder][$AO7AllNoticesandAck->create($job)] =($AO7AllNoticesandAck)->documentName;

      $AO7Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO7Confof104Appointment();
      $docs[$AO7Confof104Appointment->documentFolder][$AO7Confof104Appointment->create($job)] =($AO7Confof104Appointment)->documentName;

      $AO7ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO7ConfofBLS104Appointment();
      $docs[$AO7ConfofBLS104Appointment->documentFolder][$AO7ConfofBLS104Appointment->create($job)] =($AO7ConfofBLS104Appointment)->documentName;

      $AO7CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO7CovertoNoticeStandard();
      $docs[$AO7CovertoNoticeStandard->documentFolder][$AO7CovertoNoticeStandard->create($job)] =($AO7CovertoNoticeStandard)->documentName;
      // AO7 END //

      // AO8 START //
      $AO8104Letter = new \App\Src\Docs\NoticesAndCovers\AO8104Letter();
      $docs[$AO8104Letter->documentFolder][$AO8104Letter->create($job)] =($AO8104Letter)->documentName;

      $AO8AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO8AllNoticesandAck();
      $docs[$AO8AllNoticesandAck->documentFolder][$AO8AllNoticesandAck->create($job)] =($AO8AllNoticesandAck)->documentName;

      $AO8Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO8Confof104Appointment();
      $docs[$AO8Confof104Appointment->documentFolder][$AO8Confof104Appointment->create($job)] =($AO8Confof104Appointment)->documentName;

      $AO8ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO8ConfofBLS104Appointment();
      $docs[$AO8ConfofBLS104Appointment->documentFolder][$AO8ConfofBLS104Appointment->create($job)] =($AO8ConfofBLS104Appointment)->documentName;

      $AO8CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO8CovertoNoticeStandard();
      $docs[$AO8CovertoNoticeStandard->documentFolder][$AO8CovertoNoticeStandard->create($job)] =($AO8CovertoNoticeStandard)->documentName;
      // AO8 END //

      // AO9 START //
      $AO9104Letter = new \App\Src\Docs\NoticesAndCovers\AO9104Letter();
      $docs[$AO9104Letter->documentFolder][$AO9104Letter->create($job)] =($AO9104Letter)->documentName;

      $AO9AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO9AllNoticesandAck();
      $docs[$AO9AllNoticesandAck->documentFolder][$AO9AllNoticesandAck->create($job)] =($AO9AllNoticesandAck)->documentName;

      $AO9Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO9Confof104Appointment();
      $docs[$AO9Confof104Appointment->documentFolder][$AO9Confof104Appointment->create($job)] =($AO9Confof104Appointment)->documentName;

      $AO9ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO9ConfofBLS104Appointment();
      $docs[$AO9ConfofBLS104Appointment->documentFolder][$AO9ConfofBLS104Appointment->create($job)] =($AO9ConfofBLS104Appointment)->documentName;

      $AO9CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO9CovertoNoticeStandard();
      $docs[$AO9CovertoNoticeStandard->documentFolder][$AO9CovertoNoticeStandard->create($job)] =($AO9CovertoNoticeStandard)->documentName;
      // AO9 END //

      // AO10 START //
      $AO10104Letter = new \App\Src\Docs\NoticesAndCovers\AO10104Letter();
      $docs[$AO10104Letter->documentFolder][$AO10104Letter->create($job)] =($AO10104Letter)->documentName;

      $AO10AllNoticesandAck = new \App\Src\Docs\NoticesAndCovers\AO10AllNoticesandAck();
      $docs[$AO10AllNoticesandAck->documentFolder][$AO10AllNoticesandAck->create($job)] =($AO10AllNoticesandAck)->documentName;

      $AO10Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO10Confof104Appointment();
      $docs[$AO10Confof104Appointment->documentFolder][$AO10Confof104Appointment->create($job)] =($AO10Confof104Appointment)->documentName;

      $AO10ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO10ConfofBLS104Appointment();
      $docs[$AO10ConfofBLS104Appointment->documentFolder][$AO10ConfofBLS104Appointment->create($job)] =($AO10ConfofBLS104Appointment)->documentName;

      $AO10CovertoNoticeStandard = new \App\Src\Docs\NoticesAndCovers\AO10CovertoNoticeStandard();
      $docs[$AO10CovertoNoticeStandard->documentFolder][$AO10CovertoNoticeStandard->create($job)] =($AO10CovertoNoticeStandard)->documentName;
      // AO10 END //

      //  App\Docs\ScheduleOfCondition
      $AO1ConfofSOCtoBO = new \App\Src\Docs\ScheduleOfCondition\AO1ConfofSOCtoBO();
      $docs[$AO1ConfofSOCtoBO->documentFolder][$AO1ConfofSOCtoBO->create($job)] =($AO1ConfofSOCtoBO)->documentName;

      $AO1CovertoBOSOC = new \App\Src\Docs\ScheduleOfCondition\AO1CovertoBOSOC();
      $docs[$AO1CovertoBOSOC->documentFolder][$AO1CovertoBOSOC->create($job)] =($AO1CovertoBOSOC)->documentName;

      $AO1CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO1CovertoSOC();
      $docs[$AO1CovertoSOC->documentFolder][$AO1CovertoSOC->create($job)] =($AO1CovertoSOC)->documentName;

      $AO2ConfofSOCtoBO = new \App\Src\Docs\ScheduleOfCondition\AO2ConfofSOCtoBO();
      $docs[$AO2ConfofSOCtoBO->documentFolder][$AO2ConfofSOCtoBO->create($job)] =($AO2ConfofSOCtoBO)->documentName;

      $AO2CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO2CovertoSOC();
      $docs[$AO2CovertoSOC->documentFolder][$AO2CovertoSOC->create($job)] =($AO2CovertoSOC)->documentName;

      $AO3CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO3CovertoSOC();
      $docs[$AO3CovertoSOC->documentFolder][$AO3CovertoSOC->create($job)] =($AO3CovertoSOC)->documentName;

      $AO4CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO4CovertoSOC();
      $docs[$AO4CovertoSOC->documentFolder][$AO4CovertoSOC->create($job)] =($AO4CovertoSOC)->documentName;

      $AO5CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO5CovertoSOC();
      $docs[$AO5CovertoSOC->documentFolder][$AO5CovertoSOC->create($job)] =($AO5CovertoSOC)->documentName;

      $AO6CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO6CovertoSOC();
      $docs[$AO6CovertoSOC->documentFolder][$AO6CovertoSOC->create($job)] =($AO6CovertoSOC)->documentName;

      $AO7CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO7CovertoSOC();
      $docs[$AO7CovertoSOC->documentFolder][$AO7CovertoSOC->create($job)] =($AO7CovertoSOC)->documentName;

      $AO8CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO8CovertoSOC();
      $docs[$AO8CovertoSOC->documentFolder][$AO8CovertoSOC->create($job)] =($AO8CovertoSOC)->documentName;

      $AO9CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO9CovertoSOC();
      $docs[$AO9CovertoSOC->documentFolder][$AO9CovertoSOC->create($job)] =($AO9CovertoSOC)->documentName;

      $AO10CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO10CovertoSOC();
      $docs[$AO10CovertoSOC->documentFolder][$AO10CovertoSOC->create($job)] =($AO10CovertoSOC)->documentName;

      // section 10
      $AO1107LettertoAOS = new \App\Src\Docs\Section10\AO1107LettertoAOS();
      $docs[$AO1107LettertoAOS->documentFolder][$AO1107LettertoAOS->create($job)] =($AO1107LettertoAOS)->documentName;

      $AO2107LettertoAOS = new \App\Src\Docs\Section10\AO2107LettertoAOS();
      $docs[$AO2107LettertoAOS->documentFolder][$AO2107LettertoAOS->create($job)] =($AO2107LettertoAOS)->documentName;

      $AO3107LettertoAOS = new \App\Src\Docs\Section10\AO3107LettertoAOS();
      $docs[$AO3107LettertoAOS->documentFolder][$AO3107LettertoAOS->create($job)] =($AO3107LettertoAOS)->documentName;

      $AO4107LettertoAOS = new \App\Src\Docs\Section10\AO4107LettertoAOS();
      $docs[$AO4107LettertoAOS->documentFolder][$AO4107LettertoAOS->create($job)] =($AO4107LettertoAOS)->documentName;

      $AO5107LettertoAOS = new \App\Src\Docs\Section10\AO5107LettertoAOS();
      $docs[$AO5107LettertoAOS->documentFolder][$AO5107LettertoAOS->create($job)] =($AO5107LettertoAOS)->documentName;

      $AO6107LettertoAOS = new \App\Src\Docs\Section10\AO6107LettertoAOS();
      $docs[$AO6107LettertoAOS->documentFolder][$AO6107LettertoAOS->create($job)] =($AO6107LettertoAOS)->documentName;

      $AO7107LettertoAOS = new \App\Src\Docs\Section10\AO7107LettertoAOS();
      $docs[$AO7107LettertoAOS->documentFolder][$AO7107LettertoAOS->create($job)] =($AO7107LettertoAOS)->documentName;

      $AO8107LettertoAOS = new \App\Src\Docs\Section10\AO8107LettertoAOS();
      $docs[$AO8107LettertoAOS->documentFolder][$AO8107LettertoAOS->create($job)] =($AO8107LettertoAOS)->documentName;

      $AO9107LettertoAOS = new \App\Src\Docs\Section10\AO9107LettertoAOS();
      $docs[$AO9107LettertoAOS->documentFolder][$AO9107LettertoAOS->create($job)] =($AO9107LettertoAOS)->documentName;

      $AO10107LettertoAOS = new \App\Src\Docs\Section10\AO10107LettertoAOS();
      $docs[$AO10107LettertoAOS->documentFolder][$AO10107LettertoAOS->create($job)] =($AO10107LettertoAOS)->documentName;
  
      // section 12
      $AO1toBOSection12request = new \App\Src\Docs\Section12Request\AO1toBOSection12request();
      $docs[$AO1toBOSection12request->documentFolder][$AO1toBOSection12request->create($job)] =($AO1toBOSection12request)->documentName;

      $AO2toBOSection12request = new \App\Src\Docs\Section12Request\AO2toBOSection12request();
      $docs[$AO2toBOSection12request->documentFolder][$AO2toBOSection12request->create($job)] =($AO2toBOSection12request)->documentName;

      $AO3toBOSection12request = new \App\Src\Docs\Section12Request\AO3toBOSection12request();
      $docs[$AO3toBOSection12request->documentFolder][$AO3toBOSection12request->create($job)] =($AO3toBOSection12request)->documentName;

      $AO4toBOSection12request = new \App\Src\Docs\Section12Request\AO4toBOSection12request();
      $docs[$AO4toBOSection12request->documentFolder][$AO4toBOSection12request->create($job)] =($AO4toBOSection12request)->documentName;

      $AO5toBOSection12request = new \App\Src\Docs\Section12Request\AO5toBOSection12request();
      $docs[$AO5toBOSection12request->documentFolder][$AO5toBOSection12request->create($job)] =($AO5toBOSection12request)->documentName;

      $AO6toBOSection12request = new \App\Src\Docs\Section12Request\AO6toBOSection12request();
      $docs[$AO6toBOSection12request->documentFolder][$AO6toBOSection12request->create($job)] =($AO6toBOSection12request)->documentName;

      $AO7toBOSection12request = new \App\Src\Docs\Section12Request\AO7toBOSection12request();
      $docs[$AO7toBOSection12request->documentFolder][$AO7toBOSection12request->create($job)] =($AO7toBOSection12request)->documentName;

      $AO8toBOSection12request = new \App\Src\Docs\Section12Request\AO8toBOSection12request();
      $docs[$AO8toBOSection12request->documentFolder][$AO8toBOSection12request->create($job)] =($AO8toBOSection12request)->documentName;

      $AO9toBOSection12request = new \App\Src\Docs\Section12Request\AO9toBOSection12request();
      $docs[$AO9toBOSection12request->documentFolder][$AO9toBOSection12request->create($job)] =($AO9toBOSection12request)->documentName;

      $AO10toBOSection12request = new \App\Src\Docs\Section12Request\AO10toBOSection12request();
      $docs[$AO10toBOSection12request->documentFolder][$AO10toBOSection12request->create($job)] =($AO10toBOSection12request)->documentName;

       // section 8 AO1RequestforAccess
      $AO1RequestforAccess = new \App\Src\Docs\Section8\AO1RequestforAccess();
      $docs[$AO1RequestforAccess->documentFolder][$AO1RequestforAccess->create($job)] =($AO1RequestforAccess)->documentName;

      $AO2RequestforAccess = new \App\Src\Docs\Section8\AO2RequestforAccess();
      $docs[$AO2RequestforAccess->documentFolder][$AO2RequestforAccess->create($job)] =($AO2RequestforAccess)->documentName;

      $AO3RequestforAccess = new \App\Src\Docs\Section8\AO3RequestforAccess();
      $docs[$AO3RequestforAccess->documentFolder][$AO3RequestforAccess->create($job)] =($AO3RequestforAccess)->documentName;

      $AO4RequestforAccess = new \App\Src\Docs\Section8\AO4RequestforAccess();
      $docs[$AO4RequestforAccess->documentFolder][$AO4RequestforAccess->create($job)] =($AO4RequestforAccess)->documentName;

      $AO5RequestforAccess = new \App\Src\Docs\Section8\AO5RequestforAccess();
      $docs[$AO5RequestforAccess->documentFolder][$AO5RequestforAccess->create($job)] =($AO5RequestforAccess)->documentName;

      $AO6RequestforAccess = new \App\Src\Docs\Section8\AO6RequestforAccess();
      $docs[$AO6RequestforAccess->documentFolder][$AO6RequestforAccess->create($job)] =($AO6RequestforAccess)->documentName;

      $AO7RequestforAccess = new \App\Src\Docs\Section8\AO7RequestforAccess();
      $docs[$AO7RequestforAccess->documentFolder][$AO7RequestforAccess->create($job)] =($AO7RequestforAccess)->documentName;

      $AO8RequestforAccess = new \App\Src\Docs\Section8\AO8RequestforAccess();
      $docs[$AO8RequestforAccess->documentFolder][$AO8RequestforAccess->create($job)] =($AO8RequestforAccess)->documentName;

      $AO9RequestforAccess = new \App\Src\Docs\Section8\AO9RequestforAccess();
      $docs[$AO9RequestforAccess->documentFolder][$AO9RequestforAccess->create($job)] =($AO9RequestforAccess)->documentName;

      $AO10RequestforAccess = new \App\Src\Docs\Section8\AO10RequestforAccess();
      $docs[$AO10RequestforAccess->documentFolder][$AO10RequestforAccess->create($job)] =($AO10RequestforAccess)->documentName;
      
      
      $folders = array_keys($docs);

      //$jobdoc = 
      //$folderslist = \App\JobDocs::find($folderslist[0])->get('subfolder');
      foreach($folders as $each){
         $folderslist[$each] = \DB::table('job_docs')->where('folder', $each)->where('job_id', $jobid)->whereRaw('subfolder <> ""')->select('subfolder')->groupBy('subfolder')->get()->pluck('subfolder');

      }
      //$folderslist = $folders;
      $documents = [];
      
      return view('admin.my_jobs_documents',['docs'=>$docs, 'job'=>$job, 'folderslist'=>$folderslist, 'documents'=>$documents]);
    }


   public function genSurveying($jobid=null){
      //$finfo = finfo_open(FILEINFO_MIME,"/etc/magic");
      # get job details from db
      $job= \App\Job::find($jobid);
      $docs=[];

      if ($job->surveying->surveying_service == "Valuation Report") 
      {
         $valuationInfo = new \App\Src\Docs\Surveying\ValuationWord();
         $docs[$valuationInfo->documentFolder][$valuationInfo->create($job)] =($valuationInfo)->documentName;
         $path5 = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/".$valuationInfo->documentFolder."/".$valuationInfo->documentName.'.docx';
      }
      if($job->surveying->surveying_service == "Boundary Determination Report")
      {
         $draftword = new \App\Src\Docs\Surveying\DraftWord();
         $docs[$draftword->documentFolder][$draftword->create($job)] =($draftword)->documentName;
         $path1 = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/".$draftword->documentFolder."/".$draftword->documentName.'.docx';

         // $dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/sketchpad");
      }
      
      $JobInfo = new \App\Src\Docs\Surveying\JobInfo();
      $docs[$JobInfo->documentFolder][$JobInfo->create($job)] =($JobInfo)->documentName;
      $path4 = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/".$JobInfo->documentFolder."/".$JobInfo->documentName.'.docx';

      $docs[$JobInfo->completedFolder]['']='';
      $docs[$JobInfo->desktopFolder]['']='';
      $docs[$JobInfo->inspectionFolder]['']='';
      $docs[$JobInfo->landRegisteryFolder]['']='';
      $docs[$JobInfo->reportSiteFolder]['']='';
      $docs[$JobInfo->scannedFolder]['']='';
      // $docs[$JobInfo->sketchFolder]['']='';

      $conf = new \App\Src\Docs\Surveying\ConfirmationLetterInvoice();
      $docs[$conf->documentFolder][$conf->create($job)] =($conf)->documentName;
      $path3 = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/".$conf->documentFolder."/".$conf->documentName.'.docx';

      $rep = new \App\Src\Docs\Surveying\ReportCoverLetter();
      $docs[$rep->documentFolder][$rep->create($job)] =($rep)->documentName;
      $path6 = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/".$rep->documentFolder."/".$rep->documentName.'.docx';

      /*$draft = new \App\Src\Docs\Surveying\DraftReport();
      $docs[$draft->documentFolder][$draft->create($job)] =($draft)->documentName;
      $path = base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/Draft Report/".$draft->documentName.'.xlsx';*/

      // $jobdocs = JobDocs::where('job_id','=', $job->id)->get();
      // //dd($docs);
      // if($jobdocs->count() == 0)
      // {
      //    /*$jobdoc = new JobDocs;
      //    $jobdoc->job_id = $job->id;
      //    $jobdoc->user_id = Auth::user()->id;
      //    $jobdoc->folder = $draft->documentFolder;
      //    $jobdoc->original_name = $draft->documentName.".xlsx";
      //    $jobdoc->type = 'xlsx';
      //    $jobdoc->mimetype = 'application/xlsx';
      //    $jobdoc->link = array_keys($docs[$draft->documentFolder])[0];
      //    $jobdoc->size = filesize($path);
      //    $jobdoc->save();*/   
         
      //    $this->docCreate($job->id,$conf->documentFolder,$conf->documentName,array_keys($docs[$conf->documentFolder])[0],filesize($path3));

      //    $this->docCreate($job->id,$rep->documentFolder,$rep->documentName,array_keys($docs[$rep->documentFolder])[0],filesize($path6));

      //    $this->docCreate($job->id,$JobInfo->documentFolder,$JobInfo->documentName,array_keys($docs[$JobInfo->documentFolder])[0],filesize($path4));


      //    if ($job->surveying->surveying_service == "Valuation Report") {
      //       $this->docCreate($job->id,$valuationInfo->documentFolder,$valuationInfo->documentName,array_keys($docs[$valuationInfo->documentFolder])[0],filesize($path5));
      //    }
      //    if($job->surveying->surveying_service == "Boundary Determination Report")
      //    {
      //       $this->docCreate($job->id,$draftword->documentFolder,$draftword->documentName,array_keys($docs[$draftword->documentFolder])[0],filesize($path1));
      //    }

      // }
      // else
      // {
      //    $folder = [];
      //    foreach ($jobdocs as $key => $value) 
      //    {
      //       array_push($folder, $value->folder,$value->original_name);
      //       /*if($value->type == 'xlsx')
      //       {
      //             $jobdoc = JobDocs::find($value->id);
      //             $jobdoc->job_id = $job->id;
      //             $jobdoc->user_id = Auth::user()->id;
      //             $jobdoc->folder = $draft->documentName;
      //             $jobdoc->original_name = $draft->documentFolder.".xlsx";
      //             $jobdoc->type = 'xlsx';
      //             $jobdoc->mimetype = 'application/xlsx';
      //             $jobdoc->link = array_keys($docs[$draft->documentFolder])[0];
      //             $jobdoc->size = filesize($path);
      //             $jobdoc->save();
      //       }*/
      //       if($value->type == 'docx' && $value->generate == 1)
      //       {
      //          if ($value->folder == $rep->documentFolder) 
      //          {
      //             $this->docUpdate($value->id,$job->id,$rep->documentFolder,$rep->documentName,array_keys($docs[$rep->documentFolder])[0],filesize($path6));
      //          }

      //          if ($value->folder == $conf->documentFolder) 
      //          {
      //             $this->docUpdate($value->id,$job->id,$conf->documentFolder,$conf->documentName,array_keys($docs[$conf->documentFolder])[0],filesize($path3));

      //          }

      //          if ($value->folder == $JobInfo->documentFolder) 
      //          {
      //             $this->docUpdate($value->id,$job->id,$JobInfo->documentFolder,$JobInfo->documentName,array_keys($docs[$JobInfo->documentFolder])[0],filesize($path4));
      //          }

      //          if ($job->surveying->surveying_service == "Valuation Report") 
      //          {
      //             if ($value->folder == $valuationInfo->documentFolder) 
      //             {
      //                $this->docUpdate($value->id,$job->id,$valuationInfo->documentFolder,$valuationInfo->documentName,array_keys($docs[$valuationInfo->documentFolder])[0],filesize($path5));
      //             }
      //          }
      //          if($job->surveying->surveying_service == "Boundary Determination Report")
      //          {
      //             if ($value->folder == $draftword->documentFolder) 
      //             {
      //                $this->docUpdate($value->id,$job->id,$draftword->documentFolder,$draftword->documentName,array_keys($docs[$draftword->documentFolder])[0],filesize($path1));
      //             }
      //          }
      //       }
      //    }
      //    //dd($folder);
      //    if(!in_array('Report Cover Letter',$folder) || !in_array('Client Report Cover Letter.docx', $folder))
      //    {
      //       //dd('true');
      //       $this->docCreate($job->id,$rep->documentFolder,$rep->documentName,array_keys($docs[$rep->documentFolder])[0],filesize($path6));
      //    }
      //    if(!in_array('Job Information',$folder) || !in_array('Job Info Sheet.docx', $folder))
      //    {
      //       $this->docCreate($job->id,$JobInfo->documentFolder,$JobInfo->documentName,array_keys($docs[$JobInfo->documentFolder])[0],filesize($path6));
      //    }
      //    if(!in_array('Confirmation Letter & Invoice',$folder) || !in_array('Client Service Conf Letter.docx', $folder))
      //    {
      //       $this->docCreate($job->id,$conf->documentFolder,$conf->documentName,array_keys($docs[$conf->documentFolder])[0],filesize($path3));
      //    }
      //    if(!in_array('Draft Report',$folder) || !in_array('Boundary Report Word Document.docx', $folder))
      //    {
      //       if($job->surveying->surveying_service == "Boundary Determination Report")
      //       {
      //          $this->docCreate($job->id,$draftword->documentFolder,$draftword->documentName,array_keys($docs[$draftword->documentFolder])[0],filesize($path1));
      //       }
      //    }
      //    if(!in_array('Valuation Report',$folder) || !in_array('Valuation Report Word Document.docx', $folder))
      //    {
      //       if ($job->surveying->surveying_service == "Valuation Report") {
      //          $this->docCreate($job->id,$valuationInfo->documentFolder,$valuationInfo->documentName,array_keys($docs[$valuationInfo->documentFolder])[0],filesize($path5));
      //       }
      //    }
      // }
      $documents = [];
      return view('admin.my_jobs_documents_surveying',['docs'=>$docs, 'job'=>$job , 'documents'=>$documents]);
   }

   public function docCreate($job_id,$folder,$original_name,$link,$size)
   {
      $jobdoc = new JobDocs;
      $jobdoc->job_id = $job_id;
      $jobdoc->user_id = Auth::user()->id;
      $jobdoc->folder = $folder;
      $jobdoc->original_name = $original_name.".docx";
      $jobdoc->type = 'docx';
      $jobdoc->mimetype = 'application/docx';
      $jobdoc->link = $link;
      $jobdoc->size = $size;
      $jobdoc->generate = 1;
      $jobdoc->save();
   }

   public function docUpdate($id,$job_id,$folder,$original_name,$link,$size)
   {
      $jobdoc = JobDocs::find($id);
      $jobdoc->job_id = $job_id;
      $jobdoc->user_id = Auth::user()->id;
      $jobdoc->folder = $folder;
      $jobdoc->original_name = $original_name.".docx";
      $jobdoc->type = 'docx';
      $jobdoc->mimetype = 'application/docx';
      $jobdoc->link = $link;
      $jobdoc->size = $size;
      $jobdoc->generate = 1;
      $jobdoc->save();
   }

   // zip and download.
   public function download($jobid=null){
      $folder = storage_path('app/public/jobs/'.$jobid.'/my-jobs/');
      $zip=storage_path('app/public/jobs/'.$jobid.'/archived/'.time().'_'.$jobid.'.zip');
      \Zipper::make($zip)->add($folder)->close();
      return response()->download($zip);
   }

  //download standard document details
   public function downloadStandardPdf($jobid,Request $request)
   {
//ini_set('max_execution_time', -1);
	//ini_set('memory_limit','-1');
	//set_time_limit(3000);

      $content = "";
      if ($request->get('job_type') == "partywall") {

	
            foreach ($request->get('formData') as $key=>$value) 
            {

                  $files = File::files(storage_path('app/public/jobs/'.$jobid.'/my-jobs/'.$value[0]));
                  foreach ($files as $key => $value) {
                        $ext = pathinfo($value, PATHINFO_EXTENSION);
                        if ($ext == 'html') {
                              $content = $content.file_get_contents($value);      
                        }
                  }
                  // $job_doc = JobDocs::where([['job_id',$jobid],['folder',$value[1]],['upload','=',0]])->first();
                  // $content = $content.file_get_contents(base_path()."/storage/app/public/jobs/{$jobid}/my-jobs/surveying/{$value[0]}/{$value[1]}.html");
            }
            $content = str_replace("</body>", " ", $content);
            $content = str_replace("<!DOCTYPE html>", " ", $content);
            $content = str_replace("</html>", " ", $content);
            $content = str_replace("</body>", " ", $content);
            $content = str_replace("<html>", " ", $content);      
            $pdf =  PDF::loadHtml($content);
            $pdf->save( base_path()."/storage/app/public/jobs/{$jobid}/my-jobs/standardDocument.pdf");
            return response("/storage/jobs/{$jobid}/my-jobs/standardDocument.pdf",202);
      }
      else{
            foreach ($request->get('formData') as $key=>$value) 
            {

                  $files = File::files(storage_path('app/public/jobs/'.$jobid.'/my-jobs/surveying/'.$value[0]));
                  foreach ($files as $key => $value) {
                        $ext = pathinfo($value, PATHINFO_EXTENSION);
                        if ($ext == 'html') {
                              $content = $content.file_get_contents($value);      
                        }
                  }
                  // $job_doc = JobDocs::where([['job_id',$jobid],['folder',$value[1]],['upload','=',0]])->first();
                  // $content = $content.file_get_contents(base_path()."/storage/app/public/jobs/{$jobid}/my-jobs/surveying/{$value[0]}/{$value[1]}.html");
            }
            $content = str_replace("</body>", " ", $content);
            $content = str_replace("<!DOCTYPE html>", " ", $content);
            $content = str_replace("</html>", " ", $content);
            $content = str_replace("</body>", " ", $content);
            $content = str_replace("<html>", " ", $content);      
            $pdf =  PDF::loadHtml($content);
            $pdf->save( base_path()."/storage/app/public/jobs/{$jobid}/my-jobs/surveying/standardDocument.pdf");
            return response("/storage/jobs/{$jobid}/my-jobs/surveying/standardDocument.pdf",202);
      }

   }
   //download sketch image
   public function downloadSketch(Request $request,$job_id=null)
   {
      $type = explode("/", $request->prev_url) ;
      
      $image = $request->file; // image base64 encoded
      
      preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
      $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
      $image = str_replace(' ', '+', $image);
      $imageName = 'sketch_' . time() . '.' . $image_extension[1]; //generating unique file name;
      File::put(base_path().'/storage/app/public/jobs/'.$job_id.'/my-jobs/sketchpad/' . $imageName, base64_decode($image));
      
      
      $job_docs = new JobDocs;
      $job_docs->job_id = $job_id;
      $job_docs->user_id = Auth::user()->id;
      if ($type[5] == "doc") {
            $job_docs->folder = "Job information/Job Sketch";
      }
      else
      {
            $job_docs->folder = "Sketchpad";     
      }
      $job_docs->original_name = $imageName;
      $job_docs->type = "jpg";
      $job_docs->mimetype = "image/jpg";
      $job_docs->link = "storage/jobs/".$job_id."/my-jobs/sketchpad/". $imageName."";
      $job_docs->upload = 1;
      $job_docs->save();
      return response($request->prev_url,202);
   }
   //download uploaded document details
   public function downloadUploadedPdf($jobid,Request $request)
   {
      $oMerger = PDFMerger::init();
      foreach ($request->get('formData') as $value)
      {         

            $job_doc = JobDocs::where([['job_id',$jobid],['folder',$value[0]],['original_name','=',$value[1]],['upload','=',1]])->first();
            $oMerger->addPDF(public_path()."/".$job_doc->link,'all');
      }
      $oMerger->merge();
      $oMerger->save(base_path()."/storage/app/public/jobs/{$jobid}/my-jobs/uploads/uploadedDocument.pdf");  
        return response("/storage/jobs/{$jobid}/my-jobs/uploads/uploadedDocument.pdf",202);

   }
   public function read_docx($filename){

       $striped_content = '';
       $content = '';

       if(!$filename || !file_exists($filename)) return false;

       $zip = zip_open($filename);
       if (!$zip || is_numeric($zip)) return false;

       while ($zip_entry = zip_read($zip)) {

           if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

           if (zip_entry_name($zip_entry) != "word/document.xml") continue;

           $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

           zip_entry_close($zip_entry);
       }
       zip_close($zip);      
       $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
       $content = str_replace('</w:r></w:p>', "\r\n", $content);
       $striped_content = strip_tags($content);

       return $striped_content;
   }
   public function regen($jobid=null){
      //$finfo = finfo_open(FILEINFO_MIME, "/etc/magic");
    	# get job details from db
       
      //$jobid = '100299';
      
      //$jobslist = \DB::table('jobs')->all()->pluck('id');
      $jobslist = \DB::select('SELECT id FROM jobs');
      
      foreach($jobslist as $each){
        
         $jobid = $each->id;
         
       	$job = \App\Job::find($jobid);
         $docs=[];
         //die( "--".$jobid );
         
         $AO1ReturningAwardToBos = new \App\Src\Docs\Awards\AO1ReturningAwardToBos();
         $docs[$AO1ReturningAwardToBos->documentFolder][$AO1ReturningAwardToBos->create($job)] =($AO1ReturningAwardToBos)->documentName;
         
         $DraftAO12SurvAward = new \App\Src\Docs\Awards\DraftAO12SurvAward();
         $docs[$DraftAO12SurvAward->documentFolder][$DraftAO12SurvAward->create($job)] =($DraftAO12SurvAward)->documentName;
         
         $DraftAO1ASAward = new \App\Src\Docs\Awards\DraftAO1ASAward();
         $docs[$DraftAO1ASAward->documentFolder][$DraftAO1ASAward->create($job)] =($DraftAO1ASAward)->documentName;
         
         //  App\Docs\ScheduleOfCondition
         $AO1ConfofSOCtoBO = new \App\Src\Docs\ScheduleOfCondition\AO1ConfofSOCtoBO();
         $docs[$AO1ConfofSOCtoBO->documentFolder][$AO1ConfofSOCtoBO->create($job)] =($AO1ConfofSOCtoBO)->documentName;
         
         // AO1LetterBeforeActiontoBOAOS
         $AO1LetterBeforeActiontoBOAOS = new \App\Src\Docs\Invoice\AO1LetterBeforeActiontoBOAOS();
         $docs[$AO1LetterBeforeActiontoBOAOS->documentFolder][$AO1LetterBeforeActiontoBOAOS->create($job)] =($AO1LetterBeforeActiontoBOAOS)->documentName;
         
         $AO1LetterBeforeActiontoBOAS = new \App\Src\Docs\Invoice\AO1LetterBeforeActiontoBOAS();
         $docs[$AO1LetterBeforeActiontoBOAS->documentFolder][$AO1LetterBeforeActiontoBOAS->create($job)] =($AO1LetterBeforeActiontoBOAS)->documentName;
         
         $AO1FinalLetter = new \App\Src\Docs\FinalLetters\AO1FinalLetter();
         $docs[$AO1FinalLetter->documentFolder][$AO1FinalLetter->create($job)] =($AO1FinalLetter)->documentName;
         
         // AO2 START //
         $AO2ConfofAOdissenttoBO = new \App\Src\Docs\ConfirmationOfDissent\AO2ConfofAOdissenttoBO();
         $docs[$AO2ConfofAOdissenttoBO->documentFolder][$AO2ConfofAOdissenttoBO->create($job)] =($AO2ConfofAOdissenttoBO)->documentName;
         
         $AO2CovertoBOawardBOSwithINV = new \App\Src\Docs\Awards\AO2CovertoBOawardBOSwithINV();
         $docs[$AO2CovertoBOawardBOSwithINV->documentFolder][$AO2CovertoBOawardBOSwithINV->create($job)] =($AO2CovertoBOawardBOSwithINV)->documentName;
         
         $AO1CoverToBoAwardBosWithInv = new \App\Src\Docs\Awards\AO1CoverToBoAwardBosWithInv();
         $docs[$AO1CoverToBoAwardBosWithInv->documentFolder][$AO1CoverToBoAwardBosWithInv->create($job)] =($AO1CoverToBoAwardBosWithInv)->documentName;
         
         // section 12
         $AO1toBOSection12request = new \App\Src\Docs\Section12Request\AO1toBOSection12request();
         $docs[$AO1toBOSection12request->documentFolder][$AO1toBOSection12request->create($job)] =($AO1toBOSection12request)->documentName;
         
         $AO2107LettertoAOS = new \App\Src\Docs\Section10\AO2107LettertoAOS();
         $docs[$AO2107LettertoAOS->documentFolder][$AO2107LettertoAOS->create($job)] =($AO2107LettertoAOS)->documentName;
         
         // section 10
         $AO1107LettertoAOS = new \App\Src\Docs\Section10\AO1107LettertoAOS();
         $docs[$AO1107LettertoAOS->documentFolder][$AO1107LettertoAOS->create($job)] =($AO1107LettertoAOS)->documentName;
         
         // section 8 AO1RequestforAccess
         $AO1RequestforAccess = new \App\Src\Docs\Section8\AO1RequestforAccess();
         $docs[$AO1RequestforAccess->documentFolder][$AO1RequestforAccess->create($job)] =($AO1RequestforAccess)->documentName;
         $AO1CovertoSOC = new \App\Src\Docs\ScheduleOfCondition\AO1CovertoSOC();
         $docs[$AO1CovertoSOC->documentFolder][$AO1CovertoSOC->create($job)] =($AO1CovertoSOC)->documentName;
         
         $AO1CovertoBOSOC = new \App\Src\Docs\ScheduleOfCondition\AO1CovertoBOSOC();
         $docs[$AO1CovertoBOSOC->documentFolder][$AO1CovertoBOSOC->create($job)] =($AO1CovertoBOSOC)->documentName;
         
         // AO2 START //
         $AO2104Letter = new \App\Src\Docs\NoticesAndCovers\AO2104Letter();
         $docs[$AO2104Letter->documentFolder][$AO2104Letter->create($job)] =($AO2104Letter)->documentName;
         
         $AO1ConfofBLS104Appointment = new \App\Src\Docs\NoticesAndCovers\AO1ConfofBLS104Appointment();
         $docs[$AO1ConfofBLS104Appointment->documentFolder][$AO1ConfofBLS104Appointment->create($job)] =($AO1ConfofBLS104Appointment)->documentName;
         
         $AO1Confof104Appointment = new \App\Src\Docs\NoticesAndCovers\AO1Confof104Appointment();
         $docs[$AO1Confof104Appointment->documentFolder][$AO1Confof104Appointment->create($job)] =($AO1Confof104Appointment)->documentName;
         
         // AO1 START //
         $AO1104Letter = new \App\Src\Docs\NoticesAndCovers\AO1104Letter();
         $docs[$AO1104Letter->documentFolder][$AO1104Letter->create($job)] =($AO1104Letter)->documentName;
         
         echo "--done--$jobid--";
         usleep(500000);

      }
 
      //die('--done--');
      
    }

}  
