<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ao_4 extends Model
{
    protected $table='ao_4';
    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
