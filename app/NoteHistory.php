<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteHistory extends Model
{

    protected $fillable = ['note_id','user_id','title' ,'body'];
    public function note(){
    	return $this->belongsTo(Note::class);
    }
    
}
