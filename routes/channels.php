<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('chatroom.{jobid}', function ($user, $jobid) {
if (Auth::user()->role==9) {
	$output = \App\Job_User::where('job_id', $jobid)->first();
}else{
	$output = \App\Job_User::where('user_id',$user->id)->where('job_id', $jobid)->first();
}
	
    return $output ? $user: true;
});
