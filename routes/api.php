<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('payment', function (Request $request) {
	header('Access-Control-Allow-Origin: *'); 

	$payment = new  \App\SitePayment;
	$payment->name= $request->full_name;
	$payment->amount= substr($request->amount, 0, -2);
	$payment->email= $request->email;
	

    
    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    \Stripe\Stripe::setApiKey("sk_test_cTtRAp2zFSYTdz5hLS0svSq0");
    $charge = \Stripe\Charge::create([
        'amount' => $request->amount,
        'currency' => 'gbp',
        'description' => 'BLsurveyors Charge',
        'source' => $request->token,
    ]);


    if ($charge->status=="succeeded") {
      $message="good";
      $payment->save();
    }
    else{
      $message="fail";
    }

    return  $message;


});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
