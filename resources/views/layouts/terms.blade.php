<?php
    if (Auth::check() && Auth::user()->role == 3) 
    {
            # code...
        $surveyors = [];
        foreach (Auth::user()->jobs as $job) {
            for ($i=0; $i <9 ; $i++) { 
                if ($i >0) 
                {
                    $a = 'ao'.$i;
                    if ($job->$a != null) {
                        if($job->$a->surveyor_name != null)
                        {

                            array_push($surveyors, ['name' => $job->$a->surveyor_name] );
                        }

                    }
                    
                }
                else
                {
                    if ($job->ao != null) {
                        if ($job->ao->surveyor_name != null) 
                        {
                            array_push($surveyors, ['name' => $job->ao->surveyor_name]);
                        }
                    }
                }
            }
            if ($job->bo != null) 
            {
               if ($job->bo->surveyor_name != null) {
                    array_push($surveyors,['name' => $job->bo->surveyor_name]);
               }
            }
            if ($job->surveying != null) 
            {
                if ($job->surveying->surveryor_name != null) {
                    array_push($surveyors,['name' => $job->surveying->surveryor_name]);
                }
            }
        }
        //dd($surveyors);
        $user = new App\User;
        $surveyors = $user->unique_multidim_array($surveyors,'name');

        $count = count($surveyors);

        $surveyors_name = [];
        foreach ($surveyors as $key => $value) {

            array_push($surveyors_name, implode(',', $value));
        }
        $surveyors = implode(",", $surveyors_name);
        //dd($surveyors);


    }

?>
<!DOCTYPE html>
<html class="home page-template-default page page-id-8 js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths preserve3d not-ie gr__blsurveyors_com" lang="en-GB" prefix="og: http://ogp.me/ns#">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
       
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no"/>
        <title>Party Wall Surveyor London | Party Wall Surveyors &amp; Experts</title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" id="theme-style-css" href="/old/style.css" type="text/css" media="all">
        <link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-32x32.jpg" sizes="32x32">
        <link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-192x192.jpg" sizes="192x192">
        <link rel="apple-touch-icon-precomposed" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-180x180.jpg">
        {{-- LightBox --}}
        <link href="/lightbox/css/lightbox.css" rel="stylesheet">
        {{-- FancyBox --}}
        


        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="msapplication-TileImage" content="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-270x270.jpg">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{!! asset('css/radio.css') !!}">
        
        @yield('head')
        <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
                <style>
                h1 h2 h3 h4 h5{
                    font-family: 'poppinsbold', sans-serif, Helvetica, Arial;
                }
                p, address{
                   font-family: 'averageregular';
                }
                .admin-menu-item{
                    background-color: #414861;
                    color:#fff;
                    padding: 15px 5px 1px 5px;
                    min-height: 105px;
                    text-align: center;
                    }
                .item-active{
                    background-color: #fff;
                    color:#414861;
                 
                    border: solid 1px #424961;
                    border-bottom: none;
                }


                    #admin-menu img{
                        margin-bottom: 15px;
                    width: 30px;
                    }

                    .modal-header {
    background-color: #00b29d;
    padding: 15px;
    border-bottom: 1px solid #e5e5e5;
    color: #fff;
    /* border-radius: 0px; */
}
.close {
    float: right;
    font-size: 40px;
    font-weight: 700;
    line-height: 1;
    color: #f5f5f5;
    text-shadow: 0 1px 0 #fff;
    opacity: 1;
    filter: alpha(opacity=20);
}
.btn.focus, .btn:focus, .btn:hover {
    color: #424961;
background-color: #00B1A0;
    text-decoration: none;
}

    .nav-pills>li>a {
    border-radius: 0px;
    border: solid 0.5px #dddddd;
    font-weight: bold;
    color: #424961;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #424961;
}
.blue-btn{
    border-radius: 5px;
    color: #fff;
    background-color: #00B1A0;
    border-bottom: solid 2px #02937E;
}
h1{
        color: #414861;
    font-weight: bold;
    font-size: 3.5em;
}

.progress {
border-radius: 0px;
}
.progress-bar {
    background-color: #00b29b;
    }

/* FancyBox CSS */
.fancybox-content {
    max-width  : 91%!important;
    max-height : 91%!important;
}
.fancybox-iframe {
    height: 100%!important;
}
footer.primary ul.social-media a:nth-child(3){
    margin-right: 10px;
}

</style>
    </head>
    <body class="home page-template-default page page-id-8 desktop-device" data-gr-c-s-loaded="true" style="">
        @include('components.header')
        
         @yield('content')

       

        @include('components.footer2')
        <script src="{{ asset('js/app.js') }}" ></script>
        @yield('script')  
    </body>
    <script src="/lightbox/js/lightbox.js"></script>
</html>