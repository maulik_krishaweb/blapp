<?php
    if (Auth::check() && Auth::user()->role == 3) 
    {
            # code...
        $surveyors = [];
        foreach (Auth::user()->jobs as $job) {
            for ($i=0; $i <9 ; $i++) { 
                if ($i >0) 
                {
                    $a = 'ao'.$i;
                    if ($job->$a != null) {
                        if($job->$a->surveyor_name != null)
                        {

                            array_push($surveyors, ['name' => $job->$a->surveyor_name] );
                        }

                    }
                    
                }
                else
                {
                    if ($job->ao != null) {
                        if ($job->ao->surveyor_name != null) 
                        {
                            array_push($surveyors, ['name' => $job->ao->surveyor_name]);
                        }
                    }
                }
            }
            if ($job->bo != null) 
            {
               if ($job->bo->surveyor_name != null) {
                    array_push($surveyors,['name' => $job->bo->surveyor_name]);
               }
            }
            if ($job->surveying != null) 
            {
                if ($job->surveying->surveryor_name != null) {
                    array_push($surveyors,['name' => $job->surveying->surveryor_name]);
                }
            }
        }
        //dd($surveyors);
        $user = new App\User;
        $surveyors = $user->unique_multidim_array($surveyors,'name');

        $count = count($surveyors);

        $surveyors_name = [];
        foreach ($surveyors as $key => $value) {

            array_push($surveyors_name, implode(',', $value));
        }
        $surveyors = implode(",", $surveyors_name);
        //dd($surveyors);


    }

?>
<!DOCTYPE html>
<html class="home page-template-default page page-id-8 js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths preserve3d not-ie gr__blsurveyors_com" lang="en-GB" prefix="og: http://ogp.me/ns#">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
       
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no"/>
        <title>Party Wall Surveyor London | Party Wall Surveyors &amp; Experts</title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" id="theme-style-css" href="/old/style.css" type="text/css" media="all">
        <link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-32x32.jpg" sizes="32x32">
        <link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-192x192.jpg" sizes="192x192">
        <link rel="apple-touch-icon-precomposed" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-180x180.jpg">
        {{-- LightBox --}}
        <link href="/lightbox/css/lightbox.css" rel="stylesheet">
        {{-- FancyBox --}}
        


        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="msapplication-TileImage" content="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-270x270.jpg">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{!! asset('css/radio.css') !!}">
        
        @yield('head')
        <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
                <style>
                h1 h2 h3 h4 h5{
                    font-family: 'poppinsbold', sans-serif, Helvetica, Arial;
                }
                p, address{
                   font-family: 'averageregular';
                }
                .admin-menu-item{
                    background-color: #414861;
                    color:#fff;
                    padding: 15px 5px 1px 5px;
                    min-height: 105px;
                    text-align: center;
                    }
                .item-active{
                    background-color: #fff;
                    color:#414861;
                 
                    border: solid 1px #424961;
                    border-bottom: none;
                }


                    #admin-menu img{
                        margin-bottom: 15px;
                    width: 30px;
                    }

                    .modal-header {
    background-color: #00b29d;
    padding: 15px;
    border-bottom: 1px solid #e5e5e5;
    color: #fff;
    /* border-radius: 0px; */
}
.close {
    float: right;
    font-size: 40px;
    font-weight: 700;
    line-height: 1;
    color: #f5f5f5;
    text-shadow: 0 1px 0 #fff;
    opacity: 1;
    filter: alpha(opacity=20);
}
.btn.focus, .btn:focus, .btn:hover {
    color: #424961;
background-color: #00B1A0;
    text-decoration: none;
}

    .nav-pills>li>a {
    border-radius: 0px;
    border: solid 0.5px #dddddd;
    font-weight: bold;
    color: #424961;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #424961;
}
.blue-btn{
    border-radius: 5px;
    color: #fff;
    background-color: #00B1A0;
    border-bottom: solid 2px #02937E;
}
h1{
        color: #414861;
    font-weight: bold;
    font-size: 3.5em;
}

.progress {
border-radius: 0px;
}
.progress-bar {
    background-color: #00b29b;
    }

/* FancyBox CSS */
.fancybox-content {
    max-width  : 91%!important;
    max-height : 91%!important;
}
.fancybox-iframe {
    height: 100%!important;
}

footer.primary ul.social-media a:nth-child(3){
    margin-right: 10px;
}
#admin-menu .admin-menu-tab .logo-wrap{
    background-color: transparent;
}
#admin-menu .admin-menu-tab .logo-wrap img{
    margin-bottom: 0;
    width: 80px;
    height: auto;
}
#admin-menu .admin-menu-tab{
    padding-left: 5px;
    padding-right: 5px;
}
#admin-menu .admin-menu-tab p{
    font-size: 15px;
}
@media (min-width: 768px){
    #admin-menu .admin-menu-tab{
        width: 12.5% !important;
    }
}

</style>
    </head>
    <body class="home page-template-default page page-id-8 desktop-device" data-gr-c-s-loaded="true" style="">
        @include('components.header')
        @if (!Auth::guest())
        <div class="container-fluid" style="border-bottom: 1px solid #424961; ">
                <div class="container">
                        <div class="row">
                            <div class=" @if (Auth::user()->role==3) col-md-6 @else col-md-12 @endif " > 
                                <h1 style="color: #00b29d; " >Hello <span id="user_name">{{ Auth::user()->name }}</span>, </h1>
                                @if (Auth::user()->role==3)
                                   <p style="font-size: 18px">Welcome to your Berry Lodge Client Service Portal! </p>  <p style="font-size: 18px"> Here you can access all of the information regarding your @if(Auth::user()->jobs->count() > 0) {{ Auth::user()->jobs[0]->surveying ? "Surveying" : "Party Wall" }}@endif matter. </p>  <p style="font-size: 18px">You can also upload any information you would like your Surveyor {{-- Beau Monroe-Davies --}} {{-- {!! $surveyors !!} --}} to review or have on file. </p>
                                @endif
                                @if (Auth::user()->role==5)
                                   <p style="font-size: 18px">Welcome to your Berry Lodge Client Service Portal! </p>  <p style="font-size: 18px"> In this messaging forum, you can safely message all parties within the forum in an open manner.</p>
                                   <p style="font-size: 18px"> You can also upload any information you would like the various parties to view and access.</p>
                                   <p style="font-size: 18px"> All parties who are within the forum are displayed below for transparency.</p>
                                @endif
                            </div>
                             @if (Auth::user()->role==3)
                            <div class="col-md-6">
                                <?php
                                    $url = App\User::where('role', '=', 9)->orderBy('updated_at', 'desc')->first();
                                ?>
                                @if($url->client_video != null)
                                <iframe style="height: 320px !important; width: 100%; border: 0;margin-top: 22px;" src="{!! $url->client_video !!}">
                                </iframe>
                                @endif
                            </div>
                            @endif
                            
                        </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p>Email notification settings</p>
                            <form>
                                <div class="col-md-3">
                                <label class=" radio">
                                    <input class="email_notification " data-id="{{ Auth::user()->id }}" data-val="1" type="radio" name="email_notification" @if(Auth::user()->notification == 1) checked @endif>On
                                    <span class="checkround"></span>
                                </label>
                                </div>
                                <div class="col-md-3">
                                <label class=" radio">
                                    <input class="email_notification"  data-id="{{ Auth::user()->id }}" data-val="0" type="radio" name="email_notification" @if(Auth::user()->notification == 0) checked @endif>Off
                                    <span class="checkround"></span>
                                    </label>   
                                </div>
                            </form>
                        </div>

                       {{-- menu --}}
                       @if(Auth::user()->role==9 || Auth::user()->role==7 )
                            @include('components.admin_menu')
                        @elseif(Auth::user()->role==11)
                            @include('components.admin_two_menu')
                        @elseif(Auth::user()->role==5)
                            @include('components.third_party_menu')
                        @elseif(Auth::user()->role==1)
                            @include('components.meditation_menu')
                        @else
                            @include('components.client_menu')
                       @endif
                    </div>
                </div>
            </div>

         @endif 
         @yield('content')

       

        @include('components.footer2')
        <script src="{{ asset('js/app.js') }}" ></script>
        @yield('script')  
    </body>
    <script src="/lightbox/js/lightbox.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>  
</html>