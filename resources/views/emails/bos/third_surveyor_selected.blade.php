<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve selected a Third Surveyor!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/bos/6.png') !!}" width="200px">
		</div>
	</div>


	<div>
		<p>Dear {{$salutation}},</p>

		<p>Good News! We are pleased to confirm that we’ve progressed your Party Wall Surveying Matter!</p>

		<p>We have heard back from the adjoining owner’s Surveyor and as part of the requirements of Section 10 of the Party Wall etc Act 1996 a Third Surveyor has been selected.</p>

		<p>You can hear a little more about Third Surveyors on our <a href="https://www.youtube.com/watch?v=pXHnMZ-hpRM&feature=youtu.be">informative video</a> which we would advise having a quick watch of.</p>
		<p>You can find out the name of the Third Surveyor by logging into the  <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now to see the status of your Party Wall Surveying job.</p>
		<p>Kind Regards, </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;" >
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>