<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2></h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/login_email.png') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>Dear {{ $salutation }}</p>

		<p>Today is the expiry of the Party Wall Notices.</p>

		<p>As we have not received a response from the adjoining owners. You are therefore now in the legal position to appoint a Party Wall Surveyor on behalf of the non responsive adjoining owners.</p>

		<p>{{ $surveyor_name }} has spoken to a colleague of his who has confirmed he would be happy to undertake the role for the fixed price of £350 + VAT.</p>

		<p>Please note that {{ $surveyor_name }} ’s colleague also works at Berry Lodge Surveyors, however they work in a different office and we can confirm that we have established a tried and tested information barrier for this file, meaning that they will not be able to access each other files. </p>

		<p>Furthermore, we can confirm that as per Section 10 of the Party Wall etc Act 1996 and ad part of their statutory duties as a Party Wall Surveyors, they are duty bound to be impartial and we can categorically confirm they will do that.</p>

		<p>If you would like us to proceed, please contact {{ $surveyor_name }} directly and he will make the necessary appointments.</p>

		<p>Login to the <a href="https://app.blsurveyors.com/"> Berry Lodge Client Portal </a> now to see the status of your Party Wall Surveying job.</p>

		<p>Kind Regards </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>