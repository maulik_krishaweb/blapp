@extends('layouts.error') 
@section('content')

<div class="container-fluid">
  <div class="container" style="padding: 10% 0 10% 0;">
   <b style="display:inherit;text-align: center;">Whoops, there appears to have been an error. Please refresh the screen and try again. If the problem still occurs, please contact us</b>
   </div>
</div>

@endsection