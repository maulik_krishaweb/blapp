<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_full_information') ? ' has-error' : ''}}">
        <label for="surveyor_full_information" class="control-label">BO Surveyor Full Information:</label>
        <input id="surveyor_full_information"  class="form-control autocomplete" name="surveyor_full_information" value="@isset($jobs->bo->surveyor_full_information ){{$jobs->bo->surveyor_full_information}}@endisset{{old('surveyor_full_information')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            @foreach ($surveyor as $person)
                <li data-sr="b-sr" 
                data-b-name="{{ $person->name}}" 
                data-b-qualifications="{{ $person->qualifications}}" 
                data-b-company="{{ $person->company}}"
                data-b-address="{{ $person->address}}"
                data-b-email="{!! $person->email !!}"
                data-b-contact="Telephone: {{ $person->telephone}} Email: {{ $person->email}}"><a>{{ $person->name}} {{ $person->qualifications}} , {{ $person->company}}, {{ $person->address}} Telephone: {{ $person->telephone}} Email: {{ $person->email}}</a></li>
            @endforeach    
        </ul>       
    </div>
</div>
<input type="hidden" name="surveryor_email" id="surveyor_email">
<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_name') ? ' has-error' : ''}}">
        <label for="surveyor_name" class="control-label">BO Surveyor Name:</label>
        <input id="surveyor_name" type="surveyor_name" class="form-control" name="surveyor_name" value="@isset($jobs->bo->surveyor_name ){{$jobs->bo->surveyor_name}}@endisset{{old('surveyor_name')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_qualifications') ? ' has-error' : ''}}">
        <label for="surveyor_qualifications" class="control-label">BO Surveyor Qualifications:</label>
        <input id="surveyor_qualifications" type="surveyor_qualifications" class="form-control" name="surveyor_qualifications" value="@isset($jobs->bo->surveyor_qualifications ){{$jobs->bo->surveyor_qualifications}}@endisset{{old('surveyor_qualifications')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_company_name') ? ' has-error' : ''}}">
        <label for="surveyor_company_name" class="control-label">BO Surveyor Company Name:</label>
        <input id="surveyor_company_name" type="surveyor_company_name" class="form-control" name="surveyor_company_name" value="@isset($jobs->bo->surveyor_company_name ){{$jobs->bo->surveyor_company_name}}@endisset{{old('surveyor_company_name')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_company_address') ? ' has-error' : ''}}">
        <label for="surveyor_company_address" class="control-label">BO Surveyor Company Address:</label>
        <input id="surveyor_company_address" type="surveyor_company_address" class="form-control" name="surveyor_company_address" value="@isset($jobs->bo->surveyor_company_address ){{$jobs->bo->surveyor_company_address}}@endisset{{old('surveyor_company_address')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('surveyor_contact_details') ? ' has-error' : ''}}">
        <label for="surveyor_contact_details" class="control-label">BO Surveyor Contact Details:</label>
        <input id="surveyor_contact_details" type="surveyor_contact_details" class="form-control" name="surveyor_contact_details" value="@isset($jobs->bo->surveyor_contact_details ){{$jobs->bo->surveyor_contact_details}}@endisset{{old('surveyor_contact_details')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('full_name') ? ' has-error' : ''}}">
        <label for="full_name" class="control-label">BO Full Name(s):</label>
        <input id="full_name" type="full_name" class="form-control" name="full_name" value="@isset($jobs->bo->full_name ){{$jobs->bo->full_name}}@endisset{{old('full_name')}}" >
       
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('salutation') ? ' has-error' : ''}}">
        <label for="salutation" class="control-label">BO Salutation:</label>
        <input id="salutation" type="salutation" class="form-control" name="salutation" value="@isset($jobs->bo->salutation ){{$jobs->bo->salutation}}@endisset{{old('salutation')}}" >
        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('property_address_proposed_work') ? ' has-error' : ''}}">
        <label for="property_address_proposed_work" class="control-label">BO Property Address (proposed work):</label>
        <input id="property_address_proposed_work" type="text" class="form-control" name="property_address_proposed_work" value="@isset($jobs->bo->property_address_proposed_work ){{$jobs->bo->property_address_proposed_work}}@endisset{{old('property_address_proposed_work')}}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('contact_address') ? ' has-error' : ''}}">
        <label for="contact_address" class="control-label">BO Contact Address:</label>
        <input id="contact_address" type="text" class="form-control" name="contact_address" value="@isset($jobs->bo->contact_address ){{$jobs->bo->contact_address}}@endisset{{old('contact_address')}}" >      
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('contact_details_telephone_numbers_email') ? ' has-error' : ''}}">
        <label for="contact_details_telephone_numbers_email" class="control-label">BO Contact Details (Tel. Nos. and Email):</label>
        <input id="contact_details_telephone_numbers_email" type="text" class="form-control" name="contact_details_telephone_numbers_email" value="@isset($jobs->bo->contact_details_telephone_numbers_email ){{$jobs->bo->contact_details_telephone_numbers_email}}@endisset{{old('contact_details_telephone_numbers_email')}}" >       
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('owner_referral') ? ' has-error' : ''}}">
        <label for="owner_referral" class="control-label">BO owner/s (referral):</label>
        <input id="owner_referral"  class="form-control autocomplete" name="owner_referral" value="@isset($jobs->bo->owner_referral){{$jobs->bo->owner_referral}}@endisset{{old('owner_referral')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
        <li><a>owner</a></li>
        <li><a>owners</a></li>        
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('has_appointed_have_appointed') ? ' has-error' : ''}}">
        <label for="has_appointed_have_appointed" class="control-label">BO has appointed / have appointed:</label>
        <input id="has_appointed_have_appointed"  class="form-control autocomplete" name="has_appointed_have_appointed" value="@isset($jobs->bo->has_appointed_have_appointed ){{$jobs->bo->has_appointed_have_appointed}}@endisset{{old('has_appointed_have_appointed')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>have</a></li>
            <li><a>has</a></li>
        </ul>       
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('owners_owners') ? ' has-error' : ''}}">
        <label for="owners_owners" class="control-label">BO owner's/owners'</label>
        <input id="owners_owners"  class="form-control autocomplete" name="owners_owners" value="@isset($jobs->bo->owners_owners ){{$jobs->bo->owners_owners}}@endisset{{old('owners_owners')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
             <li><a>owner's</a></li>
            <li><a>owners'</a></li>
        </ul>     
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('is_are') ? ' has-error' : ''}}">
        <label for="is_are" class="control-label">BO is/are:</label>
        <input id="is_are"  class="form-control autocomplete" name="is_are" value="@isset($jobs->bo->is_are ){{$jobs->bo->is_are}}@endisset{{old('is_are')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>is an</a></li>
            <li><a>are</a></li>
        </ul>       
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('i_we_referral_upper_case') ? ' has-error' : ''}}">
        <label for="i_we_referral_upper_case" class="control-label">BO I/we (referral) Upper case:</label>
        <input id="i_we_referral_upper_case" class="form-control autocomplete" name="i_we_referral_upper_case" value="@isset($jobs->bo->i_we_referral_upper_case ){{$jobs->bo->i_we_referral_upper_case}}@endisset{{old('i_we_referral_upper_case')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>We</a></li>
        </ul>        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('i_we_referral_lower_case') ? ' has-error' : ''}}">
        <label for="i_we_referral_lower_case" class="control-label">BO I/we (referral) Lower case:</label>
        <input id="i_we_referral_lower_case"  class="form-control autocomplete" name="i_we_referral_lower_case" value="@isset($jobs->bo->i_we_referral_lower_case ){{$jobs->bo->i_we_referral_lower_case}}@endisset{{old('i_we_referral_lower_case')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>we</a></li>
        </ul>     
    </div>
</div>


<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('my_our_referral') ? ' has-error' : ''}}">
        <label for="my_our_referral" class="control-label">BO my/our (referral):</label>
        <input id="my_our_referral" class="form-control autocomplete" name="my_our_referral" value="@isset($jobs->bo->my_our_referral ){{$jobs->bo->my_our_referral}}@endisset{{old('my_our_referral')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>my</a></li>
            <li><a>our</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('he_she_they_referral') ? ' has-error' : ''}}">
        <label for="he_she_they_referral" class="control-label">BO he/she/they (referral):</label>
        <input id="he_she_they_referral" class="form-control autocomplete" name="he_she_they_referral" value="@isset($jobs->bo->he_she_they_referral ){{$jobs->bo->he_she_they_referral}}@endisset{{old('he_she_they_referral')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>he</a></li>
            <li><a>she</a></li>
            <li><a>they</a></li>
        </ul>        
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('intend_intends') ? ' has-error' : ''}}">
        <label for="intend_intends" class="control-label">BO intend/intends:</label>
        <input id="intend_intends"  class="form-control autocomplete" name="intend_intends" value="@isset($jobs->bo->intend_intends ){{$jobs->bo->intend_intends}}@endisset{{old('intend_intends')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>intend</a></li>
            <li><a>intends</a></li>
        </ul> 
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('s_s') ? ' has-error' : ''}}">
        <label for="s_s" class="control-label">BO 's/s'/':</label>
        <input id="s_s" class="form-control autocomplete" name="s_s" value="@isset($jobs->bo->s_s ){{$jobs->bo->s_s}}@endisset{{old('s_s')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>'s</a></li>
            <li><a>s'</a></li>
            <li><a>'</a></li>
        </ul>      
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('carry_carries') ? ' has-error' : ''}}">
        <label for="carry_carries" class="control-label">BO carry/carries:</label>
        <input id="carry_carries"  class="form-control autocomplete" name="carry_carries" value="@isset($jobs->bo->carry_carries ){{$jobs->bo->carry_carries}}@endisset{{old('carry_carries')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>carry</a></li>
            <li><a>carries</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{$errors->has('his_her_their') ? ' has-error' : ''}}">
        <label for="his_her_their" class="control-label">BO his/her/their:</label>
        <input id="his_her_their"  class="form-control autocomplete" name="his_her_their" value="@isset($jobs->bo->his_her_their ){{$jobs->bo->his_her_their}}@endisset{{old('his_her_their')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>his</a></li>
            <li><a>her</a></li>
            <li><a>their</a></li>
        </ul>
    </div>
</div>

