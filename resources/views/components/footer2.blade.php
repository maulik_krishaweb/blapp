<footer class="primary">
    
    	<nav class="primary tablet tablet-large desktop">
            <div class="full-wrapper">
                <div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="https://www.blsurveyors.com/about-us/">About</a></li>
<li id="menu-item-1616" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1616"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a></li>
<li id="menu-item-510" class="menu-item menu-item-type-post_type_archive menu-item-object-video-gallery menu-item-510"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>            </div>
        </nav><!-- .primary -->
        
    <div class="full-wrapper">
    	<div class="lower-footer-area">
        
        	<div class="contact-col col">
            	<h3>Contact Details</h3>
                <a href="mailto:info@blsurveyors.com" title="info@blsurveyors.com" class="email popup">info@blsurveyors.com</a>
                
				<div class="title" style="font-weight: bold;">Head Office:</div>
                <a class="tel" href="tel:020 7935 2502">020 7935 2502</a>
            </div><!-- .contact-col -->
            
            
            
            <div class="offices-col col">
            	<h3>Our Offices</h3>
                <ul class="our-offices" style="list-style: none; padding-left: 0px;">
                                	<li>
                        <div class="title" style="font-weight: bold;">Head Office</div>
                        <address>61 Highgate High Street,<br>
(Upper Floors)<br>
London, N6 5JY</address>
                        <a class="contact" href="tel:020 7935 2502"><span>Tel:</span> 020 7935 2502</a>
                    </li>
                                	<li>
                        <div class="title" style="font-weight: bold;">Central London</div>
                        <address>83 Baker Street <br>
London <br>
W1U 6AG</address>
                        <a class="contact" href="tel:020 8340 6586"><span>Tel:</span> 020 8340 6586</a>
                    </li>
                                	<li>
                        <div class="title" style="font-weight: bold;">Surrey</div>
                        <address>Global House <br>
1 Ashley Avenue<br>
Epsom, KT18 5AD<br>
</address>
                        <a class="contact" href="tel:020 3380 5596"><span>Tel:</span> 020 3380 5596</a>
                    </li>
                                </ul>
            </div><!-- .offices-col -->
            
            
            
            <div class="follow-us-col col">
            	<h3>Follow us</h3>
                <ul class="social-media" style="padding-left: 0px !important;">
					<a class="twitter popup" target="_blank" href="https://twitter.com/Berry_Lodge" title="Follow us on Twitter"><img align="left" alt="" class="" src="https://www.blsurveyors.com/wp-content/uploads/2019/07/twitter.png" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;margin-right:0;max-width:40px;padding-right:2px;padding-bottom:7px;" width="40px"></a>
                    <a href="https://www.instagram.com/berrylodgesurveyors/" target="_blank"><img align="left" alt="" class="" src="https://www.blsurveyors.com/wp-content/uploads/2019/07/instagram.png" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;margin-right:0;max-width:40px;padding-right:2px;padding-bottom:7px;" width="40px"></a>
                    <a href="https://www.youtube.com/channel/UC4cb42sRo7g9GcXHHpMpJoA" target="_blank"><img align="left" alt="" class="" src="https://www.blsurveyors.com/wp-content/uploads/2019/07/youtube.png" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;margin-right:0;max-width:40px;padding-right:2px;padding-bottom:7px;" width="340px"></a>
                    <a href="https://open.spotify.com/show/3EB4CjFu9SXAM7d03f19AS" target="_blank"><img align="left" alt="" class="" src="https://www.blsurveyors.com/wp-content/uploads/2019/07/spotify-1.png" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;margin-right:0;max-width:40px; padding-right:2px;padding-bottom:7px;" width="40px"></a>
					<a class="itunes popup"  target="_blank" href="https://itunes.apple.com/gb/podcast/the-party-wall-podcast/id1008968042?mt=2" title="Follow us on iTunes"><img align="left" alt="" class="" src="https://www.blsurveyors.com/wp-content/uploads/2019/07/itunes-logo.png" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;margin-right:0;max-width:95px;padding-right:2px;padding-bottom:7px;" width="95px"></a>
				</ul>            
			</div><!-- .follow-us-col -->
            
            
            <div class="footer-terms">
            	<div class="copyright">copyright Berry Lodge Surveyors 2015. All rights reserved.</div>
                <nav class="terms">
                	<div class="menu-footer-base-menu-container"><ul id="menu-footer-base-menu" class="menu"><li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="https://www.blsurveyors.com/terms-and-conditions/">Terms and Conditions</a></li>
<li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a href="https://www.blsurveyors.com/privacy-policy/">Privacy Policy</a></li>
</ul></div>                </nav>
                <!--
                <a class="impression-logo popup" href="http://www.weareimpression.co.uk" title="Web design Yorkshire">
                    <img src="https://www.blsurveyors.com/wp-content/themes/blsurveyors/img/global/impression-logo.png') }}" alt="Berry Lodge logo" />
                </a>
                -->
            </div>
            
        </div><!-- .lower-footer-area -->
	</div>
    </footer>




