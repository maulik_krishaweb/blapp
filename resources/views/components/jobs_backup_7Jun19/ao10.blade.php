<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_full_information') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_full_information" class="control-label">AO 2 Surveyor Full Information</label>
        <input id="ao_10_surveyor_full_information" class="form-control autocomplete" name="ao_10_surveyor_full_information" value="@isset( $jobs->ao10->surveyor_full_information ){{$jobs->ao10->surveyor_full_information }}@endisset{{ old('ao_10_surveyor_full_information') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
           @foreach ($surveyor as $person)
                <li data-sr="10"
                data-a10-name="{{ $person->name}}"
                data-a10-qualifications="{{ $person->qualifications}}"
                data-a10-company="{{ $person->company}}"
                data-a10-address="{{ $person->address}}"
                data-a10-contact="Telephone: {{ $person->telephone}} Email: {{ $person->email}}"><a>{{ $person->name}} {{ $person->qualifications}} , {{ $person->company}}, {{ $person->address}} Telephone: {{ $person->telephone}} Email: {{ $person->email}}</a></li>
            @endforeach
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_name') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_name" class="control-label">AO 2 Surveyor Name:</label>
        <input id="ao_10_surveyor_name" type="text" class="form-control" name="ao_10_surveyor_name" value="@isset( $jobs->ao10->surveyor_name ){{$jobs->ao10->surveyor_name }}@endisset{{ old('ao_10_surveyor_name') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_qualifications') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_qualifications" class="control-label">AO 2 Surveyor Qualifications:</label>
        <input id="ao_10_surveyor_qualifications" type="text" class="form-control" name="ao_10_surveyor_qualifications" value="@isset( $jobs->ao10->surveyor_qualifications ){{$jobs->ao10->surveyor_qualifications }}@endisset{{ old('ao_10_surveyor_qualifications') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_company_name') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_company_name" class="control-label">AO 2 Surveyor Company Name:</label>
        <input id="ao_10_surveyor_company_name" type="text" class="form-control" name="ao_10_surveyor_company_name" value="@isset( $jobs->ao10->surveyor_company_name ){{$jobs->ao10->surveyor_company_name }}@endisset{{ old('ao_10_surveyor_company_name') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_company_address') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_company_address" class="control-label">AO 2 Surveyor Company Address:</label>
        <input id="ao_10_surveyor_company_address" type="text" class="form-control" name="ao_10_surveyor_company_address" value="@isset( $jobs->ao10->surveyor_company_address ){{$jobs->ao10->surveyor_company_address }}@endisset{{ old('ao_10_surveyor_company_address') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_surveyor_contact_details') ? ' has-error' : '' }}">
        <label for="ao_10_surveyor_contact_details" class="control-label">AO 2 Surveyor Contact Details:</label>
        <input id="ao_10_surveyor_contact_details" type="text" class="form-control" name="ao_10_surveyor_contact_details" value="@isset( $jobs->ao10->surveyor_contact_details ){{$jobs->ao10->surveyor_contact_details }}@endisset{{ old('ao_10_surveyor_contact_details') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_ao10_full_names') ? ' has-error' : '' }}">
        <label for="ao_10_full_names" class="control-label">AO 2 Full Name(s):</label>
        <input id="ao_10_full_names" type="text" class="form-control" name="ao_10_full_names" value="@isset( $jobs->ao10->full_names ){{$jobs->ao10->full_names }}@endisset{{ old('ao_10_full_names') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_salutation') ? ' has-error' : '' }}">
        <label for="ao_10_salutation" class="control-label">AO 2 Salutation:</label>
        <input id="ao_10_salutation" type="text" class="form-control" name="ao_10_salutation" value="@isset( $jobs->ao10->salutation ){{$jobs->ao10->salutation }}@endisset{{ old('ao_10_salutation') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_property_address_adjoining') ? ' has-error' : '' }}">
        <label for="ao_10_property_address_adjoining" class="control-label">AO 2 Property Address (adjoining):</label>
        <input id="ao_10_property_address_adjoining" type="text" class="form-control" name="ao_10_property_address_adjoining" value="@isset( $jobs->ao10->property_address_adjoining ){{$jobs->ao10->property_address_adjoining }}@endisset{{ old('ao_10_property_address_adjoining') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_contact_address') ? ' has-error' : '' }}">
        <label for="ao_10_contact_address" class="control-label">AO 2 Contact Address:</label>
        <input id="ao_10_contact_address" type="text" class="form-control" name="ao_10_contact_address" value="@isset( $jobs->ao10->contact_address ){{$jobs->ao10->contact_address }}@endisset{{ old('ao_10_contact_address') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_contact_details') ? ' has-error' : '' }}">
        <label for="ao_10_contact_details" class="control-label">AO 2 Contact Details  (Tel. Nos. and Email):</label>
        <input id="ao_10_contact_details" type="text" class="form-control" name="ao_10_contact_details" value="@isset( $jobs->ao10->contact_details ){{$jobs->ao10->contact_details }}@endisset{{ old('ao_10_contact_details') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_owners_referral') ? ' has-error' : '' }}">
        <label for="ao_10_owners_referral" class="control-label">AO 2 owner/s (referral):</label>
        <input id="ao_10_owners_referral"  class="form-control autocomplete" name="ao_10_owners_referral" value="@isset( $jobs->ao10->owners_referral ){{$jobs->ao10->owners_referral }}@endisset{{ old('ao_10_owners_referral') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>owner</a></li>
            <li><a>owners</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_has_appointed_have_appointed') ? ' has-error' : '' }}">
        <label for="ao_10_has_appointed_have_appointed" class="control-label">AO 2 has appointed / have appointed:</label>
        <input id="ao_10_has_appointed_have_appointed"  class="form-control autocomplete" name="ao_10_has_appointed_have_appointed" value="@isset( $jobs->ao10->has_appointed_have_appointed ){{$jobs->ao10->has_appointed_have_appointed }}@endisset{{ old('ao_10_has_appointed_have_appointed') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>have</a></li>
            <li><a>has</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_i_we_referral') ? ' has-error' : '' }}">
        <label for="ao_10_i_we_referral" class="control-label">AO 2 I/we (referral):</label>
        <input id="ao_10_i_we_referral" class="form-control autocomplete" name="ao_10_i_we_referral" value="@isset( $jobs->ao10->i_we_referral ){{$jobs->ao10->i_we_referral }}@endisset{{ old('ao_10_i_we_referral') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>We</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_i_we_referral_lower') ? ' has-error' : '' }}">
        <label for="ao_10_i_we_referral_lower" class="control-label">AO 2 I/we (referral) lower:</label>
        <input id="ao_10_i_we_referral_lower"  class="form-control autocomplete" name="ao_10_i_we_referral_lower" value="@isset( $jobs->ao10->i_we_referral_lower ){{$jobs->ao10->i_we_referral_lower }}@endisset{{ old('ao_10_i_we_referral_lower') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>we</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_my_our_refferal') ? ' has-error' : '' }}">
        <label for="ao_10_my_our_refferal" class="control-label">AO 2 my/our (referral):</label>
        <input id="ao_10_my_our_refferal"  class="form-control autocomplete" name="ao_10_my_our_refferal" value="@isset( $jobs->ao10->my_our_refferal ){{$jobs->ao10->my_our_refferal }}@endisset{{ old('ao_10_my_our_refferal') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>my</a></li>
            <li><a>our</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_he_she_they_referral') ? ' has-error' : '' }}">
        <label for="ao_10_he_she_they_referral" class="control-label">AO 2 he/she/they (referral):</label>
        <input id="ao_10_he_she_they_referral"  class="form-control autocomplete" name="ao_10_he_she_they_referral" value="@isset( $jobs->ao10->he_she_they_referral ){{$jobs->ao10->he_she_they_referral }}@endisset{{ old('ao_10_he_she_they_referral') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>he</a></li>
            <li><a>she</a></li>
            <li><a>they</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_his_her_their') ? ' has-error' : '' }}">
        <label for="ao_10_his_her_their" class="control-label">AO 2 his/her/their:</label>
        <input id="ao_10_his_her_their" class="form-control autocomplete" name="ao_10_his_her_their" value="@isset( $jobs->ao10->his_her_their ){{$jobs->ao10->his_her_their }}@endisset{{ old('ao_10_his_her_their') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>his</a></li>
            <li><a>her</a></li>
            <li><a>their</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_owners_owners') ? ' has-error' : '' }}">
        <label for="ao_10_owners_owners" class="control-label">AO 2 owner's / owners'</label>
        <input id="ao_10_owners_owners" class="form-control autocomplete" name="ao_10_owners_owners" value="@isset( $jobs->ao10->owners_owners ){{$jobs->ao10->owners_owners }}@endisset{{ old('ao_10_owners_owners') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>owner's</a></li>
            <li><a>owners'</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_is_an_are') ? ' has-error' : '' }}">
        <label for="ao_10_is_an_are" class="control-label">AO 2 is an/are:</label>
        <input id="ao_10_is_an_are"  class="form-control autocomplete" name="ao_10_is_an_are" value="@isset( $jobs->ao10->is_an_are ){{$jobs->ao10->is_an_are }}@endisset{{ old('ao_10_is_an_are') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>is an</a></li>
            <li><a>are</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_s_s') ? ' has-error' : '' }}">
        <label for="ao_10_s_s" class="control-label">AO 2 's/s'/':</label>
        <input id="ao_10_s_s"  class="form-control autocomplete" name="ao_10_s_s" value="@isset( $jobs->ao10->s_s ){{$jobs->ao10->s_s }}@endisset{{ old('ao_10_s_s') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>'s</a></li>
            <li><a>s'</a></li>
            <li><a>'</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s1_section') ? ' has-error' : '' }}">
        <label for="ao_10_s1_section" class="control-label">AO 2 S1 Section:</label>
        <input id="ao_10_s1_section" class="form-control autocomplete" name="ao_10_s1_section" value="@isset( $jobs->ao10->s1_section ){{$jobs->ao10->s1_section }}@endisset{{ old('ao_10_s1_section') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>1(2)</a></li>
            <li><a>1(5)</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s1_description') ? ' has-error' : '' }}">
        <label for="ao_10_s1_description" class="control-label">AO 2 S1 Description:</label>
        <input id="ao_10_s1_description"  class="form-control autocomplete" name="ao_10_s1_description" value="@isset( $jobs->ao10->s1_description ){{$jobs->ao10->s1_description }}@endisset{{ old('ao_10_s1_description') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Build a new wall up to the line of junction, but wholly on the building owner's land forming the flank wall of the proposed extension</a></li>
            <li><a>Build a new wall up to the line of junction, but wholly on the building owners' land forming the flank wall of the proposed extension</a></li>
            <li><a>Build a new party wall astride the line of junction, forming the flank wall of the proposed extension</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s2_section') ? ' has-error' : '' }}">
        <label for="ao_10_s2_section" class="control-label">AO 2 S2 Section:</label>
        <input id="ao_10_s2_section" class="form-control autocomplete" name="ao_10_s2_section" value="@isset( $jobs->ao10->s2_section ){{$jobs->ao10->s2_section }}@endisset{{ old('ao_10_s2_section') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>2(2)(a)</a></li>
            <li><a>2(2)(b)</a></li>
            <li><a>2(2)(f)</a></li>
            <li><a>2(2)(g)</a></li>
            <li><a>2(2)(h)</a></li>
            <li><a>2(2)(j)</a></li>
            <li><a>2(2)(k)</a></li>
            <li><a>2(2)(i)</a></li>
            <li><a>2(2)(n)</a></li>
            <li><a>2(2)(f) & (n)</a></li>
            <li><a>2(2)(f),(g) & (n)</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s2_description') ? ' has-error' : '' }}">
        <label for="ao_10_s2_description" class="control-label">AO 2 S2 Description:</label>
        <input id="ao_10_s2_description"  class="form-control autocomplete" name="ao_10_s2_description" value="@isset( $jobs->ao10->s2_description ){{$jobs->ao10->s2_description }}@endisset{{ old('ao_10_s2_description') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Cut into the party wall for the insertion of steel beams facilitating a proposed loft conversion in the process exposing the party wall</a></li>
            <li><a>Cut away the chimney breast attached to the party wall</a></li>
            <li><a>Cut away the chimney breasts attached to the party wall</a></li>
            <li><a>Cut into the party wall for the insertion of steel beams facilitating a proposed loft conversion in the process exposing the party wall and cut away the chimney breast attached to the party wall</a></li>
            <li><a>Cut into the party wall for the insertion of steel beams facilitating a proposed loft conversion in the process exposing the party wall and cut away the chimney breasts attached to the party wall</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s6_section') ? ' has-error' : '' }}">
        <label for="ao_10_s6_section" class="control-label">AO 2 S6 Section:</label>
        <input id="ao_10_s6_section" class="form-control autocomplete" name="ao_10_s6_section" value="@isset( $jobs->ao10->s6_section ){{$jobs->ao10->s6_section }}@endisset{{ old('ao_10_s6_section') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>6(1)</a></li>
            <li><a>6(2)</a></li>
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('ao_10_s6_description') ? ' has-error' : '' }}">
        <label for="ao_10_s6_description" class="control-label">AO 2 S6 Description:</label>
        <input id="ao_10_s6_description"  class="form-control autocomplete" name="ao_10_s6_description" value="@isset( $jobs->ao10->s6_description ){{$jobs->ao10->s6_description }}@endisset{{ old('ao_10_s6_description') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Excavate to accommodate the foundations to a proposed extension</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_date_of_notice') ? ' has-error' : '' }}">
        <label for="ao_10_date_of_notice" class="control-label">AO 2 Date of Notice:</label>
        <input id="ao_10_date_of_notice" type="text" class="form-control" name="ao_10_date_of_notice" value="@isset( $jobs->ao10->date_of_notice ){{$jobs->ao10->date_of_notice }}@endisset{{ old('ao_10_date_of_notice') }}" data-toggle="dropdown" autocomplete="off"  >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_notice_notices') ? ' has-error' : '' }}">
        <label for="ao_10_notice_notices" class="control-label">AO 2 Notice/Notices:</label>
        <input id="ao_10_notice_notices" class="form-control autocomplete" name="ao_10_notice_notices" value="@isset( $jobs->ao10->notice_notices ){{$jobs->ao10->notice_notices }}@endisset{{ old('ao_10_notice_notices') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Notice</a></li>
            <li><a>Notices</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_section_sections') ? ' has-error' : '' }}">
        <label for="ao_10_section_sections" class="control-label">AO 2 Section/Sections:</label>
        <input id="ao_10_section_sections"  class="form-control autocomplete" name="ao_10_section_sections" value="@isset( $jobs->ao10->section_sections ){{$jobs->ao10->section_sections }}@endisset{{ old('ao_10_section_sections') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Section</a></li>
            <li><a>Sections</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_drawings') ? ' has-error' : '' }}">
        <label for="ao_10_drawings" class="control-label">AO 2 Drawings:</label>
        <input id="ao_10_drawings" type="text" class="form-control" name="ao_10_drawings" value="@isset( $jobs->ao10->drawings ){{$jobs->ao10->drawings }}@endisset{{ old('ao_10_drawings') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_soc_date') ? ' has-error' : '' }}">
        <label for="ao_10_soc_date" class="control-label">AO 2 SOC DATE:</label>
        <input id="ao_10_soc_date" type="text" class="form-control" name="ao_10_soc_date" value="@isset( $jobs->ao10->soc_date ){{$jobs->ao10->soc_date }}@endisset{{ old('ao_10_soc_date') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ao_10_third_surveyor') ? ' has-error' : '' }}">
        <label for="ao_10_third_surveyor" class="control-label">Third Surveyor:</label>
        <input id="ao_10_third_surveyor"  class="form-control autocomplete" name="ao_10_third_surveyor" value="@isset( $jobs->ao10->third_surveyor ){{$jobs->ao10->third_surveyor }}@endisset{{ old('ao_10_third_surveyor') }}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>David Moon DipBs FRICS of Davis Brown Chartered Surveyors, 1 Margaret Street, London W1W 8RB Email: dcmoon@davis-brown.co.uk Telephone: 0207 637 ao1066</a></li>
            <li><a>Andrew Schofield BSc FRICS, Schofield Surveyors, 4th Floor, 3-4 John Prince's Street, London, W1G 0JL Email: andrew.schofield@schofieldsurveyors.co.uk Tel: 020 3771 9445</a></li>
            <li><a>Graham North MRICS of Anstey Horne, 4 Chiswell Street, London EC1Y 4UP Email: GrahamNorth@ansteyhorne.co.ukTelephone: 020 7065 2770</a></li>
        </ul>
    </div>
</div>
