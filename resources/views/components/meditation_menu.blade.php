 <div class="col-md-9 col-xs-12" style="margin-bottom: -1px;">
    <div class="row" id="admin-menu">    
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client') }}">
                <div class="admin-menu-item @if (Request::is('client')) item-active @endif">
                    <i class="fa fa-clipboard" style="font-size:24px"></i><p style="font-family: Poppins;">Tasks </p>
                </div>
            </a>
        </div>
        <div class="col-md-2  col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/documents') }}">
                <div class="admin-menu-item @if (Request::is('client/documents')) item-active @endif">
                  <i class="fa fa-file-o" style="font-size:24px"></i><br><p style="font-family: Poppins;">Document  Storage</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/message') }}">
                <div class="admin-menu-item @if (Request::is('client/message')) item-active @endif">
                    <i class="fa fa fa-commenting-o" style="font-size:24px"></i><br><p style="font-family: Poppins;">Messages </p>
                </div>
            </a>
        </div>
    </div>
</div>