@extends('layouts.frame')

@section('content')
<style>
.filter-input {
    padding: 30px;
    margin: 10px 0px;
    border: 0;
    border-radius: 0;
    background: #eef2f6;
  }
</style>
<div class="container-fluid">
    	<div class="container" >
	       <h1>
	       	 My Jobs @if (Request::is('admin/my-jobs/archived')) <span style="color: #cccccc">Archived</span> @endif
		</h1>
	       <ol class="breadcrumb">
			<li><a href="{{ url('admin') }}">Home</a></li>
			<li class="admin/jobs">  My Jobs</li>
		</ol>
	</div>
  	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<input class="form-control filter-input" id="myInput" type="text" placeholder="Search by Job No ...">
			</div>
			<div class="col-md-5">
				<input class="form-control filter-input" id="myInputAdd" type="text" placeholder="Search by Address ...">
			</div>
			<div class="col-md-3 text-center">
				@if (Request::is('admin/my-jobs/archived'))
					<a href="{{ url('/admin/my-jobs') }}" class="btn btn-block" style="margin: 10px 0px; border-radius: 0px; padding: 18px; background-color: #414861; color: #fff;">View Current Jobs</a>
				@else
					<a href="{{ url('/admin/my-jobs/archived') }}" class="btn btn-block" style="margin: 10px 0px; border-radius: 0px; padding: 18px; background-color: #414861; color: #fff;">View Archived Jobs</a>
				@endif
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered">
			    	<thead>
				      <tr>
				      		<th>Job No</th>
				      		<th>Enquiry Id</th>
				      		<th>Type</th>
				      		<th>Invoice No</th>
				       		<th>Address</th>
				        	<th>Name</th>
				        	<th style="width:220px">Options</th>
				      	</tr>
			   	</thead>
			    	<tbody id="jobs_list">
				    	@if($jobs)
					      	@foreach($jobs as $job)
						      	@if ($job->surveying)
						      		<tr data-id="{{ $job->id }}" class="job_item" data-no="{{ $job->id}}" data-add="{!! $job->surveying->address_of_inspection !!}">
							      		<td>BLSN{{ $job->id}}</td>
							      		<td>
							      			@if ($job->enq_id)
							      			<a href="{{ url("/admin/enquiries/$job->enq_id") }}">EBLSN{{$job->enq_id}}</a>
							      			@endif
							      		</td>
							      		<td>{{ $job->surveying->surveying_service}}</td>
							      		<td>{{ $job->invoice_no}}</td>
							        	<td>{{ $job->surveying->address_of_inspection }}</td>
							        	<td>{{  $job->surveying->client_full_name }} </td>

							        	<td>
							        		<a href="{{ url('/admin/my-jobs/view/'.$job->id ) }}" data-toggle="tooltip" title="View & Edit" class="btn btn-xs btn-default">
							        			<i class="fa fa-eye" aria-hidden="true"></i>
							        		</a>
							        		<a href="{{ url('/admin/my-jobs/tasks/'.$job->id ) }}" data-toggle="tooltip" title="Tasks" class="btn btn-xs btn-default job-tasks-btn">
							        			<i class="fa fa-tasks" aria-hidden="true"></i>
							        		</a>
							        		<a href="{{ url('/admin/my-jobs/users/'.$job->id ) }}" data-toggle="tooltip" title="People" class="btn btn-xs btn-default job-tasks-btn">
							        			<i class="fa fa-users" aria-hidden="true"></i>
							        		</a>

								        	<a href="{{ url('/admin/my-jobs/surveying/'.$job->id ) }}" data-toggle="tooltip" title="Surveying Documents" class="btn btn-xs btn-default">
								        			<i class="fa fa-file-text-o" aria-hidden="true"></i>
								        	</a>

							        		<button data-toggle="tooltip" title="Payment" class="btn btn-xs btn-default payment-btn" data-id="{{$job->id }}">
							        			<i class="fa fa-credit-card" aria-hidden="true"></i>
							        		</button>
							        		<button data-toggle="tooltip" title="@if ($job->status==8)Open @else Archive @endif" class="btn btn-xs btn-default archive-btn" data-id="{{$job->id }}" data-status="{{$job->status }}">
							        			<i class="fa fa-file-zip-o" aria-hidden="true"></i>
							        		</button>
							        		@if (Auth::user()->role== 9)
							        			<button  class="btn btn-danger btn-xs pull-right delete-job"><i class="fa fa-trash-o" aria-hidden="true"></i>
							        			</button>
							        		@endif
						        		</td>
						      		</tr>
						      	@else
						      		<tr data-id="{{ $job->id }}" class="job_item" data-no="{{ $job->id}}" data-add="{{ $job->bo->property_address_proposed_work ?? '' }}">
							      		<td>BLSN{{ $job->id}}</td>
							      		<td>
							      			@if ($job->enq_id)
							      			<a href="{{ url("/admin/enquiries/$job->enq_id") }}">EBLSN{{$job->enq_id}}</a>
							      			@endif


							      		</td>
							      		<td>{{ $job->job_type}}</td>
							      		<td>{{ $job->invoice_no}}</td>
							        	<td>{{ $job->bo->property_address_proposed_work}}</td>
							        	<td>{{ $job->bo->full_name }} </td>

							        	<td>
							        		<a href="{{ url('/admin/my-jobs/view/'.$job->id ) }}" data-toggle="tooltip" title="View & Edit" class="btn btn-xs btn-default">
							        			<i class="fa fa-eye" aria-hidden="true"></i>
							        		</a>
							        		<a href="{{ url('/admin/my-jobs/tasks/'.$job->id ) }}" data-toggle="tooltip" title="Tasks" class="btn btn-xs btn-default job-tasks-btn">
							        			<i class="fa fa-tasks" aria-hidden="true"></i>
							        		</a>
							        		<a href="{{ url('/admin/my-jobs/users/'.$job->id ) }}" data-toggle="tooltip" title="People" class="btn btn-xs btn-default job-tasks-btn">
							        			<i class="fa fa-users" aria-hidden="true"></i>
							        		</a>
							        		<a href="{{ url('/admin/my-jobs/doc/'.$job->id ) }}" data-toggle="tooltip" title="Documents" class="btn btn-xs btn-default">
						        				<i class="fa fa-file-text-o" aria-hidden="true"></i>
						        			</a>
							        		<button data-toggle="tooltip" title="Payment" class="btn btn-xs btn-default payment-btn" data-id="{{$job->id }}">
							        			<i class="fa fa-credit-card" aria-hidden="true"></i>
							        		</button>
							        		<button data-toggle="tooltip" title="@if ($job->status==8)Open @else Archive @endif" class="btn btn-xs btn-default archive-btn" data-id="{{$job->id }}" data-status="{{$job->status }}">
							        			<i class="fa fa-file-zip-o" aria-hidden="true"></i>
							        		</button>

							        		@if (Auth::user()->role== 9)
							        			<button  class="btn btn-danger btn-xs pull-right delete-job"><i class="fa fa-trash-o" aria-hidden="true"></i>
							        			</button>
							        		@endif
						        		</td>
						      		</tr>

						      	@endif
					      	@endforeach
				     	@endif
				</tbody>
			</table>
		</div>
 	</div>
	<div class="container">
			{{--$jobs->links() --}}
			<span class="pull-right">
				<button type="button" class="btn blue-btn" id="add-new-job-btn" style="margin: 22px 0">Add New Job</button>
			</span>
	</div>
	@if (!Request::is('admin/jobs/archived'))
	       <div class="container">
	       	<h1>Tasks </h1>
	        	<style>a:hover { text-decoration: none; } a{ color: #424961; } </style>
		       @foreach ($tasks as $key=> $task)
				<div class="panel">
	      				<div class="panel-heading" style="border: solid 0.5px #ccc;">
	      					@if ($key ==1)
	      						<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}">Adjoining Owners Surveyor  <span data-key="{{ $key }}" class=" pull-right add-task"><i class="fa fa-angle-down"></i></span></a>
	      					@elseif($key ==2)
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}">Building Owners Surveyor  <span data-key="{{ $key }}" class=" pull-right add-task"><i class="fa fa-angle-down"></i></span></a>
	      					@elseif($key ==3)
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}">Agreed Surveyor <span data-key="{{ $key }}" class=" pull-right add-task"><i class="fa fa-angle-down"  ></i></span></a>
						@elseif($key ==4)
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}">Surveying Tasks <span data-key="{{ $key }}" class=" pull-right add-task"><i class="fa fa-angle-down"  ></i></span></a>
	      					@endif
	      				</div>
      					<div id="collapse{{ $key }}" class="panel-collapse collapse">
	 		 			@foreach($task as $item)
							<div class="panel-body" style="background-color: rgba(0, 176, 159, 0.59); color: #fff; border-bottom: solid 0.5px #fff;">
								<strong style="color: #424961;">
									@if($item->is_internal==1)
										Internal -
									@endif
								</strong>
									{{ $item->task }}
								{{-- <button type="button" data-id="{{ $item->id }}" class="btn btn-xs btn-danger delete-task-btn pull-right">Delete</button> --}}
								<button type="button" data-id="{{ $item->id }}" class="btn btn-xs btn-primary pull-right rename-task-btn" data-name="{{ $item->task }}" style="margin-right: 4px">Rename</button>	
							</div>
						@endforeach
					</div>
				</div>
			@endforeach
			<button type="button" class="btn blue-btn pull-right" data-toggle="modal" data-target="#addtaskModal">Add Task</button>
		</div>
	@endif

		<div id="addtaskModal" class="modal fade" role="dialog">
		  	<div class="modal-dialog">
		   		<div class="modal-content">
			      		<div class="modal-header">
			       	 	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        		<h4 class="modal-title">Add task</h4>
			      		</div>
			      		<div class="modal-body">
		      				<form class="form-horizontal" role="form" >
			          			<div class="form-group{{ $errors->has('task') ? ' has-error' : '' }}">
			              			<label for="task" class="col-md-4 control-label">Task</label>
			              			<div class="col-md-6">
			                  				<textarea id="task"  class="form-control" name="task" value="{{ old('task') }}" required autofocus> </textarea>
			              			</div>
			          			</div>
				                     <div class="form-group">
				                            <label for="job_type" class="col-md-4 control-label">Job Type</label>
				                            <div class="col-md-6">
					                           <select class="form-control" name="job_type" id="job_type">
						                           	<option value="1">Adjoining Owners Surveyor</option>
					  					<option value="2">Building Owners Surveyor</option>
					  					<option value="3">Agreed Surveyor</option>
					  					<option value="4">Surveying Tasks</option>
					                           </select>
				                            </div>
				                     </div>
				                     <div class="form-group">
				                            <label for="internal" class="col-md-4 control-label">Internal</label>
				                            <div class="col-md-6">
				                                	<label class="radio-inline">
										<input type="radio" name="internal" value="1">Yes
									</label>
								    	<label class="radio-inline">
								      		<input type="radio" name="internal" value="0">No
								    	</label>
				                            </div>
				                     </div>
				                     <div id="error-display"></div>
						    	<div class="modal-footer">
						      		<button type="button" class="btn blue-btn" id="add-task-btn">Add </button>
						        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						    	</div>
			       		</form>
   					</div>
  				</div>
			</div>
		</div>

		<div id="renametaskModal" class="modal fade" role="dialog">
		  	<div class="modal-dialog">
		   		<div class="modal-content">
		      		<div class="modal-header">
		       	 	<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">Rename task</h4>
		      		</div>
		      		<div class="modal-body">
	      				<form class="form-horizontal" role="form" >
		          			<div class="form-group{{ $errors->has('task') ? ' has-error' : '' }}">
		              			<label for="task" class="col-md-4 control-label">Task</label>
		              			<div class="col-md-6">
		                  			<textarea id="rename_task"  class="form-control" name="task" value="{{ old('task') }}" required autofocus> </textarea>
		              			</div>
		          			</div>
		          			<input type="hidden" name="task_id" id="rename_task_id">
		                     <div id="rename-error-display"></div>
					    	<div class="modal-footer">
					      		<button type="button" class="btn blue-btn" id="rename-task-btn">Update </button>
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					    	</div>
		       			</form>
   					</div>
  				</div>
			</div>
		</div>



{{-- Payment Modal --}}
<div id="payment" class="modal fade" role="dialog">
  	<div class="modal-dialog">
    		<div class="modal-content">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Payments</h4>
      			</div>
	      		<div class="modal-body">

      				<h3>Current Payments</h3>
				<table class="table table-bordered table-condensed">
				    <thead>
				      <tr>
				        <th>Description</th>
				        <th>Amount</th>
				        <th>Status/ Options</th>
				      </tr>
				    </thead>
				    <tbody id="payments-list" data-id="">
				    </tbody>
				</table>


	      			<h3>Add A Payment</h3>
	        		<div class="row">
	        			<div class="form-group col-md-7 col-sm-7">
					  	<label for="description">Description</label>
					  	<input type="text" class="form-control" id="payment-description">
					</div>
					<div class="form-group col-md-3 col-sm-3">
					  	<label for="amount">Amount</label>
					  	<input type="text" class="form-control" id="payment-amount">
					</div>
					<div class="form-group col-md-2 col-sm-2">
						<label for="add-payment">Add</label>
						<button class="btn btn-block btn-default" id="add-payment-btn" ><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
					</div>
	        		</div>

	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      		</div>
    		</div>
  	</div>
</div>
{{-- payment modal --}}
@endsection
@section('script')


<script type="text/javascript">

$(document).on('click', '#add-new-job-btn', function(event) {

swal("What kind of job would you like to set-up", {
  buttons: {

    partywall: {
      text: "Party Wall",
      value: "partywall",
    },
    survey: {
      text: "Other Survey Job",
      value: "survey",
    },
  },
})
.then((value) => {
  switch (value) {

    case "partywall":
     window.location.href = "{{ url('/admin/my-jobs/add/partywall') }}";
      break;

    case "survey":
      window.location.href = "{{ url('/admin/my-jobs/add/survey') }}";
      break;

    default:

  }
});

});
	//  url('/admin/my-jobs/add') "
$(document).on('click', '.payment-btn', function(event) {
	$('#payments-list').html('');
	var job_id= $(this).attr('data-id')
	$.ajax({
		url: '/admin/job/payment',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id},
		success: function(data){
			if (data.length >= 1) {
				var list = ''


				$.each(data, function(index, val) {
					list += '<tr><td>'+val.description+'</td>';
					list += '<td>£'+val.amount+'</td>';
					list += '<td>'+(val.paid ==1 ? 'Settled on '+val.updated_at : '<i class="fa fa-trash-o delete-payment" data-id="'+val.id+'" aria-hidden="true" style="padding-right: 20px"></i><button class="btn btn-xs btn-default settled-bank" id="'+val.id+'">Settled via transfer</button>');
					list += '</td></tr>';
				});
				$('#payments-list').html(list);
			}
			console.log(data);
			$('#payments-list').attr('data-id', job_id);
			$('#payment').modal("show");
		},
		error: function(data){
	    }
	})
});
$(document).on('click', '#add-payment-btn', function(event) {
	var job_id= $('#payments-list').attr('data-id');
	var description =$('#payment-description').val();
	var amount =$('#payment-amount').val();

	$.ajax({
		url: '/admin/job/add-payment',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id, amount: amount, description: description},
		success: function(data){
			if (data== 'added') {
				$('#payments-list').append('<tr><td>'+description+'</td> <td>£'+amount+'</td> <td><i class="fa fa-trash-o delete-payment" data-id="" aria-hidden="true" style="padding-right: 20px"></i><button class="btn btn-xs btn-default settled-bank" id="">Settled via transfer</button></td></tr>');
			}
			console.log(data);
		},
		error: function(data){
			console.log(data);
	    }
	})
});
$(document).on('click', '.delete-payment', function(event) {
	var payment_id= $(this).attr('data-id');
	var payment = $(this);
	$.ajax({
		url: '/admin/job/delete-payment',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {payment_id: payment_id},
		success: function(data){
			if (data== 'deleted') {
				payment.parent().parent().remove();
			}
			console.log(data);
		},
		error: function(data){
			console.log(data);
	    }
	})
});
$(document).on('click', '.settled-bank', function(event) {
	var payment_id= $(this).attr('id');
	var payment = $(this);
	$.ajax({
		url: '/admin/my-job/settled-payment',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {payment_id: payment_id},
		success: function(data){
			if (data== 'updated') {
				payment.parent().html('Settled');
			}
			console.log(data);
		},
		error: function(data){
			console.log(data);
	    }
	})
});

$(document).on('click', '.archive-btn', function(event) {
	var job_id= $(this).attr('data-id');
	if ($(this).attr('data-status')==8) {
		var status= 1;
	} else{
		var status= 8;
	}


	$.ajax({
		url: '/admin/my-jobs/archive',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id, status: status},
		success: function(data){
			console.log(data);
			if (data== 'archived') {
				location.reload();
			}

		},
		error: function(data){
			console.log(data);
	    }
	})
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#jobs_list .job_item").filter(function() {
      $(this).toggle($(this).attr('data-no').toLowerCase().indexOf(value) > -1)
    });
  });
});
$(document).ready(function(){
  $("#myInputAdd").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#jobs_list .job_item").filter(function() {
      $(this).toggle($(this).attr('data-add').toLowerCase().indexOf(value) > -1)
    });
  });
});

$(document).on('click', '#add-task-btn', function(event) {
		event.preventDefault();
		var task= $('#task').val();
		var internal= $("input[name='internal']:checked").val();
		var job_type=$('#job_type').val();


	$.ajax({
		url: '/admin/add-task',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {task: task, internal:internal, job_type:job_type},
		success: function(data){
			console.log(data);
			var status = $.parseJSON(data);
			if(status.status=='good'){
				swal("task added successfully ").then(() => {
				 location.reload();
				});
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    console.log(errors);
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
	    }

	})

});


$('.delete-task-btn').on('click',  function(event) {
	event.preventDefault();
	var task_id = $(this).attr('data-id');
	var deltask=$(this).parent();
	swal("Are you sure you wish to delete this task?", {
  buttons: ["Cancel", "Yes Delete task!"],
}).then(function(value){
	if(value==true){
		$.ajax({
			url: '/admin/delete-task',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data: {task_id: task_id},
			success: function(data){

				if(data=='good'){
					deltask.remove();
				}
			},
		});
	}
	return false;
},
	function(){
		console.log('Not deleted');
		return false;
	});
});

	$('.rename-task-btn').click(function(){
		$('#rename_task').val('');
		$('#rename_task_id').val('');
		$('#rename-error-display').html('');
		$('#rename_task').val($(this).data('name'));
		$('#rename_task_id').val($(this).data('id'));
		$('#renametaskModal').modal();
	});

	$('#rename-task-btn').click(function(){
		var id = $('#rename_task_id').val();
		var name = $('#rename_task').val();
		$.ajax({
			url:'rename-task',
			type:'post',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data:{id: id, task: name},
			success: function(data)
			{
				if (data == 'true') {
					location.reload();
					//$('#renametaskModal').modal('hide');			
				}else{
					alert('Plz try after sometime')
				}
			},
			error: function(data){
		    	var errors = $.parseJSON(data.responseText);
			    console.log(errors);
				var displayerror='<div class="alert alert-dismissible alert-danger">';
			   $.each(errors, function(index, value) {
			      displayerror+='<li>'+value+'</li>';
			    });
			    displayerror+='</div>';
			    $('#rename-error-display').html(displayerror);
		    }
		});
	});


</script>
<script type="text/javascript">
$(document).on('click', '#add-user-btn', function(event) {
		event.preventDefault();
		var name= $('#name').val();
		var email=$('#email').val();
		var role=$('#role').val();
		var password=$('#password').val();
		var password_confirmation=$('#password_confirm').val();

	$.ajax({
		url: '/add-user',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {name: name, email:email, role:role, password:password, password_confirmation:password_confirmation},
		success: function(data){
			var status = $.parseJSON(data);
			if(status.status=='good'){
				swal("User added successfully ").then(() => {
				 location.reload();
				});
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    console.log(errors);
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
	    }

	})

});


$('.delete-user-btn').on('click',  function(event) {
	event.preventDefault();
	var user_id = $(this).parent().parent().attr('data-id');
	swal("Are you sure you wish to delete this user?", {
  buttons: ["Cancel", "Yes Delete User!"],
}).then(function(value){
	if(value==true){
		$.ajax({
			url: '/delete-user',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data: {user_id: user_id},
			success: function(data){
				var status = $.parseJSON(data);
				if(status.status=='good'){
					location.reload();
				}
			},
		})
	}
	return false;
},
	function(){
		console.log('Not deleted');
		return false;
	});
});

$('.user-history-btn').on('click',  function(event) {
	event.preventDefault();
	var user_id = $(this).parent().parent().attr('data-id');
	$.ajax({
		url: '/user-history',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {user_id: user_id},
		success: function(data){
			console.log(data);
			$('#userHistoryModal').modal();
			var history = $.parseJSON(data);
			var displayhistory='<ul>';
		   	$.each(history, function(index, value) {
		    	displayhistory+='<li><strong> '+value.created_at+' :  '+value.action+' <br></strong> '+value.description+'</li>';
		    });
		    displayhistory+='</ul>';
		    $('#history-display').html(displayhistory);


		},
	})
});



$('.delete-job').on('click',  function(event) {
	event.preventDefault();
	var job_id = $(this).parent().parent().attr('data-id');

	swal("Are you sure you wish to delete this job?", {
  buttons: ["Cancel", "Yes Delete Job!"],
}).then(function(value){
	if(value==true){
		$.ajax({
			url: '/admin/my-jobs/delete',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data: {job_id: job_id},
			success: function(data){

				var status = $.parseJSON(data);
				if(status.status=='good'){
					location.reload();
				}
			},
		})
	}
	return false;
},
	function(){
		console.log('Not deleted');
		return false;
	});
});
</script>
@endsection
