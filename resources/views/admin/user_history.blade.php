<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
</style>
</head>
<body>
	<div class="container">
	<h1>User History</h1>

	<table id="history-table">
		<colgroup>
            <col width="20%">
                <col width="20%">
                    <col width="20%">
                        <col width="20%">
                        <col width="20%">
                        <col width="20%">
        </colgroup>
        <thead >
		<tr>
			<th>No</th>
			<th>Title</th>
			<th>Description</th>
			<th>Location Ip</th>
			<th>Location</th>
			<th>Created At</th>
		</tr>
        </thead>
		@foreach ($user_history as $key => $user)
		<tr>
			<td>{{ ++$key }}</td>
			<td>{{ $user->action }}</td>
			<td>{{ $user->description }}</td>
			<td>{{ $user->login_ip }}</td>
			<td>{{ $user->location }}</td>
			<td>{{ $user->created_at }}</td>

		</tr>
		@endforeach
	</table>
</div>
</body>
</html>





