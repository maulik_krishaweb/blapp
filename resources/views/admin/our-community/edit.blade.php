@extends('layouts.frame')
@section('content')
<style>
.nav-pills>li>a {
	padding: 20px;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #84939e;
}
  #myInput {
    padding: 30px;
    margin: 10px 0px;
    border: 0;
    border-radius: 0;
    background: #eef2f6;
  }
  .image-preview-input {
    position: relative;
    overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
#close-preview{ color: black; }
</style>
<br>
<div class="container-fluid">
    <div class="container">
        <div class="col-md-12">
                <h1>Our Community</h1> 
                <ol class="breadcrumb"> 
                    <li><a href="{{ url('admin') }}">Admin</a></li>
                    <li><a href="{{ url('admin/our-community') }}">Our Community</a></li>
                    <li>Edit</li>    
                </ol>
            </div>
		<form class="form-horizontal" role="form" method="post" action="{{ url('admin/our-community',$id) }}" enctype="multipart/form-data">
			{{ csrf_field() }}
            {{ method_field('PUT') }}
			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name" class="col-md-2 control-label">Name</label>
				<div class="col-md-6">
        			<input id="name" type="text" class="form-control"  value="@if(old('name')){{ old('name') }}@else{{ $data->name }}@endif" autofocus name="name"> 
    			</div>
			</div>
	        <div class="form-group">
	    		<label for="role" class="col-md-2 control-label">Picture</label>
	    		<div class="input-group image-preview col-md-6">
		    		<input type="text" class="form-control image-preview-filename" disabled="disabled" style="background: white;  margin-left: 13px;"> 
		          	<span class="input-group-btn">
		            	<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
		                	<span class="glyphicon glyphicon-remove"></span> Clear
		            	</button>
		            	<div class="btn btn-default image-preview-input" style="margin-right: 13px;">
		                	<span class="glyphicon glyphicon-folder-open"></span>
		                	<span class="image-preview-input-title">Browse</span>
		                	<input type="file" accept="image/png, image/jpeg, image/gif" name="logo"  class="picture"  /> 
		            	</div>
		          	</span>
				</div>
			</div>

			<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
				<label for="description" class="col-md-2 control-label">Description</label>
				<div class="col-md-6">
        			<textarea  class="form-control" name="description">@if(old('description')){{ old('description') }}@else{{ $data->description }}@endif</textarea>
    			</div>
			</div>
			<div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
				<label for="contact" class="col-md-2 control-label">Contact Information</label>
				<div class="col-md-6">
        			<textarea  class="form-control" name="contact">@if(old('contact')){{ old('contact') }}@else{{ $data->contact }}@endif</textarea>
    			</div>
			</div>
		    <button type="submit" class="wpcf7-form-control wpcf7-submit" style="margin-left: 577px">Edit</button>
		    	
	   	</form>
	</div>
</div><br>

@endsection
@section('script')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});
$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    })
});
</script>
<script>
    $('textarea').ckeditor({enterMode   : Number(2),});
</script>
@endsection