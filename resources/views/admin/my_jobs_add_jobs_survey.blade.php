@extends('layouts.frame')
@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha256-yMjaV542P+q1RnH6XByCPDfUFhmOafWbeLPmqKh11zo=" crossorigin="anonymous" />
@endsection
@section('content')
<div class="container-fluid">
    	<div class="container" style="min-height: 400px; padding-bottom: 50px;">
	       <h1>Add Jobs</h1>
	        <ol class="breadcrumb"> 
			<li><a href="{{ url('admin') }}">Home</a></li>
			<li><a href="{{ url('admin/my-jobs') }}">My Jobs</a></li>
			<li class="admin/jobs">{{ $addOrUpdate=='add' ? 'Add' : 'BLSN'.$jobid }}</li>	  
		</ol>
		<form action="{{ url('/admin/jobs/add/survey').($addOrUpdate=='add' ? '' : '/'.$jobid) }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="j_type" value="survey">
		       <div class="row">
		         	<div class="col-md-12">
			         	@if (session('message'))
						<div class="alert alert-info">
						       {{ session('message') }}
						</div>
					@endif
				</div>
				
		         	<style>.nav-tabs{ margin-top: 10px; margin-bottom: 20px; }</style>
		         	<div class="col-md-12">
		         		<button class="btn btn-success pull-right">{{ ucwords($addOrUpdate) }} </button>
					</div>
					<div class="col-md-12">		
						@include('components.jobs.surveying')
			        </div>
		        </div>
	       </form>
	</div>
</div>
@endsection
@section('script')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
	$(function () {
        $('.date').datetimepicker({
        	format: 'LD',
        	format: 'DD/MM/YYYY'
        	//dayViewHeaderFormat: 'm-d-y'
        });
    });
	$(document).ready(function(){
		if (!`{!! isset($jobs->surveying->surveying_service) !!}`) {
			$('#boundary_div').hide();
			$('#valuation_div').hide();
			$('#common_div').hide();
		}
	});
	$('#boundary').click(function(){
		//alert('hello');
		$('#post_cost').hide();
		$('#boundary_div').show();
		$('#boundary_post_cost').show();
		$('#common_div').show();
		$('#valuation_div').hide();
	});
	$('#surveying_service').click(function(){
		$('#boundary_div').hide();
		$('#boundary_post_cost').hide();
		$('#post_cost').show();
		$('#printing_postage_costs').prop('readonly', true);
	})

	$('#party-wall-tab').on('show.bs.tab', function(){
        	$('#party-wall-menu').show();
    	});
    	$('#survey-tab').on('show.bs.tab', function(){
        	$('#party-wall-menu').hide();
    	});

	$(document).on("focus keyup", "input.autocomplete", function() {
    // Cache useful selectors
    var $input = $(this);
    var $dropdown = $input.next("ul.dropdown-menu");
    
    // Create the no matches entry if it does not exists yet
    if (!$dropdown.data("containsNoMatchesEntry")) {
        $("input.autocomplete + ul.dropdown-menu").append('<li class="no-matches hidden"><a>No matches</a></li>');
        $dropdown.data("containsNoMatchesEntry", true);
    }
    
    // Show only matching values
    $dropdown.find("li:not(.no-matches)").each(function(key, li) {
        var $li = $(li);
        $li[new RegExp($input.val(), "i").exec($li.text()) ? "removeClass" : "addClass"]("hidden");
    });
    
    // Show a specific entry if we have no matches
    $dropdown.find("li.no-matches")[$dropdown.find("li:not(.no-matches):not(.hidden)").length > 0 ? "addClass" : "removeClass"]("hidden");
});
$(document).on("click", "input.autocomplete + ul.dropdown-menu li", function(e) {
    // Prevent any action on the window location
    e.preventDefault();
    
    // Cache useful selectors
    $li = $(this);
    $input = $li.parent("ul").prev("input");
    
    // Update input text with selected entry
    if (!$li.is(".no-matches")) {
        $input.val($li.text());
    }
});

$('#final_amount').keyup(function(e){
	//alert($('#final_amount').val());
	
	//if(e.keyCode == 8)alert('backspace trapped')

	var final_amount = $('#final_amount').val();
	if (final_amount == 0) {
		final_amount = 0;
	}
	//alert(parseFloat(final_amount));
	var vat_amount = (parseFloat(final_amount) * 20) / 100;
	$('#vat_amount').val(vat_amount.toFixed(2));  
});

$('.post_cost_option').click(function(){
	//alert('hello');
	if($(this).html() == "Valuation Report")
	{
		$('#post_cost').hide();
		$('#valuation_div').show();
		// $('#common_div').show();
	}
	$('#printing_postage_costs').prop('readonly', false);
	$('#boundary_post_cost').hide();
	$('#post_cost').show();
	$('#boundary_div').hide();
})

	$('.post_cost').click(function(){
		var value = $(this).html();
		$('#post_printing_postage_costs').val(value);
		$('#printing_postage_costs').click();
	});

	$('.land_cost').click(function(){
		var value = $(this).html();
		$('#land_registry_costs').val(value);
		$('#land_registry_costs').click();
	});

$('#land_registry_costs , #final_amount , #printing_postage_costs ').on('keyup click change keypress', function(e){
	var land_registry_costs = ($('#land_registry_costs').val()).slice(1);
	var final_amount = $('#final_amount').val();
	var vat_amount = $('#vat_amount').val();
	if ($('#post_printing_postage_costs').val() != 0 && $('#post_printing_postage_costs').val().charAt(0) == '£') {
		var printing_postage_costs = $('#post_printing_postage_costs').val().slice(1);
	}
	else
	{
		if ($('#printing_postage_costs').val().charAt(0) == '£') {
			var printing_postage_costs = $('#printing_postage_costs').val().slice(1);
		}
		else{
			var printing_postage_costs = $('#printing_postage_costs').val();	
		}
	}
	if (land_registry_costs == 0) {
		land_registry_costs = 0;
	}
	if (final_amount == 0) {
		final_amount = 0;
	}
	if (vat_amount == 0) {
		vat_amount = 0;
	}
	if (printing_postage_costs == 0) {
		printing_postage_costs = 0;
	}
	var cost_of_service = parseFloat(land_registry_costs) + parseFloat(final_amount) + parseFloat(printing_postage_costs) + parseFloat(vat_amount);
	//alert(cost_of_service);
	$('#cost_of_service').val(cost_of_service);

});




$(document).on("click", "input.autocomplete + ul.dropdown-menu li", function(e) {
    // Prevent any action on the window location
    e.preventDefault();
    
    // Cache useful selectors
    $li = $(this);
    var data_sr = $li.attr('data-sr');
    $input = $li.parent("ul").prev("input");
    
   if ($li.attr('data-sr')=='b-sr') {
   	$('#surveryor_name').val($li.attr('data-b-name'));    	
   	$('#s_surveyor_qualifications').val($li.attr('data-b-qualifications'));
   	$('#surveyor_email').val($li.attr('data-b-email'));
   	$('#surveyor_contact_details').val($li.attr('data-b-contact'));
   	// $('#surveyor_company_name').val($li.attr('data-b-company'));
   	// $('#surveyor_company_address').val($li.attr('data-b-address'));
   	// $('#surveyor_contact_details').val($li.attr('data-b-contact'));
   }
    
   if ( Math.floor(data_sr) == data_sr && $.isNumeric(data_sr) ) {
      data_a = data_sr; //(data_sr != 1)? data_sr: '';
   	$(`#ao_${data_sr}_surveryor_name`).val($li.data(`a${data_a}-name`));    	 	
   	$('#ao_'+data_sr+'_surveyor_qualifications').val($li.attr('data-a'+data_a+'-qualifications'));
   	$('#ao_'+data_sr+'_surveyor_email').val($li.attr('data-a'+data_a+'-surveyor_email'));
   	// $('#ao_'+data_sr+'_surveyor_company_name').val($li.attr('data-a'+data_a+'-company'));
   	// $('#ao_'+data_sr+'_surveyor_company_address').val($li.attr('data-a'+data_a+'-address'));
   	// $('#ao_'+data_sr+'_surveyor_contact_details').val($li.attr('data-a'+data_a+'-contact'));
   }
       
    // Update input text with selected entry
    if (!$li.is(".no-matches")) {
        $input.val($li.text());
    }
});
</script>
@endsection
