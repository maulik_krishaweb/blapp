@extends('layouts.frame')
@section('content')
<style>
.nav-pills>li>a {
	padding: 20px;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #84939e;
}
  #myInput {
    padding: 30px;
    margin: 10px 0px;
    border: 0;
    border-radius: 0;
    background: #eef2f6;
  }
  .image-preview-input {
    position: relative;
    overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
#close-preview{ color: black; }
.image-preview1-input {
    position: relative;
    overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview1-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview1-input-title {
    margin-left:2px;
}
#close-preview1{ color: black; }
#history-display ul{
  list-style-type: none;
  padding-left: 40px;
}
#history-display ul li{
  position: relative;
}
#history-display ul li:before{
  content: '';
  position: absolute;
  top: 18px;
  left: -20px;
  width: 1px;
  height: 100%;
  background-color: #00b29d;
}
#history-display ul li:last-child:before{
  display: none;
}
#history-display ul li:after{
  content: '';
  position: absolute;
  top: 6px;
  left: -27px;
  width: 15px;
  height: 15px;
  border-radius: 100%;
  background-color: #00b29d;
}
</style>
<div class="container-fluid">
    	<div class="container">
	       <div class="row">
	       	<div class="col-md-12">
		  	   	<h1>Users</h1> 
		  	   	<ol class="breadcrumb"> 
		                    <li><a href="{{ url('admin') }}">Admin</a></li>
		                    <li>Users</li>    
		              </ol>
		       </div>
	  		<div class="col-md-3 col-sm-3 col-xs-12">
				<input class="form-control" id="myInput" type="text" placeholder="Search by name ..."> 
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" @if (Request::is('admin/users')) class="active" @endif><a href="{{ url('admin/users') }}">All</a></li>
					<li role="presentation" @if (Request::is('admin/users/admins')) class="active" @endif><a href="{{ url('admin/users/admins') }}">Admins</a></li>
					<li role="presentation" @if (Request::is('admin/users/employees')) class="active" @endif><a href="{{ url('admin/users/employees') }}">Employees</a></li>
					<li role="presentation" @if (Request::is('admin/users/third-party')) class="active" @endif><a href="{{ url('admin/users/third-party') }}">Third Party</a></li>
					<li role="presentation" @if (Request::is('admin/users/clients')) class="active" @endif><a href="{{ url('admin/users/clients') }}">Clients</a></li>
					<li role="presentation" @if (Request::is('admin/users/meditation-clients')) class="active" @endif><a href="{{ url('admin/users/meditation-clients') }}">Meditation Client</a></li>
					<li role="presentation" @if (Request::is('admin/users/admin-2-user')) class="active" @endif><a href="{{ url('admin/users/admin-2-user') }}">Admin 2 User</a></li>
				</ul>
				<button type="button" class="btn blue-btn btn-block" id="addUserModalBtn" style="margin: 20px 0px">Add User</button> 
			</div>
	  		<div class="col-md-9 col-sm-9 col-xs-12">
	  			<div class="table-responsive table-bordered">
		  			<table class="table ">
					    	<thead>
					      		<tr>
					      			<th>ID</th>
					      			<th>Type</th>
					        		<th>Name</th>
					        		<th>Email</th>
					        		<th>Options</th>
					      		</tr>
					    	</thead>
					    	<tbody id="users_list">
					    		@foreach($users as $user)
					      			<tr data-id="{{ $user->id }}" class="users_item" data-name="{{ $user->name }}">
					      				<td>{{ $user->id }}</td>
					      				<td>
							      			@if($user->role==9)
							      			Admin
							      			@elseif($user->role==11)
							      			Admin 2 User
							      			@elseif($user->role==7)
							      			Employee
							      			@elseif($user->role==5)
							      			Third Party
							      			@elseif($user->role==3)
							      			Client
							      			@elseif($user->role==1)
							      			Meditation Client
							      			@endif
							      		</td>
							        	<td>{{ $user->name }}</td>
							        	<td>{{ $user->email}}</td>
					        			<td>
                                <button type="button" class="btn btn-xs btn-info user-edit-btn">Edit</button>
                                <button type="button" class="btn btn-xs btn-danger delete-user-btn">Delete</button>
                                	@if(Auth::user()->role == 9)
							        	<button type="button" class="btn btn-xs btn-info user-history-btn">History</button>
							        @endif
							        		@if ($user->jobs->count()<1 && $user->role!=9)
							        			<small class="text-danger">No Jobs Assigned</small>
							        		@else
								        		<span class="dropdown">
											  	<button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
											  		Jobs
											  		<span class="caret"></span>
											  	</button>
											  	<ul class="dropdown-menu">
												  	@if ($user->jobs->count()>=1)
												  		@foreach ($user->jobs as $job)
														    	<li><a href="{{ url("/admin/my-jobs/view/{$job->id}") }}">BLSN{{ $job->id }}</a></li>
														@endforeach
												  	@endif
											  	</ul>
											</span>
										  @endif
                                
							        	</td>
					      			</tr>
					      		@endforeach
					    	</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
	<div id="userHistoryModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
		    	<div class="modal-content">
		      		<div class="modal-header">
		       	 	<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">User History</h4>
		      		</div>
		      		<div class="modal-body">
		        		<div id="history-display"></div>
		        		<input type="hidden" name="final_user_id" id="final_user_id">
		      		</div>
		      		<div class="modal-footer">
		        		<a href="" class="btn btn-default history-download" download>Download History</a>
		        		<button type="button" class="btn btn-default " data-id="" data-dismiss="modal">Close</button>
		      		</div>
		    	</div>
	  	</div>
	</div>
	<div id="downloadHistoryModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
		    	<div class="modal-content">
		      		<div class="modal-header">
		       	 	<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">Download History</h4>
		      		</div>
		      		<div class="modal-body col-md-12">
		      			<div class="form-group col-md-6">
			      			<label>Start Date</label>
			        		<input type="text" name="start_date" class="form-control date" id="start_date">
		      			</div>
		      			<div class="form-group col-md-6">
		      				<label>End Date</label>
		        			<input type="text" name="end_date" class="form-control date" id="end_date">
		      			</div>
		      			<input type="hidden" name="" id="history_user_id">
		      		</div>
		      		<div class="modal-footer">
		        		<a href="" class="btn btn-default final-history-download" download>Download History</a>
		        		<button type="button" class="btn btn-default " data-id="" data-dismiss="modal">Close</button>
		      		</div>
		    	</div>
	  	</div>
	</div>
	<div id="addUserModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			      	<div class="modal-header">
			       	<button type="button" class="close" data-dismiss="modal">&times;</button>
			        	<h4 class="modal-title">Add User</h4>
			      	</div>
			      	<div class="modal-body">
			      		<form class="form-horizontal" role="form" >
			      			<div id="error-display"></div>
			      			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			      				<label for="name" class="col-md-2 control-label">Name</label>
			      				<div class="col-md-6">
			      					<input id="name" type="text" class="form-control"  value="{{ old('name') }}" required autofocus>
			      					<input  type="hidden" value="" id="update-user-id">
			      				</div>
			      			</div>
			      			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			      				<label for="email" class="col-md-2 control-label">E-Mail Address</label>
			      				<div class="col-md-6">
			      					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
			      				</div>
			      			</div>
			      			<div class="form-group">
			      				<label for="role" class="col-md-2 control-label">Role</label>
			      				<div class="col-md-6">
			      					<select class="form-control" name="role" id="role">
			      						<option value="9">Admin</option>
			      						<option value="7">Employee</option>
			      						<option value="5">Third Party</option>
			      						<option value="3">Client</option>
			      						<option value="1">Meditation Client</option>
			      						<option value="11">Admin 2 user</option>
			      					</select>
			      				</div>
			      			</div>
			      			<div class="form-group credentials" style="display: none">
			      				<label for="credentials" class="col-md-2 control-label">Credentials</label>
			      				<div class="col-md-6">
			      					<input id="credentials" type="text" class="form-control" name="credentials" value="{{ old('credentials') }}" required>
			      				</div>
			      			</div>
			      			<div class="form-group chatcolor" style="display: none">
			      				<label for="chatcolor" class="col-md-2 control-label">Chat Color</label>
			      				<div id="cp2" class="input-group colorpicker colorpicker-component col-md-6">
			      					<input type="color" id="chatcolor" value="{{ old('chatcolor') }}" class="form-control" /> 
			      					{{-- <span class="input-group-addon"><i></i></span>  --}}
			      				</div>
			      			</div>
			      			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			      				<label for="password" class="col-md-2 control-label">Password</label>
			      				<div class="col-md-6">
			      					<input id="password" type="password" class="form-control" name="password" required>
			      				</div>
			      			</div>
			      			<div class="form-group">
			      				<label for="password_confirm" class="col-md-2 control-label">Confirm Password</label>
			      				<div class="col-md-6">
			      					<input id="password_confirm" type="password" class="form-control" name="password_confirmation" required>
			      				</div>
			      			</div>
			      			<div class="form-group">
			      				<label for="role" class="col-md-2 control-label">Picture</label>
			      				<div class="input-group image-preview col-md-6">
			      					<input type="text" class="form-control image-preview-filename" disabled="disabled" style="background: white;  margin-left: 13px;"> 
			      					<span class="input-group-btn">
			      						<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
			      							<span class="glyphicon glyphicon-remove"></span> Clear
			      						</button>
			      						<div class="btn btn-default image-preview-input" style="margin-right: 13px;">
			      							<span class="glyphicon glyphicon-folder-open"></span>
			      							<span class="image-preview-input-title">Browse</span>
			      							<input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"  class="picture" /> 
			      						</div>
			      					</span>
			      				</div>
			      			</div>
			      			<div class="form-group">
			      				<div class="col-md-2">&nbsp;</div>
			      				<div class="col-md-6" id="update-user-pic" style="padding: 10px;">
			      				</div>
			      			</div>
			      			<div class="form-group">
			      				<div class="col-md-2">&nbsp;</div>
			      				<div class="col-md-6"><h4>Surveyor</h4></div>
			      			</div>
			      			{{-- signature upload --}}
			      			<div class="form-group signature-upload" style="display: none">
			      				<label for="role" class="col-md-2 control-label">Signature</label>
			      				<div class="input-group image-preview1 col-md-6">
			      					<input type="text" class="form-control image-preview1-filename" disabled="disabled" style="background: white;  margin-left: 13px;"> 
			      					<span class="input-group-btn">
			      						<button type="button" class="btn btn-default image-preview1-clear" style="display:none;">
			      							<span class="glyphicon glyphicon-remove"></span> Clear
			      						</button>
			      						<div class="btn btn-default image-preview1-input" style="margin-right: 13px;">
			      							<span class="glyphicon glyphicon-folder-open"></span>
			      							<span class="image-preview1-input-title">Browse</span>
			      							<input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview1"  class="signature" /> 
			      						</div>
			      					</span>
			      				</div>
			      				<div class="col-md-2">&nbsp;</div>
			      				<div class="col-md-6" id="update-signature-pic" style="padding: 10px;"></div>
			      			</div>
			      			<div class="form-group signature-upload" style="display: none">
			      				<div class="col-md-2">&nbsp;</div>
			      				<div class="col-md-6"><h4>Surveyor Signature</h4></div>
			      			</div>


			      			<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
			      				<label for="company" class="col-md-2 control-label">Company</label>
			      				<div class="col-md-6">
			      					<input id="company" type="text" class="form-control"  value="{{ old('company') }}" required autofocus>
			      				</div>
			      			</div>
			      			<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
			      				<label for="telephone" class="col-md-2 control-label">Telephone</label>
			      				<div class="col-md-6">
			      					<input id="telephone" type="text" class="form-control"  value="{{ old('telephone') }}" required autofocus>
			      				</div>
			      			</div>
			      			<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
			      				<label for="address" class="col-md-2 control-label">Address</label>
			      				<div class="col-md-6">
			      					<input id="address" type="text" class="form-control"  value="{{ old('address') }}" required autofocus>
			      				</div>
			      			</div>
			      			<div class="form-group{{ $errors->has('qualifications') ? ' has-error' : '' }}">
			      				<label for="qualifications" class="col-md-2 control-label">Qualifications</label>
			      				<div class="col-md-6">
			      					<input id="qualifications" type="text" class="form-control"  value="{{ old('qualifications') }}" required autofocus>
			      				</div>
			      			</div>

			      			<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
			      				<label for="message" class="col-md-2 control-label">message</label>
			      				<div class="col-md-6">
			      					<input id="message" type="text" class="form-control"  value="{{ old('message') }}" required autofocus>
			      				</div>
			      			</div>

			      			<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
			      				<label for="notes" class="col-md-2 control-label">Bio</label>
			      				<div class="col-md-6">
			      					<textarea id="notes" class="form-control" required="">{{ old('notes') }}</textarea>
			      				</div>
			      			</div>

			      			<div class="modal-footer">
			      				<button type="button" class="btn blue-btn" id="add-user-btn">Add </button>
			      				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      			</div>
			      		</form>
			   	</div>
			</div>
		</div>
	</div>		
	
</div>
<div style="display: none;" id="table-load"></div>
@endsection


@section('script')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/jspdf-autotable@3.2.4/dist/jspdf.plugin.autotable.js"></script>
<script type="text/javascript">

	$(function () {
        $('.date').datetimepicker({
        	format: 'LD',
        	//format: 'DD/MM/YYYY',
        	format: 'YYYY/MM/DD'
        	//dayViewHeaderFormat: 'm-d-y'
        });
    });
	$('.history-download').click(function(e){
		e.preventDefault();
		var link = $(this).attr('href');
		$('#history_user_id').val()
		$('#downloadHistoryModal').modal();

	});
	$('.final-history-download').click(function(e){
		e.preventDefault();
		var start_date = $('#start_date').val();
		var end_date = $('#end_date').val();
		var user = $('#final_user_id').val();
		if (start_date == '') {
			alert('Pls select start date');
			return false;
		}
		if (end_date == '') {
			alert('Pls select end date');
			return false;
		}
		if (end_date < start_date) {
			alert('End date must be greater than start date');
			return false;
		}

		$.ajax({
			url: '/admin/my-jobs/doc/download-user-history',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			data: {start_date: start_date, end_date: end_date, user_id: user},
			success: function(data){
				const doc = new jsPDF();
				$('#table-load').html(data);
				doc.autoTable({html: '#history-table'});
				doc.save('history.pdf');
				$('#downloadHistoryModal').modal('hide');
				$('#userHistoryModal').modal('hide');

			}
		});


	});
	$(document).ready(function(){
		// $('.colorpicker').colorpicker({
		// 	color: '#0000ff'
		// });
	  	$(document).on('change','#role',function(){
	  		if ($(this).val() == 7 || $(this).val() == 5 || $(this).val() == 9) {
	  			$('.signature-upload').show();
	  		}
	  		else{
	  			$('.signature-upload').hide();
	  		}
	  		$(this).val() == 5 ? $('.credentials').show() : $('.credentials').hide();
	  		$(this).val() == 5 ? $('.chatcolor').show() : $('.chatcolor').hide();
	  		

	  	});
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			
			$("#users_list .users_item").filter(function() {
				$(this).toggle($(this).attr('data-name').toLowerCase().indexOf(value) > -1)
		    });
		});
	});

$(document).on('click', '#add-user-btn', function(event) {
		event.preventDefault();
		// var name= $('#name').val();
		// var email=$('#email').val();
		// var role=$('#role').val();
		// var password=$('#password').val();
		// var picture= new FormData('.picture');
		// var password_confirmation=$('#password_confirm').val();
		// var user_id = $('#update-user-id').val();
		

		var formData = new FormData();
formData.append('name', $('#name').val());
formData.append('email', $('#email').val());
formData.append('role', $('#role').val());
formData.append('password', $('#password').val());
formData.append('password_confirmation', $('#password_confirmation').val());
formData.append('user_id', $('#update-user-id').val());
if ($('.picture').prop('files')[0] == undefined) {
	
	formData.append('picture','');
}
else
{
	formData.append('picture', $('.picture').prop('files')[0]);
}
formData.append('signature', $('.signature').prop('files')[0]);

formData.append('company', $('#company').val());
formData.append('qualifications', $('#qualifications').val());
formData.append('telephone', $('#telephone').val());
formData.append('address', $('#address').val());
formData.append('notes', $('#notes').val());
formData.append('message', $('#message').val());
formData.append('credentials', $('#credentials').val());
formData.append('chatcolor', $('#chatcolor').val());
   // var cv = $('#cv').prop('files')[0];
   // var form_data =$("form").serialize();
   

	$.ajax({
		url: '/admin/add-user',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		contentType: false, 
        	processData: false,
       	data: formData ,
		success: function(data){
			
			var status = $.parseJSON(data);
			if(status.status=='good'){
				swal("User added successfully ").then(() => {
				 location.reload();
				});				
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li id='+ index +'>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
		    $('#picture').html('The picture must be greater then 500KB.');
	    }
		
	})

});


$('.delete-user-btn').on('click',  function(event) {
	event.preventDefault();
	var user_id = $(this).parent().parent().attr('data-id');
	swal("Are you sure you wish to delete this user?", {
  buttons: ["Cancel", "Yes Delete User!"],
}).then(function(value){
	if(value==true){
		$.ajax({
			url: '/admin/delete-user',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data: {user_id: user_id},
			success: function(data){
				var status = $.parseJSON(data);
				if(status.status=='good'){
					location.reload();			
				}
			},
		})		
	}
	return false;
}, 
	function(){
		
		return false;
	});
});

$('.user-history-btn').on('click',  function(event) {
	event.preventDefault();
	var user_id = $(this).parent().parent().attr('data-id');
	$('#final_user_id').val(user_id);
	var url = "/admin/my-jobs/doc/download-user-history/"+user_id;
	$('.history-download').attr("href",url);
	$.ajax({
		url: '/admin/user-history',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {user_id: user_id},
		success: function(data){
			
			$('#userHistoryModal').modal();
			var history = $.parseJSON(data);
			var displayhistory='<ul>';
		   	$.each(history, function(index, value) {
		    	displayhistory+='<li><strong> '+value.created_at+' :  '+value.action+' <br></strong> '+value.description+'</li>';
		    });
		    displayhistory+='</ul>';
		    $('#history-display').html(displayhistory);
	    

		},
	})		
});


$('#addUserModalBtn').on('click',  function(event) {
	event.preventDefault();
	$('#addUserModal').modal();
	$('#name').val(' ');
	$('#email').val(' ');
	$('#company').val(' ');
	$('#role').val('');
	$('.credentials').hide();
	$('#qualification').val(' ');
	$('#image-preview1-filename').val(' ');
	$('#qualifications').val(' ');
	$('#telephone').val(' ');
	$('#address').val(' ');
	$('#message').val(' ');
	$('#notes').val(' ');
	$('#image-preview-filename').val(' ');
	$('#update-user-pic').find('img').attr('src','');
	$('#add-user-btn').text('Add');
	$('#update-user-id').val(' ')	
});

$('.user-edit-btn').on('click',  function(event) {
	event.preventDefault();
	var user_id = $(this).parent().parent().attr('data-id');

	$.ajax({
		url: '/admin/user-edit',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {user_id: user_id},
		success: function(data){
			
			$('#addUserModal').modal();
			var user = $.parseJSON(data);

			$('#name').val(user.name);
			$('#email').val(user.email);
			$('#company').val(user.company);
			$('#address').val(user.address);
			$('#telephone').val(user.telephone);
			$('#qualifications').val(user.qualifications);
			$('#notes').val(user.notes);
			$('#message').val(user.message);
			$('#credentials').val(user.credentials);
			$('#chatcolor').val(user.chatcolor);
			if(user.role == 7 || user.role == 5 || user.role == 9){
				$('.signature-upload').show();
			}
			else{
				$('.signature-upload').hide();	
			}
			user.role == 5 ? $('.credentials').show() : $('.credentials').hide();
			user.role == 5 ? $('.chatcolor').show() : $('.chatcolor').hide();
			$('#role').children().each(function(index, el) {
				if($(this).val()==user.role){
					$(this).attr('selected', ' ');
				}
			});
			$('#add-user-btn').text('Update');
			$('#update-user-id').val(user.id);
			if (user.picture) {
				$('#update-user-pic').html('<img src="/'+user.picture+'" style="width:200px">');
			}
			
			if (user.signature) {
				$('#update-signature-pic').html('<img src="/'+user.signature+'" style="width:200px">');
			}
		},
	})		
});

// image upload
$(document).on('click', '#close-preview', function(){ 
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
           $('.image-preview').popover('show');
        }, 
         function () {
           $('.image-preview').popover('hide');
        }
    );    
});

$(document).on('click', '#close-preview1', function(){ 
    $('.image-preview1').popover('hide');
    // Hover befor close the preview1
    $('.image-preview1').hover(
        function () {
           $('.image-preview1').popover('show');
        }, 
         function () {
           $('.image-preview1').popover('hide');
        }
    );    
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    })
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview1',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview1').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview1-clear').click(function(){
        $('.image-preview1').attr("data-content","").popover('hide');
        $('.image-preview1-filename').val("");
        $('.image-preview1-clear').hide();
        $('.image-preview1-input input:file').val("");
        $(".image-preview1-input-title").text("Browse"); 
    }); 
    // Create the preview image
    $(".image-preview1-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });      
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview1-input-title").text("Change");
            $(".image-preview1-clear").show();
            $(".image-preview1-filename").val(file.name);            
            img.attr('src', e.target.result);
            $(".image-preview1").attr("data-content",$(img)[0].outerHTML).popover("show");
        }        
        reader.readAsDataURL(file);
    })
});

</script>
@endsection
