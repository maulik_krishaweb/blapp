
<div class="container-fluid" style="background-color: #414861">
  <div class="container">
    <div class="row" style="padding-top: 50px;">
      <div class="col-md-6 col-sm-6 col-xs-12">
         <h4  style="color: #fff;"><strong>Upload a File</strong></h4>
        <p>Here you can upload documents, just drag and drop anywhere on the screen to start the process. Please be aware that you can only upload the following file types:  jpg, mov, mp4, pdf, doc, docx, zip</p>
      </div>

      <div id="actions"  class="col-md-6 col-sm-6 col-xs-12">
        <div class="main-buttons  text-right uploadwrap">
          <div class="col-md-12 col-sm-12 upldirsel">
          <select name="massSelect" id="massSelect" onchange="updateMassSelect(this)">
            @php if( count(explode("/", $sel_folder))==1 ){ @endphp
               <option value="{{$sel_folder}}">Root Folder</option>
            @php } @endphp
            @foreach($folders as $fname=>$folder_v)
               <option value="{{$folder_v['folder_full']}}">{{$folder_v['folder']}}</option>
            @endforeach
          </select>
          </div>
          {{-- mass select --}}
         <div class="btn-group">
          
          <button class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>Add files...</span></button>
          <button type="submit" class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Upload</span></button>
          <button type="reset" class="btn btn-danger cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel </span></button>
          
        </div>
        <div class="foldern">
           <div class="addfoldname">
           <!--<span style="color:#fe7a70"><input type="checkbox" title="This folder can be viewed by admin only." name="adminonly" id="adminonly" value="yes"> Admin Only</span>-->
           <input name="foldname" id="foldname" value="" placeholder="Folder Name"/>
           <button class="btn btn-success addfolder-button"><i class="glyphicon glyphicon-plus"></i><span>Add Folder</span></button>
           <!--<button class="btn btn-success createfolder-btn"><i class="glyphicon glyphicon-plus"></i><span>Create</span></button>-->
           </div>
        </div>
        <div class="progress-bars">
          <span class="fileupload-process">
            <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860;">
              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
          </span>
        </div>
      </div>
  </div>
  </div>
  <!-- end row -->
  <div class="table table-striped" class="files" id="previews">
    <div id="template" class="file-row row" style="margin-top: 20px;">
      <div class="col-md-2 col-sm-2">
        <span class="preview"><img data-dz-thumbnail /></span>
      </div>
      <div class="col-md-2 col-sm-3">
        <p><small class="name" data-dz-name><span class="size" data-dz-size>-</span></small></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
      </div>
      <div class="col-md-4 col-sm-2 folderpath" data-parent="">

      </div>
      <div class="col-md-2 col-sm-1">
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860; box-shadow: none;">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
      </div>
      <div class="col-md-2 col-sm-4">
        <div class="text-right">
          <button class="btn btn-primary btn-xs start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
          <button data-dz-remove class="btn btn-danger btn-xs cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
        </div>
      </div>
    </div>
    </div>

</div>
{{-- upload modal --}}
<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Document</h4>
      </div>
      <div class="modal-body">
       	<p>Select document to upload</p>
       	    <div class="input-group">
             <label class="input-group-btn">
                 <span class="btn btn-primary">
                    <i class="fa fa-upload" style="font-size:24px"></i> Browse&hellip; <input type="file" style="display: none;" multiple>
                 </span>
             </label>
             <input type="text" class="form-control" readonly>
         </div>
         <span class="help-block">
             You can select multiple files by clicking ctrl and selecting
         </span>
     </div>

      </div>
    </div>
  </div>
</div>
{{-- upload --}}



<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" integrity="sha256-Rnxk6lia2GZLfke2EKwKWPjy0q2wCW66SN1gKCXquK4=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/dropdowns.js') }}"></script>

<script>
   //Search by Job Number

$(document).ready(function() {
   
   $(document).on('click', '.addfolder-button', function(event) {
   
      var _selfold = $('#massSelect').find('option:selected').val();
      var _newfold = $('#foldname').val();
      
      if( _selfold ){
         //alert( _selfold );
         $('input[name=nav_action]').val('add_folder');
         $('<input>').attr({ type: 'hidden', name: 'sel_uplfolder', value: _selfold}).appendTo('.navform');
         $('<input>').attr({ type: 'hidden', name: 'new_folder', value: _newfold}).appendTo('.navform');
         $('.navform').submit();
      }
   });
     
   var previewNode = document.querySelector("#template");
   previewNode.id = "";
   var previewTemplate = previewNode.parentNode.innerHTML;
   previewNode.parentNode.removeChild(previewNode);

   var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
     url: "/admin/storage/upload", // Set the url
     maxFilesize:10,
     maxThumbnailFilesize: 10,
     thumbnailWidth: 80,
     thumbnailHeight: 80,
     parallelUploads: 20,

     headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     previewTemplate: previewTemplate,
     autoQueue: false, // Make sure the files aren't queued until manually added
     previewsContainer: "#previews", // Define the container to display the previews
     clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
   });

   myDropzone.on("addedfile", function(file) {
      var e = document.getElementById("massSelect");
      var selval = $('.main-buttons select').find('option:selected').val();
      //alert(selval);
      var selhtml = $('.main-buttons select').html();
      file.previewElement.querySelector('.folderpath').innerHTML = `<select class="listselct">${selhtml}</select>`;
      $(file.previewElement.querySelector('.listselct')).find(`option[value="${selval}"]`).attr('selected','selected').change();
      // Hookup the start button
      file.previewElement.querySelector(".start").onclick = function() {

          if( e.options[e.selectedIndex].text=="" ) {
            swal("Upload "+file.name, "You need to select the folder to upload into!", "info");
            console.log(e.options[e.selectedIndex].text)
            return;
          }/*else if(e.options[e.selectedIndex].text=="InHouse"){
             
               alert("'InHouse' root folder is restricted to upload files. Please create folders under 'InHouse' and upload files there");
               return;
          }*/
          //alert( file.previewElement.querySelector(".name").data('dz-name') );
          myDropzone.enqueueFile(file);
      };
   });
   
   
   myDropzone.on("sending", function(file, xhr, data) {
      var selval = $('.main-buttons select').find('option:selected').val();
      var e = $(file.previewElement.querySelector('.listselct')).find(`option:selected`); //document.getElementById("massSelect");
      
       if($(e).val()=="InHouse"){
            alert("This is a root folder and is restricted to upload files here. Please create folders under root(/) and upload files there");
            myDropzone.removeFile(file);
            return false;
       }
   	// Show the total progress bar when upload starts
   	document.querySelector("#total-progress").style.opacity = "1";
      var selected = $(file.previewElement.querySelector('.listselct')).find(":selected").val();
      //alert(selected);
   	if (selected.length > 0) {
   	    data.append("target_dir",  selected );
   	}
      // And disable the start button
      file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
   });
   
   myDropzone.on("queuecomplete", function(progress) {
      $('.navform').submit();
   });
   
   myDropzone.on("uploadprogress", function (file, progress, bytesSent) {
     file.previewElement.querySelector(".progress-bar").innerHTML = progress + "%";
   });
   
   document.querySelector("#actions .start").onclick = function() {
      var check = true;
      myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
   };
   
   document.querySelector("#actions .cancel").onclick = function() {
     myDropzone.removeAllFiles(true);
   };
      
     
});
</script>
