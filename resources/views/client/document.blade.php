@extends('layouts.frame')

@section('content')
  <style>
#job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 250px;
    text-align: left;
        border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;
    margin-top: 0px;
    border-radius: 0px;
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}
#myInput{
    border-right: none;
    border-left: none;
    border-top: none;
    border-radius: 0px;
    margin-top: -6px;
}
  a {
    color: #00b29c;
    text-decoration: none;
}

iframe{
 width: 100%; 
  height: 600px;
}
  </style>
<div class="container-fluid">
  <div class="container">
    <h1>
      Job Documents
      <span class="pull-right">
        @if (Auth::user()->jobs->count()>1)
          <div class="dropdown">
            <button class="btn btnblock dropdown-toggle" type="button" data-toggle="dropdown" id="job-search-btn">
              Select Job
              <span class="pull-right"><span class="caret "></span></span>
            </button>
            <ul class="dropdown-menu">
              <input class="form-control" id="myInput" type="text" placeholder="Search..">
              @foreach (Auth::user()->jobs as $e)
                <li><a href="{{ url("/client/documents/{$e->id}") }}">BLSN{{ $e->id}}</a></li>
              @endforeach 
            </ul>   
          </div>
        @endif
      </span>
    </h1>
      <ol class="breadcrumb"> 
        <li><a href="{{ url('client') }}">Home</a></li>
        <li><a href="{{ url('client/documents') }}">Documents</a></li>

         
        <li class="admin/jobs">BLSN{{ $job->id}}</li>

      </ol>
    </div>
</div>

<div class="container-fluid">
  <div class="container">
    <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
      @if($job->docs->count() >= 1)
        @foreach($job->docs->whereIn('type',$type ?? ['1','2','3'])->groupBy('type')  as $key => $folder)
       
            <div class="col-md-4 col-sm-4 col-xs-12">
      
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color:  #00B29D; color: #fff;">              
              @if ($key==1 )
                <i class="fa fa-file-text-o" style="font-size:24px"></i>Documents
              @elseif($key==3)
                <i class="fa fa-camera-retro" style="font-size:24px"></i>Photographs
              @elseif($key==2)
                <i class="fa fa-object-group" style="font-size:24px"></i>Drawings
              @endif
            </div>
            <div class="panel-body row">
              @foreach ($folder->slice(0, 6)  as $doc)
                 <div class="col-md-12" style="background-color: #fff;">  
                    @if($doc->type== 1) 
                      <div  class="row">
                          <p class="col-md-7 col-sm-7 col-xs-7"><i class="fa fa-file-word-o" aria-hidden="true"  style="font-size: 25px;"></i>  {{ $doc->original_name  }}</p>
                          <div class="col-md-5 col-sm-5 col-xs-5 pull-right">
                            <a href="{{ asset($doc->link) }}" class="btn btn-success btn-xs pull-right" download>Download</a>
                            <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{ asset($doc->link) }}" data-type="{{ $doc->extension}}">View</button>
                          </div>
                      </div>
                    @elseif( $doc->type== 3) 
                      <div  class="row">
                      <p class="col-md-7 col-sm-7 col-xs-7"><i class="fa fa-file-image-o" aria-hidden="true"  style="font-size: 25px;"></i> {{ $doc->original_name }}</p>
                      <div class="col-md-5 col-sm-5 col-xs-5 pull-right">
                        <a href="{{ asset($doc->link) }}" class="btn btn-success btn-xs pull-right" download>Download</a>
                        <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{ asset($doc->link) }}" data-type="{{ $doc->extension}}">View</button>
                      </div>
                    </div>
                    @elseif($doc->type== 2) 
                     <div  class="row">
                      <p class="col-md-7 col-sm-7 col-xs-7"><i class="fa fa-file-image-o" aria-hidden="true"  style="font-size: 25px;"></i> {{ $doc->original_name }}</p>
                      <div class="col-md-5 col-sm-5 col-xs-5 pull-right">
                        <a href="{{ asset($doc->link) }}" class="btn btn-success btn-xs pull-right" download>Download</a>
                        <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{ asset($doc->link) }}" data-type="{{ $doc->extension}}">View</button>
                      </div>
                    </div>
                    @endif
                  </div>
              @endforeach

              @if ($folder->count() > 6)
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <hr>
                  <p class="text-center"><a href="{{ url("/client/documents/{$job->id}/{$doc->type}") }}">View All</a> </p>
                </div>
              @endif  
            </div>
          </div>
          </div>
        @endforeach
      @else
        <h2 class="text-danger"> No documents in storage for this job</h2>
        <p>Use the form the drag and drop feature to start uploading documents. </p>
      @endif 
    </div>
  </div>
</div>



{{-- dropzone --}}
<div class="container-fluid" style="background-color: #414861">
  <div class="container">
    <div class="row" style="padding-top: 50px;">
      <div class="col-md-6 col-sm-6 col-xs-12">
         <h4  style="color: #fff;"><strong>Upload a File</strong></h4>
        <p>Here you can upload documents, just drag and drop anywhere on the screen to start the process. Please be aware that you can only upload the following file types:  jpg, mov, mp4, pdf, doc, docx, zip</p> 
      </div>
      <div id="actions"  class="col-md-6 col-sm-6 col-xs-12">
        <div class="main-buttons  text-right">
          <button class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span> Add files...</span></button>
          <button type="submit" class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Upload</span></button>
          <button type="reset" class="btn btn-danger cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel </span></button>
        </div>
        <div class="progress-bars">
          <span class="fileupload-process">
            <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860;">
              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
          </span>
        </div>
      </div>
  </div> 
  <div class="table table-striped" class="files" id="previews">
    <div id="template" class="file-row row" style="margin-top: 20px;">
      <div class="col-md-2 col-sm2">
        <span class="preview"><img data-dz-thumbnail /></span>
      </div>
      <div class="col-md-2 col-sm-2">
        <p><span class="name" data-dz-name></span> - <span class="size" data-dz-size></span></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
      </div>
      <div class="col-md-3 col-sm-2" style="color: #fff;"> 
        <label class="radio-inline">
          <input type="radio" value="1"  class="folder-type">Documents
        </label>  
        <label class="radio-inline">
          <input type="radio" value="2"  class="folder-type">Drawing
        </label>
        <label class="radio-inline">
          <input type="radio" value="3"  class="folder-type">Photographs
        </label>
      </div>
      <div class="col-md-3 col-sm-2">
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860; box-shadow: none;">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
      </div>
      <div class="col-md-2 col-sm-2">
        <div class="text-right">
          <button class="btn btn-primary btn-xs start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
          <button data-dz-remove class="btn btn-danger btn-xs cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

{{-- doc preview modal --}}
  <div class="modal fade" id="docViewModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Doc View</h4>
        </div>
        <div class="modal-body" id="doc-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>      
  </div>



@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" integrity="sha256-Rnxk6lia2GZLfke2EKwKWPjy0q2wCW66SN1gKCXquK4=" crossorigin="anonymous"></script>
<script>

// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
  url: "/admin/upload", // Set the url
  thumbnailWidth: 80,
  thumbnailHeight: 80,
  parallelUploads: 20,
  params:{ job_id: {{  $job->id }} },
  headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  previewTemplate: previewTemplate,
  autoQueue: false, // Make sure the files aren't queued until manually added
  previewsContainer: "#previews", // Define the container to display the previews
  clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
});

myDropzone.on("addedfile", function(file) {
  
  file.previewElement.querySelectorAll('.folder-type').forEach( function(radio) {
    radio.setAttribute("name", file.name);
   });

  // Hookup the start button
  file.previewElement.querySelector(".start").onclick = function() { 
    if($(`.folder-type[name="${file.name}"]:checked`).length==0) {

      swal("Upload "+file.name, "You need to select the file type!", "info");
      console.log($(`.folder-type[name="${file.name}"]:checked`).length)
    return;
  }  
   
   
    myDropzone.enqueueFile(file); 

  };
});



// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file, xhr, data) {
// Show the total progress bar when upload starts
  

document.querySelector("#total-progress").style.opacity = "1";

var selected = $(`.folder-type[name="${file.name}"]:checked`);



if (selected.length > 0) {
   
    data.append("filetype",  selected.val() );
}


    


  // And disable the start button
  file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
  document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {

      var check = true;
        $("input:radio").each(function(){
            var name = $(this).attr("name");
            if($(`input:radio[name="${name}"]:checked`).length == 0){
                check = false;
            }
        });
        
        if(!check){
          swal("Upload", "You need to select the file type for all files!", "info");
        
            return;
        }
        // else{
        //     alert('Please select one option in each question.');
        //      return;
        // }

  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};
document.querySelector("#actions .cancel").onclick = function() {
  myDropzone.removeAllFiles(true);
};


myDropzone.on("success", function(file, res) {
  $('.dz-success').remove();
  console.log(res);
});

myDropzone.on("queuecomplete", function(file) {
  location.reload();
});


$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});


$(document).on('click', '.view-doc', function(event) {
  event.preventDefault();
   $("#docViewModal").modal();
   if ($(this).attr('data-type')=='doc' || $(this).attr('data-type')=='docx' || $(this).attr('data-type')=='pdf' ) {
    $('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true"></iframe>');
   }
   else if($(this).attr('data-type')=='jpg' || $(this).attr('data-type')=='jpeg' || $(this).attr('data-type')=='png' ){
    $('#doc-body').html('<img src="'+$(this).attr('data-link')+'" style="width:100%">');
   } 
   else {
    $('#doc-body').html('<h2> Sorry on-line preview not available</h2>');
   }
   
});
       
$(document).on('click', '.view-drawings', function(event) {
  event.preventDefault();


   $("#drawings-modal").modal();
$('.item').removeClass('active');
  $('#d-first').prepend(`
   <div class="item active" style="min-height: 400px;">
             <img class="center-block" src="${$(this).attr('data-link')}" style="max-height: 500px;">
              <div class="carousel-caption">
       
               <a  class="btn btn-success btn-sm"  href="${$(this).attr('data-link')}" download>
                download
               </a>
              </div>
            </div>

    `) 
});

 $(document).on('click', '.view-photographs', function(event) {
  event.preventDefault();
   $("#photographs-modal").modal();
   $('.item').removeClass('active');
   $('#p-first').prepend(`
   <div class="item active" style="min-height: 400px;">
             <img class="center-block" src="${$(this).attr('data-link')}" style="max-height: 500px;">
              <div class="carousel-caption">
       
               <a  class="btn btn-success btn-sm"  href="${$(this).attr('data-link')}" download>
                download
               </a>
              </div>
            </div>

    `) 
   
});
</script>

@endsection