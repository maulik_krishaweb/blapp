@extends('layouts.frame')
@section('head')
<link rel="stylesheet" href="{!! asset('css/plyr.css') !!}" />
<script src="{!! asset('js/plyr.js') !!}"></script>
@endsection
@section('content')
<style>
    #job-details h4{
        color: #00b29b;
    }
    #job-details span{
        color: #414861;
    }

     #job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    width: 250px;
    border-radius: 0px;
    text-align: left;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;

    margin-top: 0px;
    border-radius: 0px;
}
#myInput{
    border-right: none;
    border-left: none;
    border-top: none;
    border-radius: 0px;
    margin-top: -6px;
}
.btn-video-icon{
  background: #f14848 !important;
  color: #ffffff !important;
  border: 0;
  margin-top: 10px;
}
.btn-mail{
  background: #2ab27b !important;
  color: #ffffff !important;
  border: 0;
  margin-top: 10px;
}
</style>
<div class="container-fluid">
    <div class="container">
        <h1>Tasks
            <span class="pull-right">
                @if (Auth::user()->jobs->count()>1)
                    <div class="dropdown">
                        <button class="btn btnblock dropdown-toggle" type="button" data-toggle="dropdown" id="job-search-btn">
                        Select Job
                            <span class="pull-right"><span class="caret "></span></span>
                        </button>
                        <ul class="dropdown-menu">
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            @foreach (Auth::user()->jobs as $e)
                               <li><a href="{{ url("/client/tasks/{$e->id}") }}">BLSN{{ $e->id}}</a></li>
                            @endforeach
                        </ul>   
                    </div>
                @endif
            </span>
        </h1>
        <!-- <h3>Mediation Property</h3> -->
        <h2>{{$tasks->where('status',1)->count() !==0 ? round(($tasks->where('status',1)->count()/$tasks->count() ) * 100): 0}}% </h2>


        <p style="color: #00b29b;font-family: 'Poppins'">Complete</p>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:  {{$tasks->where('status',1)->count() !==0 ?  round(($tasks->where('status',1)->count()/$tasks->count() ) * 100) : 0 }}%"> </div>
        </div>
        <div id="job-details">
     
            <h4><strong>Job Number</strong><span class="text-right pull-right" style="font-family: 'Raleway', sans-serif;"> BLSN {{ $job->id }}</span></h4>
            <h4><strong>Invoice Number</strong><span class="text-right pull-right" style="font-family: 'Raleway', sans-serif;">  {{ $job->invoice_no }}</span></h4>
            @if($job->bo != null)
            <h4><strong>Building Owner Names</strong></h4>
              @if($job->bo != null)<span style='font-family: "Raleway", sans-serif;'>{{ $job->bo->full_name }}</span>@endif
            @endif
            @if($job->ao != null || $job->ao2 != null || $job->ao3 != null || $job->ao4 != null || $job->ao5 != null || $job->ao6 != null || $job->ao7 != null || $job->ao8 != null || $job->ao9 != null || $job->ao10 != null)
            <h4><strong>Adjoining Owner Names</strong></h4>
              @if($job->ao != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao->full_names }}</span></h4>@endif
              @if($job->ao2 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao2->full_names }}</span></h4>@endif
              @if($job->ao3 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao3->full_names }}</span></h4>@endif
              @if($job->ao4 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao4->full_names }}</span></h4>@endif
              @if($job->ao5 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao5->full_names }}</span></h4>@endif
              @if($job->ao6 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao6->full_names }}</span></h4>@endif
              @if($job->ao7 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao7->full_names }}</span></h4>@endif
              @if($job->ao8 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao8->full_names }}</span></h4>@endif
              @if($job->ao9 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao9->full_names }}</span></h4>@endif
              @if($job->ao10 != null)<h4><span style="display: block; font-family: 'Raleway', sans-serif;">{{ $job->ao10->full_names }}</span></h4>@endif
            @endif
            
            @if ( isset($job->surveying->s_surveyor_full_information) )
               <h4><strong>Surveyor Details:</strong><span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->surveying->s_surveyor_full_information }}</span></h4>
            
            @elseif( isset($job->bo->surveyor_full_information) )
               @if($job->bo->surveyor_full_information != null)<h4><strong>Building Owner Surveyor Details:</strong><span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->bo->surveyor_full_information }}</span></h4>@endif
               @if($job->ao->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong><span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao->surveyor_full_information }}</span></h4>@endif
               @if($job->ao2->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao2->surveyor_full_information }}</span></h4>@endif
               @if($job->ao3->surveyor_full_informatio != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao3->surveyor_full_information }}</span></h4>@endif
               @if($job->ao4->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao4->surveyor_full_information }}</span></h4>@endif
               @if($job->ao5->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao5->surveyor_full_information }}</span></h4>@endif
               @if($job->ao6->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao6->surveyor_full_information }}</span></h4>@endif
               @if($job->ao7->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao7->surveyor_full_information }}</span></h4>@endif
               @if($job->ao8->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao8->surveyor_full_information }}</span></h4>@endif
               @if($job->ao9->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao9->surveyor_full_information }}</span></h4>@endif
               @if($job->ao10->surveyor_full_information != null)<h4><strong>Adjoining Owner Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao10->surveyor_full_information }}</span></h4>@endif
               @if($job->ao->third_surveyor != null)<h4><strong>Third Surveyor Details:</strong> <span style="display: block; font-family: 'Raleway', sans-serif;">  {{ $job->ao->third_surveyor }}</span></h4>
               @endif
               <h4><strong>Property Adress:   </strong><span class="text-right pull-right" style="font-family: 'Raleway', sans-serif;">  {{ $job->bo->property_address_proposed_work }}</span></h4>
            
            @endif
            
            
           
        </div>
        <hr>
        <a data-toggle="collapse" data-target="#tasks" href="#tasks" style="text-decoration: none; font-size: 1.5rem; color: #00b29b; margin-bottom: 20px;">
            View Tasks
            <span class="pull-right" ><i class="fa fa-chevron-down" style="font-size:24px; color: #00b29b"></i></span>
        </a>
        <br>
        <div id="tasks" class="collapse">
            <div class="row hidden-xs" style="background-color: #414861; color: #fff;">
                <div class="col-md-5"><h4>Task Name</h4></div>
                <div class="col-md-3"><h4>Date of Surveyor Action</h4></div>
                <div class="col-md-2">{{-- video --}}</div>
                <div class="col-md-2"><h4 class="pull-right">Task Completed</h4></div>
            </div>
            @php 
              $task_arr = ['65','66','67','68'];
            @endphp
            @foreach($tasks as $task)
              @if($task->task_id == 65 || $task->task_id == 66 || $task->task_id == 67 || $task->task_id == 68)
              <div class="row">
                    <div class="col-md-5"><h4><span>{{ $task->task }}</span></h4></div>
                    <div class="col-md-3"><h4> <span>{{ $task->updated_at }}</span></h4></div>
                    <div class="col-md-2">
                        @if($task->mail != null)
                            <button class="btn btn-default btn-mail mail" data-type="{!! $task->job_type !!}" data-id="{!! $task->id !!}"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                        @endif
                        @if($task->video != null)
                            <button class="btn btn-default btn-video-icon videorecord" data-url="{!! $task->video !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></button>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <span  class="pull-right"> 
                          @if($task->status == 1)
                            <i class="fa fa-check" aria-hidden="true" style="color: #00B29D; margin-top: 10px; font-size: 30px;"></i>
                          @endif
                        </span>
                    </div>
                </div>
                <hr> 
              @endif
            @endforeach
            @foreach($tasks as $task)
              @if($task->status == 1 && !in_array($task->task_id, $task_arr))
                <div class="row">
                    <div class="col-md-5"><h4><span>{{ $task->task }}</span></h4></div>
                    <div class="col-md-3"><h4> <span>{{ $task->updated_at }}</span></h4></div>
                    <div class="col-md-2">
                        @if($task->mail != null)
                            <button class="btn btn-default btn-mail mail" data-type="{!! $task->job_type !!}" data-id="{!! $task->id !!}"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                        @endif
                        @if($task->video != null)
                            <button class="btn btn-default btn-video-icon videorecord" data-url="{!! $task->video !!}"><i class="fa fa-video-camera" aria-hidden="true"></i></button>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <span  class="pull-right"> 
                            <i class="fa fa-check" aria-hidden="true" style="color: #00B29D; margin-top: 10px; font-size: 30px;"></i>
                        </span>
                    </div>
                </div>
                <hr> 
              @endif
            @endforeach  
        </div>
        <br>
    </div>
</div>
<div class="modal fade" id="showvideo" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Intro video</h4>
       </div>
       <div class="modal-body">
           {{-- <video width="100%" id="vid" controls>
               <source src="" type="video/webm" id="sr">
               Your browser does not support HTML5 video.
           </video> --}}
            <video id="player" src=""></video>
       </div>
       <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>
 <div id="addmail" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Receive Mail</h4>
       </div>
       <div class="modal-body">
         <div id="receive-mail"></div>
       </div>
       <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
         </form>
       </div>
     </div>
   </div>
 </div>  
@endsection
@section('script')

<script>
$(document).ready(function() {
    const player = new Plyr('#player');
    $(document).on('click', '.videorecord', function(event) {
        event.preventDefault();
        $('#player').attr('src',$(this).data('url'));
        $("#showvideo").modal();
    });

    $(document).on('click','.mail',function(event){
        event.preventDefault();
        var id = $(this).data('id');
        var job_type = $(this).data('type');
        $.ajax({
            url:"/admin/render",
            type:'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
            data:{'task_id': id, 'type': job_type},
            success: function(data)
            {
              console.log(data);
              $('#receive-mail').html(data);
                // $('#subject').val(data.subject);
                // $('#description').val(data.description);
                $('#addmail').modal();
            }

        });

    });

        

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";
        
        }

          $.ajax({
            url: '/tasks/checked',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
            data: {taskid: taskid, status: status},
            success: function(data){
                console.log(data);
                location.reload()
            },
        })  
    });

});
</script>
@endsection

