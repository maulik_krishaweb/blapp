@extends('layouts.frame')

@section('content')
<style type="text/css">
    .discount-img {
      float: right;
      margin: -60px 0px 15px 70px;
    }
    .wpcf7-response-output {
      margin: 2em auto!important;
      max-width: 560px;
      padding: 1.5em!important;
      background: #20b2aa;
      border: none!important;
      text-align: center;
      color: white;
      font-family: 'Poppins';
      font-weight: 700;
  }
  .discount_form{
    padding-top: 35px;
    padding-bottom: 45px;
    padding-left: 10px;
    padding-right: 10px;
  }
  @media (max-width: 767px){
    .discount-img {
      width: 100%;
      max-width: 231px;
      display: block;
      clear: both;
      float: none;
      margin: 0 auto;
      margin-bottom: 20px;
    }
  }
</style>
<div class="container-fluid">
    <div class="container">
        <h1 style="color: #00b29d">Send your Discount Code Now!</h1>
        <div>
            <img class="discount-img" src="{!! asset('images/discounts.png') !!}" style="height: 250px">
        {{-- <p>Here at Berry Lodge Surveyors we know that embarking on a construction project can be a costly endeavour. We thought we would try and help you as best we can and try and keep those costs to a minimum.</p> 

        <p>Here you can send your friends, family, colleagues and acquaintances a discount code. We are pleased to confirm that if they instruct us to provide a surveying service we will send you a discount code worth £125! But that’s not all, they will also receive a discount code worth £75!</p>

        <p>Don’t wait, send these out now and wait for the savings to come rolling in!</p> 

        <p>Enter the persons details you want to send the discount code to and press send, it really is as easy as that. </p> --}}

          <p>Here at Berry Lodge Surveyors we know that the costs associated with Surveying can at
            times stack up!</p>

          <p>We thought we would try and help you as best we can and try and keep those costs to a
            minimum.</p>

          <p>In the Berry Lodge Client Portal, you can send your friends, family, colleagues and
              acquaintances a discount code. We are pleased to confirm that if they instruct us to provide a surveying service we will send you a discount code worth £50! But that’s not all, they will also receive a discount code worth £25!</p>

          <p>Don’t wait, send these out now and wait for the savings to come rolling in!</p>

          <p>Enter the person’s details you want to send the discount code to and press send, it really is as easy as that.</p>

        </div>
        <div class="discount_form">
            
        <form method="post" action="{!! url('/client/discount-post') !!}">
          <div class="row">
            {{ csrf_field() }}
            <div class="col-md-6 col-sm-6">
                <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                   <label for="full_name" class="control-label">Full Name*</label>
                   <input id="full_name" type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" >
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                   <label for="email" class="control-label">Email Address*</label>
                   <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" >
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                   <label for="contact_number" class="control-label">Contact Number*</label>
                   <input id="contact_number" type="text" class="form-control" name="contact_number" value="{{ old('contact_number') }}" >
                </div>
                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                   <label for="location" class="control-label">Location*</label>
                   <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" >
                </div>
            </div>
            <div class="col-md-12">
                <!-- <button class="btn btn-success btn-lg" style="margin-bottom: 15px">Send</button> -->
                <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit">
            </div>
            </div>
        </form>
        @if (session('success'))
            {{-- <div class="alert alert-info">
                   {{ session('success') }}
            </div> --}}
            {{-- <div class="wpcf7-response-output alert" style="display: block;" role="alert">{{ session('success') }}</div> --}}
            <div class="wpcf7-response-output alert" style="display: block;" role="alert">
              Yippee!<br>
              We have sent your discount code!<br>
              We will get in touch with the recipient, hopefully they will become a Berry<br>
              Lodge Client!<br>
              Thank You!
            </div>

        @endif


        </div>
    </div>
</div>
@endsection

