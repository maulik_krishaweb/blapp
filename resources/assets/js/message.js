import Pusher from 'pusher-js';


window.Vue = require('vue');
window.Echo = require('laravel-echo');

var moment = require('moment');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '8352962562d7c8d08706', 
      cluster: 'eu',
    encrypted: true
});


Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));

const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        usersInRoom: [],
        jobid: '',
   
    },
    methods: {
        addMessage(message) {
            // Add to existing messages
            message.created_at=moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
            console.log("thismessages:"+JSON.stringify(this.messages))
            console.log(message)
            this.messages.push(message);

            // Persist to the database etc
            message.jobid =this.jobid;
            axios.post('/admin/messages', message).then(response => {
                // Do whatever;
            })
            $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")}, 1000); 
        }
    },
    created() {
        var array = window.location.href.split('/');
        var lastsegment = array[array.length-1];
        this.jobid = lastsegment;
 console.log("before get"+lastsegment)
        axios.get('/admin/messages/'+lastsegment).then(response => {
            this.messages = response.data;
            console.log(response.data)
        });
 

        Echo.join(`chatroom.${this.jobid}`)
            .here((users) => {
                this.usersInRoom = users;
            })
            .joining((user) => {
                this.usersInRoom.push(user);
            })
            .leaving((user) => {
                this.usersInRoom = this.usersInRoom.filter(u => u != user)
            })
            .listen('MessagePosted', (e) => {
                this.messages.push({
                    message: e.message.message,
                    who: 'bubble-left',
                    user: e.user
                });
                
            });
    }
});